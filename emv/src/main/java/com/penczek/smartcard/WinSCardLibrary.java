package com.penczek.smartcard;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.NativeLongByReference;
import com.sun.jna.ptr.PointerByReference;

public interface WinSCardLibrary extends SCardLibrary {

	public abstract int SCardConnectA(Pointer hContext, String szReader, int dwShareMode, int dwPreferredProtocols, PointerByReference phCard, NativeLongByReference pdwActiveProtocol);

	public abstract int SCardGetStatusChangeA(Pointer hContext, int dwTimeout, SCardReaderState[] rgReaderStates, int cReaders);

	public abstract int SCardListReaderGroupsA(Pointer hContext, Memory mszGroups, NativeLongByReference pcchGroups);

	public abstract int SCardListReadersA(Pointer hContext, String mszGroups, Memory mszReaders, NativeLongByReference pcchReaders);

	public abstract int SCardStatusA(Pointer hCard, Memory szReaderName, NativeLongByReference pcchReaderLen, NativeLongByReference pdwState, NativeLongByReference pdwProtocol, byte[] pbAtr, NativeLongByReference pcbAtrLen);

}
