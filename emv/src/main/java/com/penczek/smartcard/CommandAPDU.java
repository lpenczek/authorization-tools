package com.penczek.smartcard;

import com.penczek.util.HexadecimalStringUtil;
import java.io.Serializable;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class CommandAPDU implements Serializable {

	private static final long serialVersionUID = 0x58875fe1cbe621dL;
	private byte[] apdu;
	private int lc = 0;
	private int le = 0;
	private int macOffset = 0;
	private byte[] mac;
	private Exception exception;

	public CommandAPDU(int cla, int ins, int p1, int p2) {
		this(cla, ins, p1, p2, null, 0, 0, 0);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] icv, byte[] kmac) {
		this(cla, ins, p1, p2, null, 0, 0, 0, icv, kmac);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, int le) {
		this(cla, ins, p1, p2, null, 0, 0, le);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, int le, byte[] icv, byte[] kmac) {
		this(cla, ins, p1, p2, null, 0, 0, le, icv, kmac);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data) {
		this(cla, ins, p1, p2, data, 0);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data, byte[] icv, byte[] kmac) {
		this(cla, ins, p1, p2, data, 0, icv, kmac);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data, int le) {
		this(cla, ins, p1, p2, data, 0, data != null ? data.length : 0, le);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data, int le, byte[] icv, byte[] kmac) {
		this(cla, ins, p1, p2, data, 0, data != null ? data.length : 0, le, icv, kmac);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data, int dataOffset, int dataLength) {
		this(cla, ins, p1, p2, data, dataOffset, dataLength, 0);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data, int dataOffset, int dataLength, byte[] icv, byte[] kmac) {
		this(cla, ins, p1, p2, data, dataOffset, dataLength, 0, icv, kmac);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data, int dataOffset, int dataLength, int le) {
		this(cla, ins, p1, p2, data, dataOffset, dataLength, le, null, null);
	}

	public CommandAPDU(int cla, int ins, int p1, int p2, byte[] data, int dataOffset, int dataLength, int le, byte[] icv, byte[] kmac) {
		if (dataOffset < 0 || dataLength < 0) {
			throw new IllegalArgumentException("Offset and length must not be negative");
		}
		if (data == null) {
			if (dataOffset != 0 || dataLength != 0) {
				throw new IllegalArgumentException("offset and length must be 0 if array is null");
			}
		} else if (dataLength + dataOffset > data.length) {
			throw new IllegalArgumentException("Offset plus length exceed array size");
		}
		if (dataLength > 255) {
			throw new IllegalArgumentException("Lc is too large");
		}
		if (le < 0) {
			throw new IllegalArgumentException("Le must not be negative");
		}
		if (le > 255) {
			throw new IllegalArgumentException("Le is too large");
		}
		if (icv != null && kmac == null) {
			throw new IllegalArgumentException("MAC key cannot be null for MAC APDUs");
		}
		this.le = le;
		this.lc = dataLength;
		if (kmac != null) {
			macOffset = 5 + this.lc;
			this.lc += 8;
		}
		if (this.lc == 0) {
			if (this.le == 0) {
				apdu = new byte[4];
			} else {
				apdu = new byte[5];
				apdu[4] = (byte) this.le;
			}
		} else {
			apdu = new byte[(this.le == 0 ? 5 : 6) + this.lc];
			apdu[4] = (byte) this.lc;
			if (data != null) {
				System.arraycopy(data, dataOffset, apdu, 5, dataLength);
			}
			if (this.le > 0) {
				apdu[apdu.length - 1] = (byte) this.le;
			}
		}
		this.apdu[0] = (byte) cla;
		this.apdu[1] = (byte) ins;
		this.apdu[2] = (byte) p1;
		this.apdu[3] = (byte) p2;
		// calculate and append MAC
		if (kmac != null) {
			try {
				this.generateMac(icv, kmac);
			} catch (Exception e) {
				this.mac = null;
				this.exception = e;
			}
		}
	}

	public CommandAPDU(byte[] apdu) {
		if (apdu == null || apdu.length < 4) {
			throw new IllegalArgumentException("Invalid C-APDU");
		}
		this.apdu = apdu;
		if (apdu.length == 5) {
			this.le = apdu[4] & 0xff;
		} else if (apdu.length > 5) {
			this.lc = apdu[4] & 0xff;
			if (apdu.length > this.lc + 6) {
				throw new IllegalArgumentException("Invalid C-APDU");
			} else if (apdu.length == this.lc + 6) {
				this.le = apdu[apdu.length - 1] & 0xff;
			}
		}
		if (le < 0) {
			throw new IllegalArgumentException("Le must not be negative");
		}
		if (le > 255) {
			throw new IllegalArgumentException("Le is too large");
		}
		if (lc < 0) {
			throw new IllegalArgumentException("Lc must not be negative");
		}
		if (lc > 255) {
			throw new IllegalArgumentException("Lc is too large");
		}
	}

	private void generateMac(byte[] icv, byte[] kmac) throws Exception {
		// use apdu buffer for padding and mac generation, because the mac wil overwrite the padding
		// do a pad2
		int total = (macOffset / 8 + 1) * 8;
		apdu[macOffset] = (byte) 0x80;
		for (int i = macOffset + 1; i < total; i++) {
			apdu[i] = 0;
		}
		System.out.println("MAC " + macOffset);
		System.out.println("Total " + total);
		System.out.println("APDU " + HexadecimalStringUtil.bytesToString(apdu));
		SecretKeyFactory skf = SecretKeyFactory.getInstance("DES");
		SecretKey kmac1 = skf.generateSecret(new DESKeySpec(kmac, 0));
		SecretKey kmac2 = skf.generateSecret(new DESKeySpec(kmac, 8));
		Cipher cbc = Cipher.getInstance("DES/CBC/NoPadding");
		Cipher ecb = Cipher.getInstance("DES/ECB/NoPadding");
		// trata ICV, encriptando com a kmac
		if (icv != null) {
			ecb.init(Cipher.ENCRYPT_MODE, kmac1);
			icv = ecb.doFinal(icv);
		} else {
			icv = new byte[8];
		}
		// MAC algorithm 3 - CBC key part 1
		cbc.init(Cipher.ENCRYPT_MODE, kmac1, new IvParameterSpec(icv));
		this.mac = cbc.doFinal(this.apdu, 0, total);
		// output transformation 3
		// Hq (last block encrypted) is decrypted with the key K? and the result encrypted with the key K:
		// G = eK(dK?(Hq))
		// hq1
		ecb.init(Cipher.DECRYPT_MODE, kmac2);
		this.mac = ecb.doFinal(this.mac, this.mac.length - 8, 8);
		// g
		ecb.init(Cipher.ENCRYPT_MODE, kmac1);
		this.mac = ecb.doFinal(this.mac);
		System.arraycopy(this.mac, 0, this.apdu, macOffset, 8);
	}

	public int getCLA() {
		return this.apdu[0] & 0xff;
	}

	public int getINS() {
		return this.apdu[1] & 0xff;
	}

	public int getP1() {
		return this.apdu[2] & 0xff;
	}

	public int getP2() {
		return this.apdu[3] & 0xff;
	}

	public int getLc() {
		return this.lc;
	}

	public byte[] getData() {
		if (this.apdu.length <= 5) return null;
		byte[] data = new byte[this.lc];
		System.arraycopy(this.apdu, 5, data, 0, this.lc);
		return data;
	}

	public int getLe() {
		return this.le;
	}

	public void setLe(int le) {
		if (le < 0) {
			throw new IllegalArgumentException("Le must not be negative");
		}
		if (le > 255) {
			throw new IllegalArgumentException("Le is too large");
		}
		if (this.le == 0) {
			// resizes and alloc le byte
			byte[] b = new byte[this.apdu.length + 1];
			System.arraycopy(this.apdu, 0, b, 0, this.apdu.length);
			this.apdu = b;
		}
		this.apdu[this.apdu.length - 1] = (byte) le;
		this.le = le;
	}

	public byte[] getBytes() {
		return apdu.clone();
	}

	@Override
	public String toString() {
		return HexadecimalStringUtil.bytesToString(this.apdu);
	}

	public Exception getException() {
		return exception;
	}

	public int getMacOffset() {
		return macOffset;
	}

	public byte[] getMac() {
		return mac;
	}

}
