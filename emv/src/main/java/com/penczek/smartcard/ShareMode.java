package com.penczek.smartcard;

public enum ShareMode {

	SCARD_SHARE_EXCLUSIVE(1),
	SCARD_SHARE_SHARED(2),
	SCARD_SHARE_DIRECT(3);

	private int value;

	private ShareMode(int v) {
		this.value = v;
	}

	public int getValue() {
		return value;
	}

}
