package com.penczek.smartcard;

import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.NativeLongByReference;
import com.sun.jna.ptr.PointerByReference;

public interface LinuxSCardLibrary extends SCardLibrary {

	public abstract int SCardConnect(Pointer hContext, String szReader, int dwShareMode, int dwPreferredProtocols, PointerByReference phCard, NativeLongByReference pdwActiveProtocol);

	public abstract int SCardGetStatusChange(Pointer hContext, int dwTimeout, SCardReaderState[] rgReaderStates, int cReaders);

	public abstract int SCardListReaderGroups(Pointer hContext, Memory mszGroups, NativeLongByReference pcchGroups);

	public abstract int SCardListReaders(Pointer hContext, String mszGroups, Memory mszReaders, NativeLongByReference pcchReaders);

	public abstract int SCardStatus(Pointer hCard, Memory szReaderName, NativeLongByReference pcchReaderLen, NativeLongByReference pdwState, NativeLongByReference pdwProtocol, byte[] pbAtr, NativeLongByReference pcbAtrLen);

}
