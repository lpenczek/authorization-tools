package com.penczek.smartcard;

import com.sun.jna.Library;
import com.sun.jna.Pointer;

public interface LinuxKernelLibrary extends Library {

	public abstract Pointer dlopen(String filename, int flag);

	public abstract Pointer dlsym(Pointer handle, String symbol);

	public abstract int dlclose(Pointer handle);

}
