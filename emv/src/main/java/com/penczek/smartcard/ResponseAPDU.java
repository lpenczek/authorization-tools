package com.penczek.smartcard;

import com.penczek.util.HexadecimalStringUtil;
import java.io.Serializable;

public class ResponseAPDU implements Serializable {

	private static final long serialVersionUID = 0x60a0a3aae9b650f1L;
	private byte[] apdu;

	public ResponseAPDU(byte[] data) {
		if (data.length < 2) {
			throw new IllegalArgumentException("r-apdu must be at least 2 bytes long");
		}
		this.apdu = data.clone();
	}

	public int getLr() {
		return this.apdu.length - 2;
	}

	public byte[] getData() {
		int i = this.getLr();
		if (i == 0) return null;
		byte[] data = new byte[i];
		System.arraycopy(this.apdu, 0, data, 0, i);
		return data;
	}

	public int getSW1() {
		return this.apdu[this.apdu.length - 2] & 0xff;
	}

	public int getSW2() {
		return this.apdu[this.apdu.length - 1] & 0xff;
	}

	public int getSW() {
		return this.getSW1() << 8 | this.getSW2();
	}

	public byte[] getBytes() {
		return apdu.clone();
	}

	@Override
	public String toString() {
		return HexadecimalStringUtil.bytesToString(this.apdu);
	}

}
