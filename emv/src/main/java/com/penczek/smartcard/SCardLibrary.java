package com.penczek.smartcard;

import com.sun.jna.Library;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.NativeLongByReference;
import com.sun.jna.ptr.PointerByReference;

public interface SCardLibrary extends Library {

	public static final int SCARD_PROTOCOL_T0 = 0x0001;
	public static final int SCARD_PROTOCOL_T1 = 0x0002;
	public static final int SCARD_PROTOCOL_RAW = 0x10000;
	public static final int SCARD_STATE_PRESENT = 0x0020;
	public static final int SCARD_STATE_MUTE = 0x0200;

	public abstract int SCardBeginTransaction(Pointer hCard);

	public abstract int SCardDisconnect(Pointer hCard, int dwDisposition);

	public abstract int SCardEndTransaction(Pointer hCard, int dwDisposition);

	public abstract int SCardEstablishContext(int dwScope, int pvReserved1, int pvReserved2, PointerByReference phContext);

	public abstract int SCardReconnect(Pointer hCard, int dwShareMode, int dwPreferredProtocols, int dwInitialization, IntByReference pdwActiveProtocol);

	public abstract int SCardReleaseContext(Pointer hContext);

	public abstract int SCardTransmit(Pointer hCard, SCardIORequest pioSendPci, byte[] pbSendBuffer, int cbSendLength, SCardIORequest pioRecvPci, byte[] pbRecvBuffer, NativeLongByReference pcbRecvLength);

}
