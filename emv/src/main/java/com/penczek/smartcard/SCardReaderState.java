package com.penczek.smartcard;

import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;

public class SCardReaderState extends Structure {

	public String szReader;
	public Pointer pvUserData;
	public NativeLong dwCurrentState = new NativeLong();
	public NativeLong dwEventState = new NativeLong();
	public NativeLong cbAtr = new NativeLong();
	public byte rgbAtr[] = new byte[36];

	public SCardReaderState(String readerName, int state) {
		this.szReader = readerName;
		dwCurrentState.setValue(state);
	}

	public int getEventState() {
		return OperatingSystem.CURRENT == OperatingSystem.MACOS && NativeLong.SIZE == 8
				? (int) (dwCurrentState.longValue() >> 32)
				: dwEventState.intValue();
	}

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList(
				"szReader",
				"pvUserData",
				"dwCurrentState",
				"dwEventState",
				"cbAtr",
				"rgbAtr"
		);
	}

}
