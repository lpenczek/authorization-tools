package com.penczek.smartcard;

public enum CardState {

	SCARD_STATE_UNAWARE(0),
	SCARD_STATE_MUTE(512);

	private int value;

	private CardState(int v) {
		this.value = v;
	}

	public int getValue() {
		return value;
	}

}
