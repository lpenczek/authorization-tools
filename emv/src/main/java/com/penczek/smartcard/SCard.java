package com.penczek.smartcard;

import com.penczek.util.HexadecimalStringUtil;
import com.sun.jna.Memory;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.NativeLongByReference;
import com.sun.jna.ptr.PointerByReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SCard {

	public static final int SCARD_PROTOCOL_T0 = 1;
	public static final int SCARD_PROTOCOL_T1 = 2;
	private static final Logger LOG = Logger.getLogger(SCard.class.getName());

	private SCardLibrary sCardLibrary;
	private boolean windows;
	private String readerName;
	private Pointer context;
	private Pointer card;
	private int protocol;

	public SCard() {
		switch (OperatingSystem.CURRENT) {
			case LINUX:
				sCardLibrary = (LinuxSCardLibrary) Native.loadLibrary("pcsclite", LinuxSCardLibrary.class);
				break;
			case MACOS:
				sCardLibrary = (LinuxSCardLibrary) Native.loadLibrary("PCSC", LinuxSCardLibrary.class);
				break;
			case WINDOWS:
				sCardLibrary = (WinSCardLibrary) Native.loadLibrary("winscard", WinSCardLibrary.class);
				windows = true;
		}
	}

	public SCard(String readerName) {
		this();
		this.readerName = readerName;
	}

	public void establishContext(Scope scope) throws SCardException {
		PointerByReference phContext = new PointerByReference();
		int retVal = this.sCardLibrary.SCardEstablishContext(scope.getValue(), 0, 0, phContext);
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
		this.context = phContext.getValue();
	}

	public String[] listReaders() throws SCardException {
		NativeLongByReference readerCount = new NativeLongByReference();
		int retVal = this.windows
				? ((WinSCardLibrary) this.sCardLibrary).SCardListReadersA(this.context, null, null, readerCount)
				: ((LinuxSCardLibrary) this.sCardLibrary).SCardListReaders(this.context, null, null, readerCount);
		if (retVal != 0) {
			SCardException e = new SCardException(retVal);
			if ("SCARD_E_NO_READERS_AVAILABLE".equals(e.getError())) {
				return new String[0];
			}
			throw e;
		}
		int count = readerCount.getValue().intValue();
		Memory readers = new Memory(count);
		retVal = this.windows
				? ((WinSCardLibrary) this.sCardLibrary).SCardListReadersA(this.context, null, readers, readerCount)
				: ((LinuxSCardLibrary) this.sCardLibrary).SCardListReaders(this.context, null, readers, readerCount);
		if (retVal != 0) {
			SCardException e = new SCardException(retVal);
			if ("SCARD_E_NO_READERS_AVAILABLE".equals(e.getError())) {
				return new String[0];
			}
			throw e;
		}
		List<String> names = new LinkedList<String>();
		String name;
		for (int off = 0; off + 1 < count; off += name.length() + 1) {
			name = readers.getString(off);
			names.add(name);
		}
		return names.toArray(new String[names.size()]);
	}

	public void connect(ShareMode shareMode, int preferredProtocols) throws SCardException {
		PointerByReference phCard = new PointerByReference();
		NativeLongByReference pProtocol = new NativeLongByReference();
		int retVal = windows
				? ((WinSCardLibrary) this.sCardLibrary).SCardConnectA(this.context, this.readerName, shareMode.getValue(), preferredProtocols, phCard, pProtocol)
				: ((LinuxSCardLibrary) this.sCardLibrary).SCardConnect(this.context, this.readerName, shareMode.getValue(), preferredProtocols, phCard, pProtocol);
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
		this.card = phCard.getValue();
		this.protocol = pProtocol.getValue().intValue();
	}

	public void disconnect(Disposition disposition) throws SCardException {
		int retVal = this.sCardLibrary.SCardDisconnect(this.card, disposition.getValue());
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
		this.card = null;
	}

	public void reconnect(ShareMode shareMode, int preferredProtocols, Disposition disposition) throws SCardException {
		IntByReference pProtocol = new IntByReference();
		int retVal = this.sCardLibrary.SCardReconnect(this.card, shareMode.getValue(), preferredProtocols, disposition.getValue(), pProtocol);
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
		this.protocol = pProtocol.getValue();
	}

	public Map<String, Object> status() throws SCardException {
		NativeLongByReference pReaderLen = new NativeLongByReference();
		NativeLongByReference pState = new NativeLongByReference();
		NativeLongByReference pProtocol = new NativeLongByReference();
		byte byteATR[] = new byte[32];
		NativeLongByReference pATRLen = new NativeLongByReference(new NativeLong(byteATR.length));
		int memSize = this.readerName.length() + 2;
		Memory memReaderNames = new Memory(memSize);
		memReaderNames.setString(0L, this.readerName);
		pReaderLen.setValue(new NativeLong(memSize));
		int retVal = windows
				? ((WinSCardLibrary) this.sCardLibrary).SCardStatusA(this.card, null, pReaderLen, pState, pProtocol, byteATR, pATRLen)
				: ((LinuxSCardLibrary) this.sCardLibrary).SCardStatus(this.card, null, pReaderLen, pState, pProtocol, byteATR, pATRLen);
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
		byte result[] = new byte[pATRLen.getValue().intValue()];
		System.arraycopy(byteATR, 0, result, 0, result.length);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("status", new Integer(pState.getValue().intValue()));
		map.put("protocol", new Integer(pProtocol.getValue().intValue()));
		map.put("atr", result);
		return map;
	}

	private byte[] _internalTransmit(byte[] command, SCardIORequest sCardIO) throws SCardException {
		if (LOG.isLoggable(Level.FINE)) LOG.fine("HOST - " + HexadecimalStringUtil.bytesToString(command));
		byte[] response = new byte[514];
		NativeLongByReference pRecvLength = new NativeLongByReference(new NativeLong(514L));
		int retVal = this.sCardLibrary.SCardTransmit(this.card, sCardIO, command, command.length, null, response, pRecvLength);
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
		byte result[] = new byte[pRecvLength.getValue().intValue()];
		System.arraycopy(response, 0, result, 0, result.length);
		if (LOG.isLoggable(Level.FINE)) LOG.fine("ICC  - " + HexadecimalStringUtil.bytesToString(result));
		return result;
	}

	public int getStatusChange(int timeout) throws SCardException {
		SCardReaderState states[] = new SCardReaderState[] {
			new SCardReaderState(this.readerName, CardState.SCARD_STATE_UNAWARE.getValue())
		};
		int retVal = windows
				? ((WinSCardLibrary) this.sCardLibrary).SCardGetStatusChangeA(this.context, timeout, states, states.length)
				: ((LinuxSCardLibrary) this.sCardLibrary).SCardGetStatusChange(this.context, timeout, states, states.length);
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
		return states[0].getEventState();
	}

	public void releaseContext() throws SCardException {
		int retVal = this.sCardLibrary.SCardReleaseContext(this.context);
		this.context = null;
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
	}

	public void beginTransaction() throws SCardException {
		int retVal = this.sCardLibrary.SCardBeginTransaction(this.card);
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
	}

	public void endTransaction(Disposition action) throws SCardException {
		int retVal = this.sCardLibrary.SCardEndTransaction(this.card, action.getValue());
		if (retVal != 0) {
			throw new SCardException(retVal);
		}
	}

	public ResponseAPDU transmit(CommandAPDU capdu) throws SCardException, Exception {
		SCardIORequest sCardIO = new SCardIORequest();
		sCardIO.cbPciLength = 8;
		sCardIO.dwProtocol = this.protocol;
		byte[] response = this._internalTransmit(capdu.getBytes(), sCardIO);
		int lr = response.length;
		if (lr == 2 && response[0] == 0x6C) {
			// cmd again expected with le
			capdu.setLe(0x0FF & response[1]);
			response = this._internalTransmit(capdu.getBytes(), sCardIO);
			lr = response.length;
		}
		if (response[lr - 2] == 0x61) {
			// get response expected
			do {
				response = this._internalTransmit(new CommandAPDU(0x00, 0xC0, 0x00, 0x00, 0x0FF & response[lr - 1]).getBytes(), sCardIO);
				lr = response.length;
			} while (response[lr - 2] == 0x61); // get response expected
		}
		return new ResponseAPDU(response);
	}

	public Pointer getContext() {
		return this.context;
	}

	public Pointer getCard() {
		return this.card;
	}

	public int getProtocol() {
		return this.protocol;
	}

}
