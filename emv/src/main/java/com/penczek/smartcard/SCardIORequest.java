package com.penczek.smartcard;

import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;

public class SCardIORequest extends Structure {

	public int dwProtocol;
	public int cbPciLength;

	@Override
	protected List<?> getFieldOrder() {
		return Arrays.asList("dwProtocol", "cbPciLength");
	}

}
