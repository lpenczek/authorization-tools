package com.penczek.smartcard;

import com.sun.jna.Library;

public interface WinKernel32Library extends Library {

	public abstract int LoadLibraryA(String lpFileName);

	public abstract void FreeLibrary(int hModule);

	public abstract int GetProcAddress(int hModule, String lpProcName);

}
