package com.penczek.smartcard;

public class SCardException extends Exception {

	private final int code;
	private final String error;

	public SCardException(int code) {
		super(decode(code));
		this.code = code;
		this.error = this.getMessage();
	}

	public SCardException(int code, String msg) {
		super(msg);
		this.code = code;
		this.error = decode(code);
	}

	private static String decode(int code) {
		switch (code) {
			case 0: return "SCARD_S_SUCCESS";
			case 5: return "ERROR_ACCESS_DENIED";
			case 6: return "ERROR_INVALID_HANDLE";
			case 87: return "ERROR_INVALID_PARAMETER";
			case 122: return "ERROR_INSUFFICIENT_BUFFER";
			case 1734: return "RPC_S_INVALID_BOUND";
			case 0x80100001: return "SCARD_F_INTERNAL_ERROR";
			case 0x80100002: return "SCARD_E_CANCELLED";
			case 0x80100003: return "SCARD_E_INVALID_HANDLE";
			case 0x80100004: return "SCARD_E_INVALID_PARAMETER";
			case 0x80100005: return "SCARD_E_INVALID_TARGET";
			case 0x80100006: return "SCARD_E_NO_MEMORY";
			case 0x80100007: return "SCARD_F_WAITED_TOO_LONG";
			case 0x80100008: return "SCARD_E_INSUFFICIENT_BUFFER";
			case 0x80100009: return "SCARD_E_UNKNOWN_READER";
			case 0x8010000a: return "SCARD_E_TIMEOUT";
			case 0x8010000b: return "SCARD_E_SHARING_VIOLATION";
			case 0x8010000c: return "SCARD_E_NO_SMARTCARD";
			case 0x8010000d: return "SCARD_E_UNKNOWN_CARD";
			case 0x8010000e: return "SCARD_E_CANT_DISPOSE";
			case 0x8010000f: return "SCARD_E_PROTO_MISMATCH";
			case 0x80100010: return "SCARD_E_NOT_READY";
			case 0x80100011: return "SCARD_E_INVALID_VALUE";
			case 0x80100012: return "SCARD_E_SYSTEM_CANCELLED";
			case 0x80100013: return "SCARD_F_COMM_ERROR";
			case 0x80100014: return "SCARD_F_UNKNOWN_ERROR";
			case 0x80100015: return "SCARD_E_INVALID_ATR";
			case 0x80100016: return "SCARD_E_NOT_TRANSACTED";
			case 0x80100017: return "SCARD_E_READER_UNAVAILABLE";
			case 0x80100019: return "SCARD_E_PCI_TOO_SMALL";
			case 0x8010001a: return "SCARD_E_READER_UNSUPPORTED";
			case 0x8010001b: return "SCARD_E_DUPLICATE_READER";
			case 0x8010001c: return "SCARD_E_CARD_UNSUPPORTED";
			case 0x8010001d: return  "SCARD_E_NO_SERVICE";
			case 0x8010001e: return "SCARD_E_SERVICE_STOPPED";
			case 0x8010002e: return  "SCARD_E_NO_READERS_AVAILABLE ";
			case 0x80100065: return "SCARD_W_UNSUPPORTED_CARD";
			case 0x80100066: return "SCARD_W_UNRESPONSIVE_CARD";
			case 0x80100067: return "SCARD_W_UNPOWERED_CARD";
			case 0x80100068: return "SCARD_W_RESET_CARD";
			case 0x80100069: return "SCARD_W_REMOVED_CARD";
		}
		return String.valueOf(code);
	}

	public int getCode() {
		return code;
	}

	public String getError() {
		return error;
	}

	@Override
	public String toString() {
		return this.code + ": " + this.error + " - " + this.getMessage();
	}

}
