package com.penczek.smartcard;

public enum Disposition {

	SCARD_LEAVE_CARD(0),
	SCARD_RESET_CARD(1),
	SCARD_UNPOWER_CARD(2),
	SCARD_EJECT_CARD(3);

	private int value;

	private Disposition(int v) {
		this.value = v;
	}

	public int getValue() {
		return value;
	}

}
