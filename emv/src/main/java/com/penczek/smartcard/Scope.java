package com.penczek.smartcard;

public enum Scope {

	SCARD_SCOPE_USER(0),
	SCARD_SCOPE_TERMINAL(1),
	SCARD_SCOPE_SYSTEM(2);

	private int value;

	private Scope(int v) {
		this.value = v;
	}

	public int getValue() {
		return value;
	}

}
