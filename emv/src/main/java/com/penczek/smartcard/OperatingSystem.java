package com.penczek.smartcard;

public enum OperatingSystem {

	WINDOWS,
	LINUX,
	MACOS;

	public static final OperatingSystem CURRENT;

	static {
		String osName = System.getProperty("os.name").toLowerCase();
		if (osName.indexOf("linux") >= 0) {
			CURRENT = LINUX;
		} else if (osName.indexOf("windows") >= 0) {
			CURRENT = WINDOWS;
		} else if (osName.indexOf("mac") >= 0) {
			CURRENT = MACOS;
		} else {
			throw new IllegalArgumentException("Unknown operating system " + osName);
		}
	}

}
