package com.penczek.emv;

import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.TLVData;

public class FinalResult {

	private final TLVData secondGenerateACData;
	private final byte[] issuerScriptResults;
	private final String[] debugScripts;

	public FinalResult(TLVData secondGenerateACData, byte[] issuerScriptResults, String[] debugScripts) {
		this.secondGenerateACData = secondGenerateACData;
		this.issuerScriptResults = issuerScriptResults;
		this.debugScripts = debugScripts;
	}

	public TLVData getSecondGenerateACData() {
		return secondGenerateACData;
	}

	public boolean isApproved() {
		if (this.secondGenerateACData == null) return false;
		Number n = (Number) this.secondGenerateACData.getDataElementValue(EMVDataElement.CRYPTOGRAM_INFORMATION_DATA);
		return n != null && n.intValue() == 0x040;
	}

	public byte[] getIssuerScriptResults() {
		return issuerScriptResults;
	}

	public String[] getDebugScripts() {
		return debugScripts;
	}

}
