package com.penczek.emv;

/**
 *
 * @author lpenczek
 */
public class Transaction {
	public String amount;
	public String unpredictableNumber;
	public String tlvTerminal;
	public String tlvIcc;	
	public String date;
	public String transactionType;
	public String tvr;
}
