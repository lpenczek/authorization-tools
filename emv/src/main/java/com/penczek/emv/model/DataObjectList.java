package com.penczek.emv.model;

import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.TLVDataParser;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class DataObjectList {

	private final DataObjectListEntry[] entries;

	public DataObjectList(byte[] dol) {
		if (dol == null) {
			entries = null;
			return;
		}
		int tag, lg, j, i = 0, length = dol.length;
		List<DataObjectListEntry> ent = new LinkedList<DataObjectListEntry>();
		while (i < length) {
			// TAG
			tag = dol[i];
			if (tag == 0 || tag == -1) {
				// 0x00 e 0xFF podem ocorrer como padding antes, entre e depois de objetos TLV
				i++;
				continue;
			}
			tag &= 0x00FF;
			// T - 1o byte
			// verifica se tag possui mais bytes, para EMV o maximo sao 2 bytes por tag
			if ((tag & 0x001F) == 0x001F) {
				if (++i >= length) throw new RuntimeException("Invalid TLV tag: " + i);
				if (dol[i] < 0) throw new RuntimeException("EMV TLV's tags does not support more than 2 bytes: " + dol[i]);
				tag = tag << 8 | dol[i];
			}
			// LENGTH
			if (++i >= length) throw new RuntimeException("Invalid TLV data: " + i + " > " + length);
			lg = 0x0FF & dol[i];
			ent.add(new DataObjectListEntry(EMVDataElement.getByTag(tag), lg));
			i++;
		}
		this.entries = ent.toArray(new DataObjectListEntry[ent.size()]);
	}

	public DataObjectList(DataObjectListEntry... entries) {
		this.entries = entries;
	}

	public DataObjectListEntry[] getEntries() {
		return entries;
	}

	public int size() {
		return this.entries.length;
	}

	public byte[] getDolData(Map<EMVDataElement, byte[]> values) {
		int dataSize = 0;
		for (DataObjectListEntry e : entries) {
			dataSize += e.getLength();
		}
		byte[] dolData = new byte[dataSize];
		int i = 0;
		for (DataObjectListEntry e : entries) {
			byte[] value = values.get(e.getTag());
			if (value != null && value.length > 0) {
				System.arraycopy(value, 0, dolData, i, e.getLength());
			}
			i += e.getLength();
		}
		return dolData;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(512);
		sb.append("DOL [\n");
		for (DataObjectListEntry entry : entries) {
			sb.append('\t').append(entry).append('\n');
		}
		sb.append(']');
		return sb.toString();
	}

	public String toHexadecimal() {
		StringBuilder sb = new StringBuilder(4096);
		for (DataObjectListEntry entry : entries) {
			int tag = entry.getTag().getTag();
			sb.append(String.format(tag > 0x0FF ? "%04X" : "%02X", tag)).append(TLVDataParser.formatLength(entry.getLength()));
		}
		return sb.toString();
	}

}
