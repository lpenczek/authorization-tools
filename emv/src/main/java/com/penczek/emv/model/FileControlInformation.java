package com.penczek.emv.model;

import com.penczek.tlv.ConstructedTLV;
import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.PrimitiveTLV;
import com.penczek.tlv.TLVData;
import com.penczek.tlv.TLVDataParser;
import com.penczek.util.HexadecimalStringUtil;
import java.text.ParseException;
import java.util.Arrays;

public class FileControlInformation {

	private final byte[] name;
	private final Integer sfi;
	private final String applicationLabel;
	private final String applicationPreferredName;
	private final Integer applicationPriotity;
	private final Integer issuerCodeTableIndex;
	private final String[] languagePreference;
	private final DataObjectList pdol;
	private final byte[] bankIdentifierCode;
	private final byte[] internationalBankAccountNumber;
	private final String issuerCountryCodeA2;
	private final String issuerCountryCodeA3;
	private final Integer issuerIdentificationNumber;
	private final String issuerURL;
	private final Integer logEntry;

	public FileControlInformation(byte[] data) throws ParseException {
		this(TLVDataParser.parseData(data));
	}

	public FileControlInformation(TLVData tlvData) {
		ConstructedTLV fci = (ConstructedTLV) tlvData.getDataElement(EMVDataElement.FILE_CONTROL_INFORMATION_TEMPLATE);
		if (fci == null) {
			throw new IllegalArgumentException("No FILE_CONTROL_INFORMATION_TEMPLATE tag found in arguments");
		}
		PrimitiveTLV tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.DEDICATED_FILE_NAME);
		this.name = tlv != null ? (byte[]) tlv.getInterpretedValue() : null;
		ConstructedTLV fciProp = (ConstructedTLV) fci.getChild(EMVDataElement.FILE_CONTROL_INFORMATION_PROPRIETARY_TEMPLATE);
		if (fciProp != null) {
			tlv = (PrimitiveTLV) fciProp.getChild(EMVDataElement.APPLICATION_LABEL);
			this.applicationLabel = tlv != null ? (String) tlv.getInterpretedValue() : null;
			tlv = (PrimitiveTLV) fciProp.getChild(EMVDataElement.APPLICATION_PREFERRED_NAME);
			this.applicationPreferredName = tlv != null ? (String) tlv.getInterpretedValue() : null;
			tlv = (PrimitiveTLV) fciProp.getChild(EMVDataElement.APPLICATION_PRIORITY_INDICATOR);
			this.applicationPriotity = tlv != null ? ((Number) tlv.getInterpretedValue()).intValue() : null;
			tlv = (PrimitiveTLV) fciProp.getChild(EMVDataElement.ISSUER_CODE_TABLE_INDEX);
			this.issuerCodeTableIndex = tlv != null ? ((Number) tlv.getInterpretedValue()).intValue() : null;
			tlv = (PrimitiveTLV) fciProp.getChild(EMVDataElement.LANGUAGE_PREFERENCE);
			if (tlv != null) {
				String lp = (String) tlv.getInterpretedValue();
				if (lp != null && lp.length() > 0) {
					this.languagePreference = new String[lp.length() >> 1];
					for (int i = 0; i < lp.length(); i += 2) {
						this.languagePreference[i >> 1] = lp.substring(i, i + 2);
					}
				} else {
					this.languagePreference = null;
				}
			} else {
				this.languagePreference = null;
			}
			tlv = (PrimitiveTLV) fciProp.getChild(EMVDataElement.PROCESSING_OPTIONS_DATA_OBJECT_LIST);
			this.pdol = tlv != null ? new DataObjectList((byte[]) tlv.getInterpretedValue()) : null;
			tlv = (PrimitiveTLV) fciProp.getChild(EMVDataElement.SHORT_FILE_IDENTIFIER);
			this.sfi = tlv != null ? ((Number) tlv.getInterpretedValue()).intValue() : null;
			ConstructedTLV fciIssuer = (ConstructedTLV) fci.getChild(EMVDataElement.FILE_CONTROL_INFORMATION_ISSUER_DISCRETIONARY_DATA);
			if (fciIssuer != null) {
				//BF0C
				tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.BANK_IDENTIFIER_CODE);
				this.bankIdentifierCode = tlv != null ? (byte[]) tlv.getInterpretedValue() : null;
				tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.INTERNATIONAL_BANK_ACCOUNT_NUMBER);
				this.internationalBankAccountNumber = tlv != null ? (byte[]) tlv.getInterpretedValue() : null;
				tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.ISSUER_COUNTRY_CODE_ALPHA2);
				this.issuerCountryCodeA2 = tlv != null ? (String) tlv.getInterpretedValue() : null;
				tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.ISSUER_COUNTRY_CODE_ALPHA3);
				this.issuerCountryCodeA3 = tlv != null ? (String) tlv.getInterpretedValue() : null;
				tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.ISSUER_IDENTIFICATION_NUMBER);
				this.issuerIdentificationNumber = tlv != null ? ((Number) tlv.getInterpretedValue()).intValue() : null;
				tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.ISSUER_URL);
				this.issuerURL = tlv != null ? (String) tlv.getInterpretedValue() : null;
				tlv = (PrimitiveTLV) fci.getChild(EMVDataElement.LOG_ENTRY);
				this.logEntry = tlv != null ? ((Number) tlv.getInterpretedValue()).intValue() : null;
			} else {
				this.bankIdentifierCode = null;
				this.internationalBankAccountNumber = null;
				this.issuerCountryCodeA2 = null;
				this.issuerCountryCodeA3 = null;
				this.issuerIdentificationNumber = null;
				this.issuerURL = null;
				this.logEntry = null;
			}
		} else {
			this.sfi = null;
			this.applicationLabel = null;
			this.applicationPreferredName = null;
			this.applicationPriotity = null;
			this.issuerCodeTableIndex = null;
			this.languagePreference = null;
			this.pdol = null;
			this.bankIdentifierCode = null;
			this.internationalBankAccountNumber = null;
			this.issuerCountryCodeA2 = null;
			this.issuerCountryCodeA3 = null;
			this.issuerIdentificationNumber = null;
			this.issuerURL = null;
			this.logEntry = null;
		}
	}

	public byte[] getName() {
		return name;
	}

	public int getSfi() {
		return sfi;
	}

	public String getApplicationLabel() {
		return applicationLabel;
	}

	public String getApplicationPreferredName() {
		return applicationPreferredName;
	}

	public int getApplicationPriotity() {
		return applicationPriotity;
	}

	public int getIssuerCodeTableIndex() {
		return issuerCodeTableIndex;
	}

	public String[] getLanguagePreference() {
		return languagePreference;
	}

	public DataObjectList getPdol() {
		return pdol;
	}

	public byte[] getBankIdentifierCode() {
		return bankIdentifierCode;
	}

	public byte[] getInternationalBankAccountNumber() {
		return internationalBankAccountNumber;
	}

	public String getIssuerCountryCodeA2() {
		return issuerCountryCodeA2;
	}

	public String getIssuerCountryCodeA3() {
		return issuerCountryCodeA3;
	}

	public int getIssuerIdentificationNumber() {
		return issuerIdentificationNumber;
	}

	public String getIssuerURL() {
		return issuerURL;
	}

	public int getLogEntry() {
		return logEntry;
	}

	@Override
	public String toString() {
		return "FCI ["
				+ "\nname = " + (name != null ? HexadecimalStringUtil.bytesToString(name) : null)
				+ "\nsfi = " + sfi
				+ "\napplicationLabel = " + applicationLabel
				+ "\napplicationPreferredName = " + applicationPreferredName
				+ "\napplicationPriotity = " + applicationPriotity
				+ "\nissuerCodeTableIndex = " + issuerCodeTableIndex
				+ "\nlanguagePreference = " + (languagePreference != null ? Arrays.asList(languagePreference) : null)
				+ "\npdol = " + pdol
				+ "\nbankIdentifierCode = " + (bankIdentifierCode != null ? HexadecimalStringUtil.bytesToString(bankIdentifierCode) : null)
				+ "\ninternationalBankAccountNumber = " + (internationalBankAccountNumber != null ? HexadecimalStringUtil.bytesToString(internationalBankAccountNumber) : null)
				+ "\nissuerCountryCodeA2 = " + issuerCountryCodeA2
				+ "\nissuerCountryCodeA3 = " + issuerCountryCodeA3
				+ "\nissuerIdentificationNumber = " + issuerIdentificationNumber
				+ "\nissuerURL = " + issuerURL
				+ "\nlogEntry = " + logEntry
				+ "\n]";
	}

}
