package com.penczek.emv.model;

import com.penczek.tlv.EMVDataElement;

public class DataObjectListEntry {

	private final EMVDataElement tag;
	private final int length;

	public DataObjectListEntry(EMVDataElement tag, int length) {
		this.tag = tag;
		this.length = length;
	}

	public EMVDataElement getTag() {
		return tag;
	}

	public int getLength() {
		return length;
	}

	@Override
	public String toString() {
		return this.tag.toString() + " - " + this.length;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof DataObjectListEntry)) {
			return false;
		}
		return (this.tag != null ? tag.equals(tag) : tag == null)
						&& (this.length == length);
	}

	@Override
	public int hashCode() {
		return this.length + (this.tag != null ? this.tag.hashCode() : 0);
	}

}
