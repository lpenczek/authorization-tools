package com.penczek.emv.model;

import com.penczek.tlv.ConstructedTLV;
import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.PrimitiveTLV;
import com.penczek.tlv.TLV;
import com.penczek.tlv.TLVData;
import com.penczek.util.HexadecimalStringUtil;
import java.util.Arrays;

public class FirstACResponse {

	private final byte cid;
	private final short atc;
	private final byte[] ac;
	private final byte[] iad;

	public FirstACResponse(byte[] data) {
		this.cid = data[0];
		this.atc = (short) ((data[1] << 8) | data[2]);
		System.out.println("data[1]" + data[1]);
		System.out.println("data[2]" + data[2]);
		System.out.println("ATC " + this.atc);
		this.ac = Arrays.copyOfRange(data, 3, 11);
		if (data.length > 11) {
			this.iad = Arrays.copyOfRange(data, 11, data.length);
		} else {
			this.iad = null;
		}
	}

	public FirstACResponse(ConstructedTLV format2) {
		this.cid = ((Number) ((PrimitiveTLV) format2.getChild(EMVDataElement.CRYPTOGRAM_INFORMATION_DATA)).getInterpretedValue()).byteValue();
		this.atc = ((Number) ((PrimitiveTLV) format2.getChild(EMVDataElement.APPLICATION_TRANSACTION_COUNTER)).getInterpretedValue()).shortValue();
		this.ac = ((PrimitiveTLV) format2.getChild(EMVDataElement.APPLICATION_CRYPTOGRAM)).getValue();
		this.iad = ((PrimitiveTLV) format2.getChild(EMVDataElement.ISSUER_APPLICATION_DATA)).getValue();
	}

	@Override
	public String toString() {
		return "Processing options ["
						+ "\nCID = " + String.format("%02x", this.cid)
						+ "\nATC = " + this.atc
						+ "\nAC = " + HexadecimalStringUtil.bytesToString(this.ac)
						+ "\nIAD = " + HexadecimalStringUtil.bytesToString(this.iad)
						+ "\n]";
	}

	public String toTLV() {
		TLV[] tlvs = new TLV[this.iad != null ? 4 : 3];
		tlvs[0] = new PrimitiveTLV(EMVDataElement.CRYPTOGRAM_INFORMATION_DATA, new byte[] { this.cid });
		tlvs[1] = new PrimitiveTLV(EMVDataElement.APPLICATION_TRANSACTION_COUNTER, new byte[] { (byte)(this.atc>>8), (byte) this.atc });
		System.out.println(tlvs[1]);
		tlvs[2] = new PrimitiveTLV(EMVDataElement.APPLICATION_CRYPTOGRAM, this.ac);
		if (this.iad != null) {
			tlvs[3] = new PrimitiveTLV(EMVDataElement.ISSUER_APPLICATION_DATA, this.iad);
		}
		return new TLVData(tlvs).toHexadecimal();
	}

	public byte getCid() {
		return cid;
	}

	public short getAtc() {
		return atc;
	}

	public byte[] getAc() {
		return ac;
	}

	public byte[] getIad() {
		return iad;
	}

}
