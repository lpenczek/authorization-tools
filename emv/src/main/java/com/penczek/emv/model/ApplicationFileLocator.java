package com.penczek.emv.model;

public class ApplicationFileLocator {

	private final Integer sfi;
	private final Integer firstRecord;
	private final Integer lastRecord;
	private final Integer countOfflineAuth;

	public ApplicationFileLocator(byte[] data) {
		this(data, 0);
	}

	public ApplicationFileLocator(byte[] data, int offset) {
		this.sfi = data[offset] >> 3;
		this.firstRecord = Integer.valueOf(data[offset + 1]);
		this.lastRecord = Integer.valueOf(data[offset + 2]);
		this.countOfflineAuth = Integer.valueOf(data[offset + 3]);
	}

	public Integer getSfi() {
		return sfi;
	}

	public Integer getFirstRecord() {
		return firstRecord;
	}

	public Integer getLastRecord() {
		return lastRecord;
	}

	public Integer getCountOfflineAuth() {
		return countOfflineAuth;
	}

	@Override
	public String toString() {
		return "AFL ["
				+ "\nsfi = " + sfi
				+ "\nfirstRecord = " + firstRecord
				+ "\nlastRecord = " + lastRecord
				+ "\ncountOfflineAuth = " + countOfflineAuth
				+ "\n]";
	}
	
}
