package com.penczek.emv.model;

import com.penczek.util.HexadecimalStringUtil;

public class ApplicationIdentifier {

	private final byte[] aid;
	private final FileControlInformation fci;

	public ApplicationIdentifier(byte[] aid, FileControlInformation fci) {
		this.aid = aid;
		this.fci = fci;
	}

	public byte[] getAid() {
		return aid;
	}

	public FileControlInformation getFci() {
		return fci;
	}

	@Override
	public String toString() {
		return fci != null
				? fci.getApplicationPreferredName() != null
					? fci.getApplicationPreferredName()
					: fci.getApplicationLabel()
				:
				"AFL ["
				+ "\naid = " + HexadecimalStringUtil.bytesToString(aid)
				+ "\nfci = " + fci
				+ "\n]";
	}

}
