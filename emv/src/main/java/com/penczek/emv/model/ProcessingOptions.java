package com.penczek.emv.model;

import com.penczek.tlv.ConstructedTLV;
import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.PrimitiveTLV;
import com.penczek.util.HexadecimalStringUtil;
import java.util.Arrays;

public class ProcessingOptions {
	
	private final byte[] aip;
	private final ApplicationFileLocator[] afls;
	
	public ProcessingOptions(byte[] data) {
		this.aip = Arrays.copyOfRange(data, 0, 2);
		this.afls = new ApplicationFileLocator[(data.length - 2) / 4];
		for (int i = 2, j = 0; i < data.length; i += 4, j++) {
			this.afls[j] = new ApplicationFileLocator(data, i);
		}
	}

	public ProcessingOptions(ConstructedTLV format2) {
		this.aip = ((PrimitiveTLV) format2.getChild(EMVDataElement.APPLICATION_INTERCHANGE_PROFILE)).getValue();
		byte[] afls = ((PrimitiveTLV) format2.getChild(EMVDataElement.APPLICATION_FILE_LOCATOR)).getValue();
		this.afls = new ApplicationFileLocator[afls.length / 4];
		for (int i = 0, j = 0; i < afls.length; i += 4, j++) {
			this.afls[j] = new ApplicationFileLocator(afls, i);
		}
	}

	public byte[] getAip() {
		return aip;
	}

	public ApplicationFileLocator[] getAfls() {
		return afls;
	}

	@Override
	public String toString() {
		return "Processing options ["
				+ "\nAIP = " + HexadecimalStringUtil.bytesToString(this.aip)
				+ "\nAFLs = " + Arrays.<ApplicationFileLocator>asList(this.afls)
				+ "\n]";
	}
	
}
