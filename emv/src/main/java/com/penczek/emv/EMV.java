package com.penczek.emv;

import com.penczek.emv.model.ApplicationFileLocator;
import com.penczek.emv.model.ApplicationIdentifier;
import com.penczek.emv.model.DataObjectList;
import com.penczek.emv.model.FileControlInformation;
import com.penczek.emv.model.FirstACResponse;
import com.penczek.emv.model.ProcessingOptions;
import com.penczek.smartcard.CommandAPDU;
import com.penczek.smartcard.Disposition;
import com.penczek.smartcard.ResponseAPDU;
import com.penczek.smartcard.SCard;
import com.penczek.smartcard.Scope;
import com.penczek.smartcard.ShareMode;
import com.penczek.tlv.ConstructedTLV;
import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.PrimitiveTLV;
import com.penczek.tlv.TLV;
import com.penczek.tlv.TLVData;
import com.penczek.tlv.TLVDataParser;
import com.penczek.util.HexadecimalStringUtil;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.LogManager;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class EMV {

	public static final int PIN_OK = Integer.MIN_VALUE;

	private Random rand = new Random();
	private boolean selected;
	private SCard channel;
	private int status;
	private int protocol;
	private byte[] atr;
	private byte[] aid;
	private FileControlInformation fci;
	private ProcessingOptions gpo;
	private TLVData tlvData;
	private String track2;
	private DataObjectList cdol1;
	private String pan;
	private int panSequence;
	private DataObjectList cdol2;
	private List<ApplicationIdentifier> availableAIDs;
	private FirstACResponse firstACResponse;

	public EMV() throws Exception {
		channel = new SCard();
		channel.establishContext(Scope.SCARD_SCOPE_SYSTEM);
		String readers[] = channel.listReaders();
		if (readers.length <= 0) {
			throw new Exception("No smartcard readers found");
		}
		channel = new SCard(readers[0]);
		channel.establishContext(Scope.SCARD_SCOPE_SYSTEM);
		channel.connect(ShareMode.SCARD_SHARE_SHARED, 3);
		Map<String, Object> stat = channel.status();
		this.status = ((Number) stat.get("status")).intValue();
		this.protocol = ((Number) stat.get("protocol")).intValue();
		this.atr = (byte[]) stat.get("atr");
		final byte[][] aids = new byte[][] {
			/* TLOG */
			HexadecimalStringUtil.bytesFromString("A0000005081214"),
			HexadecimalStringUtil.bytesFromString("A0000005081010"),
			HexadecimalStringUtil.bytesFromString("A0000005082020"),
			HexadecimalStringUtil.bytesFromString("A0000005082014"),
			HexadecimalStringUtil.bytesFromString("A0000005082016"),
			HexadecimalStringUtil.bytesFromString("A0000005081204"),
			/* MASTERCARD */
			HexadecimalStringUtil.bytesFromString("A0000000040000"),
			HexadecimalStringUtil.bytesFromString("A0000000041010"),
			HexadecimalStringUtil.bytesFromString("A00000000410101213"),
			HexadecimalStringUtil.bytesFromString("A00000000410101215"),
			HexadecimalStringUtil.bytesFromString("A0000000042010"),
			HexadecimalStringUtil.bytesFromString("A0000000042203"),
			HexadecimalStringUtil.bytesFromString("A0000000043010"),
			HexadecimalStringUtil.bytesFromString("A0000000043060"),
			HexadecimalStringUtil.bytesFromString("A000000004306001"),
			HexadecimalStringUtil.bytesFromString("A0000000044010"),
			HexadecimalStringUtil.bytesFromString("A0000000045010"),
			HexadecimalStringUtil.bytesFromString("A0000000045555"),
			HexadecimalStringUtil.bytesFromString("A0000000046000"),
			HexadecimalStringUtil.bytesFromString("A0000000048002"),
			HexadecimalStringUtil.bytesFromString("A0000000049999"),
			/* VISA */
			HexadecimalStringUtil.bytesFromString("A000000003000000"),
			HexadecimalStringUtil.bytesFromString("A00000000300037561"),
			HexadecimalStringUtil.bytesFromString("A00000000305076010"),
			HexadecimalStringUtil.bytesFromString("A0000000031010"),
			HexadecimalStringUtil.bytesFromString("A000000003101001"),
			HexadecimalStringUtil.bytesFromString("A000000003101002"),
			HexadecimalStringUtil.bytesFromString("A0000000032010"),
			HexadecimalStringUtil.bytesFromString("A0000000032020"),
			HexadecimalStringUtil.bytesFromString("A0000000033010"),
			HexadecimalStringUtil.bytesFromString("A0000000034010"),
			HexadecimalStringUtil.bytesFromString("A0000000035010"),
			HexadecimalStringUtil.bytesFromString("A000000003534441"),
			HexadecimalStringUtil.bytesFromString("A0000000035350"),
			HexadecimalStringUtil.bytesFromString("A000000003535041"),
			HexadecimalStringUtil.bytesFromString("A0000000036010"),
			HexadecimalStringUtil.bytesFromString("A0000000036020"),
			HexadecimalStringUtil.bytesFromString("A0000000038002"),
			HexadecimalStringUtil.bytesFromString("A0000000038010"),
			HexadecimalStringUtil.bytesFromString("A0000000039010"),
			HexadecimalStringUtil.bytesFromString("A000000003999910")
		};
		availableAIDs = new ArrayList<ApplicationIdentifier>(4);
		for (byte[] aid : aids) {
			try {
				FileControlInformation fci = selectAID(aid);
				availableAIDs.add(new ApplicationIdentifier(aid, fci));
			} catch (Exception e) {
			}
		}
		int num = availableAIDs.size();
		if (num == 0) {
			channel.disconnect(Disposition.SCARD_UNPOWER_CARD);
			throw new Exception("Card not recognized");
		}
		if (num == 1) {
			this.initialize(availableAIDs.get(0));
		}
	}

	public List<ApplicationIdentifier> getAvailableAIDs() {
		return Collections.unmodifiableList(availableAIDs);
	}

	public boolean isSelected() {
		return selected;
	}

	public final void initialize(ApplicationIdentifier aid) throws Exception {
		if (aid == null || aid.getAid() == null) {
			throw new Exception("Invalid AID");
		}
		try {
			this.fci = selectAID(aid.getAid());
			this.aid = aid.getAid();
		} catch (Exception e) {}
		if (this.aid == null) {
			channel.disconnect(Disposition.SCARD_UNPOWER_CARD);
			throw new Exception("AID not found");
		}
		this.selected = true;
		this.gpo = getProcessingOptions();
		List<TLV> tlvs = new LinkedList<TLV>();
		for (ApplicationFileLocator afl : gpo.getAfls()) {
			for (int i = afl.getFirstRecord(); i <= afl.getLastRecord(); i++) {
				TLV[] rr = readRecord(afl.getSfi(), i).getData();
				for (TLV t : rr) {
					tlvs.add(t);
				}
			}
		}
		this.tlvData = new TLVData(tlvs.toArray(new TLV[tlvs.size()]));
		this.pan = (String) this.tlvData.getDataElementValue(EMVDataElement.APPLICATION_PRIMARY_ACCOUNT_NUMBER);
		Number n = (Number) this.tlvData.getDataElementValue(EMVDataElement.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER);
		this.panSequence = n != null ? n.intValue() : 0;
		byte[] bb = this.tlvData.getDataElementBinaryValue(EMVDataElement.TRACK_2_EQUIVALENT_DATA);
		if (bb != null) {
			this.track2 = HexadecimalStringUtil.bytesToString(bb).replace('D', '=').replaceAll("F+$", "");
		}
		this.cdol1 = new DataObjectList(this.tlvData.getDataElementBinaryValue(EMVDataElement.CARD_RISK_MANAGEMENT_DATA_OBJECT_LIST_1));
		this.cdol2 = new DataObjectList(this.tlvData.getDataElementBinaryValue(EMVDataElement.CARD_RISK_MANAGEMENT_DATA_OBJECT_LIST_2));
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			this.channel.disconnect(Disposition.SCARD_UNPOWER_CARD);
		} catch (Exception e) {}
	}

	private FileControlInformation selectAID(byte[] aid) throws Exception {
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x00, 0xA4, 04, 0x0, aid));
		if (r.getSW() == 0x9000) {
			FileControlInformation fci = new FileControlInformation(r.getData());
			return fci;
		} else {
			throw new Exception("Cannot select AIP: " + String.format("%04x", r.getSW()));
		}
	}

	public int getATC() throws Exception {
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x80, 0xCA, 0x9F, 0x36));
		if (r.getSW() == 0x9000) {
			return ((Number) TLVDataParser.parseData(r.getData()).getDataElementValue(EMVDataElement.APPLICATION_TRANSACTION_COUNTER)).intValue();
		} else {
			throw new Exception("Cannot get ATC: " + String.format("%04x", r.getSW()));
		}
	}

	public int getLastOnlineATC() throws Exception {
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x80, 0xCA, 0x9F, 0x13, new byte[0]));
		if (r.getSW() == 0x9000) {
			return ((Number) TLVDataParser.parseData(r.getData()).getDataElementValue(EMVDataElement.LAST_ONLINE_APPLICATION_TRANSACTION_COUNTER_REGISTER)).intValue();
		} else {
			throw new Exception("Cannot get last online ATC: " + String.format("%04x", r.getSW()));
		}
	}

	public int getPinTryCounter() throws Exception {
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x80, 0xCA, 0x9F, 0x17));
		if (r.getSW() == 0x9000) {
			return ((Number) TLVDataParser.parseData(r.getData()).getDataElementValue(EMVDataElement.PERSONAL_IDENTIFICATION_NUMBER_TRY_COUNTER)).intValue();
		} else {
			throw new Exception("Cannot get pin tries: " + String.format("%04x", r.getSW()));
		}
	}

	public TLVData getData(int i, int j) throws Exception {
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x80, 0xCA, i, j));
		if (r.getSW() == 0x9000) {
			return TLVDataParser.parseData(r.getData());
		} else {
			throw new Exception("Cannot get data " + String.format("%02x", i) + String.format("%02x", j) + ": " + String.format("%04x", r.getSW()));
		}
	}

	private ProcessingOptions getProcessingOptions() throws Exception {
		byte[] pdolData = new byte[]{(byte) 0x83, 0};
		DataObjectList pdol = this.fci.getPdol();
		if (pdol != null) {
			pdolData = pdol.getDolData(Map.of(EMVDataElement.TERMINAL_COUNTRY_CODE, new byte[]{0, 0x76}));
			pdolData = HexadecimalStringUtil.bytesFromString(new PrimitiveTLV(EMVDataElement.COMMAND_TEMPLATE, pdolData).toHexadecimal());
		}
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x80, 0xA8, 0, 0, pdolData));
		if (r.getSW() == 0x9000) {
			TLVData tlv = TLVDataParser.parseData(r.getData());
			PrimitiveTLV format1 = (PrimitiveTLV) tlv.getDataElement(EMVDataElement.RESPONSE_MESSAGE_TEMPLATE_FORMAT_1);
			if (format1 != null) {
				return new ProcessingOptions(format1.getValue());
			} else {
				ConstructedTLV format2 = (ConstructedTLV) tlv.getDataElement(EMVDataElement.RESPONSE_MESSAGE_TEMPLATE_FORMAT_2);
				if (format2 != null) {
					return new ProcessingOptions(format2);
				} else {
					throw new Exception("Invalid response " + tlv);
				}
			}
		} else {
			throw new Exception("Cannot get processing options: " + String.format("%04x", r.getSW()));
		}
	}

	private TLVData readRecord(int sfi, int rec) throws Exception {
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x00, (byte) 0xB2, (byte) rec, (byte) (sfi << 3 | 4)));
		if (r.getSW() == 0x9000) {
			return TLVDataParser.parseData(r.getData());
		} else {
			throw new Exception("Cannot read record: " + String.format("%04x", r.getSW()));
		}
	}

	public FirstACResponse generateAC(byte[] cdol1) throws Exception {
		int p1 = 0x80; // ARQC
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x80, 0xAE, p1, 0x0, cdol1));
		if (r.getSW() == 0x9000) {
			TLVData tlv = TLVDataParser.parseData(r.getData());
			PrimitiveTLV format1 = (PrimitiveTLV) tlv.getDataElement(EMVDataElement.RESPONSE_MESSAGE_TEMPLATE_FORMAT_1);
			if (format1 != null) {
				return new FirstACResponse(format1.getValue());
			} else {
				ConstructedTLV format2 = (ConstructedTLV) tlv.getDataElement(EMVDataElement.RESPONSE_MESSAGE_TEMPLATE_FORMAT_2);
				if (format2 != null) {
					return new FirstACResponse(format2);
				} else {
					throw new Exception("Invalid response " + tlv);
				}
			}
		} else {
			throw new Exception("Cannot generate AC: " + String.format("%04x", r.getSW()));
		}
	}

	public TLVData generate2ndAC(byte[] cdol2, boolean tc) throws Exception {
		int p1 = tc ? 0x40 : 0; // TC ou AAC
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x80, 0xAE, p1, 0x0, cdol2));
		if (r.getSW() == 0x9000) {
			return TLVDataParser.parseData(r.getData());
		} else {
			throw new Exception("Cannot generate 2nd AC: " + String.format("%04x", r.getSW()));
		}
	}

	public boolean externalAuthenticate(byte[] iad) throws Exception {
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0x00, 0x82, 0, 0, iad));
		int i = r.getSW();
		if (i != 0x9000 && i != 0x6300) {
			throw new Exception("Cannot external authenticate: " + String.format("%04x", i));
		}
		return i == 0x9000;
	}

	public int verifyPIN(String pin) throws Exception {
		int len = pin.length();
		if (len % 2 != 0) pin += 'F';
		byte[] param = new byte[] {
			(byte) (0x20 + len), (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF
		};
		byte[] epin = HexadecimalStringUtil.bytesFromString(pin);
		System.arraycopy(epin, 0, param, 1, epin.length);
		ResponseAPDU r = this.channel.transmit(new CommandAPDU(0, 0x20, 0, (byte) 0x80, param));
		int i = r.getSW();
		if (i != 0x9000 && i != 0x6983 && (i & 0x6300) != 0x6300) {
			throw new Exception("Cannot verify PIN: " + String.format("%04x", i));
		}
		if (i == 0x9000) {
			return PIN_OK;
		}
		if (i == 0x6983) {
			return -1;
		}
		return r.getSW2();
	}

	public Transaction newTransaction() throws Exception {
		return this.newTransaction(new BigDecimal(Math.abs(rand.nextInt())), "00", "0000048800");
	}

	public Transaction newTransaction(BigDecimal amount, String transactionType, String tvr) throws Exception {
		Transaction t = new Transaction();
		t.transactionType = transactionType;
		t.tvr = tvr;
		t.amount = amount != null ? String.format("%012d", amount.setScale(2, RoundingMode.HALF_UP).unscaledValue()) : "000000000000";
		t.unpredictableNumber = String.format("%08x", Math.abs(rand.nextInt())).toUpperCase();
		t.date = new SimpleDateFormat("yyMMdd").format(new Date());
		t.tlvTerminal = "9F0206" + t.amount
				+ "9F0306" + "000000000000"
				+ "9F1A02" + "0076"
				+ "9505" + tvr
				+ "5F2A02" + "0986"
				+ "9A03" + t.date
				+ "9C01" + transactionType
				+ "9F3704" + t.unpredictableNumber
				+ "8202" + HexadecimalStringUtil.bytesToString(this.getGpo().getAip())
				+ "9F3501" + "21"
				+ "9F3403" + "3F0000";

		Map<EMVDataElement, byte[]> values = Map.ofEntries(
				Map.entry(EMVDataElement.AMOUNT_AUTHORISED_NUMERIC, HexadecimalStringUtil.bytesFromString(t.amount)),
				Map.entry(EMVDataElement.AMOUNT_OTHER_NUMERIC, HexadecimalStringUtil.bytesFromString("000000000000")),
				Map.entry(EMVDataElement.TERMINAL_COUNTRY_CODE, HexadecimalStringUtil.bytesFromString("0076")),
				Map.entry(EMVDataElement.TERMINAL_VERIFICATION_RESULTS, HexadecimalStringUtil.bytesFromString(tvr)),
				Map.entry(EMVDataElement.TRANSACTION_CURRENCY_CODE, HexadecimalStringUtil.bytesFromString("0986")),
				Map.entry(EMVDataElement.TRANSACTION_DATE, HexadecimalStringUtil.bytesFromString(t.date)),
				Map.entry(EMVDataElement.TRANSACTION_TYPE, HexadecimalStringUtil.bytesFromString(transactionType)),
				Map.entry(EMVDataElement.UNPREDICTABLE_NUMBER, HexadecimalStringUtil.bytesFromString(t.unpredictableNumber)),
				Map.entry(EMVDataElement.TERMINAL_TYPE, HexadecimalStringUtil.bytesFromString("21")),
				Map.entry(EMVDataElement.CARDHOLDER_VERIFICATION_METHOD_RESULTS, HexadecimalStringUtil.bytesFromString("3F0000"))
		);

		/* Terminal type
      Environment                     Financial Institution   Merchant  Cardholder
      Attended
        Online only                   11                      21        —
        Offline w/ online capability  12                      22        —
        Offline only                  13                      23        —
      Unattended
        Online only                   14 (ATM)                24        34
        Offline w/ online capability  15 (ATM)                25        35
        Offline only                  16 (ATM)                26        36
		 */
		this.firstACResponse = this.generateAC(this.cdol1.getDolData(values));
		/*
    Visa / CCD
      AMOUNT_AUTHORISED_NUMERIC - 6
      AMOUNT_OTHER_NUMERIC - 6
      TERMINAL_COUNTRY_CODE - 2
      TERMINAL_VERIFICATION_RESULTS - 5
      TRANSACTION_CURRENCY_CODE - 2
      TRANSACTION_DATE - 3
      TRANSACTION_TYPE - 1
      UNPREDICTABLE_NUMBER - 4
    Master
      AMOUNT_AUTHORISED_NUMERIC - 6
      AMOUNT_OTHER_NUMERIC - 6
      TERMINAL_COUNTRY_CODE - 2
      TERMINAL_VERIFICATION_RESULTS - 5
      TRANSACTION_CURRENCY_CODE - 2
      TRANSACTION_DATE - 3
      TRANSACTION_TYPE - 1
      UNPREDICTABLE_NUMBER - 4
      TERMINAL_TYPE - 1
      DATA_AUTHENTICATION_CODE - 2
      CARDHOLDER_VERIFICATION_METHOD_RESULTS - 3  3F0000 - no CVM performed
		 */
		t.tlvIcc = this.firstACResponse.toTLV();
		return t;
	}

	public SCard getChannel() {
		return channel;
	}

	public int getStatus() {
		return status;
	}

	public int getProtocol() {
		return protocol;
	}

	public byte[] getAtr() {
		return atr;
	}

	public byte[] getAid() {
		return aid;
	}

	public FileControlInformation getFic() {
		return fci;
	}

	public ProcessingOptions getGpo() {
		return gpo;
	}

	public TLVData getTlvData() {
		return tlvData;
	}

	public String getTrack2() {
		return track2;
	}

	public DataObjectList getCdol1() {
		return cdol1;
	}

	public DataObjectList getCdol2() {
		return cdol2;
	}

	public FinalResult finalizeTransaction(Transaction t, TLVData issuerResponse, boolean approved) throws Exception {
		List<String> debug = new LinkedList<String>();
		TLV[] beforeScripts = issuerResponse.getDataElements(EMVDataElement.ISSUER_SCRIPT_TEMPLATE_1);
		TLV[] afterScripts = issuerResponse.getDataElements(EMVDataElement.ISSUER_SCRIPT_TEMPLATE_2);
		byte[] issuerScriptResults = new byte[((beforeScripts != null ? beforeScripts.length : 0) + (afterScripts != null ? afterScripts.length : 0)) * 5];
		int i = 0;
		byte[] tvr = HexadecimalStringUtil.bytesFromString(t.tvr);
		if (beforeScripts != null) {
			for (TLV bsc : beforeScripts) {
				ConstructedTLV cbsc = (ConstructedTLV) bsc;
				int id = ((Number) ((PrimitiveTLV) cbsc.getChild(EMVDataElement.ISSUER_SCRIPT_IDENTIFIER)).getInterpretedValue()).intValue();
				TLV[] cmds = cbsc.getChildren(EMVDataElement.ISSUER_SCRIPT_COMMAND);
				boolean error = false;
				for (TLV cmd : cmds) {
					byte[] apdu = (byte[]) ((PrimitiveTLV) cmd).getInterpretedValue();
					ResponseAPDU r = this.channel.transmit(new CommandAPDU(apdu));
					debug.add(HexadecimalStringUtil.bytesToString(apdu) + " : " + r);
					int sw1 = r.getSW1();
					if (sw1 != 0x90 && sw1 != 0x62 && sw1 != 0x63) {
						error = true;
						tvr[4] |= 0x20;
						break;
					}
				}
				issuerScriptResults[i] = (byte) ((error ? 0x10 : 0x20) | (i > 14 ? 0x0F : (i + 1)));
				issuerScriptResults[i + 1] = (byte) ((id >> 24) & 0xFF);
				issuerScriptResults[i + 2] = (byte) ((id >> 16) & 0xFF);
				issuerScriptResults[i + 3] = (byte) ((id >> 8) & 0xFF);
				issuerScriptResults[i + 4] = (byte) (id & 0xFF);
				i += 5;
			}
		}
		PrimitiveTLV iad = (PrimitiveTLV) issuerResponse.getDataElement(EMVDataElement.ISSUER_AUTHENTICATION_DATA);
		// PENDING: use CDOL2
		byte[] cdol2 = new byte[19];
		System.arraycopy(iad.getValue(), 0, cdol2, 0, 8);
		boolean tc;
		if (approved) {
			cdol2[8] = 0;
			cdol2[9] = 0;
			tc = true;
		} else {
			cdol2[8] = 0;
			cdol2[9] = 1;
			tc = false;
		}
		System.arraycopy(HexadecimalStringUtil.bytesFromString(t.unpredictableNumber), 0, cdol2, 10, 4);
		System.arraycopy(tvr, 0, cdol2, 14, 5);
		TLVData ac = this.generate2ndAC(cdol2, tc);
		if (afterScripts != null) {
			for (TLV sc : afterScripts) {
				ConstructedTLV csc = (ConstructedTLV) sc;
				int id = ((Number) ((PrimitiveTLV) csc.getChild(EMVDataElement.ISSUER_SCRIPT_IDENTIFIER)).getInterpretedValue()).intValue();
				TLV[] cmds = csc.getChildren(EMVDataElement.ISSUER_SCRIPT_COMMAND);
				boolean error = false;
				for (TLV cmd : cmds) {
					byte[] apdu = (byte[]) ((PrimitiveTLV) cmd).getInterpretedValue();
					ResponseAPDU r = this.channel.transmit(new CommandAPDU(apdu));
					debug.add(HexadecimalStringUtil.bytesToString(apdu) + " : " + r);
					int sw1 = r.getSW1();
					if (sw1 != 0x90 && sw1 != 0x62 && sw1 != 0x63) {
						error = true;
						break;
					}
				}
				issuerScriptResults[i] = (byte) ((error ? 0x10 : 0x20) | (i > 14 ? 0x0F : (i + 1)));
				issuerScriptResults[i + 1] = (byte) ((id >> 24) & 0xFF);
				issuerScriptResults[i + 2] = (byte) ((id >> 16) & 0xFF);
				issuerScriptResults[i + 3] = (byte) ((id >> 8) & 0xFF);
				issuerScriptResults[i + 4] = (byte) (id & 0xFF);
				i += 5;
			}
		}
		return new FinalResult(ac, issuerScriptResults.length > 0 ? issuerScriptResults : null, debug.size() > 0 ? debug.toArray(new String[debug.size()]) : null);
	}

	public static void main(String[] args) throws Exception {
		try {
			LogManager.getLogManager().readConfiguration(new ByteArrayInputStream((
					"handlers = java.util.logging.ConsoleHandler\n" +
					"java.util.logging.ConsoleHandler.level = FINEST\n" +
					"com.level = FINEST\n"
					).getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		EMV emv = new EMV();
		if (!emv.isSelected()) {
			List<ApplicationIdentifier> aids = emv.getAvailableAIDs();
			for (int i = 0; i < aids.size(); i++) {
				System.out.println(i + " - " + aids.get(i).getFci().getApplicationLabel());
			}
			Scanner sc = new Scanner(System.in);
			int id = -1;
			while (id < 0 || id >= aids.size()) {
				System.out.println("Type the AID number and enter:");
				try {
					id = sc.nextInt();
				} catch (Exception e) {
				}
			}
			emv.initialize(aids.get(id));
		}
		System.out.println("FCI " + emv.getFic());
		System.out.println("GPO " + emv.getGpo());
		System.out.println("Card " + emv.getTlvData());
		System.out.println("CDOL1 " + emv.getCdol1());
		System.out.println("CDOL2 " + emv.getCdol2());
//		System.out.println("DF60 " + emv.getData(0xDF, 0x60));

		Transaction t = emv.newTransaction();
		System.out.println("AID " + HexadecimalStringUtil.bytesToString(emv.getAid()));
		System.out.println("PAN " + emv.pan);
		System.out.println("PAN# " + emv.panSequence);
		System.out.println("Amount " + t.amount);
		System.out.println("Date " + t.date);
		System.out.println("UN " + t.unpredictableNumber);
		System.out.println(String.format("ATC %x (%d)", emv.firstACResponse.getAtc(), emv.firstACResponse.getAtc()));
		System.out.println("TLVterm " + t.tlvTerminal);
		System.out.println("TLVicc " + t.tlvIcc);
	}

	public String getPan() {
		return pan;
	}

	public int getPanSequence() {
		return panSequence;
	}

	public static byte[] generateAC(String pan, String tlv) throws Exception {
		return generateAC(pan, TLVDataParser.parseData(tlv));
	}

	public static byte[] generateAC(String pan, TLVData tlvData) throws Exception {
		// Issuer MK
		byte[] imkAC = adjustParity(HexadecimalStringUtil.bytesFromString("101112131415161718191A1B1C1D1E1F"));
		// GENERATE CARD MK
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DESede");
		Cipher c = Cipher.getInstance("DESede/ECB/NoPadding");
		byte[] block = HexadecimalStringUtil.bytesFromString(pan.substring(pan.length() - 14) + "00");
		byte[] block2 = new BigInteger(block).xor(new BigInteger(HexadecimalStringUtil.bytesFromString("FFFFFFFFFFFFFFFF"))).toByteArray();
		SecretKey secretKey = keyFactory.generateSecret(new DESedeKeySpec(adjustKey(imkAC)));
		c.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] mk1 = c.doFinal(block);
		byte[] mk2 = c.doFinal(block2);
		byte[] mkAC = Arrays.copyOf(mk1, 16);
		System.arraycopy(mk2, 0, mkAC, 8, 8);
		mkAC = adjustKey(adjustParity(mkAC));
		// GENERATE CARD SESSION KEY
		byte[] atc = ((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.APPLICATION_TRANSACTION_COUNTER)).getValue();
		byte[] r1 = Arrays.copyOf(atc, 8);
		r1[2] = -16; // F0
		byte[] r2 = Arrays.copyOf(r1, 8);
		r2[2] = 15; // 0F
		secretKey = keyFactory.generateSecret(new DESedeKeySpec(adjustKey(mkAC)));
		c.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] sk1 = c.doFinal(r1);
		byte[] sk2 = c.doFinal(r2);
		byte[] skAC = Arrays.copyOf(sk1, 16);
		System.arraycopy(sk2, 0, skAC, 8, 8);
		skAC = adjustKey(adjustParity(skAC));
		// DATA TO AC
		byte[] data = new byte[72];
		int i = 0;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.AMOUNT_AUTHORISED_NUMERIC)).getValue(),
				0, data, i, 6);
		i += 6;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.AMOUNT_OTHER_NUMERIC)).getValue(),
				0, data, i, 6);
		i += 6;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TERMINAL_COUNTRY_CODE)).getValue(),
				0, data, i, 2);
		i += 2;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TERMINAL_VERIFICATION_RESULTS)).getValue(),
				0, data, i, 5);
		i += 5;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TRANSACTION_CURRENCY_CODE)).getValue(),
				0, data, i, 2);
		i += 2;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TRANSACTION_DATE)).getValue(),
				0, data, i, 3);
		i += 3;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TRANSACTION_TYPE)).getValue(),
				0, data, i, 1);
		i += 1;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.UNPREDICTABLE_NUMBER)).getValue(),
				0, data, i, 4);
		i += 4;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.APPLICATION_INTERCHANGE_PROFILE)).getValue(),
				0, data, i, 2);
		i += 2;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.APPLICATION_TRANSACTION_COUNTER)).getValue(),
				0, data, i, 2);
		i += 2;
		System.arraycopy(((PrimitiveTLV) tlvData.getDataElement(EMVDataElement.ISSUER_APPLICATION_DATA)).getValue(),
				0, data, i, 32);
		i += 32;
		data[i] = (byte) 0x80; // pad3 - 0x80 + 0x00 ...
		// GENERATE AC = MAC algorithm specified in Annex A1.2.1 and ISO/IEC 9797-1 Algorithm 3 with DES,
		// and s=8. This method shall be used for a Cryptogram Version of '5'.
		SecretKeyFactory desKeyFactory = SecretKeyFactory.getInstance("DES");
		secretKey = desKeyFactory.generateSecret(new DESKeySpec(skAC));
		c = Cipher.getInstance("DES/CBC/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(new byte[8]));
		byte[] enc = c.doFinal(data);
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		secretKey = desKeyFactory.generateSecret(new DESKeySpec(skAC, 8));
		c = Cipher.getInstance("DES/ECB/NoPadding");
		c.init(Cipher.DECRYPT_MODE, secretKey);
		enc = c.doFinal(enc);
		secretKey = desKeyFactory.generateSecret(new DESKeySpec(skAC, 16));
		c.init(Cipher.ENCRYPT_MODE, secretKey);
		return c.doFinal(enc);
	}

	static byte[] adjustParity(byte[] key) {
		// ajusta paridade impar
		for (int i = 0; i < key.length; i++) {
			// conta quantos 1 nos bits mais significativos
			int r = 0;
			for (int j = 7; j >= 0; j--) {
				int w = (key[i] & (1 << j));
				if (w != 0) r++;
			}
			if (r % 2 == 0) {
				key[i] ^= 1;
			}
		}
		return key;
	}

	static byte[] adjustKey(final byte[] key) throws Exception {
		if (key.length == 24) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 24);
			return kn;
		}
		if (key.length == 8) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 8);
			System.arraycopy(key, 0, kn, 8, 8);
			System.arraycopy(key, 0, kn, 16, 8);
			return kn;
		}
		if (key.length == 16) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 16);
			System.arraycopy(key, 0, kn, 16, 8);
			return kn;
		}
		throw new Exception("Invalid length " + key.length);
	}

}
