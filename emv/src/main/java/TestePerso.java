import com.penczek.emv.model.FileControlInformation;
import com.penczek.smartcard.CommandAPDU;
import com.penczek.smartcard.Disposition;
import com.penczek.smartcard.ResponseAPDU;
import com.penczek.smartcard.SCard;
import com.penczek.smartcard.Scope;
import com.penczek.smartcard.ShareMode;
import com.penczek.util.HexadecimalStringUtil;
import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.logging.LogManager;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class TestePerso {

	public static void main(String[] args) throws Exception {
		try {
			LogManager.getLogManager().readConfiguration(new ByteArrayInputStream((
					"handlers = java.util.logging.ConsoleHandler\n" +
					"java.util.logging.ConsoleHandler.level = FINE\n" +
					"com.level = FINE\n"
					).getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		SCard channel = new SCard();
		channel.establishContext(Scope.SCARD_SCOPE_SYSTEM);
		String readers[] = channel.listReaders();
		if (readers.length <= 0) {
			throw new Exception("No smartcard readers found");
		}
		channel = new SCard(readers[0]);
		channel.establishContext(Scope.SCARD_SCOPE_SYSTEM);
		channel.connect(ShareMode.SCARD_SHARE_SHARED, 3);
		String aid = "A0000005082020";
		ResponseAPDU r = channel.transmit(new CommandAPDU(
						0x00, 0xA4, 04, 0x0, HexadecimalStringUtil.bytesFromString(aid)));
		if (r.getSW() != 0x9000) {
			throw new Exception("Cannot select AIP: " + String.format("%04x", r.getSW()));
		}
		System.out.println("FCI " + new FileControlInformation(r.getData()));
		
		
		//r = channel.transmit(new CommandAPDU(0, 0xAE, 0, 0, new byte[32]));
		//System.out.println("Gen AC: " + String.format("%04x", r.getSW()) + (r.getData() != null ? " data " + HexadecimalStringUtil.bytesToString(r.getData()) : ""));
		

		String rterm = "0102030405060708";

		r = channel.transmit(new CommandAPDU(0x80, 0x50, 0, 0, HexadecimalStringUtil.bytesFromString(rterm), 0));
		if (r.getSW() != 0x9000) {
			throw new Exception("Cannot init update: " + String.format("%04x", r.getSW()) + (r.getData() != null ? " data " + HexadecimalStringUtil.bytesToString(r.getData()) : ""));
		}
		String s = HexadecimalStringUtil.bytesToString(r.getData());
		System.out.println("Resposta " + s);
		String keyDataDer = s.substring(8, 20);
		System.out.println("Key der " + keyDataDer);
		String seqCounter = s.substring(24, 28);
		System.out.println("Seq counter " + seqCounter);
		String rcard = s.substring(28, 40);
		System.out.println("Rcard " + rcard);
		String cardcrypto = s.substring(40, 56);
		System.out.println("CardCrypt " + cardcrypto);

		byte[] kmc = HexadecimalStringUtil.bytesFromString("476F6F644361726445636F46726F7461");
		
		byte[] kenc1 = encrypt3DES(HexadecimalStringUtil.bytesFromString(keyDataDer + "F001"), kmc);
		byte[] kenc2 = encrypt3DES(HexadecimalStringUtil.bytesFromString(keyDataDer + "0F01"), kmc);
		byte[] kenc = Arrays.copyOf(kenc1, 16);
		System.arraycopy(kenc2, 0, kenc, 8, 8);

		byte[] kmac1 = encrypt3DES(HexadecimalStringUtil.bytesFromString(keyDataDer + "F002"), kmc);
		byte[] kmac2 = encrypt3DES(HexadecimalStringUtil.bytesFromString(keyDataDer + "0F02"), kmc);
		byte[] kmac = Arrays.copyOf(kmac1, 16);
		System.arraycopy(kmac2, 0, kmac, 8, 8);

		byte[] kdec1 = encrypt3DES(HexadecimalStringUtil.bytesFromString(keyDataDer + "F003"), kmc);
		byte[] kdec2 = encrypt3DES(HexadecimalStringUtil.bytesFromString(keyDataDer + "0F03"), kmc);
		byte[] kdec = Arrays.copyOf(kdec1, 16);
		System.arraycopy(kdec2, 0, kdec, 8, 8);
	
		System.out.println("+++++++++++++++++++++++ seq counter " + seqCounter + " ++++++++++++++++++++++++++++++++++++++");
		byte[] skuenc = encrypt3DESCBC(HexadecimalStringUtil.bytesFromString(
				"0182"
						+ seqCounter // seq counter
						+ "000000000000000000000000"), kenc);
		byte[] skumac = encrypt3DESCBC(HexadecimalStringUtil.bytesFromString(
				"0101"
						+ seqCounter // seq counter
						+ "000000000000000000000000"), kmac);
		byte[] skudec = encrypt3DESCBC(HexadecimalStringUtil.bytesFromString(
				"0181"
						+ seqCounter // seq counter
						+ "000000000000000000000000"), kdec);
		
		byte[] cardCryptoData = HexadecimalStringUtil.bytesFromString(""
				+ rterm // Rterm
				+ seqCounter // seq counter
				+ rcard // Rcard
		);
		cardCryptoData = pad2(cardCryptoData);
		byte[] bcardCrypto = generateMAC3DESAlg1(cardCryptoData, skuenc);
		System.out.println("Expected " + cardcrypto + ", found " + HexadecimalStringUtil.bytesToString(bcardCrypto));
		
		byte[] aidpadded = pad2(HexadecimalStringUtil.bytesFromString(aid));
		byte[] brcard = generateMACAlg3(aidpadded, skumac);
		System.out.println("Expected " + rcard + ", found " + HexadecimalStringUtil.bytesToString(brcard));

		byte[] hostCryptoData = HexadecimalStringUtil.bytesFromString(""
				+ seqCounter // seq counter
				+ rcard // Rcard
				+ rterm // Rterm
		);
		hostCryptoData = pad2(hostCryptoData);

		byte[] bhostCrypto = generateMAC3DESAlg1(hostCryptoData, skuenc);
		
		/*
		String hostCrypto = HexadecimalStringUtil.bytesToString(bhostCrypto);
		String cmd = "8482010010" + hostCrypto;
		
		byte[] extAuth = HexadecimalStringUtil.bytesFromString(cmd);
		extAuth = pad2(extAuth);
		byte[] bmac = generateMACAlg3(extAuth, skumac);
		s = hostCrypto + HexadecimalStringUtil.bytesToString(bmac);

		r = channel.transmit(new CommandAPDU(0x84, 0x82, 1, 0, HexadecimalStringUtil.bytesFromString(s)));
		*/
		CommandAPDU cmd = new CommandAPDU(0x84, 0x82, 1, 0, bhostCrypto, null, skumac);
		r = channel.transmit(cmd);
		if (r.getSW() != 0x9000) {
			throw new Exception("Cannot ext authenticate: " + String.format("%04x", r.getSW()));
		}

		cmd = new CommandAPDU(0x84, 0x10, 0, 0, cmd.getMac(), skumac);
		r = channel.transmit(cmd);
		if (r.getSW() != 0x9000) {
			throw new Exception("Cannot test: " + String.format("%04x", r.getSW()));
		}
		System.out.println("returned " + HexadecimalStringUtil.bytesToString(r.getData()));

		s = "800010" + HexadecimalStringUtil.bytesToString(
				encrypt3DES(HexadecimalStringUtil.bytesFromString("017C6E7080E57A8F9EA275BCE9EC5BDF"), skudec)
		);

		cmd = new CommandAPDU(0x84, 0xE2, 0x60, 0, HexadecimalStringUtil.bytesFromString(s), cmd.getMac(), skumac);
		r = channel.transmit(cmd);
		if (r.getSW() != 0x9000) {
			throw new Exception("Cannot store: " + String.format("%04x", r.getSW()));
		}
		System.out.println("KCV expected 5704E3E1E036C8A9 returned " + HexadecimalStringUtil.bytesToString(r.getData()));
		
		// 017C6E7080E57A8F9EA275BCE9EC5BDF true 5704E3E1E036C8A9
//		byte[] keys = decrypt3DES(HexadecimalStringUtil.bytesFromString("270E413856E063C3D53F7CCF6A7AE3E549AB163DCD957FF72B03F47E704ECE55EC138E3F8408279BF6EAD6394A30B284"), skudec);
//		System.out.println("KEYS " + HexadecimalStringUtil.bytesToString(keys));

		/* */
		
		channel.disconnect(Disposition.SCARD_LEAVE_CARD);
	}

	static byte[] encrypt3DES(byte[] data, byte[] key) throws Exception {
		if (key.length == 8 || key.length == 16) {
			byte[] newkey = new byte[24];
			System.arraycopy(key, 0, newkey, 0, 8);
			System.arraycopy(key, key.length == 8 ? 0 : 8, newkey, 8, 8);
			System.arraycopy(key, 0, newkey, 16, 8);
			key = newkey;
		}
		SecretKey skey = SecretKeyFactory.getInstance("DESede").generateSecret(new DESedeKeySpec(key));
		Cipher c = Cipher.getInstance("DESede/ECB/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] encrypt3DESCBC(byte[] data, byte[] key) throws Exception {
		if (key.length == 8 || key.length == 16) {
			byte[] newkey = new byte[24];
			System.arraycopy(key, 0, newkey, 0, 8);
			System.arraycopy(key, key.length == 8 ? 0 : 8, newkey, 8, 8);
			System.arraycopy(key, 0, newkey, 16, 8);
			key = newkey;
		}
		SecretKey skey = SecretKeyFactory.getInstance("DESede").generateSecret(new DESedeKeySpec(key));
		Cipher c = Cipher.getInstance("DESede/CBC/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, skey, new IvParameterSpec(new byte[8]));
		return c.doFinal(data);
	}

	static byte[] encryptDESCBC(byte[] data, byte[] key) throws Exception {
		return encryptDESCBC(data, key, 0);
	}

	static byte[] encryptDESCBC(byte[] data, byte[] key, int offset) throws Exception {
		SecretKey skey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key, offset));
		Cipher c = Cipher.getInstance("DES/CBC/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, skey, new IvParameterSpec(new byte[8]));
		return c.doFinal(data);
	}

	static byte[] encryptDES(byte[] data, byte[] key) throws Exception {
		return encryptDES(data, key, 0);
	}

	static byte[] encryptDES(byte[] data, byte[] key, int offset) throws Exception {
		SecretKey skey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key, offset));
		Cipher c = Cipher.getInstance("DES/ECB/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] decryptDES(byte[] data, byte[] key) throws Exception {
		return decryptDES(data, key, 0);
	}

	static byte[] decryptDES(byte[] data, byte[] key, int offset) throws Exception {
		SecretKey skey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key, offset));
		Cipher c = Cipher.getInstance("DES/ECB/NoPadding");
		c.init(Cipher.DECRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] decrypt3DES(byte[] data, byte[] key) throws Exception {
		if (key.length == 8 || key.length == 16) {
			byte[] newkey = new byte[24];
			System.arraycopy(key, 0, newkey, 0, 8);
			System.arraycopy(key, key.length == 8 ? 0 : 8, newkey, 8, 8);
			System.arraycopy(key, 0, newkey, 16, 8);
			key = newkey;
		}
		SecretKey skey = SecretKeyFactory.getInstance("DESede").generateSecret(new DESedeKeySpec(key));
		Cipher c = Cipher.getInstance("DESede/ECB/NoPadding");
		c.init(Cipher.DECRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] pad2(byte[] data) {
		// pad3 - 0x80 + 0x00 ...
		byte[] newdata = new byte[(data.length / 8 + 1) * 8];
		System.arraycopy(data, 0, newdata, 0, data.length);
		newdata[data.length] = (byte) 0x80;
		// remaining 00 padding - do nothing
		return newdata;
	}

	static byte[] generateMACAlg3(byte[] data, byte[] key) throws Exception {
		byte[] enc = encryptDESCBC(data, key);
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		// output transformation 3
		// Hq (last block encrypted) is decrypted with the key K? and the result encrypted with the key K:
		// G = eK(dK?(Hq))
		// hq1
		enc = decryptDES(enc, key, 8);
		// g
		enc = encryptDES(enc, key);
		return enc;
	}

	static byte[] generateMAC3DESAlg1(byte[] data, byte[] key) throws Exception {
		byte[] enc = encrypt3DESCBC(data, key);
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		// output transformation 1 - no change
		// G = Hq
		// -- nao faz nada
		return enc;
	}

}
