import com.penczek.emv.model.FileControlInformation;
import com.penczek.smartcard.CommandAPDU;
import com.penczek.smartcard.ResponseAPDU;
import com.penczek.smartcard.SCard;
import com.penczek.smartcard.Scope;
import com.penczek.smartcard.ShareMode;
import com.penczek.util.HexadecimalStringUtil;
import java.io.ByteArrayInputStream;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.logging.LogManager;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class TesteGenAC {

	public static void main(String[] args) throws Exception {
		try {
			LogManager.getLogManager().readConfiguration(new ByteArrayInputStream((
					"handlers = java.util.logging.ConsoleHandler\n" +
					"java.util.logging.ConsoleHandler.level = FINE\n" +
					"com.level = FINE\n"
					).getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		SCard channel = new SCard();
		channel.establishContext(Scope.SCARD_SCOPE_SYSTEM);
		String readers[] = channel.listReaders();
		if (readers.length <= 0) {
			throw new Exception("No smartcard readers found");
		}
		channel = new SCard(readers[0]);
		channel.establishContext(Scope.SCARD_SCOPE_SYSTEM);
		channel.connect(ShareMode.SCARD_SHARE_SHARED, 3);
		String aid = "A0000005082020";
		ResponseAPDU r = channel.transmit(new CommandAPDU(
						0x00, 0xA4, 04, 0x0, HexadecimalStringUtil.bytesFromString(aid)));
		if (r.getSW() != 0x9000) {
			throw new Exception("Cannot select AIP: " + String.format("%04x", r.getSW()));
		}
		System.out.println("FCI " + new FileControlInformation(r.getData()));
		
		r = channel.transmit(new CommandAPDU(0, 0x20, 0, 0));
		System.out.println("Expected 017C6E7080E57A8F9EA275BCE9EC5BDF");
		System.out.println("KEY : " + String.format("%04x", r.getSW()) + (r.getData() != null ? " data " + HexadecimalStringUtil.bytesToString(r.getData()) : ""));

		byte[] data = new byte[32];
		r = channel.transmit(new CommandAPDU(0, 0xAE, 0, 0, data));
		System.out.println("Anterior 5E851E4D2A105C75");
		System.out.println("Gen AC: " + String.format("%04x", r.getSW()) + (r.getData() != null ? " data " + HexadecimalStringUtil.bytesToString(r.getData()) : ""));
		
		byte[] key = HexadecimalStringUtil.bytesFromString("017C6E7080E57A8F9EA275BCE9EC5BDF");
		data = pad2(data);
		System.out.println("Expected 3DES mac1 " + HexadecimalStringUtil.bytesToString(generateMAC3DESAlg1(data, key)));
		System.out.println("Expected 1DES mac1 " + HexadecimalStringUtil.bytesToString(generateMACAlg1(data, key)));
		System.out.println("Expected mac3 " + HexadecimalStringUtil.bytesToString(generateMACAlg3(data, key)));
	}

	/**
	 * The MAC algorithm specified in Annex A1.2.1 and ISO/IEC 9797-1 Algorithm 3 with DES,
	 * and s=8. This method shall be used for a Cryptogram Version of '5'.
	 * @param data
	 * @param ak
	 * @return
	 * @throws Exception 
	 */
	static byte[] generateMACAlg3(byte[] data, byte[] ak) throws Exception {
		ak = adjustKey(ak);
		byte[] iv = new byte[8];
		// usa apenas os 8 primeiros bytes
		SecretKeyFactory DES_KEY_FACTORY = SecretKeyFactory.getInstance("DES");
		SecretKey secretKey = DES_KEY_FACTORY.generateSecret(new DESKeySpec(ak));
		Cipher DES_CBC = Cipher.getInstance("DES/CBC/NoPadding");
		DES_CBC.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
		byte[] enc = DES_CBC.doFinal(data);
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		// output transformation 3
		// Hq (last block encrypted) is decrypted with the key K' and the result encrypted with the key K:
		// G = eK(dK'(Hq))
		// hq1
		secretKey = DES_KEY_FACTORY.generateSecret(new DESKeySpec(ak, 8));
		Cipher DES = Cipher.getInstance("DES/ECB/NoPadding");
		DES.init(Cipher.DECRYPT_MODE, secretKey);
		enc = DES.doFinal(enc);
		// g
		secretKey = DES_KEY_FACTORY.generateSecret(new DESKeySpec(ak, 16));
		DES.init(Cipher.ENCRYPT_MODE, secretKey);
		enc = DES.doFinal(enc);
		return enc;
	}

	static byte[] adjustKey(final byte[] key) throws InvalidKeyException {
		if (key.length == 24) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 24);
			return kn;
		}
		if (key.length == 8) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 8);
			System.arraycopy(key, 0, kn, 8, 8);
			System.arraycopy(key, 0, kn, 16, 8);
			return kn;
		}
		if (key.length == 16) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 16);
			System.arraycopy(key, 0, kn, 16, 8);
			return kn;
		}
		throw new InvalidKeyException("Invalid length " + key.length);
	}

	static byte[] generateMAC3DESAlg1(byte[] data, byte[] ak) throws Exception {
		Cipher c = Cipher.getInstance("DESede/CBC/NoPadding");
		SecretKey tak = SecretKeyFactory.getInstance("DESede").generateSecret(new DESedeKeySpec(adjustKey(ak)));
		c.init(Cipher.ENCRYPT_MODE, tak, new IvParameterSpec(new byte[8]));
		byte[] enc = c.doFinal(data);
		byte[] hq = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		return hq;
	}

	static byte[] generateMACAlg1(byte[] data, byte[] ak) throws Exception {
		Cipher c = Cipher.getInstance("DES/CBC/NoPadding");
		SecretKey tak = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(ak));
		c.init(Cipher.ENCRYPT_MODE, tak, new IvParameterSpec(new byte[8]));
		byte[] enc = c.doFinal(data);
		byte[] hq = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		return hq;
	}

	static byte[] pad2(byte[] data) {
		// pad3 - 0x80 + 0x00 ...
		byte[] newdata = new byte[(data.length / 8 + 1) * 8];
		System.arraycopy(data, 0, newdata, 0, data.length);
		newdata[data.length] = (byte) 0x80;
		// remaining 00 padding - do nothing
		return newdata;
	}

}
