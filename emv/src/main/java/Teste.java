import com.penczek.smartcard.CommandAPDU;
import com.penczek.util.HexadecimalStringUtil;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.security.spec.RSAPublicKeySpec;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class Teste {

	public static void main(String[] args) throws Exception {
		byte[] kmc = HexadecimalStringUtil.bytesFromString("476F6F644361726445636F46726F7461");

		// KEYDATA = 00008322836852072041
		byte[] kenc1 = encrypt3DES(HexadecimalStringUtil.bytesFromString("836852072041" + "F001"), kmc);
		byte[] kenc2 = encrypt3DES(HexadecimalStringUtil.bytesFromString("836852072041" + "0F01"), kmc);
		byte[] kenc = Arrays.copyOf(kenc1, 16);
		System.arraycopy(kenc2, 0, kenc, 8, 8);

		byte[] kmac1 = encrypt3DES(HexadecimalStringUtil.bytesFromString("836852072041" + "F002"), kmc);
		byte[] kmac2 = encrypt3DES(HexadecimalStringUtil.bytesFromString("836852072041" + "0F02"), kmc);
		byte[] kmac = Arrays.copyOf(kmac1, 16);
		System.arraycopy(kmac2, 0, kmac, 8, 8);

		byte[] kdec1 = encrypt3DES(HexadecimalStringUtil.bytesFromString("836852072041" + "F003"), kmc);
		byte[] kdec2 = encrypt3DES(HexadecimalStringUtil.bytesFromString("836852072041" + "0F03"), kmc);
		byte[] kdec = Arrays.copyOf(kdec1, 16);
		System.arraycopy(kdec2, 0, kdec, 8, 8);


		String seqCounter = "0007";
		String rterm = "3780A71F74AA5F3A";
		String rcard = "4D2A8FC60920";
		String cardcrypto = "E613CA333E94ED82";
		String hostcrypto = "149081A7733AE395";
		String aid = "A000000003000000";
		String cmd = "8482010010149081A7733AE395";
		String cmdmac = "D4BEEF7C94E43511";
		String previousmac = "0000000000000000";
		/*
		cmd = "84E40000114F07A0000005081214";
		cmdmac = "34F0D03787F598B4";
		previousmac = "D4BEEF7C94E43511";
		/*
		cmd = "84E40000104F06A000000508FE";
		cmdmac = "BE0B8EADA4E2B899";
		previousmac = "34F0D03787F598B4";
		*/
		personalization(seqCounter, kenc, kmac, kdec, rterm, rcard, cardcrypto, aid, hostcrypto, cmd, cmdmac, previousmac);


		personalization(
				"0008",
				kenc, kmac, kdec,
				"3780A71F74AA5F3A",
				"21865A62D7CF",
				"44E2BE9634C04D79",
				"A0000005081214",
				"D94CC71EE0380E42",
				"8482010010D94CC71EE0380E42",
				"D2BF1A1D7FA2B591",
				"0000000000000000");

		/*
### INITIALIZE UPDATE:
->80500000083780A71F74AA5F3A
<-00008322836852072041010200074D2A8FC60920E613CA333E94ED829000
KEYDATA		00008322836852072041
VersionKMC	01
Id SecChPrt	02
SeqCounter	0007
Card(RCARD)	4D2A8FC60920
Card crypto	E613CA333E94ED82
### EXTERNAL AUTHENTICATE:
->8482010010149081A7733AE395D4BEEF7C94E43511
<-9000



### INITIALIZE UPDATE:
->80500000083780A71F74AA5F3A
<-000083228368520720410102000821865A62D7CF44E2BE9634C04D799000
KEYDATA		00008322836852072041
VersionKMC	01
Id SecChPrt	02
SeqCounter	0008
Card(RCARD)	21865A62D7CF
Card crypto	44E2BE9634C04D79
### EXTERNAL AUTHENTICATE:
->8482010010D94CC71EE0380E42D2BF1A1D7FA2B591
<-9000

		*/



/*
Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive TRACK_2_EQUIVALENT_DATA (57) - binary 6056630000000301D15012011583200000000F
	Primitive CARDHOLDER_NAME (5F20) - value LOTE 1 TESTE CARD
	Primitive APPLICATION_CURRENCY_CODE (9F42) - value 986
	Primitive APPLICATION_CURRENCY_EXPONENT (9F44) - value 2
	Primitive STATIC_DATA_AUTHENTICATION_TAG_LIST (9F4A) - binary 82
	Primitive SERVICE_CODE (5F30) - value 201
	Primitive TRACK_1_DISCRETIONARY_DATA (9F1F) - value 00000200409
	Primitive APPLICATION_VERSION_NUMBER_ICC (9F08) - binary 0001
	Primitive CARD_RISK_MANAGEMENT_DATA_OBJECT_LIST_1 (8C) - binary 9F02069F03069F1A0295055F2A029A039C019F3704
	Primitive CARD_RISK_MANAGEMENT_DATA_OBJECT_LIST_2 (8D) - binary 8A029F02069F03069F1A0295055F2A029A039C019F3704910A
	Primitive ISSUER_PUBLIC_KEY_EXPONENT (9F32) - binary 010001
	Primitive CERTIFICATION_AUTHORITY_PUBLIC_KEY_INDEX_ICC (8F) - binary F0
Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive ISSUER_PUBLIC_KEY_CERTIFICATE (90) - binary

219F603A87B6C856D8443AA1B3DB43CC15673532FC9C4773986D902C87C7B16367892990BE9DF461811CC443C37BD78D48F708E1598AD274B56B80634D1046311940E70891D6E49F9CFBBED416490F167D322F8D33613552CB861FD6CC4D66BB88D58AC3E454203F2E1DD5A4B72B0C832529B08F7302E5B3F7669AA0B183DE8FED88698BD8ECAAAEEFB170F923BF68362FF212FDD136CE896FE11E5F3C4321E187332463B819B05D72FFBE8B095242153A27F3E97555B5FE290820424A083F05

	Primitive ISSUER_PUBLIC_KEY_REMAINDER (92) - binary

E73EC41F

Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive SIGNED_STATIC_APPLICATION_DATA (93) - binary 26123FD1C2A2A31D1F544FDF0EA9F53AE054E63345E66530203CAED38BE83146A1192754BBF25E36129AFEF1489223A5426BC993D4C68A9BB64F3636D360820B134E5D3F42EB6FD485A3B33211A0BAC17E473478AB9DF0535CBC82007D8AF362DF08E1A0753CA8C81647B9814CFED55E70DA01E9E4E6A48278CBF2AA7A1563F832DF9AD9BB403C491FCE8F178CE417D35B1D403A578019429F2FCC32EEF0F7EF
Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive APPLICATION_EFFECTIVE_DATE (5F25) - value Mon Oct 01 00:00:00 BRT 2012
	Primitive APPLICATION_EXPIRATION_DATE (5F24) - value Sat Jan 31 00:00:00 BRST 2015
	Primitive APPLICATION_PRIMARY_ACCOUNT_NUMBER (5A) - value 6056630000000301
	Primitive APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER (5F34) - value 0
	Primitive APPLICATION_USAGE_CONTROL (9F07) - binary FFC0
	Primitive CARDHOLDER_VERIFICATION_METHOD_LIST (8E) - binary 00000000000000000200
	Primitive ISSUER_ACTION_CODE_DEFAULT (9F0D) - binary FCF8FCF8F0
	Primitive ISSUER_ACTION_CODE_DENIAL (9F0E) - binary 0000000000
	Primitive ISSUER_ACTION_CODE_ONLINE (9F0F) - binary FCF8FCF8F0
	Primitive ISSUER_COUNTRY_CODE (5F28) - value 76
*/
		String caPublicMod = (
				"A1 18 B3 4F 9E 91 0D A9 2A BF DB E6 CD 32 0D 87 68 92 63 F7 8C C3 40" +
				"D1 FF D3 B8 68 E6 56 9F D9 64 52 E8 39 1E D2 2B 53 36 63 FF 72 08 C3" +
				"AB 94 E9 75 14 FF 0E BD 33 9C 5F A4 57 4B 46 0D 22 DA D2 A6 C5 0B F8" +
				"32 EB FD C1 9A C3 38 C3 8E E8 7A C7 17 95 65 04 94 54 1D 77 DA 11 E4" +
				"1A 9E 0A 85 AF 5E 66 01 87 2B 99 1F 9F DB 19 CD C1 A2 DF 4E 26 E6 7F" +
				"08 58 20 75 43 E0 B4 EA 7D 79 9A 09 FB EA 03 70 DE 47 A6 E4 24 7A 84" +
				"16 F4 CB C4 A3 5A C6 D2 4A BC AB D8 53 A4 79 69 02 48 5F D9 2A 4A 37" +
				"E8 83 07 6E BB 69 A1 07 68 8F 24 A4 3D E8 4E 55 5A BB B7 62 04 65 45" +
				"6F 58 6B D3 7E 7A B6 A5"
				).replaceAll("\\s*", "");

		String cardData = ""
				// afl0301 = AFLs marcados para auth, sem tag 70 e length quando SFI <= 10
				+ "5F25031210015F24031501315A0860566300000003015F3401009F0702FFC08E0A000000000000000002009F0D05FCF8FCF8F09F0E0500000000009F0F05FCF8FCF8F05F28020076"
				// + tags dentro do STATIC_DATA_AUTHENTICATION_TAG_LIST = AIP 82 (so pode ser o AIP, inclusive)
				+ "5800";
		sda(caPublicMod, "03",
				// ISSUER_PUBLIC_KEY_CERTIFICATE 90
				"219F603A87B6C856D8443AA1B3DB43CC15673532FC9C4773986D902C87C7B16367892990BE9DF461811CC443C37BD78D48F708E1598AD274B56B80634D1046311940E70891D6E49F9CFBBED416490F167D322F8D33613552CB861FD6CC4D66BB88D58AC3E454203F2E1DD5A4B72B0C832529B08F7302E5B3F7669AA0B183DE8FED88698BD8ECAAAEEFB170F923BF68362FF212FDD136CE896FE11E5F3C4321E187332463B819B05D72FFBE8B095242153A27F3E97555B5FE290820424A083F05",
				// ISSUER_PUBLIC_KEY_REMAINDER 92
				"E73EC41F",
				// ISSUER_PUBLIC_KEY_EXPONENT 9F32
				"010001",
				// SIGNED_STATIC_APPLICATION_DATA 93
				"26123FD1C2A2A31D1F544FDF0EA9F53AE054E63345E66530203CAED38BE83146A1192754BBF25E36129AFEF1489223A5426BC993D4C68A9BB64F3636D360820B134E5D3F42EB6FD485A3B33211A0BAC17E473478AB9DF0535CBC82007D8AF362DF08E1A0753CA8C81647B9814CFED55E70DA01E9E4E6A48278CBF2AA7A1563F832DF9AD9BB403C491FCE8F178CE417D35B1D403A578019429F2FCC32EEF0F7EF",
				// card data to hash
				cardData
		);

/*
Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive ISSUER_COUNTRY_CODE (5F28) - value 76
	Primitive CARD_RISK_MANAGEMENT_DATA_OBJECT_LIST_1 (8C) - binary 9F02069F03069F1A0295055F2A029A039C019F3704
	Primitive CARD_RISK_MANAGEMENT_DATA_OBJECT_LIST_2 (8D) - binary 91088A029F37049505
	Primitive CARDHOLDER_VERIFICATION_METHOD_LIST (8E) - binary 000000000000000042030000
Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive APPLICATION_EFFECTIVE_DATE (5F25) - value Thu Mar 05 00:00:00 BRT 2015
	Primitive APPLICATION_EXPIRATION_DATE (5F24) - value Sat Feb 29 00:00:00 BRT 2020
	Primitive APPLICATION_PRIMARY_ACCOUNT_NUMBER (5A) - value 6056640350222538
	Primitive APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER (5F34) - value 0
	Primitive APPLICATION_USAGE_CONTROL (9F07) - binary AB80
	Primitive ISSUER_ACTION_CODE_DEFAULT (9F0D) - binary F87088E800
	Primitive ISSUER_ACTION_CODE_DENIAL (9F0E) - binary 0000000000
	Primitive ISSUER_ACTION_CODE_ONLINE (9F0F) - binary F8F8FCE800
	Primitive STATIC_DATA_AUTHENTICATION_TAG_LIST (9F4A) - binary 82
Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive TRACK_2_EQUIVALENT_DATA (57) - binary 6056640350222538D20026051617501600091F
	Primitive CARDHOLDER_NAME (5F20) - value TESTE REF CHIP DEZ
	Primitive APPLICATION_CURRENCY_CODE (9F42) - value 986
	Primitive SERVICE_CODE (5F30) - value 605
	Primitive APPLICATION_VERSION_NUMBER_ICC (9F08) - binary 0003
Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive ISSUER_PUBLIC_KEY_CERTIFICATE (90) - binary

7AD8CF7543DD806E492A2C76732A671397C3F5EC015E4441D2FA8D3992F2378D80EE284F29F3FED5A54534139D5D0A03AC4B22546A42191C21FC14DE0823E99CD4E4BBEB9546113340769741A6C7DC095DFE9FD012A6FC5E8D6BDF0F9116027B5372F0EBE15F403EA33D6E0F5D517DB80472E4F1B1B9ED268525C26E3017D47CB5233402E394F4D460A01CF355C72708C8A4CEC6E09023806B243057C42D8C1C97816D1C18015D93102C4709CD02351C

	Primitive ISSUER_PUBLIC_KEY_REMAINDER (92) - binary

908C387E14ACDF0F9F1DE436A5A07308812D6AE3A16170EDF2522B36FBE94358F50C0B69

Constructed READ_RECORD_RESPONSE_MESSAGE_TEMPLATE (70)
	Primitive CERTIFICATION_AUTHORITY_PUBLIC_KEY_INDEX_ICC (8F) - binary F1
	Primitive ISSUER_PUBLIC_KEY_EXPONENT (9F32) - binary 010001
	Primitive SIGNED_STATIC_APPLICATION_DATA (93) - binary 05E459C6657D120FD68F095C8427810AC28F2DC80178925AC6F999EC22E4E6040B1C1E4D779B274E465A04B636389D807928A2258438F9BCB21377A7CA4DC79E829D76F3174D9C44DA801A81D1AD5409FA303A37E0579C005085BE864FA9F38A46DF3A194FF6CE00A5EBA113A96563A0665FD52C55F8A69B16D3BFF577C82B57566FC2FD8C01635CF8FF600B4BAB9D73B84AD7FED171FB3B5095A4850B772059C30F18792094C49D6D69EE1D15DDFC06


AFLs = [AFL [
sfi = 1
firstRecord = 1
lastRecord = 4
countOfflineAuth = 2
], AFL [
sfi = 2
firstRecord = 1
lastRecord = 1
countOfflineAuth = 0
]]
]
HOST - 00B2010C
ICC  - 613B
HOST - 00C000003B
ICC  - 70395F280200768C159F02069F03069F1A0295055F2A029A039C019F37048D0991088A029F370495058E0C000000000000000042030000000000009000
HOST - 00B2020C
ICC  - 613D
HOST - 00C000003D
ICC  - 703B5F25031503055F24032002295A0860566403502225385F3401009F0702AB809F0D05F87088E8009F0E0500000000009F0F05F8F8FCE8009F4A01829000
HOST - 00B2030C
ICC  - 6143
HOST - 00C0000043
ICC  - 704157136056640350222538D20026051617501600091F5F201254455354452052454620434849502044455A00000000000000009F420209865F300206059F080200039000
HOST - 00B2040C
ICC  - 61DC
HOST - 00C00000DC
ICC  - 7081D99081B07AD8CF7543DD806E492A2C76732A671397C3F5EC015E4441D2FA8D3992F2378D80EE284F29F3FED5A54534139D5D0A03AC4B22546A42191C21FC14DE0823E99CD4E4BBEB9546113340769741A6C7DC095DFE9FD012A6FC5E8D6BDF0F9116027B5372F0EBE15F403EA33D6E0F5D517DB80472E4F1B1B9ED268525C26E3017D47CB5233402E394F4D460A01CF355C72708C8A4CEC6E09023806B243057C42D8C1C97816D1C18015D93102C4709CD02351C9224908C387E14ACDF0F9F1DE436A5A07308812D6AE3A16170EDF2522B36FBE94358F50C0B699000
HOST - 00B20114
ICC  - 61BF
HOST - 00C00000BF
ICC  - 7081BC8F01F19F32030100019381B005E459C6657D120FD68F095C8427810AC28F2DC80178925AC6F999EC22E4E6040B1C1E4D779B274E465A04B636389D807928A2258438F9BCB21377A7CA4DC79E829D76F3174D9C44DA801A81D1AD5409FA303A37E0579C005085BE864FA9F38A46DF3A194FF6CE00A5EBA113A96563A0665FD52C55F8A69B16D3BFF577C82B57566FC2FD8C01635CF8FF600B4BAB9D73B84AD7FED171FB3B5095A4850B772059C30F18792094C49D6D69EE1D15DDFC069000
		*/

		caPublicMod = (""
				+ "D2 AF C4 05 C1 0B 23 1B 1D 78 90 94 FB 63 46 58 5F 24 60 6C 41 C6 83"
				+ "A1 F4 BB 1E F5 D1 79 0A CD C1 11 44 BA B2 B9 C0 57 0C 12 AE EA 51 87"
				+ "85 B7 7E E2 7F 79 92 E8 06 0E DD 23 86 51 32 EE 1B AC 16 36 A7 B9 EC"
				+ "0E 2A 2E D2 09 A1 07 46 AC 67 BC 46 4A A6 01 5B 7E D3 10 FD B9 AD FE"
				+ "11 A0 63 18 EA 89 B7 6E F2 63 EF 6F 26 3A 20 BC F3 BC A8 7E A4 C2 8E"
				+ "E9 BB 18 D7 71 13 90 03 47 3A CA BA E3 40 29 18 32 30 C3 35 83 03 91"
				+ "C7 23 FC 2F F8 AB 53 BE A5 42 3F 6F 8E EC E0 90 5B C4 16 FD F8 3A 8D"
				+ "38 19 BF 21 7C D0 5E AE 12 A0 DC C8 69 27 1D"
				).replaceAll("\\s*", "");
		cardData = ""
				// afl0101 = AFLs marcados para auth, sem tag 70 e length quando SFI <= 10
				+ "5F280200768C159F02069F03069F1A0295055F2A029A039C019F37048D0991088A029F370495058E0C00000000000000004203000000000000"
				// afl0102 = AFLs marcados para auth, sem tag 70 e length quando SFI <= 10
				+ "5F25031503055F24032002295A0860566403502225385F3401009F0702AB809F0D05F87088E8009F0E0500000000009F0F05F8F8FCE8009F4A0182"
				// + tags dentro do STATIC_DATA_AUTHENTICATION_TAG_LIST = AIP 82 (so pode ser o AIP, inclusive)
				+ "5800";
		sda(caPublicMod, "010001",
				// ISSUER_PUBLIC_KEY_CERTIFICATE 90
				"7AD8CF7543DD806E492A2C76732A671397C3F5EC015E4441D2FA8D3992F2378D80EE284F29F3FED5A54534139D5D0A03AC4B22546A42191C21FC14DE0823E99CD4E4BBEB9546113340769741A6C7DC095DFE9FD012A6FC5E8D6BDF0F9116027B5372F0EBE15F403EA33D6E0F5D517DB80472E4F1B1B9ED268525C26E3017D47CB5233402E394F4D460A01CF355C72708C8A4CEC6E09023806B243057C42D8C1C97816D1C18015D93102C4709CD02351C",
				// ISSUER_PUBLIC_KEY_REMAINDER 92
				"908C387E14ACDF0F9F1DE436A5A07308812D6AE3A16170EDF2522B36FBE94358F50C0B69",
				// ISSUER_PUBLIC_KEY_EXPONENT 9F32
				"010001",
				// SIGNED_STATIC_APPLICATION_DATA 93
				"05E459C6657D120FD68F095C8427810AC28F2DC80178925AC6F999EC22E4E6040B1C1E4D779B274E465A04B636389D807928A2258438F9BCB21377A7CA4DC79E829D76F3174D9C44DA801A81D1AD5409FA303A37E0579C005085BE864FA9F38A46DF3A194FF6CE00A5EBA113A96563A0665FD52C55F8A69B16D3BFF577C82B57566FC2FD8C01635CF8FF600B4BAB9D73B84AD7FED171FB3B5095A4850B772059C30F18792094C49D6D69EE1D15DDFC06",
				// card data to hash
				cardData
		);
		/*
		caPublic = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(new BigInteger(1, HexadecimalStringUtil.bytesFromString(caPublicMod)), new BigInteger("010001", 16)));
		System.out.println("CA public " + caPublic.toString());
		rsa.init(Cipher.DECRYPT_MODE, caPublic);
		issuerCert = rsa.doFinal(HexadecimalStringUtil.bytesFromString(
				""
				));
		System.out.println(HexadecimalStringUtil.bytesToString(issuerCert));
		// 6A02605664FF12240000010101B003D9E36579B94A5FF3150B64643D85C06E6E9F0682BE56CDD69FCB053913495BDBC327DA3CAC0EA2A0DA1D55DF7C66A0C6F6A9039FA72753C434F4A63BED54062799DF1F6D6E1F315A8F4109721126E11F4FF562C18A4AE6A4D9F0C2A5C2A8E44D6A98628C7E25290584F0F3D9ECE6566FDB7688596649BEC89A1CBC8BBED075538300D0D83FF8755E1CE73668357736313687B22464E1A0F5E54F072A0045A73BBC
		// remainder 908C387E14ACDF0F9F1DE436A5A07308812D6AE3A16170EDF2522B36FBE94358F50C0B69
				*/

// 6056630000000186
//7081A057136056630000000186D15011011865500000000F5F201A4C4F5445203120544553544520434152442020202020202020209F420209869F4401029F4A01825F300201019F1F13313836353530303030303030303030303030309F080200018C159F02069F03069F1A0295055F2A029A039C019F37048D198A029F02069F03069F1A0295055F2A029A039C019F370491089F32030100019204E73EC41F8F01F0
//7081C39081C0219F603A87B6C856D8443AA1B3DB43CC15673532FC9C4773986D902C87C7B16367892990BE9DF461811CC443C37BD78D48F708E1598AD274B56B80634D1046311940E70891D6E49F9CFBBED416490F167D322F8D33613552CB861FD6CC4D66BB88D58AC3E454203F2E1DD5A4B72B0C832529B08F7302E5B3F7669AA0B183DE8FED88698BD8ECAAAEEFB170F923BF68362FF212FDD136CE896FE11E5F3C4321E187332463B819B05D72FFBE8B095242153A27F3E97555B5FE290820424A083F05
//7081A39381A05EF6624BE73D6CB45892633D804B56F40276D22D32F6C0985CF38F5A8BCDB7405FCC4B5272BD222F54C35F0B31CE86EFF5088606D30C9440950A7F21DE3CC4B254716117D7393E765D7702644015517DDB0E76624EA2E62233B6E81888BE5A911A654C28971FAE58D66D41555B625E5258DC1A3D37E643A8B762450221F90D99ED2AC21B4EB4E99408A8EB5EF636F8EE4DFF19471CA423893EBD15CF62BEC414
//70485F25031210015F24031501315A0860566300000001865F3401009F0702FFC08E0A000000000000000002009F0D05FCF8FCF8F09F0E0500000000009F0F05FCF8FCF8F05F28020076

		System.out.println("Cartao 6056630000000186");
		cardData = ""
				// afl0301 = AFLs marcados para auth, sem tag 70 e length quando SFI <= 10
//				+ "5F25031210015F24031501315A0860566300000001865F3401009F0702FFC08E0A000000000000000002009F0D05FCF8FCF8F09F0E0500000000009F0F05FCF8FCF8F05F28020076"
				+ "5F25031210015F24032501315A0860566300000001865F3401009F0702FFC08E0A000000000000000002009F0D05FCF8FCF8F09F0E0500000000009F0F05FCF8FCF8F05F28020076"
				// + tags dentro do STATIC_DATA_AUTHENTICATION_TAG_LIST = AIP 82 (so pode ser o AIP, inclusive)
				+ "5800";
		caPublicMod = (
				"A1 18 B3 4F 9E 91 0D A9 2A BF DB E6 CD 32 0D 87 68 92 63 F7 8C C3 40" +
				"D1 FF D3 B8 68 E6 56 9F D9 64 52 E8 39 1E D2 2B 53 36 63 FF 72 08 C3" +
				"AB 94 E9 75 14 FF 0E BD 33 9C 5F A4 57 4B 46 0D 22 DA D2 A6 C5 0B F8" +
				"32 EB FD C1 9A C3 38 C3 8E E8 7A C7 17 95 65 04 94 54 1D 77 DA 11 E4" +
				"1A 9E 0A 85 AF 5E 66 01 87 2B 99 1F 9F DB 19 CD C1 A2 DF 4E 26 E6 7F" +
				"08 58 20 75 43 E0 B4 EA 7D 79 9A 09 FB EA 03 70 DE 47 A6 E4 24 7A 84" +
				"16 F4 CB C4 A3 5A C6 D2 4A BC AB D8 53 A4 79 69 02 48 5F D9 2A 4A 37" +
				"E8 83 07 6E BB 69 A1 07 68 8F 24 A4 3D E8 4E 55 5A BB B7 62 04 65 45" +
				"6F 58 6B D3 7E 7A B6 A5"
				).replaceAll("\\s*", "");
		sda(caPublicMod, "03",
				// ISSUER_PUBLIC_KEY_CERTIFICATE 90
				"219F603A87B6C856D8443AA1B3DB43CC15673532FC9C4773986D902C87C7B16367892990BE9DF461811CC443C37BD78D48F708E1598AD274B56B80634D1046311940E70891D6E49F9CFBBED416490F167D322F8D33613552CB861FD6CC4D66BB88D58AC3E454203F2E1DD5A4B72B0C832529B08F7302E5B3F7669AA0B183DE8FED88698BD8ECAAAEEFB170F923BF68362FF212FDD136CE896FE11E5F3C4321E187332463B819B05D72FFBE8B095242153A27F3E97555B5FE290820424A083F05",
				// ISSUER_PUBLIC_KEY_REMAINDER 92
				"E73EC41F",
				// ISSUER_PUBLIC_KEY_EXPONENT 9F32
				"010001",
				// SIGNED_STATIC_APPLICATION_DATA 93
				"5EF6624BE73D6CB45892633D804B56F40276D22D32F6C0985CF38F5A8BCDB7405FCC4B5272BD222F54C35F0B31CE86EFF5088606D30C9440950A7F21DE3CC4B254716117D7393E765D7702644015517DDB0E76624EA2E62233B6E81888BE5A911A654C28971FAE58D66D41555B625E5258DC1A3D37E643A8B762450221F90D99ED2AC21B4EB4E99408A8EB5EF636F8EE4DFF19471CA423893EBD15CF62BEC414",
//				"007B95444B3EE483EDAC83738D0C15363F6046AA976E4AC1E7E19BF31C8113A6E099C1E6841C54ECD50D5CE2E49516C378914ABA8B12C22A9A9C7766A570F6BECA4BB8E174778C88AA535D017F75D9623686589EB21A1F22885CFDFDA01F1E2A92651D8C57E30475859BD1ECA547EDF8C9F4A230834EEB1ADA815EBD56AA34ECA44FF842B024768158EA67DD9FB548827388957EBCD47C60AE890DEF88993157",
				// card data to hash
				cardData
		);
		//58B0B09158DA95497712911EDB61719070AA379F

	}

	private static void personalization(String seqCounter, byte[] kenc, byte[] kmac, byte[] kdec, String rterm, String rcard, String cardcrypto, String aid, String hostcrypto, String cmd, String cmdmac, String previousmac) throws Exception {
		System.out.println("+++++++++++++++++++++++ seq counter " + seqCounter + " ++++++++++++++++++++++++++++++++++++++");
		byte[] skuenc = encrypt3DESCBC(HexadecimalStringUtil.bytesFromString(
				"0182"
						+ seqCounter // seq counter
						+ "000000000000000000000000"), kenc);
		byte[] skumac = encrypt3DESCBC(HexadecimalStringUtil.bytesFromString(
				"0101"
						+ seqCounter // seq counter
						+ "000000000000000000000000"), kmac);
		byte[] skudec = encrypt3DESCBC(HexadecimalStringUtil.bytesFromString(
				"0181"
						+ seqCounter // seq counter
						+ "000000000000000000000000"), kdec);

		byte[] cardCryptoData = HexadecimalStringUtil.bytesFromString(""
				+ rterm // Rterm
				+ seqCounter // seq counter
				+ rcard // Rcard
		);
		cardCryptoData = pad2(cardCryptoData);
		byte[] bcardCrypto = generateMAC3DESAlg1(cardCryptoData, skuenc);
		System.out.println("Expected " + cardcrypto + ", found " + HexadecimalStringUtil.bytesToString(bcardCrypto));

		byte[] aidpadded = pad2(HexadecimalStringUtil.bytesFromString(aid));
		byte[] brcard = generateMACAlg3(aidpadded, skumac);
		System.out.println("Expected " + rcard + ", found " + HexadecimalStringUtil.bytesToString(brcard));

		byte[] hostCryptoData = HexadecimalStringUtil.bytesFromString(""
				+ seqCounter // seq counter
				+ rcard // Rcard
				+ rterm // Rterm
		);
		hostCryptoData = pad2(hostCryptoData);
		byte[] bhostCrypto = generateMAC3DESAlg1(hostCryptoData, skuenc);
		System.out.println("Expected " + hostcrypto + ", found " + HexadecimalStringUtil.bytesToString(bhostCrypto));


		byte[] icv = !"0000000000000000".equals(previousmac) ? HexadecimalStringUtil.bytesFromString(previousmac) : null;

		System.out.println("COMMAND " + cmd);

		byte[] extAuth = HexadecimalStringUtil.bytesFromString(cmd);


		CommandAPDU command = new CommandAPDU(extAuth[0], extAuth[1], extAuth[2], extAuth[3], extAuth.length > 5 ? Arrays.copyOfRange(extAuth, 5, extAuth.length) : null, 9, icv, skumac);
		System.out.println("CMDAPDU " + command);

		extAuth = pad2(extAuth);
		// encripta icv
		byte[] mac = generateMACAlg3(extAuth, skumac, icv != null ? encryptDES(icv, skumac, 0) : null);
		System.out.println("Expected " + cmdmac + ", found " + HexadecimalStringUtil.bytesToString(mac));

		byte[] keys = decrypt3DES(HexadecimalStringUtil.bytesFromString("270E413856E063C3D53F7CCF6A7AE3E549AB163DCD957FF72B03F47E704ECE55EC138E3F8408279BF6EAD6394A30B284"), skudec);
		System.out.println("KEYS " + HexadecimalStringUtil.bytesToString(keys));
	}

	private static void sda(String caPublicMod, String caExpoent, String issuerCertEnc, String remainder92, String issuerExpoent, String signedStaticDataEnc, String cardDataToHash) throws Exception {
		System.out.println("+++++++++++++++++++++++++++ SDA ++++++++++++++++++++++++");
		PublicKey caPublic = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(new BigInteger(1, HexadecimalStringUtil.bytesFromString(caPublicMod)), new BigInteger(caExpoent, 16)));
		System.out.println("CA public " + caPublic.toString());
		Cipher rsa = Cipher.getInstance("RSA/ECB/NoPadding");
		rsa.init(Cipher.DECRYPT_MODE, caPublic);
		byte[] issuerCert = rsa.doFinal(HexadecimalStringUtil.bytesFromString(issuerCertEnc));
		System.out.println("ISSUER CERT " + HexadecimalStringUtil.bytesToString(issuerCert));
		if (
				issuerCert[issuerCert.length - 1] != (byte) 0xBC
				|| issuerCert[0] != 0x6A
				|| issuerCert[1] != 2
				) {
			throw new Exception("Invalid issuer certificate");
		}
		// verify pan
		String pan = HexadecimalStringUtil.bytesToString(Arrays.copyOfRange(issuerCert, 2, 6)).replaceAll("F*$", "");
		System.out.println("PAN " + pan);
		// hash
		// Concatenate from left to right the second to the tenth data elements in Table 6
		// (that is, Certificate Format through Issuer Public Key or Leftmost Digits of the Issuer Public Key),
		// followed by the Issuer Public Key Remainder (if present),
		// and finally the Issuer Public Key Exponent
		if (issuerCert[11] != 1) {
			throw new Exception("Unknown hash algorithm");
		}
		byte[] remainder = HexadecimalStringUtil.bytesFromString(remainder92);
		byte[] expoent = HexadecimalStringUtil.bytesFromString(issuerExpoent);
		byte[] dataToHash = new byte[issuerCert.length - 22 + remainder.length + expoent.length];
		System.arraycopy(issuerCert, 1, dataToHash, 0, issuerCert.length - 22);
		System.arraycopy(remainder, 0, dataToHash, issuerCert.length - 22, remainder.length);
		System.arraycopy(expoent, 0, dataToHash, issuerCert.length - 22 + remainder.length, expoent.length);
		System.out.println("Data to hash " + HexadecimalStringUtil.bytesToString(dataToHash));
		MessageDigest sha1 = MessageDigest.getInstance("SHA-1");
		byte[] hash = sha1.digest(dataToHash);
		System.out.println("HASH " + HexadecimalStringUtil.bytesToString(hash));
		if (!Arrays.equals(hash, Arrays.copyOfRange(issuerCert, issuerCert.length - 21, issuerCert.length - 1))) {
			throw new Exception("Hash failed");
		}
		// issuer public key
		if (issuerCert[12] != 1) {
			throw new Exception("Unknown public key algorithm");
		}
		int pkLength = 0x0FF & issuerCert[13];
		System.out.println("Length " + pkLength);
		System.out.println("Issuer " + (issuerCert.length - 36));
		System.out.println("remainder " + (remainder.length));
		if (issuerCert.length - 36 + remainder.length != pkLength) {
			throw new Exception("Invalid Public key data length");
		}
		int expLength = 0x0FF & issuerCert[14];
		if (expoent.length != expLength) {
			throw new Exception("Invalid Expoent length");
		}
		byte[] pkMod = new byte[pkLength];
		System.arraycopy(issuerCert, 15, pkMod, 0, issuerCert.length - 36);
		System.arraycopy(remainder, 0, pkMod, issuerCert.length - 36, remainder.length);
		PublicKey issuerPublic = KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(new BigInteger(1, pkMod), new BigInteger(1, expoent)));
		System.out.println("Issuer PK " + HexadecimalStringUtil.bytesToString(issuerPublic.getEncoded()));
		byte[] signedDataEnc = HexadecimalStringUtil.bytesFromString(signedStaticDataEnc);
		if (signedDataEnc.length != pkLength) {
			throw new Exception("Invalid static data length");
		}
		rsa.init(Cipher.DECRYPT_MODE, issuerPublic);
		byte[] signedData = rsa.doFinal(signedDataEnc);
		System.out.println("Signed data " + HexadecimalStringUtil.bytesToString(signedData));
		if (
				signedData[0] != 0x6A
				|| signedData[1] != 3
				|| signedData[signedData.length - 1] != (byte) 0xBC
				) {
			throw new Exception("Invalid static data");
		}
		if (signedData[2] != 1) {
			throw new Exception("Unknown hash algorithm");
		}
		byte[] cardData = HexadecimalStringUtil.bytesFromString(cardDataToHash);
		dataToHash = new byte[signedData.length - 22 + cardData.length];
		System.arraycopy(signedData, 1, dataToHash, 0, signedData.length - 22);
		// + AFLs marcados para auth, sem tag 70 e length quando SFI <= 10
		System.arraycopy(cardData, 0, dataToHash, signedData.length - 22, cardData.length);
		System.out.println("Data to hash  " + HexadecimalStringUtil.bytesToString(dataToHash));
		sha1.reset();
		hash = sha1.digest(dataToHash);
		System.out.println("HASH " + HexadecimalStringUtil.bytesToString(hash));
		if (!Arrays.equals(hash, Arrays.copyOfRange(signedData, signedData.length - 21, signedData.length - 1))) {
			System.out.flush();
			System.arraycopy(hash, 0, signedData, signedData.length - 21, hash.length);
			System.out.println("New signedData " + HexadecimalStringUtil.bytesToString(signedData));
			rsa.init(Cipher.ENCRYPT_MODE, issuerPublic);
			signedDataEnc = rsa.doFinal(signedData);
			System.out.println("New " + HexadecimalStringUtil.bytesToString(signedDataEnc));
			throw new Exception("Hash failed");
		}
		byte[] dac_9F45 = Arrays.copyOfRange(signedData, 3, 5);
		System.out.println("DAC " + HexadecimalStringUtil.bytesToString(dac_9F45));
	}

	static byte[] encrypt3DES(byte[] data, byte[] key) throws Exception {
		if (key.length == 8 || key.length == 16) {
			byte[] newkey = new byte[24];
			System.arraycopy(key, 0, newkey, 0, 8);
			System.arraycopy(key, key.length == 8 ? 0 : 8, newkey, 8, 8);
			System.arraycopy(key, 0, newkey, 16, 8);
			key = newkey;
		}
		SecretKey skey = SecretKeyFactory.getInstance("DESede").generateSecret(new DESedeKeySpec(key));
		Cipher c = Cipher.getInstance("DESede/ECB/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] encrypt3DESCBC(byte[] data, byte[] key) throws Exception {
		if (key.length == 8 || key.length == 16) {
			byte[] newkey = new byte[24];
			System.arraycopy(key, 0, newkey, 0, 8);
			System.arraycopy(key, key.length == 8 ? 0 : 8, newkey, 8, 8);
			System.arraycopy(key, 0, newkey, 16, 8);
			key = newkey;
		}
		SecretKey skey = SecretKeyFactory.getInstance("DESede").generateSecret(new DESedeKeySpec(key));
		Cipher c = Cipher.getInstance("DESede/CBC/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, skey, new IvParameterSpec(new byte[8]));
		return c.doFinal(data);
	}
/*
	static byte[] encryptDESCBC(byte[] data, byte[] key) throws Exception {
		return encryptDESCBC(data, key, 0);
	}
*/
	static byte[] encryptDESCBC(byte[] data, byte[] key, int offset, byte[] icv) throws Exception {
		SecretKey skey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key, offset));
		Cipher c = Cipher.getInstance("DES/CBC/NoPadding");
		if (icv == null) icv = new byte[8];
		System.out.println("ICV " + HexadecimalStringUtil.bytesToString(icv));
		c.init(Cipher.ENCRYPT_MODE, skey, new IvParameterSpec(icv));
		return c.doFinal(data);
	}

	static byte[] encryptDES(byte[] data, byte[] key) throws Exception {
		return encryptDES(data, key, 0);
	}

	static byte[] encryptDES(byte[] data, byte[] key, int offset) throws Exception {
		SecretKey skey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key, offset));
		Cipher c = Cipher.getInstance("DES/ECB/NoPadding");
		c.init(Cipher.ENCRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] decryptDES(byte[] data, byte[] key) throws Exception {
		return decryptDES(data, key, 0);
	}

	static byte[] decryptDES(byte[] data, byte[] key, int offset) throws Exception {
		SecretKey skey = SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(key, offset));
		Cipher c = Cipher.getInstance("DES/ECB/NoPadding");
		c.init(Cipher.DECRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] decrypt3DES(byte[] data, byte[] key) throws Exception {
		if (key.length == 8 || key.length == 16) {
			byte[] newkey = new byte[24];
			System.arraycopy(key, 0, newkey, 0, 8);
			System.arraycopy(key, key.length == 8 ? 0 : 8, newkey, 8, 8);
			System.arraycopy(key, 0, newkey, 16, 8);
			key = newkey;
		}
		SecretKey skey = SecretKeyFactory.getInstance("DESede").generateSecret(new DESedeKeySpec(key));
		Cipher c = Cipher.getInstance("DESede/ECB/NoPadding");
		c.init(Cipher.DECRYPT_MODE, skey);
		return c.doFinal(data);
	}

	static byte[] pad2(byte[] data) {
		// pad3 - 0x80 + 0x00 ...
		byte[] newdata = new byte[(data.length / 8 + 1) * 8];
		System.arraycopy(data, 0, newdata, 0, data.length);
		newdata[data.length] = (byte) 0x80;
		// remaining 00 padding - do nothing
		return newdata;
	}

	static byte[] generateMACAlg3(byte[] data, byte[] key) throws Exception {
		return generateMACAlg3(data, key, new byte[8]);
	}

	static byte[] generateMACAlg3(byte[] data, byte[] key, byte[] icv) throws Exception {
		byte[] enc = encryptDESCBC(data, key, 0, icv);
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		// output transformation 3
		// Hq (last block encrypted) is decrypted with the key K? and the result encrypted with the key K:
		// G = eK(dK?(Hq))
		// hq1
		enc = decryptDES(enc, key, 8);
		// g
		enc = encryptDES(enc, key);
		return enc;
	}

	static byte[] generateMAC3DESAlg1(byte[] data, byte[] key) throws Exception {
		byte[] enc = encrypt3DESCBC(data, key);
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		// output transformation 1 - no change
		// G = Hq
		// -- nao faz nada
		return enc;
	}

}
