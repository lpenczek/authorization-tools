package com.penczek.emv.model;

import static com.penczek.tlv.EMVDataElement.AMOUNT_AUTHORISED_NUMERIC;
import static com.penczek.tlv.EMVDataElement.AMOUNT_OTHER_NUMERIC;
import static com.penczek.tlv.EMVDataElement.APPLICATION_CRYPTOGRAM;
import static com.penczek.tlv.EMVDataElement.APPLICATION_INTERCHANGE_PROFILE;
import static com.penczek.tlv.EMVDataElement.APPLICATION_PRIMARY_ACCOUNT_NUMBER;
import static com.penczek.tlv.EMVDataElement.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER;
import static com.penczek.tlv.EMVDataElement.APPLICATION_TRANSACTION_COUNTER;
import static com.penczek.tlv.EMVDataElement.CRYPTOGRAM_INFORMATION_DATA;
import static com.penczek.tlv.EMVDataElement.DEDICATED_FILE_NAME;
import static com.penczek.tlv.EMVDataElement.ISSUER_APPLICATION_DATA;
import static com.penczek.tlv.EMVDataElement.TERMINAL_COUNTRY_CODE;
import static com.penczek.tlv.EMVDataElement.TERMINAL_VERIFICATION_RESULTS;
import static com.penczek.tlv.EMVDataElement.TRANSACTION_CURRENCY_CODE;
import static com.penczek.tlv.EMVDataElement.TRANSACTION_DATE;
import static com.penczek.tlv.EMVDataElement.TRANSACTION_TYPE;
import static com.penczek.tlv.EMVDataElement.UNPREDICTABLE_NUMBER;
import static org.junit.Assert.*;

import com.penczek.tlv.EMVDataElement;
import com.penczek.util.HexadecimalStringUtil;
import java.util.Map;
import org.junit.Test;

/**
 *
 * @author Leonardo Penczek
 */
public class DataObjectListTest {

	String tlv1 = "5A08";
	String tlv2 = "5F3401";
	String tlv3 = "8202";
	String tlv4 = "8407";
	String tlv5 = "9F0206";
	String tlv6 = "9F0306";
	String tlv7 = "9F1A02";
	String tlv8 = "9505";
	String tlv9 = "5F2A02";
	String tlv10 = "9A03";
	String tlv11 = "9C01";
	String tlv12 = "9F3704";
	String tlv13 = "9F2701";
	String tlv14 = "9F3602";
	String tlv15 = "9F1020";
	String tlv16 = "9F2608";

	String template1 = "70";
	String template2 = "77";
	String template3 = "A5";
	String template4 = "BF0C";

	@Test
	public void testParse() throws Exception {
		String pak;
		pak = tlv1 + tlv2 + tlv3 + tlv4 + tlv5 + tlv6 + tlv7 + tlv8 + tlv9 + tlv10
						+ tlv11 + tlv12 + tlv13 + tlv14 + tlv15 + tlv16;
		System.out.println("Pak " + pak);
		DataObjectList dol = new DataObjectList(HexadecimalStringUtil.bytesFromString(pak));

		for (DataObjectListEntry el : dol.getEntries()) {
			System.out.println(el);
		}

		assertArrayEquals(new DataObjectListEntry[]{
			new DataObjectListEntry(APPLICATION_PRIMARY_ACCOUNT_NUMBER, 8),
			new DataObjectListEntry(APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER, 1),
			new DataObjectListEntry(APPLICATION_INTERCHANGE_PROFILE, 2),
			new DataObjectListEntry(DEDICATED_FILE_NAME, 7),
			new DataObjectListEntry(AMOUNT_AUTHORISED_NUMERIC, 6),
			new DataObjectListEntry(AMOUNT_OTHER_NUMERIC, 6),
			new DataObjectListEntry(TERMINAL_COUNTRY_CODE, 2),
			new DataObjectListEntry(TERMINAL_VERIFICATION_RESULTS, 5),
			new DataObjectListEntry(TRANSACTION_CURRENCY_CODE, 2),
			new DataObjectListEntry(TRANSACTION_DATE, 3),
			new DataObjectListEntry(TRANSACTION_TYPE, 1),
			new DataObjectListEntry(UNPREDICTABLE_NUMBER, 4),
			new DataObjectListEntry(CRYPTOGRAM_INFORMATION_DATA, 1),
			new DataObjectListEntry(APPLICATION_TRANSACTION_COUNTER, 2),
			new DataObjectListEntry(ISSUER_APPLICATION_DATA, 32),
			new DataObjectListEntry(APPLICATION_CRYPTOGRAM, 8)
		}, dol.getEntries());
	}

	@Test
	public void testgetDolData() {
		DataObjectList dol = new DataObjectList(new DataObjectListEntry(EMVDataElement.TERMINAL_COUNTRY_CODE, 2));
		byte[] dolData = dol.getDolData(Map.of(EMVDataElement.TERMINAL_COUNTRY_CODE, new byte[]{0, 0x76, 0}));
		System.out.println("Data " + HexadecimalStringUtil.bytesToString(dolData));
		assertArrayEquals(new byte[]{0, 0x76}, dolData);
	}

}
