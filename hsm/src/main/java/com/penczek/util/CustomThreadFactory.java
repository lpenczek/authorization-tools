package com.penczek.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class CustomThreadFactory implements ThreadFactory {

	private final AtomicInteger threadNumber = new AtomicInteger();
	private final String name;
	private final boolean daemon;
	private final int priority;
	private final int maxSimultaneousThreads;
	private final ThreadGroup threadGroup;

	public CustomThreadFactory(String name) {
		this(null, name);
	}

	public CustomThreadFactory(ThreadGroup group, String name) {
		this(group, name, false);
	}

	public CustomThreadFactory(String name, boolean daemon) {
		this(null, name, daemon);
	}

	public CustomThreadFactory(ThreadGroup group, String name, boolean daemon) {
		this(group, name, daemon, Thread.NORM_PRIORITY, 8192);
	}

	public CustomThreadFactory(String name, boolean daemon, int priority, int maxSimultaneousThreads) {
		this(null, name, daemon, priority, maxSimultaneousThreads);
	}

	public CustomThreadFactory(ThreadGroup group, String name, boolean daemon, int priority, int maxSimultaneousThreads) {
		this.name = name;
		this.daemon = daemon;
		this.priority = priority;
		this.maxSimultaneousThreads = maxSimultaneousThreads;
		this.threadGroup = group != null ? group : new ThreadGroup(name);
	}

	@Override
	public Thread newThread(Runnable r) {
		int id = this.threadNumber.incrementAndGet();
		if (id > this.maxSimultaneousThreads) {
			id = 1;
			this.threadNumber.set(1);
		}
		Thread t = new Thread(this.threadGroup, r, new StringBuilder(this.name.length() + 16).append(this.name).append('[').append(id).append(']').toString());
		t.setDaemon(this.daemon);
		t.setPriority(this.priority);
		return t;
	}

}
