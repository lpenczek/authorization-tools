package com.penczek.pinpad;

public class EncryptException extends PinpadException {

	private static final long serialVersionUID = 1L;

	public EncryptException() {}

	public EncryptException(String msg) {
		super(msg);
	}

}
