package com.penczek.pinpad;

public class PinpadException extends Exception {

	private static final long serialVersionUID = 1L;

	public PinpadException() {}

	public PinpadException(String msg) {
		super(msg);
	}

}
