package com.penczek.pinpad;

import com.penczek.hsm.Pinblock;
import com.penczek.util.HexadecimalStringUtil;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

public class PinPadHSM {

	private Cipher c;
	private SecretKeyFactory keyFactory;
	private SecretKey tmk;
	private SecretKey tmkU1;
	private SecretKey tmkU2;
	private SecretKey tmkT1;
	private SecretKey tmkT2;
	private SecretKey tmkT3;
	private SecretKey tpk;

	public PinPadHSM(String tmk, String tpktmk) throws InvalidKeyException, PinpadException {
		char scheme = Character.toUpperCase(tpktmk.charAt(0));
		byte[] tpk = null;
		/*
		None/Z - Encryption of a single length DES key using ANSI X9.17 methods. Used for encryption of keys under a variant LMK, and can also be used for the import or export of keys.
		U - Encryption of a double length DES key using the variant method; used for encryption of keys under a variant LMK.
		T - Encryption of a triple length DES key using the variant method; used for encryption of keys under a variant LMK.
		X - Encryption of a double length key using ANSI X9.17 methods; only available for import and export of keys. This mode is enabled within the Configure Security command.
		Y - Encryption of a triple length key using ANSI X9.17 methods; only available for import and export of keys. This mode is enabled within the Configure Security command.
		V - Encryption of keys using Verifone/GISKE methods; used for export of DES keys.
		R - Encryption of keys using ANSI TR-31 KeyBlock methods; only used for import or export of keys. The use of this scheme requires optional licence HSM8-LIC006.
		S - Encryption of DES, RSA & HMAC keys using Thales KeyBlock methods; used for encryption of keys under a KeyBlock LMK.
		*/
		switch (scheme) {
			// tamanho string vezes 2 porque esta como string, cada 2 chars contem um byte
			case 'Z':
				if (tpktmk.length() != 1 + 8 * 2) throw new InvalidKeyException("Invalid single length DES key ANSI X9.17");
				tpk = HexadecimalStringUtil.bytesFromString(tpktmk.substring(1));
				break;
			case 'X':
				if (tpktmk.length() != 1 + 16 * 2) throw new InvalidKeyException("Invalid double length DES key ANSI X9.17");
				tpk = HexadecimalStringUtil.bytesFromString(tpktmk.substring(1));
				break;
			case 'Y':
				if (tpktmk.length() != 1 + 24 * 2) throw new InvalidKeyException("Invalid triple length DES key ANSI X9.17");
				tpk = HexadecimalStringUtil.bytesFromString(tpktmk.substring(1));
				break;
			case 'U':
				if (tpktmk.length() != 1 + 16 * 2) throw new InvalidKeyException("Invalid double length DES key variant");
				tpk = HexadecimalStringUtil.bytesFromString(tpktmk.substring(1));
				break;
			case 'T':
				if (tpktmk.length() != 1 + 24 * 2) throw new InvalidKeyException("Invalid triple length DES key variant");
				tpk = HexadecimalStringUtil.bytesFromString(tpktmk.substring(1));
				break;
			// sem scheme, deve ser um digito hexadecimal
			case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
			case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
				// vezes 2 porque esta como string, cada 2 chars contem um byte
				if (tpktmk.length() == 8 * 2) {
					scheme = 'Z';
				} else if (tpktmk.length() == 16 * 2) {
					scheme = 'X';
				} else if (tpktmk.length() == 24 * 2) {
					scheme = 'Y';
				} else {
					throw new InvalidKeyException("Invalid DES key ANSI X9.17 length: " + tpktmk.length());
				}
				tpk = HexadecimalStringUtil.bytesFromString(tpktmk);
				break;
			case 'V':
			case 'R':
			case 'S':
			default:
				throw new InvalidKeyException("Method not supported");
		}
		this.init(HexadecimalStringUtil.bytesFromString(tmk), scheme, tpk);
	}

	public PinPadHSM(byte[] tmk, char tpkScheme, byte[] tpktmk) throws InvalidKeyException, PinpadException {
		this.init(tmk, tpkScheme, tpktmk);
	}

	private void init(byte[] tmk, char tpkScheme, byte[] tpktmk) throws InvalidKeyException, PinpadException {
		try {
			this.c = Cipher.getInstance("DESede/ECB/NoPadding");
			this.keyFactory = SecretKeyFactory.getInstance("DESede");
		} catch (Exception e) {
			throw new PinpadException("Cannot create the pinpad");
		}
		this.setTMK(tmk);
		this.setTPKtmk(tpkScheme, tpktmk);
	}

	public void setTMK(byte[] tmk) throws InvalidKeyException {
		try {
			byte[] tmk24 = this.prepareDESedeKey(tmk);
			this.tmk = this.keyFactory.generateSecret(new DESedeKeySpec(tmk24));
			byte b8 = tmk24[8];
			// generate U variants
			// variant 1 - A6
			tmk24[8] = (byte) (b8 ^ 0xA6); // byte[8] original xor A6
			this.tmkU1 = this.keyFactory.generateSecret(new DESedeKeySpec(tmk24));
			// variant 2 - 5A
			tmk24[8] = (byte) (b8 ^ 0x5A); // byte[8] original xor 5A
			this.tmkU2 = keyFactory.generateSecret(new DESedeKeySpec(tmk24));
			// generate T variants
			// variant 1 - 6A
			tmk24[8] = (byte) (b8 ^ 0x6A); // byte[8] original xor 6A
			this.tmkT1 = this.keyFactory.generateSecret(new DESedeKeySpec(tmk24));
			// variant 2 - DE
			tmk24[8] = (byte) (b8 ^ 0xDE); // byte[8] original xor DE
			this.tmkT2 = this.keyFactory.generateSecret(new DESedeKeySpec(tmk24));
			// variant 3 - 2B
			tmk24[8] = (byte) (b8 ^ 0x2B); // byte[8] original xor 2B
			this.tmkT3 = this.keyFactory.generateSecret(new DESedeKeySpec(tmk24));
		} catch (Exception e) {
			throw new InvalidKeyException(e.getMessage());
		}
	}

	public void setTPKtmk(char scheme, byte[] tpktmk) throws InvalidKeyException {
		if (scheme == 'Z' && tpktmk.length != 8) {
			throw new InvalidKeyException("Invalid single length DES key");
		} else if ((scheme == 'X' || scheme == 'U') && tpktmk.length != 16) {
			throw new InvalidKeyException("Invalid double length DES key");
		} else if ((scheme == 'Y' || scheme == 'T') && tpktmk.length != 24) {
			throw new InvalidKeyException("Invalid triple length DES key");
		}
		if (scheme == 'Z' || scheme == 'X' || scheme == 'Y') {
			// ANSI X9.17 - DESede ECB normal
			try {
				this.c.init(Cipher.DECRYPT_MODE, this.tmk);
				this.tpk = this.keyFactory.generateSecret(new DESedeKeySpec(this.prepareDESedeKey(this.c.doFinal(tpktmk))));
			} catch (Exception e) {
				throw new InvalidKeyException(e.getMessage());
			}
		} else if (scheme == 'U') {
			// double variant
			// TPK sob TMK U AAAA AAAA AAAA AAAA   BBBB BBBB BBBB BBBB
			byte[] tpk1 = Arrays.copyOfRange(tpktmk, 0, 8);
			byte[] tpk2 = Arrays.copyOfRange(tpktmk, 8, 16);
			try {
				c.init(Cipher.DECRYPT_MODE, tmkU1);
				tpk1 = c.doFinal(tpk1);
				c.init(Cipher.DECRYPT_MODE, tmkU2);
				tpk2 = c.doFinal(tpk2);
				byte[] tpkclear = new byte[24];
				System.arraycopy(tpk1, 0, tpkclear, 0, 8);
				System.arraycopy(tpk2, 0, tpkclear, 8, 8);
				System.arraycopy(tpk1, 0, tpkclear, 16, 8);
				this.tpk = this.keyFactory.generateSecret(new DESedeKeySpec(tpkclear));
			} catch (Exception e) {
				throw new InvalidKeyException(e.getMessage());
			}
		} else if (scheme == 'T') {
			// triple variant
			// TPK sob TMK T AAAA AAAA AAAA AAAA   BBBB BBBB BBBB BBBB   CCCC CCCC CCCC CCCC
			byte[] tpk1 = Arrays.copyOfRange(tpktmk, 0, 8);
			byte[] tpk2 = Arrays.copyOfRange(tpktmk, 8, 16);
			byte[] tpk3 = Arrays.copyOfRange(tpktmk, 16, 24);
			try {
				c.init(Cipher.DECRYPT_MODE, tmkT1);
				tpk1 = c.doFinal(tpk1);
				c.init(Cipher.DECRYPT_MODE, tmkT2);
				tpk2 = c.doFinal(tpk2);
				c.init(Cipher.DECRYPT_MODE, tmkT3);
				tpk3 = c.doFinal(tpk3);
				byte[] tpkclear = new byte[24];
				System.arraycopy(tpk1, 0, tpkclear, 0, 8);
				System.arraycopy(tpk2, 0, tpkclear, 8, 8);
				System.arraycopy(tpk3, 0, tpkclear, 16, 8);
				this.tpk = this.keyFactory.generateSecret(new DESedeKeySpec(tpkclear));
			} catch (Exception e) {
				throw new InvalidKeyException(e.getMessage());
			}
		} else {
			throw new InvalidKeyException("Key scheme no supported: " + scheme);
		}
	}

	private byte[] prepareDESedeKey(byte[] key) {
		if (key.length == 24) return key;
		if (key.length == 8) {
			byte[] n = new byte[24];
			System.arraycopy(key, 0, n, 0, 8);
			System.arraycopy(key, 0, n, 8, 8);
			System.arraycopy(key, 0, n, 16, 8);
			return n;
		}
		if (key.length == 16) {
			byte[] n = new byte[24];
			System.arraycopy(key, 0, n, 0, 16);
			System.arraycopy(key, 0, n, 16, 8);
			return n;
		}
		return null;
	}

	public String encrypt(String pin, String pan) throws EncryptException {
		return this.encrypt(pin, pan, Pinblock.Format.ISO_0);
	}

	public String encrypt(String pin, String pan, Pinblock.Format format) throws EncryptException {
		try {
			// monta pinblock ISO-0
			this.c.init(Cipher.ENCRYPT_MODE, this.tpk);
			return HexadecimalStringUtil.bytesToString(this.c.doFinal(Pinblock.createPinblock(format, pin, pan).getPinblock()));
		} catch (Exception e) {
			throw new EncryptException("Cannot encrypt the pin: " + e.getMessage());
		}
	}

}
