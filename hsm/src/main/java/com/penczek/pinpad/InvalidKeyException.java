package com.penczek.pinpad;

public class InvalidKeyException extends PinpadException {

	private static final long serialVersionUID = 1L;

	public InvalidKeyException() {}

	public InvalidKeyException(String msg) {
		super(msg);
	}

}
