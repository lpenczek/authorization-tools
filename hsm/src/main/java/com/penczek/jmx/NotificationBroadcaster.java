package com.penczek.jmx;

import com.penczek.util.CustomThreadFactory;
import java.lang.management.ManagementFactory;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicLong;
import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.MBeanServer;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;
import javax.management.ObjectName;

public class NotificationBroadcaster extends NotificationBroadcasterSupport {

	private static final ThreadPoolExecutor POOL = (ThreadPoolExecutor) Executors.newCachedThreadPool(
			new CustomThreadFactory("NotifBcaster")
	);

	private final AtomicLong jmxSequence = new AtomicLong();
	private String jmxName;

	public NotificationBroadcaster() {
		super(POOL,
				new MBeanNotificationInfo(new String[] { "ERROR", "WARN", "INFO"}, Notification.class.getName(), "Alertas"),
				new MBeanNotificationInfo(new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE }, AttributeChangeNotification.class.getName(), "Atualiza��o de atributo")
		);
	}

	public String getJmxName() {
		return jmxName;
	}

	public void setJmxName(String jmxName) {
		this.jmxName = jmxName;
	}

	public void create() throws Exception {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ObjectName name = new ObjectName(this.jmxName);
		if (mbs.isRegistered(name)) {
			mbs.unregisterMBean(name);
		}
		mbs.registerMBean(this, name);
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName();
	}

	public void sendErrorNotification(String message) {
		super.sendNotification(new Notification("ERROR", this, this.jmxSequence.getAndIncrement(), System.currentTimeMillis(), message));
	}

	public void sendWarnNotification(String message) {
		super.sendNotification(new Notification("WARN", this, this.jmxSequence.getAndIncrement(), System.currentTimeMillis(), message));
	}

	public void sendInfoNotification(String message) {
		super.sendNotification(new Notification("INFO", this, this.jmxSequence.getAndIncrement(), System.currentTimeMillis(), message));
	}

	public void sendAttributeChangeNotification(String attributeName, String attributeType, Object oldValue, Object newValue) {
		if (this.equal(oldValue, newValue)) return;
		this.sendAttributeChangeNotification("Changed " + attributeName + " from " + oldValue + " to " + newValue, attributeName, attributeType, oldValue, newValue);
	}

	public void sendAttributeChangeNotification(String msg, String attributeName, String attributeType, Object oldValue, Object newValue) {
		if (this.equal(oldValue, newValue)) return;
		super.sendNotification(new AttributeChangeNotification(this, this.jmxSequence.getAndIncrement(), System.currentTimeMillis(), msg, attributeName, attributeType, oldValue, newValue));
	}

	private boolean equal(Object oldValue, Object newValue) {
		if (oldValue == newValue) return true;
		if (oldValue == null) return newValue == null;
		if (newValue == null) return false;
		return oldValue.equals(newValue);
	}

}
