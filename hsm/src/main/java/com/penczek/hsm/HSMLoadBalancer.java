package com.penczek.hsm;

import com.penczek.jmx.NotificationBroadcaster;
import com.penczek.tlv.TLVData;
import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HSMLoadBalancer extends NotificationBroadcaster implements HSM, HSMLoadBalancerMBean {

	private static final Logger LOG = Logger.getLogger(HSMFailOver.class.getName());

	private int waitTime;
	private final ArrayList<HSM> hsms;
	private long[] timestamps; // timestamp do ultimo erro, de cada cliente
	private int total;
	private long totalTPS;
	private int maxWeight; // maximum weight of all the servers in S
	private int gcdWeight; // greatest common divisor of all server weights in S
	private volatile int _cursor; // indicates the server selected last time
	private volatile int currentWeight; // current weight in scheduling

	// locks
	final ReentrantReadWriteLock rwl;
	private final ReentrantReadWriteLock.ReadLock rl;
	private final ReentrantReadWriteLock.WriteLock wl;

	public HSMLoadBalancer() {
		this.hsms = new ArrayList<HSM>(4);
		this.waitTime = 1000;
		this.currentWeight = 0;
		this._cursor = -1;
		this.rwl = new ReentrantReadWriteLock();
		rl = rwl.readLock();
		wl = rwl.writeLock();
	}

	@Override
	public int getWaitTime() {
		return waitTime;
	}

	@Override
	public void setWaitTime(int waitTime) {
		this.waitTime = waitTime;
	}

	@Override
	public List<HSM> getHsms() {
		return Collections.unmodifiableList(this.hsms);
	}

	@Override
	public void setHsms(List<HSM> hsms) {
		wl.lock();
		try {
			this.sendAttributeChangeNotification("hsms", List.class.getName(), this.hsms, hsms);
			this.hsms.clear();
			this.hsms.addAll(hsms);
			this.total = this.hsms.size();
			this.timestamps = new long[this.total];
			this.maxWeight = 0;
			this.gcdWeight = 0;
			this.totalTPS = 0;
			for (HSM hsm : hsms) {
				int tps = hsm.getTps();
				if (tps <= 0) {
					tps = 2000000000; // max int arredondado para divisivel por 100
				}
				this.totalTPS += tps;
				if (this.totalTPS > 2000000000) {
					this.totalTPS = 2000000000;
				}
				if (tps > this.maxWeight) {
					this.maxWeight = tps;
				}
				this.gcdWeight = this.gcd(tps, this.gcdWeight);
			}
			this._cursor = -1;
			this.currentWeight = 0;
			LOG.log(Level.FINE, "Initialized with {0} nodes, maxWeight {1}, gcd {2}", new Object[]{this.total, this.maxWeight, this.gcdWeight});
		} finally {
			wl.unlock();
		}
	}

	private int gcd(int a, int b) {
		if (b == 0) return a;
		if (a > b) return gcd(b, a % b);
		return gcd(a, b % a);
	}

	private int nextCursor() throws ConnectException {
		if (this.hsms.isEmpty()) throw new ConnectException("No HSM clients configured");
		int i = 0, j = 0;
		synchronized (this) {
			// i e j servem para parar o laco caso todos as posicoes tenham sido testadas
			for (; i <= 1 && j <= 1;) {
				this._cursor = (this._cursor + 1) % this.total;
				if (this._cursor == 0) {
					i++;
					this.currentWeight -= this.gcdWeight;
					if (this.currentWeight <= 0) {
						j++;
						this.currentWeight = this.maxWeight;
					}
		        }
				int tps = this.hsms.get(this._cursor).getTps();
				if (tps <= 0) {
					tps = 2000000000;
				}
				if (this.timestamps[this._cursor] == 0 || (this.timestamps[this._cursor] + this.waitTime) < System.currentTimeMillis()) {
					if (this.timestamps[this._cursor] != 0) {
						this.timestamps[this._cursor] = 0;
					}
					if (tps >= this.currentWeight) {
						return this._cursor;
					}
				} else {
					this.currentWeight -= this.gcdWeight;
					if (this.currentWeight < 0) {
						this.currentWeight = 0;
					}
				}
			}
		}
		return 0;
	}

	private void errorOffline() {
		this.sendErrorNotification("All clients offline");
		LOG.severe("All clients offline");
	}

	private void errorOffline(int pos, Exception e) {
		this.sendWarnNotification("Error on HSM #" + pos + ": " + e);
		LOG.log(Level.FINE, "Error on HSM #" + pos, e);
		synchronized (this) {
			this.timestamps[pos] = System.currentTimeMillis();
		}
	}

	@Override
	public String[] generateKey(DataKeyType type, String mkLMK, KeyScheme scheme) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String[] o = this.hsms.get(pos).generateKey(type, mkLMK, scheme);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockZPK, String zpkLMK) throws IOException {
		return this.verifyPinblock(pan, pinblockTPK, Pinblock.Format.ISO_0, tpkLMK, pinblockZPK, Pinblock.Format.ISO_0, zpkLMK);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockZPK, Pinblock.Format formatPZPK, String zpkLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyPinblock(pan, pinblockTPK, formatPTPK, tpkLMK, pinblockZPK, formatPZPK, zpkLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockLMK) throws IOException {
		return this.verifyPinblock(pan, pinblockTPK, Pinblock.Format.ISO_0, tpkLMK, pinblockLMK);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyPinblock(pan, pinblockTPK, formatPTPK, tpkLMK, pinblockLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String translatePinblock(String pan, String sourcePinblockZPK, String sourceZPKlmk, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(pan, sourcePinblockZPK, Pinblock.Format.ISO_0, sourceZPKlmk, Pinblock.Format.ISO_0, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String sourcePan, String sourcePinblockZPK, String sourceZPKlmk, String destinationPan, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(sourcePan, sourcePinblockZPK, Pinblock.Format.ISO_0, sourceZPKlmk, Pinblock.Format.ISO_0, destinationPan, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String pan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(pan, sourcePinblockZPK, formatSourcePZPK, sourceZPKlmk, formatDestinationPZPK, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String sourcePan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).translatePinblock(sourcePan, sourcePinblockZPK, formatSourcePZPK, sourceZPKlmk, formatDestinationPZPK, destinationPan, destinationZPKlmk);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, String sourceTPKlmk, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(pan, sourcePinblockTPK, Pinblock.Format.ISO_0, sourceTPKlmk, Pinblock.Format.ISO_0, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, String sourceTPKlmk, String destinationPan, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(sourcePan, sourcePinblockTPK, Pinblock.Format.ISO_0, sourceTPKlmk, Pinblock.Format.ISO_0, destinationPan, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(pan, sourcePinblockTPK, formatSourcePTPK, sourceTPKlmk, formatDestinationPZPK, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).translatePinblockTPKtoZPK(sourcePan, sourcePinblockTPK, formatSourcePTPK, sourceTPKlmk, formatDestinationPZPK, destinationPan, destinationZPKlmk);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public byte[] generateMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					byte[] o = this.hsms.get(pos).generateMAC(content, akLMK, akType, alg);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public byte[] generateMAC(byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					byte[] o = this.hsms.get(pos).generateMAC(content, offset, length, akLMK, akType, alg);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyMAC(content, akLMK, akType, alg);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyMAC(byte[] mac, byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyMAC(mac, content, akLMK, akType, alg);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyMAC(byte[] mac, byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyMAC(mac, content, offset, length, akLMK, akType, alg);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String generateCVV(String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).generateCVV(pan, expirationDate, serviceCode, cvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyCVV(String cvv, String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyCVV(cvv, pan, expirationDate, serviceCode, cvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String generateCVV2(String pan, String expirationDate, String cvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).generateCVV2(pan, expirationDate, cvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyCVV2(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyCVV2(cvv, pan, expirationDate, cvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String generateCVVEMV(String pan, String expirationDate, String cvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).generateCVVEMV(pan, expirationDate, cvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyCVVEMV(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyCVVEMV(cvv, pan, expirationDate, cvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String generatePVV(String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).generatePVV(pan, pinblockPK, pkLMK, pkType, index, pvkPairLMK, excludedPins);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String generatePVV(String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).generatePVV(pan, pinblockPK, formatPbPK, pkLMK, pkType, index, pvkPairLMK, excludedPins);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyPVV(String pvv, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException {
		return this.verifyPVV(pvv, pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, index, pvkPairLMK);
	}

	@Override
	public boolean verifyPVV(String pvv, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyPVV(pvv, pan, pinblockPK, formatPbPK, pkLMK, pkType, index, pvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, String zpkLMK, int index, String pvkPairLMK) throws IOException {
		return this.verifyPVV(pvv, token, pan, pinblockPK, Pinblock.Format.ISO_0, zpkLMK, index, pvkPairLMK);
	}

	@Override
	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, Pinblock.Format formatPbPK, String zpkLMK, int index, String pvkPairLMK) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyPVV(pvv, token, pan, pinblockPK, formatPbPK, zpkLMK, index, pvkPairLMK);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyARQC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyARQC(scheme, mkAcLMK, pan, panSequence, emvData);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public byte[] verifyARQCGenerateARPC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData, String arc) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					byte[] o = this.hsms.get(pos).verifyARQCGenerateARPC(scheme, mkAcLMK, pan, panSequence, emvData, arc);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String generateKCV(String key, DataKeyType keyType) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).generateKCV(key, keyType);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException {
		return this.generateIBMOffset(pan, pinblockPK, pinLength, Pinblock.Format.ISO_0, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData, excludedPins);
	}

	@Override
	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					String o = this.hsms.get(pos).generateIBMOffset(pan, pinblockPK, pinLength, formatPbPK, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData, excludedPins);
					this.timestamps[pos] = 0;
					return o;
				} catch (RuntimeException e) {
					throw e;
				} catch (IOException e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException {
		return this.verifyIBMOffset(offset, pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData);
	}

	@Override
	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException {
		rl.lock();
		try {
			int cursor = this.nextCursor(), pos;
			for (int i = 0; i < this.total; i++) {
				// tenta todos os cliente a partir da posicao cursor
				pos = i + cursor;
				if (pos >= total) pos -= total;
				LOG.log(Level.FINER, "Going to HSM #{0}", pos);
				try {
					boolean o = this.hsms.get(pos).verifyIBMOffset(offset, pan, pinblockPK, formatPbPK, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData);
					this.timestamps[pos] = 0;
					return o;
				} catch (Exception e) {
					this.errorOffline(pos, e);
				}
			}
		} finally {
			rl.unlock();
		}
		errorOffline();
		throw new ConnectException("All clients offline");
	}

	@Override
	public boolean loadExcludedPINTable(int pinLength, String... pins) throws IOException {
		// ao contrario de todos os demais comandos, esse deve ser executado em TODOS os nodos
		boolean ok = true;
		for (int i = 0; i < this.total; i++) {
			LOG.log(Level.FINER, "Going to HSM #{0}", i);
			try {
				ok &= this.hsms.get(i).loadExcludedPINTable(pinLength, pins);
			} catch (RuntimeException e) {
				throw e;
			} catch (IOException e) {
				this.sendErrorNotification("Error on HSM #" + i + ": " + e);
				LOG.log(Level.FINE, "Error on HSM #" + i, e);
				ok = false;
			}
		}
		return ok;
	}

	@Override
	public int getTps() {
		return (int) this.totalTPS;
	}

}
