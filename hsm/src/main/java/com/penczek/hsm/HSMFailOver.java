package com.penczek.hsm;

import com.penczek.jmx.NotificationBroadcaster;
import com.penczek.tlv.TLVData;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HSMFailOver extends NotificationBroadcaster implements HSM, HSMFailOverMBean {

	private static final Logger LOG = Logger.getLogger(HSMFailOver.class.getName());

	private HSM main;
	private HSM backup;

	@Override
	public HSM getMain() {
		return this.main;
	}

	@Override
	public void setMain(HSM main) {
		this.sendAttributeChangeNotification("main", HSM.class.getName(), this.main, main);
		this.main = main;
	}

	@Override
	public HSM getBackup() {
		return this.backup;
	}

	@Override
	public void setBackup(HSM backup) {
		this.sendAttributeChangeNotification("backup", HSM.class.getName(), this.backup, backup);
		this.backup = backup;
	}

	@Override
	public String[] generateKey(DataKeyType type, String mkLMK, KeyScheme scheme) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateKey(type, mkLMK, scheme);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateKey(type, mkLMK, scheme);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockZPK, String zpkLMK) throws IOException {
		return this.verifyPinblock(pan, pinblockTPK, Pinblock.Format.ISO_0, tpkLMK, pinblockZPK, Pinblock.Format.ISO_0, zpkLMK);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockZPK, Pinblock.Format formatPZPK, String zpkLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyPinblock(pan, pinblockTPK, formatPTPK, tpkLMK, pinblockZPK, formatPZPK, zpkLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyPinblock(pan, pinblockTPK, formatPTPK, tpkLMK, pinblockZPK, formatPZPK, zpkLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockLMK) throws IOException {
		return this.verifyPinblock(pan, pinblockTPK, Pinblock.Format.ISO_0, tpkLMK, pinblockLMK);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyPinblock(pan, pinblockTPK, formatPTPK, tpkLMK, pinblockLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyPinblock(pan, pinblockTPK, formatPTPK, tpkLMK, pinblockLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String translatePinblock(String pan, String sourcePinblockZPK, String sourceZPKlmk, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(pan, sourcePinblockZPK, Pinblock.Format.ISO_0, sourceZPKlmk, Pinblock.Format.ISO_0, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String sourcePan, String sourcePinblockZPK, String sourceZPKlmk, String destinationPan, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(sourcePan, sourcePinblockZPK, Pinblock.Format.ISO_0, sourceZPKlmk, Pinblock.Format.ISO_0, destinationPan, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String pan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(pan, sourcePinblockZPK, formatSourcePZPK, sourceZPKlmk, formatDestinationPZPK, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String sourcePan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.translatePinblock(sourcePan, sourcePinblockZPK, formatSourcePZPK, sourceZPKlmk, formatDestinationPZPK, destinationPan, destinationZPKlmk);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.translatePinblock(sourcePan, sourcePinblockZPK, formatSourcePZPK, sourceZPKlmk, formatDestinationPZPK, destinationPan, destinationZPKlmk);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, String sourceTPKlmk, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(pan, sourcePinblockTPK, Pinblock.Format.ISO_0, sourceTPKlmk, Pinblock.Format.ISO_0, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, String sourceTPKlmk, String destinationPan, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(sourcePan, sourcePinblockTPK, Pinblock.Format.ISO_0, sourceTPKlmk, Pinblock.Format.ISO_0, destinationPan, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(pan, sourcePinblockTPK, formatSourcePTPK, sourceTPKlmk, formatDestinationPZPK, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.translatePinblockTPKtoZPK(sourcePan, sourcePinblockTPK, formatSourcePTPK, sourceTPKlmk, formatDestinationPZPK, destinationPan, destinationZPKlmk);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.translatePinblockTPKtoZPK(sourcePan, sourcePinblockTPK, formatSourcePTPK, sourceTPKlmk, formatDestinationPZPK, destinationPan, destinationZPKlmk);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public byte[] generateMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateMAC(content, akLMK, akType, alg);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateMAC(content, akLMK, akType, alg);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public byte[] generateMAC(byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateMAC(content, offset, length, akLMK, akType, alg);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateMAC(content, offset, length, akLMK, akType, alg);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyMAC(content, akLMK, akType, alg);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyMAC(content, akLMK, akType, alg);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyMAC(byte[] mac, byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyMAC(mac, content, akLMK, akType, alg);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyMAC(mac, content, akLMK, akType, alg);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyMAC(byte[] mac, byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyMAC(mac, content, offset, length, akLMK, akType, alg);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyMAC(mac, content, offset, length, akLMK, akType, alg);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String generateCVV(String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateCVV(pan, expirationDate, serviceCode, cvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateCVV(pan, expirationDate, serviceCode, cvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyCVV(String cvv, String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyCVV(cvv, pan, expirationDate, serviceCode, cvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyCVV(cvv, pan, expirationDate, serviceCode, cvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String generateCVV2(String pan, String expirationDate, String cvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateCVV2(pan, expirationDate, cvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateCVV2(pan, expirationDate, cvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyCVV2(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyCVV2(cvv, pan, expirationDate, cvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyCVV2(cvv, pan, expirationDate, cvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String generateCVVEMV(String pan, String expirationDate, String cvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateCVVEMV(pan, expirationDate, cvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateCVVEMV(pan, expirationDate, cvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyCVVEMV(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyCVVEMV(cvv, pan, expirationDate, cvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyCVVEMV(cvv, pan, expirationDate, cvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String generatePVV(String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException {
		return this.generatePVV(pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, index, pvkPairLMK, excludedPins);
	}

	@Override
	public String generatePVV(String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generatePVV(pan, pinblockPK, formatPbPK, pkLMK, pkType, index, pvkPairLMK, excludedPins);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generatePVV(pan, pinblockPK, formatPbPK, pkLMK, pkType, index, pvkPairLMK, excludedPins);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyPVV(String pvv, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException {
		return this.verifyPVV(pvv, pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, index, pvkPairLMK);
	}

	@Override
	public boolean verifyPVV(String pvv, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyPVV(pvv, pan, pinblockPK, formatPbPK, pkLMK, pkType, index, pvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyPVV(pvv, pan, pinblockPK, formatPbPK, pkLMK, pkType, index, pvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, String zpkLMK, int index, String pvkPairLMK) throws IOException {
		return this.verifyPVV(pvv, token, pan, pinblockPK, Pinblock.Format.ISO_0, zpkLMK, index, pvkPairLMK);
	}

	@Override
	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, Pinblock.Format formatPbPK, String zpkLMK, int index, String pvkPairLMK) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyPVV(pvv, token, pan, pinblockPK, formatPbPK, zpkLMK, index, pvkPairLMK);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyPVV(pvv, token, pan, pinblockPK, formatPbPK, zpkLMK, index, pvkPairLMK);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyARQC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyARQC(scheme, mkAcLMK, pan, panSequence, emvData);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyARQC(scheme, mkAcLMK, pan, panSequence, emvData);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public byte[] verifyARQCGenerateARPC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData, String arc) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyARQCGenerateARPC(scheme, mkAcLMK, pan, panSequence, emvData, arc);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyARQCGenerateARPC(scheme, mkAcLMK, pan, panSequence, emvData, arc);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String generateKCV(String key, DataKeyType keyType) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateKCV(key, keyType);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateKCV(key, keyType);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException {
		return this.generateIBMOffset(pan, pinblockPK, pinLength, Pinblock.Format.ISO_0, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData, excludedPins);
	}

	@Override
	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.generateIBMOffset(pan, pinblockPK, pinLength, formatPbPK, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData, excludedPins);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.generateIBMOffset(pan, pinblockPK, pinLength, formatPbPK, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData, excludedPins);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException {
		return this.verifyIBMOffset(offset, pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData);
	}

	@Override
	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException {
		try {
			LOG.log(Level.FINER, "Going to main HSM");
			return this.main.verifyIBMOffset(offset, pan, pinblockPK, formatPbPK, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendWarnNotification("Error on main HSM: " + e);
			LOG.log(Level.FINE, "Error on main HSM, going to backup", e);
			try {
				return this.backup.verifyIBMOffset(offset, pan, pinblockPK, formatPbPK, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData);
			} catch (RuntimeException x) {
				throw x;
			} catch (IOException x) {
				this.sendErrorNotification("Error on backup HSM: " + e);
				throw x;
			}
		}
	}

	@Override
	public boolean loadExcludedPINTable(int pinLength, String... pins) throws IOException {
		// ao contrario de todos os demais comandos, esse deve ser executado em TODOS os nodos
		LOG.log(Level.FINER, "Going to main HSM");
		boolean ok = true;
		try {
			ok &= this.main.loadExcludedPINTable(pinLength, pins);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendErrorNotification("Error on main HSM : " + e);
			LOG.log(Level.FINE, "Error on main HSM", e);
			ok = false;
		}
		LOG.log(Level.FINER, "Going to backup HSM");
		try {
			ok &= this.backup.loadExcludedPINTable(pinLength, pins);
		} catch (RuntimeException e) {
			throw e;
		} catch (IOException e) {
			this.sendErrorNotification("Error on backup HSM : " + e);
			LOG.log(Level.FINE, "Error on backup HSM", e);
			ok = false;
		}
		return ok;
	}

	@Override
	public int getTps() {
		return this.main != null ? this.main.getTps() : 0;
	}

}
