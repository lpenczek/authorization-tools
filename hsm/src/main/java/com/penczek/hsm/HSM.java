package com.penczek.hsm;

import com.penczek.tlv.TLVData;
import java.io.IOException;

public interface HSM {

	public int getTps();

	public String[] generateKey(DataKeyType type, String mkLMK, KeyScheme scheme) throws IOException;

	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockZPK, String zpkLMK) throws IOException;

	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockZPK, Pinblock.Format formatPZPK, String zpkLMK) throws IOException;

	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockLMK) throws IOException;

	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockLMK) throws IOException;

	public String translatePinblock(String pan, String sourcePinblockZPK, String sourceZPKlmk, String destinationZPKlmk) throws IOException;

	public String translatePinblock(String pan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException;

	public String translatePinblock(String sourcePan, String sourcePinblockZPK, String sourceZPKlmk, String destinationPan, String destinationZPKlmk) throws IOException;

	public String translatePinblock(String sourcePan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException;

	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, String sourceTPKlmk, String destinationZPKlmk) throws IOException;

	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException;

	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, String sourceTPKlmk, String destinationPan, String destinationZPKlmk) throws IOException;

	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException;

	public byte[] generateMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException;

	public byte[] generateMAC(byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException;

	public boolean verifyMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException;

	public boolean verifyMAC(byte[] mac, byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException;

	public boolean verifyMAC(byte[] mac, byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException;

	public String generateCVV(String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException;

	public boolean verifyCVV(String cvv, String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException;

	public String generateCVV2(String pan, String expirationDate, String cvkPairLMK) throws IOException;

	public boolean verifyCVV2(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException;

	public String generateCVVEMV(String pan, String expirationDate, String cvkPairLMK) throws IOException;

	public boolean verifyCVVEMV(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException;

	public String generatePVV(String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException;

	public String generatePVV(String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException;

	public boolean verifyPVV(String pvv, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException;

	public boolean verifyPVV(String pvv, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException;

	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, String zpkLMK, int index, String pvkPairLMK) throws IOException;

	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, Pinblock.Format formatPbPK, String zpkLMK, int index, String pvkPairLMK) throws IOException;

	public boolean verifyARQC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData) throws IOException;

	public byte[] verifyARQCGenerateARPC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData, String arc) throws IOException;

	public String generateKCV(String key, DataKeyType keyType) throws IOException;

	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException;

	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException;

	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException;

	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException;

	public boolean loadExcludedPINTable(int pinLength, String... pins) throws IOException;

}
