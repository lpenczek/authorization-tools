package com.penczek.hsm;

public interface HSMFailOverMBean {

	HSM getMain();

	void setMain(HSM main);

	HSM getBackup();

	void setBackup(HSM backup);

	public int getTps();

}
