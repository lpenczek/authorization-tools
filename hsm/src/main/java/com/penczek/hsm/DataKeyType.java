package com.penczek.hsm;

public enum DataKeyType {
	ZPK(MasterKeyType.ZMK),
	TPK(MasterKeyType.TMK),
	ZAK(MasterKeyType.ZMK),
	TAK(MasterKeyType.TMK),
	CVK(MasterKeyType.ZMK),
	PVK(MasterKeyType.TMK),
	MK_AC(MasterKeyType.ZMK),
	MK_SMI(MasterKeyType.ZMK),
	MK_SMC(MasterKeyType.ZMK)
	;

	private final MasterKeyType parent;

	DataKeyType(MasterKeyType p) {
		this.parent = p;
	}

	public MasterKeyType getParent() {
		return this.parent;
	}

}
