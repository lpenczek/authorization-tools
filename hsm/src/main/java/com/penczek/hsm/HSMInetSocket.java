package com.penczek.hsm;

import com.penczek.jmx.NotificationBroadcaster;
import com.penczek.util.HexadecimalStringUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class HSMInetSocket extends NotificationBroadcaster implements HSM, HSMInetSocketMBean {

	private static final int MILLIS = 1000;

	private String ip;
	private int port;
	private boolean tcp;
	private boolean persistent;
	private int readTimeout = 5000;
	private int connectionTimeout = 2000;
	private int tps;
	private boolean burstMode;
	private Socket socket;
	private OutputStream out;
	private InputStream in;
	private long transactionTimeSlice;
	private volatile long last;
	private volatile int quantity;

	protected HSMInetSocket() {}

	protected HSMInetSocket(String ip, int port) {
		this(ip, port, true, false);
	}

	protected HSMInetSocket(String ip, int port, boolean tcp, boolean persistent) {
		this.ip = ip;
		this.port = port;
		this.tcp = tcp;
		this.persistent = tcp ? persistent : false;
	}

	@Override
	protected void finalize() throws Throwable {
		this.disconnect();
	}

	@Override
	public String getIp() {
		return this.ip;
	}

	@Override
	public void setIp(String ip) {
		this.sendAttributeChangeNotification("ip", String.class.getName(), this.ip, ip);
		this.ip = ip;
	}

	@Override
	public int getPort() {
		return this.port;
	}

	@Override
	public void setPort(int port) {
		this.sendAttributeChangeNotification("port", "int", this.port, port);
		this.port = port;
	}

	@Override
	public boolean isTcp() {
		return tcp;
	}

	@Override
	public void setTcp(boolean tcp) {
		this.sendAttributeChangeNotification("tcp", "boolean", this.tcp, tcp);
		this.tcp = tcp;
	}

	@Override
	public boolean isPersistent() {
		return this.persistent;
	}

	@Override
	public void setPersistent(boolean persistent) {
		this.sendAttributeChangeNotification("persistent", "boolean", this.persistent, persistent);
		this.persistent = persistent;
	}

	@Override
	public int getConnectionTimeout() {
		return this.connectionTimeout;
	}

	@Override
	public void setConnectionTimeout(int connectionTimeout) {
		this.sendAttributeChangeNotification("connectionTimeout", "int", this.connectionTimeout, connectionTimeout);
		this.connectionTimeout = connectionTimeout;
	}

	@Override
	public int getReadTimeout() {
		return this.readTimeout;
	}

	@Override
	public void setReadTimeout(int readTimeout) {
		this.sendAttributeChangeNotification("readTimeout", "int", this.readTimeout, readTimeout);
		this.readTimeout = readTimeout;
	}

	@Override
	public int getTps() {
		return this.tps;
	}

	@Override
	public void setTps(int tps) {
		this.sendAttributeChangeNotification("tps", "int", this.tps, tps);
		this.tps = tps;
		this.transactionTimeSlice = tps > 0 ? MILLIS / tps : 0;
	}

	@Override
	public boolean isBurstMode() {
		return burstMode;
	}

	@Override
	public void setBurstMode(boolean burstMode) {
		this.sendAttributeChangeNotification("burstMode", "boolean", this.burstMode, burstMode);
		this.burstMode = burstMode;
	}

	// m�todos conex�o TCP
	protected void connect() throws IOException {
		getLogger().log(Level.FINE, "Connecting to {0}:{1}", new Object[]{this.ip, this.port});
		this.socket = new Socket();
		this.socket.setKeepAlive(true);
		this.socket.setReuseAddress(true);
		this.socket.setTcpNoDelay(true);
		this.socket.setSoTimeout(this.readTimeout);
		this.socket.connect(new InetSocketAddress(this.ip, this.port), this.connectionTimeout);
		this.out = socket.getOutputStream();
		this.in = socket.getInputStream();
		getLogger().fine("Connected");
	}

	protected void disconnect() {
		if (this.socket != null) {
			getLogger().fine("Disconnecting");
			try { this.socket.close(); } catch (Exception e) {}
			this.in = null;
			this.out = null;
			this.socket = null;
			getLogger().fine("Disconnected");
		}
	}

	@Override
	public synchronized void reset() {
		this.disconnect();
	}

	@SuppressWarnings("deprecation")
	protected synchronized String sendMessage(String msg) throws IOException {
		Logger log = getLogger();
		log.fine(msg);
		// controle de tps
		if (this.transactionTimeSlice > 0) {
			long now = System.currentTimeMillis();
			if (this.burstMode) {
				long diff = now - last;
				if (diff > MILLIS) {
					this.last = now;
					this.quantity = 1;
				} else {
					if (++this.quantity > this.tps) {
						try {
							Thread.sleep(MILLIS - diff);
						} catch (InterruptedException e) {}
						this.last = now;
						this.quantity = 1;
					}
				}
			} else {
				long diff = (this.last + this.transactionTimeSlice) - now;
				if (diff > 0) {
					try {
						Thread.sleep(diff);
					} catch (InterruptedException e) {}
				}
			}
			this.last = System.currentTimeMillis();
		}
		int length = msg.length();
		byte[] r = new byte[length + 2];
		r[0] = (byte) ((length >>> 8) & 0xFF);
		r[1] = (byte) (length & 0xFF);
		msg.getBytes(0, msg.length(), r, 2);
		String s = new String(this.tcp ? this.sendTCP(r) : this.sendUDP(r), 0);
		log.fine(s);
		return s;
	}

	private byte[] sendTCP(byte[] r) throws IOException {
		Logger log = getLogger();
		try {
			boolean usingPersistent = false;
			if (this.socket == null || this.socket.isClosed() || !this.socket.isConnected()) {
				this.disconnect();
				this.connect();
			} else {
				usingPersistent = true;
			}
			if (log.isLoggable(Level.FINEST)) {
				log.log(Level.FINEST, "Send {0}", HexadecimalStringUtil.bytesToString(r));
			}
			try {
				out.write(r);
			} catch (SocketException e) {
				if (!usingPersistent) throw e;
				this.disconnect();
				this.connect();
				out.write(r);
			}
			out.flush();
			int i = in.read();
			if (i < 0) return null;
			int length = i << 8;
			i = in.read();
			if (i < 0) return null;
			length |= i;
			r = new byte[length];
			for (int total = 0; total < length;) {
				total += (i = in.read(r, total, length - total));
				if (i < 0) break;
			}
			if (log.isLoggable(Level.FINEST)) {
				log.log(Level.FINEST, "Read {0}", HexadecimalStringUtil.bytesToString(r));
			}
			return r;
		} catch (IOException e) {
			this.disconnect();
			log.log(Level.FINEST, "IO error {0}", e);
			this.sendErrorNotification("Unexpected error " + e);
			throw e;
		} catch (RuntimeException e) {
			this.disconnect();
			log.log(Level.FINEST, "Unexpected error {0}", e);
			this.sendErrorNotification("Unexpected error " + e);
			throw e;
		} finally {
			if (!this.persistent) {
				this.disconnect();
			}
		}
	}

	private byte[] sendUDP(byte[] r) throws IOException {
		Logger log = getLogger();
		DatagramSocket udpSocket = null;
		try {
			udpSocket = new DatagramSocket();
			udpSocket.setReuseAddress(true);
			udpSocket.setSoTimeout(this.readTimeout);
			DatagramPacket packet = new DatagramPacket(r, r.length, new InetSocketAddress(this.ip, this.port));
			if (log.isLoggable(Level.FINEST)) {
				log.log(Level.FINEST, "Sending {0} to {1}:{2}", new Object[]{HexadecimalStringUtil.bytesToString(r), this.ip, this.port});
			}
			udpSocket.send(packet);
			// get response
			packet.setData(r.length > 1024 ? r : new byte[1024]);
			udpSocket.receive(packet);
			if (packet.getLength() < 2) return null;
			r = packet.getData();
			int length = r[0];
			if (length < 0) return null;
			length <<= 8;
			if (r[1] < 0) return null;
			length |= r[1];
			r = new byte[length];
			System.arraycopy(packet.getData(), 2, r, 0, length);
			if (log.isLoggable(Level.FINEST)) {
				log.log(Level.FINEST, "Read {0}", HexadecimalStringUtil.bytesToString(r));
			}
			return r;
		} catch (IOException e) {
			log.log(Level.FINEST, "IO error {0}", e);
			this.sendErrorNotification("Unexpected error " + e);
			throw e;
		} catch (RuntimeException e) {
			log.log(Level.FINEST, "Unexpected error {0}", e);
			this.sendErrorNotification("Unexpected error " + e);
			throw e;
		} finally {
			if (udpSocket != null) try { udpSocket.close(); } catch (Exception e) {}
		}
	}

	protected abstract Logger getLogger();

}
