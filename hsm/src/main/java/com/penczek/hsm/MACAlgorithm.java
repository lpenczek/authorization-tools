package com.penczek.hsm;

public enum MACAlgorithm {
	/** ISO 9797 MAC algorithm 1 (3DES CBC) padding method 1 (0x00 pad). */
	ISO9797_ALG1_PAD1,
	/** ISO 9797 MAC algorithm 1 (3DES CBC) padding method 2 (add 0x80 and 0x00 pad). */
	ISO9797_ALG1_PAD2,
	/** ISO 9797 MAC algorithm 3 (DES CBC + 3DES last block) padding method 1 (0x00 pad). */
	ISO9797_ALG3_PAD1,
	/** ISO 9797 MAC algorithm 3 (DES CBC + 3DES last block) padding method 2 (add 0x80 and 0x00 pad). */
	ISO9797_ALG3_PAD2
}
