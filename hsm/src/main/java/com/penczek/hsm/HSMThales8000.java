package com.penczek.hsm;

import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.PrimitiveTLV;
import com.penczek.tlv.TLVData;
import com.penczek.util.HexadecimalStringUtil;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HSMThales8000 extends HSMInetSocket {

	private static final Logger log = Logger.getLogger(HSMThales8000.class.getName());
	private volatile int actual = 0;

	public HSMThales8000() {}

	public HSMThales8000(String ip, int port) {
		super(ip, port);
	}

	public HSMThales8000(String ip, int port, boolean persistent) {
		super(ip, port, true, persistent);
	}

	public HSMThales8000(String ip, int port, boolean tcp, boolean persistent) {
		super(ip, port, tcp, persistent);
	}

	@Override
	protected Logger getLogger() {
		return log;
	}

	private synchronized String nextId() {
		if (this.actual > 65535) this.actual = 0;
		return String.format("%04x", this.actual++);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockZPK, String zpkLMK) throws IOException {
		return this.verifyPinblock(pan, pinblockTPK, Pinblock.Format.ISO_0, tpkLMK, pinblockZPK, Pinblock.Format.ISO_0, zpkLMK);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockZPK, Pinblock.Format formatPZPK, String zpkLMK) throws IOException {
		String s = this.translatePinblockTPKtoZPK(pan, pinblockTPK, formatPTPK, tpkLMK, formatPZPK, zpkLMK);
		return pinblockZPK.equals(s);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, String tpkLMK, String pinblockLMK) throws IOException {
		return this.verifyPinblock(pan, pinblockTPK, Pinblock.Format.ISO_0, tpkLMK, pinblockLMK);
	}

	@Override
	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format formatPTPK, String tpkLMK, String pinblockLMK) throws IOException {
		String id = this.nextId();
		String s = this.sendMessage(id + "BC" + tpkLMK.toUpperCase() + pinblockTPK.toUpperCase() + this.getFormat(formatPTPK) + Pinblock.extractAccountNumber(pan) + pinblockLMK.toUpperCase());
		if (s == null || !s.equals(id + "BD00")) {
			log.log(Level.INFO, "Cannot verify PBtpk with PBlmk: {0}", s);
			return false;
		}
		return true;
	}

	@Override
	public String translatePinblock(String pan, String sourcePinblockZPK, String sourceZPKlmk, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(pan, sourcePinblockZPK, Pinblock.Format.ISO_0, sourceZPKlmk, Pinblock.Format.ISO_0, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String sourcePan, String sourcePinblockZPK, String sourceZPKlmk, String destinationPan, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(sourcePan, sourcePinblockZPK, Pinblock.Format.ISO_0, sourceZPKlmk, Pinblock.Format.ISO_0, destinationPan, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String pan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException {
		return this.translatePinblock(pan, sourcePinblockZPK, formatSourcePZPK, sourceZPKlmk, formatDestinationPZPK, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblock(String sourcePan, String sourcePinblockZPK, Pinblock.Format formatSourcePZPK, String sourceZPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException {
		String id = this.nextId();
		String s = this.sendMessage(
				id + "CC" + sourceZPKlmk.toUpperCase() + destinationZPKlmk.toUpperCase() + "12" + sourcePinblockZPK.toUpperCase()
				+ this.getFormat(formatSourcePZPK) + this.getFormat(formatDestinationPZPK)
				+ Pinblock.extractAccountNumber(sourcePan)
				+ (destinationPan != null ? ';' + Pinblock.extractAccountNumber(destinationPan) : "")
		);
		if (s == null || !s.startsWith(id + "CD00")) {
			log.log(Level.INFO, "Cannot translate PBsourceZpk to PBdestinationZpk: {0}", s);
			return null;
		}
		return s.substring(10, 26);
	}

	@Override
	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, String sourceTPKlmk, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(pan, sourcePinblockTPK, Pinblock.Format.ISO_0, sourceTPKlmk, Pinblock.Format.ISO_0, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, String sourceTPKlmk, String destinationPan, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(sourcePan, sourcePinblockTPK, Pinblock.Format.ISO_0, sourceTPKlmk, Pinblock.Format.ISO_0, destinationPan, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String pan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationZPKlmk) throws IOException {
		return this.translatePinblockTPKtoZPK(pan, sourcePinblockTPK, formatSourcePTPK, sourceTPKlmk, formatDestinationPZPK, null, destinationZPKlmk);
	}

	@Override
	public String translatePinblockTPKtoZPK(String sourcePan, String sourcePinblockTPK, Pinblock.Format formatSourcePTPK, String sourceTPKlmk, Pinblock.Format formatDestinationPZPK, String destinationPan, String destinationZPKlmk) throws IOException {
		String id = this.nextId();
		String s = this.sendMessage(
				id + "CA" + sourceTPKlmk.toUpperCase() + destinationZPKlmk.toUpperCase() + "12" + sourcePinblockTPK.toUpperCase()
				+ this.getFormat(formatSourcePTPK) + this.getFormat(formatDestinationPZPK)
				+ Pinblock.extractAccountNumber(sourcePan)
				+ (destinationPan != null ? ';' + Pinblock.extractAccountNumber(destinationPan) : "")
		);
		if (s == null || !s.startsWith(id + "CB00")) {
			log.log(Level.INFO, "Cannot translate PBsourceTpk to PBdestinationZpk: {0}", s);
			return null;
		}
		return s.substring(10, 26);
	}

	@Override
	public String[] generateKey(DataKeyType type, String mkLMK, KeyScheme scheme) throws IOException {
		if (type == DataKeyType.CVK || type == DataKeyType.PVK) {
			if (scheme != KeyScheme.ANSIX9_17_DOUBLE && scheme != KeyScheme.VARIANT_DOUBLE) {
				throw new IllegalArgumentException("Invalid key scheme (" + scheme + ") for key type " + type);
			}
		}
		String id = this.nextId();
		if (type == DataKeyType.CVK) {
			String s = this.sendMessage(id + "AS;0" + (scheme == KeyScheme.VARIANT_DOUBLE ? scheme.getTag() : '0') + '1');
			if (s == null || !s.startsWith(id + "AT00")) {
				log.log(Level.INFO, "Cannot generate {0}: {1}", new Object[]{type, s});
				return null;
			}
			int length = scheme == KeyScheme.VARIANT_DOUBLE ? 33 : 32;
			String cvkLMK = s.substring(8, 8 + length).toUpperCase();
			char sch = scheme.getTag();
			s = this.sendMessage(id + "AU" + mkLMK.toUpperCase() + cvkLMK + ';' + sch + '0' + '1');
			if (s == null || !s.startsWith(id + "AV00")) {
				log.log(Level.INFO, "Cannot generate {0}: {1}", new Object[]{type, s});
				return null;
			}
			return new String[] { s.substring(8, 8 + length), cvkLMK };
		} else {
			String req = null;
			String resp = null;
			switch (type) {
				case TPK:
				case PVK:
					req = "HC";
					resp = "HD";
					break;
				case ZPK:
					req = "IA";
					resp = "IB";
					break;
				case TAK:
					req = "HA";
					resp = "HB";
					break;
				case ZAK:
					req = "FI1";
					resp = "FJ";
					break;
			}
			char sch = scheme.getTag();
			String pk = this.sendMessage(id + req + mkLMK.toUpperCase() + ';' + sch + sch + '1');
			if (pk == null || !pk.startsWith(id + resp + "00")) {
				log.log(Level.INFO, "Cannot generate {0}: {1}", new Object[]{type, pk});
				return null;
			}
			// scheme + pkMK, scheme + pkLMK
			int length = sch == 'T' || sch == 'Y' ? 49 : sch == 'U' || sch == 'X' ? 33 : 16;
			return new String[] { pk.substring(8, 8 + length), pk.substring(8 + length, 8 + length * 2) };
		}
	}

	@Override
	public byte[] generateMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		return this.generateMAC(content, 0, content.length, akLMK, akType, alg);
	}

	@Override
	public byte[] generateMAC(byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		if (length <= 32000) {
			// fazer 1 requisicao
			return this.generateMAC(content, offset, length, akLMK, akType, alg, 0, null);
		} else {
			// fazer diversas requisicoes de 32000 de conteudo cada (1a com 1, demais com 2 e IV, ultima com 3 e IV
			// na ultima versao do payshield 9000, a ultima deve ter ao menos 24 bytes
			// neste caso fazemos o penultimo com 32000 - 32 (para ser divisivel por 8, 16, 24 e 32)
			int q = length < 32024 ? 32000 - 32 : 32000;
			// 1a requisicao
			byte[] iv = this.generateMAC(content, offset, q, akLMK, akType, alg, 1, null);
			if (iv == null) return null;
			// intermediarias
			for (offset += q, length -= q; length > 32000; offset += q, length -= q) {
				q = length < 32024 ? 32000 - 32 : 32000;
				iv = this.generateMAC(content, offset, q, akLMK, akType, alg, 2, iv);
				if (iv == null) return null;
			}
			// ultima requisicao
			return this.generateMAC(content, offset, length, akLMK, akType, alg, 3, iv);
		}
	}

	@SuppressWarnings("deprecation")
	private byte[] generateMAC(byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg, int step, byte[] iv) throws IOException {
		String id = this.nextId();
		StringBuilder pak = new StringBuilder(120 + length);
		pak.append(id).append("M6");
		pak.append((char) ('0' + step)); // step
		pak.append('0'); // 0 - binary, 2 - text
		pak.append('0'); // 0 - MAC size 8, 1 - MAC size 16
		// algoritmo
		switch (alg) {
			case ISO9797_ALG1_PAD1:
			case ISO9797_ALG1_PAD2:
				pak.append('1');
				break;
			case ISO9797_ALG3_PAD1:
			case ISO9797_ALG3_PAD2:
				pak.append('3');
				break;
		}
		if (step == 1 || step == 2) {
			pak.append('0'); // nopadding
		} else {
			// padding
			switch (alg) {
				case ISO9797_ALG1_PAD1:
				case ISO9797_ALG3_PAD1:
					pak.append('1');
					break;
				case ISO9797_ALG1_PAD2:
				case ISO9797_ALG3_PAD2:
					pak.append('2');
					break;
			}
		}
		switch (akType) {
			case TAK:
				pak.append("003");
				break;
			case ZAK:
				pak.append("008");
				break;
			default:
				throw new IllegalArgumentException("Invalid key type " + akType);
		}
		pak.append(akLMK.toUpperCase());
		if (iv != null) {
			pak.append(new String(iv, 0));
		}
		pak.append(String.format("%04X", length));
		pak.append(new String(content, 0, offset, length));
		String s = this.sendMessage(pak.toString());
		if (s == null || !s.startsWith(id + "M700")) {
			log.log(Level.INFO, "Cannot generate MAC ({0}): {1}", new Object[]{(char) ('0' + step), s});
			return null;
		}
		byte[] out = new byte[s.length() - 8];
		s.getBytes(8, s.length(), out, 0);
		return out;
	}

	@Override
	public boolean verifyMAC(byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		// ultimos 8 bytes sao o mac
		byte[] mac = new byte[8];
		System.arraycopy(content, content.length - 8, mac, 0, 8);
		return this.verifyMAC(mac, content, 0, content.length - 8, akLMK, akType, alg);
	}

	@Override
	public boolean verifyMAC(byte[] mac, byte[] content, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		return this.verifyMAC(mac, content, 0, content.length, akLMK, akType, alg);
	}

	@Override
	public boolean verifyMAC(byte[] mac, byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg) throws IOException {
		// ultimos 8 bytes sao o mac
		if (length <= 32000) {
			// fazer 1 requisicao
			return this.verifyMAC(mac, content, offset, length, akLMK, akType, alg, 0, null) != null;
		} else {
			// fazer diversas requisicoes de 32000 de conteudo cada (1a com 1, demais com 2 e IV, ultima com 3 e IV
			// na ultima versao do payshield 9000, a ultima deve ter ao menos 24 bytes
			// neste caso fazemos o penultimo com 32000 - 32 (para ser divisivel por 8, 16, 24 e 32)
			int q = length < 32024 ? 32000 - 32 : 32000;
			// 1a requisicao
			byte[] iv = this.verifyMAC(mac, content, offset, q, akLMK, akType, alg, 1, null);
			if (iv == null) return false;
			// intermediarias
			for (offset += q, length -= q; length > 32000; offset += q, length -= q) {
				q = length < 32024 ? 32000 - 32 : 32000;
				iv = this.verifyMAC(mac, content, offset, q, akLMK, akType, alg, 2, iv);
				if (iv == null) return false;
			}
			// ultima requisicao
			return this.verifyMAC(mac, content, offset, length, akLMK, akType, alg, 3, iv) != null;
		}
	}

	@SuppressWarnings("deprecation")
	private byte[] verifyMAC(byte[] mac, byte[] content, int offset, int length, String akLMK, DataKeyType akType, MACAlgorithm alg, int step, byte[] iv) throws IOException {
		if (mac == null || content == null) return null;
		String id = this.nextId();
		StringBuilder pak = new StringBuilder(120 + length);
		pak.append(id).append("M8");
		pak.append((char) ('0' + step)); // step
		pak.append('0'); // 0 - binary, 2 - text
		pak.append('0'); // 0 - MAC size 8, 1 - MAC size 16
		// algoritmo
		switch (alg) {
			case ISO9797_ALG1_PAD1:
			case ISO9797_ALG1_PAD2:
				pak.append('1');
				break;
			case ISO9797_ALG3_PAD1:
			case ISO9797_ALG3_PAD2:
				pak.append('3');
				break;
		}
		if (step == 1 || step == 2) {
			pak.append('0'); // nopadding
		} else {
			// padding
			switch (alg) {
				case ISO9797_ALG1_PAD1:
				case ISO9797_ALG3_PAD1:
					pak.append('1');
					break;
				case ISO9797_ALG1_PAD2:
				case ISO9797_ALG3_PAD2:
					pak.append('2');
					break;
			}
		}
		switch (akType) {
			case TAK:
				pak.append("003");
				break;
			case ZAK:
				pak.append("008");
				break;
			default:
				throw new IllegalArgumentException("Invalid key type " + akType);
		}
		pak.append(akLMK.toUpperCase());
		if (iv != null) {
			pak.append(new String(iv, 0));
		}
		pak.append(String.format("%04X", length));
		pak.append(new String(content, 0, offset, length));
		if (step == 0 || step == 3) {
			pak.append(new String(mac, 0));
		}
		String s = this.sendMessage(pak.toString());
		if (s == null || !s.startsWith(id + "M900")) {
			log.log(Level.INFO, "Cannot generate MAC ({0}): {1}", new Object[]{(char) ('0' + step), s});
			return null;
		}
		if (s.length() > 8) {
			byte[] out = new byte[s.length() - 8];
			s.getBytes(8, s.length(), out, 0);
			return out;
		}
		return new byte[0];
	}

	@Override
	public String generateCVV(String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException {
		String id = this.nextId();
		String s = this.sendMessage(id + "CW" + cvkPairLMK.toUpperCase() + pan + ';' + expirationDate + serviceCode);
		if (s == null || !s.startsWith(id + "CX00")) {
			log.log(Level.INFO, "Cannot generate CVV: {0}", s);
			return null;
		}
		return s.substring(8, 8 + 3);
	}

	@Override
	public boolean verifyCVV(String cvv, String pan, String expirationDate, String serviceCode, String cvkPairLMK) throws IOException {
		String id = this.nextId();
		String s = this.sendMessage(id + "CY" + cvkPairLMK.toUpperCase() + cvv + pan + ';' + expirationDate + serviceCode);
		if (s == null || !s.startsWith(id + "CZ00")) {
			log.log(Level.INFO, "Cannot verify CVV: {0}", s);
			return false;
		}
		return true;
	}

	@Override
	public String generateCVV2(String pan, String expirationDate, String cvkPairLMK) throws IOException {
		return this.generateCVV(pan, expirationDate, "000", cvkPairLMK);
	}

	@Override
	public boolean verifyCVV2(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException {
		return this.verifyCVV(cvv, pan, expirationDate, "000", cvkPairLMK);
	}

	@Override
	public String generateCVVEMV(String pan, String expirationDate, String cvkPairLMK) throws IOException {
		return this.generateCVV(pan, expirationDate, "999", cvkPairLMK);
	}

	@Override
	public boolean verifyCVVEMV(String cvv, String pan, String expirationDate, String cvkPairLMK) throws IOException {
		return this.verifyCVV(cvv, pan, expirationDate, "999", cvkPairLMK);
	}

	@Override
	public String generatePVV(String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException {
		return this.generatePVV(pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, index, pvkPairLMK, excludedPins);
	}

	@Override
	public String generatePVV(String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK, String... excludedPins) throws IOException {
		if (pkType != DataKeyType.ZPK && pkType != DataKeyType.TPK) {
			throw new IllegalArgumentException("Invalid key type " + pkType);
		}
		if (index < 0 || index > 9) {
			throw new IllegalArgumentException("Invalid index " + index);
		}
		String id = this.nextId();
		StringBuilder sb = new StringBuilder(1024);
		sb.append(id).append("FW").append(pkType == DataKeyType.ZPK ? "001" : "002");
		sb.append(pkLMK.toUpperCase()).append(pvkPairLMK.toUpperCase()).append(pinblockPK.toUpperCase()).append(this.getFormat(formatPbPK));
		sb.append(Pinblock.extractAccountNumber(pan)).append(index);
		if (excludedPins != null && excludedPins.length > 0) {
			sb.append('*');
			sb.append(String.format("%02d", excludedPins.length));
			sb.append(String.format("%02d", excludedPins[0].length()));
			for (String s : excludedPins) {
				sb.append(s);
			}
		}
		String s = this.sendMessage(sb.toString());
		if (s == null || !s.startsWith(id + "FX00")) {
			log.log(Level.INFO, "Cannot generate PVV: {0}", s);
			return null;
		}
		return s.substring(8, 8 + 4);
	}

	@Override
	public boolean verifyPVV(String pvv, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException {
		return this.verifyPVV(pvv, pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, index, pvkPairLMK);
	}

	@Override
	public boolean verifyPVV(String pvv, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, int index, String pvkPairLMK) throws IOException {
		String req = null;
		String resp = null;
		switch (pkType) {
			case TPK:
				req = "DC";
				resp = "DD";
				break;
			case ZPK:
				req = "EC";
				resp = "ED";
				break;
			default:
				throw new IllegalArgumentException("Invalid key type " + pkType);
		}
		if (index < 0 || index > 9) {
			throw new IllegalArgumentException("Invalid index " + index);
		}
		String id = this.nextId();
		String s = this.sendMessage(id + req + pkLMK.toUpperCase() + pvkPairLMK.toUpperCase() + pinblockPK.toUpperCase() + this.getFormat(formatPbPK) + Pinblock.extractAccountNumber(pan) + index + pvv);
		if (s == null || !s.startsWith(id + resp + "00")) {
			log.log(Level.INFO, "Cannot verify PVV: {0}", s);
			return false;
		}
		return true;
	}

	@Override
	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, String zpkLMK, int index, String pvkPairLMK) throws IOException {
		return this.verifyPVV(pvv, token, pan, pinblockPK, Pinblock.Format.ISO_0, zpkLMK, index, pvkPairLMK);
	}

	@Override
	public boolean verifyPVV(String pvv, String token, String pan, String pinblockPK, Pinblock.Format formatPbPK, String zpkLMK, int index, String pvkPairLMK) throws IOException {
		if (index < 0 || index > 9) {
			throw new IllegalArgumentException("Invalid index " + index);
		}
		String id = this.nextId();
		String s = this.sendMessage(
				id + "EC" + zpkLMK.toUpperCase() + pvkPairLMK.toUpperCase() + pinblockPK.toUpperCase() + this.getFormat(formatPbPK)
				+ Pinblock.extractAccountNumber(token)
				+ ';' + Pinblock.extractAccountNumber(pan)
				+ index + pvv
		);
		if (s == null || !s.startsWith(id + "ED00")) {
			log.log(Level.INFO, "Cannot verify PVV: {0}", s);
			return false;
		}
		return true;
	}

	@Override
	public boolean verifyARQC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData) throws IOException {
		return this.verifyARQCGenerateARPC(scheme, mkAcLMK, pan, panSequence, emvData, true, null) != null;
	}

	@Override
	public byte[] verifyARQCGenerateARPC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData emvData, String arc) throws IOException {
		return this.verifyARQCGenerateARPC(scheme, mkAcLMK, pan, panSequence, emvData, false, arc);
	}

	@SuppressWarnings("deprecation")
	private byte[] verifyARQCGenerateARPC(EMVScheme scheme, String mkAcLMK, String pan, int panSequence, TLVData tlvData, boolean onlyVerify, String arc) throws IOException {
		// gets information
		PrimitiveTLV ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.APPLICATION_TRANSACTION_COUNTER);
		if (ptlv == null) return null;
		byte[] atc = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.UNPREDICTABLE_NUMBER);
		if (ptlv == null) return null;
		byte[] unpredictableNumber = ptlv.getValue();
		// transaction data
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.AMOUNT_AUTHORISED_NUMERIC);
		if (ptlv == null) return null;
		byte[] amountAuthorized = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.AMOUNT_OTHER_NUMERIC);
		if (ptlv == null) return null;
		byte[] amountOther = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TERMINAL_COUNTRY_CODE);
		if (ptlv == null) return null;
		byte[] terminalCountry = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TERMINAL_VERIFICATION_RESULTS);
		if (ptlv == null) return null;
		byte[] tvr = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TRANSACTION_CURRENCY_CODE);
		if (ptlv == null) return null;
		byte[] currency = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TRANSACTION_DATE);
		if (ptlv == null) return null;
		byte[] date = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.TRANSACTION_TYPE);
		if (ptlv == null) return null;
		byte[] transactionType = ptlv.getValue();
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.APPLICATION_INTERCHANGE_PROFILE);
		if (ptlv == null) return null;
		byte[] aip = ptlv.getValue();
		byte[] iad_cvr;
		if (scheme == EMVScheme.MASTERCARD_MCHIP) {
			ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.CARD_VERIFICATION_RESULTS_MCHIP);
			if (ptlv != null) {
				iad_cvr = ptlv.getValue();
			} else {
				// verifica se tem IAD, e extrai CVR (apenas 4 bytes do IAD, comecando no 2)
				ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.ISSUER_APPLICATION_DATA);
				if (ptlv == null) return null;
				iad_cvr = Arrays.copyOfRange(ptlv.getValue(), 2, 6);
			}
		} else {
			ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.ISSUER_APPLICATION_DATA);
			if (ptlv == null) return null;
			iad_cvr = ptlv.getValue();
		}
		// retrieve ARQC
		ptlv = (PrimitiveTLV) tlvData.getDataElement(EMVDataElement.APPLICATION_CRYPTOGRAM);
		if (ptlv == null) return null;
		byte[] arqc = ptlv.getValue();
		// concatenate transaction data
		int length = amountAuthorized.length + amountOther.length + terminalCountry.length
				+ tvr.length + currency.length + date.length + transactionType.length
				+ unpredictableNumber.length + aip.length + atc.length + iad_cvr.length;
		// prepares data with pad2 method (80 + 00..)
		byte[] data = new byte[(length / 8 + 1) * 8];
		int pos = 0;
		System.arraycopy(amountAuthorized, 0, data, pos, amountAuthorized.length);
		pos += amountAuthorized.length;
		System.arraycopy(amountOther, 0, data, pos, amountOther.length);
		pos += amountOther.length;
		System.arraycopy(terminalCountry, 0, data, pos, terminalCountry.length);
		pos += terminalCountry.length;
		System.arraycopy(tvr, 0, data, pos, tvr.length);
		pos += tvr.length;
		System.arraycopy(currency, 0, data, pos, currency.length);
		pos += currency.length;
		System.arraycopy(date, 0, data, pos, date.length);
		pos += date.length;
		System.arraycopy(transactionType, 0, data, pos, transactionType.length);
		pos += transactionType.length;
		System.arraycopy(unpredictableNumber, 0, data, pos, unpredictableNumber.length);
		pos += unpredictableNumber.length;
		System.arraycopy(aip, 0, data, pos, aip.length);
		pos += aip.length;
		System.arraycopy(atc, 0, data, pos, atc.length);
		pos += atc.length;
		System.arraycopy(iad_cvr, 0, data, pos, iad_cvr.length);
		pos += iad_cvr.length;
		// padding 2
		data[pos] = (byte) 0x80;
		// ^ the other 00 bytes do not need to be added (array full of zeroes when created)
		// format pan + sequence
		String pfpan = pan + String.format("%02d", panSequence);
		pfpan = pfpan.substring(pfpan.length() - 16);
		// HSM KQ command
		String id = this.nextId();
		char schemeId;
		switch (scheme) {
			case EUROPAY:
			case MASTERCARD_MCHIP:
			case CCD:
				schemeId = '1';
				break;
			case VISA_VSDC:
			default:
				schemeId = '0';
		}
		if (scheme == EMVScheme.CCD) {
			unpredictableNumber = new byte[4];
		}
		String s = this.sendMessage(id + "KQ" + (onlyVerify ? '0' : '1') + schemeId + mkAcLMK.toUpperCase()
				+ new String(HexadecimalStringUtil.bytesFromString(pfpan), 0)
				+ new String(atc, 0)
				+ new String(unpredictableNumber, 0)
				+ String.format("%02X", data.length)
				+ new String(data, 0)
				+ ';'
				+ new String(arqc, 0)
				+ (!onlyVerify ? arc : "")
		);
		if (s == null || !s.startsWith(id + "KR00")) {
			log.log(Level.INFO, "Cannot verify ARQC: {0}", s);
			return null;
		}
		if (onlyVerify) return new byte[0];
		byte[] arpc = new byte[8];
		s.getBytes(8, 16, arpc, 0);
		return arpc;
	}

	@Override
	public String generateKCV(String key, DataKeyType keyType) throws IOException {
		String id = this.nextId();
		StringBuilder sb = new StringBuilder(64);
		sb.append(id).append("BUFF");
		if (key.length() == 16) {
			sb.append('0');
		} else if (key.length() == 49) {
			sb.append('2');
		} else {
			sb.append('1');
		}
		sb.append(key.toUpperCase()).append(';');
		switch (keyType) {
			case ZPK: sb.append("001"); break;
			case TPK: sb.append("002"); break;
			case ZAK: sb.append("008"); break;
			case TAK: sb.append("003"); break;
			case CVK: sb.append("402"); break;
			case PVK: sb.append("002"); break;
			case MK_AC: sb.append("109"); break;
			case MK_SMI: sb.append("209"); break;
			case MK_SMC: sb.append("309"); break;
		}
		String s = this.sendMessage(sb.toString());
		if (s == null || !s.startsWith(id + "BV00")) {
			log.log(Level.INFO, "Cannot generate KCV: {0}", s);
			return null;
		}
		return s.substring(8, 8 + 6);
	}

	@Override
	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException {
		return this.generateIBMOffset(pan, pinblockPK, pinLength, Pinblock.Format.ISO_0, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData, excludedPins);
	}

	@Override
	public String generateIBMOffset(String pan, String pinblockPK, int pinLength, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData, String... excludedPins) throws IOException {
		if (pkType != DataKeyType.ZPK && pkType != DataKeyType.TPK) {
			throw new IllegalArgumentException("Invalid key type " + pkType);
		}
		String id = this.nextId();
		StringBuilder sb = new StringBuilder(1024);
		sb.append(id).append("BK").append(pkType == DataKeyType.ZPK ? "001" : "002");
		sb.append(pkLMK.toUpperCase()).append(pvkPairLMK.toUpperCase()).append(pinblockPK.toUpperCase()).append(this.getFormat(formatPbPK));
		sb.append(String.format("%02d", pinLength)).append(Pinblock.extractAccountNumber(pan));
		sb.append(decimalizationTable.toUpperCase()).append('P').append(validationData);
		if (excludedPins != null && excludedPins.length > 0) {
			sb.append('*');
			sb.append(String.format("%02d", excludedPins.length));
			sb.append(String.format("%02d", excludedPins[0].length()));
			for (String s : excludedPins) {
				sb.append(s);
			}
		}
		String s = this.sendMessage(sb.toString());
		// 02 - warning pvk double but valid OK
		if (s == null || !s.startsWith(id + "BL02") && !s.startsWith(id + "BL00")) {
			log.log(Level.INFO, "Cannot generate IBM offset: {0}", s);
			return null;
		}
		int i = s.indexOf('F', 8);
		return s.substring(8, i >= 8 ? i : 8 + 12);
	}

	@Override
	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException {
		return this.verifyIBMOffset(offset, pan, pinblockPK, Pinblock.Format.ISO_0, pkLMK, pkType, pvkPairLMK, decimalizationTable, validationData);
	}

	@Override
	public boolean verifyIBMOffset(String offset, String pan, String pinblockPK, Pinblock.Format formatPbPK, String pkLMK, DataKeyType pkType, String pvkPairLMK, String decimalizationTable, String validationData) throws IOException {
		String req = null;
		String resp = null;
		switch (pkType) {
			case TPK:
				req = "DA";
				resp = "DB";
				break;
			case ZPK:
				req = "EA";
				resp = "EB";
				break;
			default:
				throw new IllegalArgumentException("Invalid key type " + pkType);
		}
		String id = this.nextId();
		int pinLen = offset.indexOf('F');
		if (pinLen < 0) pinLen = offset.length();
		String s = this.sendMessage(id + req + pkLMK.toUpperCase() + pvkPairLMK.toUpperCase() + "12" + pinblockPK.toUpperCase() + this.getFormat(formatPbPK)
				+ String.format("%02d", pinLen) + Pinblock.extractAccountNumber(pan)
				+ decimalizationTable.toUpperCase() + 'P' + validationData + offset + (offset.length() < 12 ? "FFFFFFFFFFFF".substring(offset.length()) : ""));
		// 02 - warning pvk double but valid OK
		if (s == null || !s.startsWith(id + resp + "02") && !s.startsWith(id + resp + "00")) {
			log.log(Level.INFO, "Cannot verify IBM offset: {0}", s);
			return false;
		}
		return true;
	}

	@Override
	public boolean loadExcludedPINTable(int pinLength, String... pins) throws IOException {
		String id = this.nextId();
		StringBuilder sb = new StringBuilder(64);
		sb.append(id).append("BM");
		sb.append(String.format("%02d", pins != null ? pins.length : 0));
		sb.append(String.format("%02d", pinLength));
		if (pins != null) {
			for (String pin : pins) {
				if (pin.length() != pinLength) {
					throw new IllegalArgumentException("Invalid PIN argument " + pin);
				}
				sb.append(pin);
			}
		}
		String s = this.sendMessage(sb.toString());
		if (s == null || !s.startsWith(id + "BN00")) {
			log.log(Level.INFO, "Cannot load excluded PIN table: {0}", s);
			return false;
		}
		return true;
	}

	private String getFormat(Pinblock.Format format) {
		switch (format) {
			case ISO_0: return "01";
			case ISO_1: return "05";
			case ISO_3: return "47";
		}
		return null;
	}

}
