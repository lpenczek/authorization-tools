package com.penczek.hsm.server;

public enum KeyType {

	ZMK(4, 0, "000"),
	ZPK(6, 0, "001"),
	TPK(14, 0, "002"),
	TMK(14, 0, "002"),
	TAK(16, 0, "003"),
	ZAK(26, 0, "008"),
	CVK(14, 4, "402"),
	PVK(14, 0, "002"),
	DEK(32, 0, "00B"),
	ZEK(30, 0, "00A"),
	MK_AC(28, 1, "109"),
	MK_SMI(28, 2, "209"),
	MK_SMC(28, 3, "309"),
	DTAB(18, 1, "104")
	;

	private int pairFirstElementIndex;
	private int variant;
	/**
	 * The Key Type code used within commands is formed by using the Variant code as the
	 * first character then the LMK pair code (4/5 = 00, 6/7 = 01, ..., 38/39 = 0E)
	 * as the second character. For example the code for a ZPK is 001.
	 */
	private String code;

	private KeyType(int pi, int v, String c) {
		this.pairFirstElementIndex = pi;
		this.variant = v;
		this.code = c;
	}

	public int getPairFirstElementIndex() {
		return pairFirstElementIndex;
	}

	public int getVariant() {
		return variant;
	}

	public String getCode() {
		return this.code;
	}

	public static KeyType valueOfCode(String code) {
		for (KeyType kt : KeyType.values()) {
			if (kt.code.equals(code)) return kt;
		}
		return null;
	}

}
