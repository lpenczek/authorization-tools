package com.penczek.hsm.server;

import com.penczek.util.HexadecimalStringUtil;
import java.security.InvalidKeyException;
import javax.crypto.Cipher;
import javax.crypto.spec.DESedeKeySpec;

public class EncryptedKey {

	private KeyScheme scheme;
	private byte[] value;

	public EncryptedKey(final KeyScheme scheme, final byte[] value) throws InvalidEncryptedKeyException {
		setScheme(scheme);
		setValue(value);
	}

	public EncryptedKey(String encryptedKey) throws InvalidEncryptedKeyException {
		if (encryptedKey == null) {
			throw new InvalidEncryptedKeyException("Null value");
		}
		encryptedKey = encryptedKey.replaceAll("\\s+", "").toUpperCase();
		if (encryptedKey.isEmpty()) {
			throw new InvalidEncryptedKeyException("Empty value");
		}
		char schemeTag = encryptedKey.charAt(0);
		KeyScheme scheme = KeyScheme.valueOf(schemeTag);
		if (scheme == null) {
			// pode ser ANSIX9.17 (plain) quando 1 caracter � hexadecimal
			if ((schemeTag >= '0' && schemeTag <= '9') || (schemeTag >= 'A' && schemeTag <= 'F')) {
				if (encryptedKey.length() == 8 * 2) {
					scheme = KeyScheme.ANSIX9_17_SINGLE;
				} else if (encryptedKey.length() == 16 * 2) {
					scheme = KeyScheme.ANSIX9_17_DOUBLE;
				} else if (encryptedKey.length() == 24 * 2) {
					scheme = KeyScheme.ANSIX9_17_TRIPLE;
				}
			}
			this.setScheme(scheme);
			this.setValue(HexadecimalStringUtil.bytesFromString(encryptedKey));
		} else {
			this.setScheme(scheme);
			this.setValue(HexadecimalStringUtil.bytesFromString(encryptedKey.substring(1)));
		}
	}

	protected void setScheme(final KeyScheme scheme) throws InvalidEncryptedKeyException {
		if (scheme == null) {
			throw new InvalidEncryptedKeyException("Null scheme");
		}
		this.scheme = scheme;
	}

	public byte[] getValue() {
		return value;
	}

	protected void setValue(final byte[] value) throws InvalidEncryptedKeyException {
		if (this.scheme == null) {
			throw new InvalidEncryptedKeyException("Null scheme");
		}
		if (value == null) {
			throw new InvalidEncryptedKeyException("Null value");
		}
		if (value.length != this.scheme.getSize()) {
			throw new InvalidEncryptedKeyException("Invalid length: expected " + this.scheme.getSize() + " actual " + value.length);
		}
		this.value = value;
	}

	public byte[] decrypt(byte[] key) throws HSMCipherException, InvalidKeyException {
		byte[] clearKey = HSMCipher.doFinal(this.value, this.scheme, key, Cipher.DECRYPT_MODE);
		if (!DESedeKeySpec.isParityAdjusted(HSMCipher.adjustKey(clearKey), 0)) {
			throw new InvalidKeyException("Parity error");
		}
		return clearKey;
	}

	public String toString() {
		StringBuilder s = new StringBuilder(this.value.length * 2 + 1);
		if (this.scheme != KeyScheme.ANSIX9_17_SINGLE) {
			s.append(this.scheme.getTag());
		}
		s.append(HexadecimalStringUtil.bytesToString(this.value));
		return s.toString();
	}

	public String toFormattedString() {
		StringBuilder s = new StringBuilder(this.value.length * 2 + 1 + (this.value.length / 4));
		if (this.scheme != KeyScheme.ANSIX9_17_SINGLE) {
			s.append(this.scheme.getTag());
		}
		String key = HexadecimalStringUtil.bytesToString(this.value);
		for (int i = 0; i < key.length(); i++) {
			if (i > 0 && i % 4 == 0) s.append(' ');
			s.append(key.charAt(i));
		}
		return s.toString();
	}

	public static EncryptedKey encrypt(final byte[] clearkey, final KeyScheme scheme, final byte[] key) throws HSMCipherException, InvalidKeyException, InvalidEncryptedKeyException {
		return new EncryptedKey(scheme, HSMCipher.doFinal(clearkey, scheme, key, Cipher.ENCRYPT_MODE));
	}

	public KeyScheme getScheme() {
		return scheme;
	}

}
