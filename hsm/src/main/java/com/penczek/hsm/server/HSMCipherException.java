package com.penczek.hsm.server;

public class HSMCipherException extends java.lang.Exception {

	public HSMCipherException() {
	}

	public HSMCipherException(String msg) {
		super(msg);
	}

	public HSMCipherException(Throwable cause) {
		super(cause);
	}

	public HSMCipherException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
