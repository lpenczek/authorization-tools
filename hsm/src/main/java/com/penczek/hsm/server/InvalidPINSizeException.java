package com.penczek.hsm.server;

class InvalidPINSizeException extends RuntimeException {

	private final byte size;

	public InvalidPINSizeException(byte size) {
		this.size = size;
	}

	public byte getSize() {
		return size;
	}

}
