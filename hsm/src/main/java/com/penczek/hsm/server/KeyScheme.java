package com.penczek.hsm.server;

public enum KeyScheme {

	ANSIX9_17_SINGLE('Z', 8),
	ANSIX9_17_DOUBLE('X', 16),
	ANSIX9_17_TRIPLE('Y', 24),
	VARIANT_DOUBLE('U', 16),
	VARIANT_TRIPLE('T', 24);

	private char tag;
	private int size;

	private KeyScheme(char t, int s) {
		this.tag = t;
		this.size = s;
	}

	public char getTag() {
		return this.tag;
	}

	public int getSize() {
		return this.size;
	}

	public static KeyScheme valueOf(char tag) {
		for (KeyScheme k : KeyScheme.values()) {
			if (k.tag == tag) return k;
		}
		return null;
	}

}
