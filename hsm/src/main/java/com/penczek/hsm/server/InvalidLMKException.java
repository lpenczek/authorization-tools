package com.penczek.hsm.server;

public class InvalidLMKException extends Exception {

	public InvalidLMKException() {
	}

	public InvalidLMKException(String msg) {
		super(msg);
	}

	public InvalidLMKException(Throwable cause) {
		super(cause);
	}

	public InvalidLMKException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
