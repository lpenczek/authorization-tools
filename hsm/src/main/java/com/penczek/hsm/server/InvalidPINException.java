package com.penczek.hsm.server;

public class InvalidPINException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidPINException() {}

	public InvalidPINException(String message) {
		super(message);
	}

}
