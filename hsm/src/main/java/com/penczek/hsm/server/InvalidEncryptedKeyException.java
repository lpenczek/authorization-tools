package com.penczek.hsm.server;

public class InvalidEncryptedKeyException extends Exception {

	public InvalidEncryptedKeyException() {
	}

	public InvalidEncryptedKeyException(String msg) {
		super(msg);
	}

	public InvalidEncryptedKeyException(Throwable cause) {
		super(cause);
	}

	public InvalidEncryptedKeyException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
