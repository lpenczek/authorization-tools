package com.penczek.hsm.server;

import java.security.InvalidKeyException;
import java.security.SecureRandom;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class HSMCipher {

	private static final Cipher DESEDE;
	private static final Cipher HOST_STORAGE_DESEDE;
	private static final Cipher DESEDE_CBC;
	private static final Cipher DES;
	private static final Cipher DES_CBC;
	private static final SecretKeyFactory DESEDE_KEY_FACTORY;
	private static final SecretKeyFactory DES_KEY_FACTORY;

	static {
		Cipher c = null;
		Cipher d = null;
		Cipher e = null;
		Cipher f = null;
		Cipher g = null;
		SecretKeyFactory kf = null;
		SecretKeyFactory dkf = null;
		try {
			c = Cipher.getInstance("DESede/ECB/NoPadding");
			d = Cipher.getInstance("DESede/CFB/NoPadding");
			e = Cipher.getInstance("DESede/CBC/NoPadding");
			f = Cipher.getInstance("DES/ECB/NoPadding");
			g = Cipher.getInstance("DES/CBC/NoPadding");
			kf = SecretKeyFactory.getInstance("DESede");
			dkf = SecretKeyFactory.getInstance("DES");
		} catch (Exception ex) {
			throw new RuntimeException("Cannot initialize DESede cryptosystem", ex);
		}
		DESEDE = c;
		HOST_STORAGE_DESEDE = d;
		DESEDE_CBC = e;
		DES = f;
		DES_CBC = g;
		DESEDE_KEY_FACTORY = kf;
		DES_KEY_FACTORY = dkf;
	}

	private LMK lmk;

	/** Creates a new instance of HSMCipher */
	public HSMCipher(LMK lmk) {
		this.lmk = lmk;
	}

	public byte[] encryptPINLMK(byte[] pin) throws HSMCipherException {
		try {
			byte[] desede = adjustKey(this.lmk.getPair(2));
			SecretKey p23 = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(desede));
			synchronized (HOST_STORAGE_DESEDE) {
				HOST_STORAGE_DESEDE.init(Cipher.ENCRYPT_MODE, p23, new IvParameterSpec(Arrays.copyOfRange(desede, 0, 8)));
				return HOST_STORAGE_DESEDE.doFinal(pin);
			}
		} catch (Exception e) {
			throw new HSMCipherException(e);
		}
	}

	public byte[] decryptPINLMK(byte[] pin) throws HSMCipherException {
		try {
			byte[] desede = adjustKey(this.lmk.getPair(2));
			SecretKey p23 = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(desede));
			synchronized (HOST_STORAGE_DESEDE) {
				HOST_STORAGE_DESEDE.init(Cipher.DECRYPT_MODE, p23, new IvParameterSpec(Arrays.copyOfRange(desede, 0, 8)));
				return HOST_STORAGE_DESEDE.doFinal(pin);
			}
		} catch (Exception e) {
			throw new HSMCipherException(e);
		}
	}

	public byte[] encryptBlock(byte[] block, EncryptedKey key, KeyType type) throws HSMCipherException, InvalidKeyException {
		return this.doBlock(block, null, key, type, DESEDE, Cipher.ENCRYPT_MODE);
	}

	public byte[] decryptBlock(byte[] block, EncryptedKey key, KeyType type) throws HSMCipherException, InvalidKeyException {
		return this.doBlock(block, null, key, type, DESEDE, Cipher.DECRYPT_MODE);
	}

	public byte[] encryptBlock(byte[] block, byte[] clearKey) throws HSMCipherException, InvalidKeyException {
		return this.doBlock(block, null, clearKey, DESEDE, Cipher.ENCRYPT_MODE);
	}

	public byte[] decryptBlock(byte[] block, byte[] clearKey) throws HSMCipherException, InvalidKeyException {
		return this.doBlock(block, null, clearKey, DESEDE, Cipher.DECRYPT_MODE);
	}

	public byte[] encryptDESedeCBCBlock(byte[] block, byte[] iv, EncryptedKey key, KeyType type) throws HSMCipherException, InvalidKeyException {
		return this.doBlock(block, iv, key, type, DESEDE_CBC, Cipher.ENCRYPT_MODE);
	}

	public byte[] decryptDESedeCBCBlock(byte[] block, byte[] iv, EncryptedKey key, KeyType type) throws HSMCipherException, InvalidKeyException {
		return this.doBlock(block, iv, key, type, DESEDE_CBC, Cipher.DECRYPT_MODE);
	}

	private byte[] doBlock(byte[] block, byte[] iv, EncryptedKey key, KeyType type, Cipher c, int mode) throws HSMCipherException, InvalidKeyException {
		return this.doBlock(block, iv, key.decrypt(this.lmk.getPair(type)), c, mode);
	}

	private byte[] doBlock(byte[] block, byte[] iv, byte[] clearKey, Cipher c, int mode) throws HSMCipherException, InvalidKeyException {
		clearKey = adjustKey(clearKey);
		try {
			SecretKey secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(clearKey));
			synchronized (c) {
				if (iv != null) {
					c.init(mode, secretKey, new IvParameterSpec(iv));
				} else {
					c.init(mode, secretKey);
				}
				return c.doFinal(block);
			}
		} catch (Exception e) {
			throw new HSMCipherException(e);
		}
	}

	public byte[] encryptDESBlock(byte[] block, EncryptedKey key, KeyType keyType) throws InvalidKeyException, HSMCipherException {
		return this.encryptDESBlock(block, key, keyType, 0);
	}

	public byte[] encryptDESBlock(byte[] block, EncryptedKey key, KeyType keyType, int keyoffset) throws InvalidKeyException, HSMCipherException {
		return this.doDESBlock(block, key, keyType, keyoffset, Cipher.ENCRYPT_MODE);
	}

	public byte[] encryptDESBlock(byte[] block, byte[] clearKey) throws InvalidKeyException, HSMCipherException {
		return this.encryptDESBlock(block, clearKey, 0);
	}

	public byte[] encryptDESBlock(byte[] block, byte[] clearKey, int keyoffset) throws InvalidKeyException, HSMCipherException {
		return this.doDESBlock(block, clearKey, keyoffset, Cipher.ENCRYPT_MODE);
	}

	public byte[] decryptDESBlock(byte[] block, EncryptedKey key, KeyType keyType) throws InvalidKeyException, HSMCipherException {
		return this.decryptDESBlock(block, key, keyType, 0);
	}

	public byte[] decryptDESBlock(byte[] block, EncryptedKey key, KeyType keyType, int keyoffset) throws InvalidKeyException, HSMCipherException {
		return this.doDESBlock(block, key, keyType, keyoffset, Cipher.DECRYPT_MODE);
	}

	public byte[] decryptDESBlock(byte[] block, byte[] clearKey) throws InvalidKeyException, HSMCipherException {
		return this.decryptDESBlock(block, clearKey, 0);
	}

	public byte[] decryptDESBlock(byte[] block, byte[] clearKey, int keyoffset) throws InvalidKeyException, HSMCipherException {
		return this.doDESBlock(block, clearKey, keyoffset, Cipher.DECRYPT_MODE);
	}

	private byte[] doDESBlock(byte[] block, EncryptedKey key, KeyType keyType, int keyoffset, int mode) throws HSMCipherException, InvalidKeyException {
		return this.doDESBlock(block, adjustKey(key.decrypt(this.lmk.getPair(keyType))), keyoffset, mode);
	}

	private byte[] doDESBlock(byte[] block, byte[] clearKey, int keyoffset, int mode) throws HSMCipherException, InvalidKeyException {
		clearKey = adjustKey(clearKey);
		try {
			// usa apenas os 8 primeiros bytes
			SecretKey secretKey = DES_KEY_FACTORY.generateSecret(new DESKeySpec(clearKey, keyoffset));
			synchronized (DES) {
				DES.init(mode, secretKey);
				return DES.doFinal(block);
			}
		} catch (Exception e) {
			throw new HSMCipherException(e);
		}
	}

	public byte[] encryptDESCBCBlock(byte[] block, byte[] iv, EncryptedKey key, KeyType keyType) throws InvalidKeyException, HSMCipherException {
		return this.encryptDESCBCBlock(block, iv, key.decrypt(this.lmk.getPair(keyType)));
	}

	public byte[] encryptDESCBCBlock(byte[] block, byte[] iv, byte[] clearKey) throws InvalidKeyException, HSMCipherException {
		try {
			// usa apenas os 8 primeiros bytes
			SecretKey secretKey = DES_KEY_FACTORY.generateSecret(new DESKeySpec(clearKey));
			synchronized (DES_CBC) {
				DES_CBC.init(Cipher.ENCRYPT_MODE, secretKey, new IvParameterSpec(iv));
				return DES_CBC.doFinal(block);
			}
		} catch (Exception e) {
			throw new HSMCipherException(e);
		}
	}

	/**
	 * The key check value (KCV) is the result (the first few digits (up to
	 * 6 normally)) of DES encrypting x'0000000000000000' under the key.
	 */
	public byte[] calculateKeyCheckValue(byte[] key) throws InvalidKeyException, HSMCipherException {
		key = adjustKey(key);
		try {
			SecretKey secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(key));
			synchronized (DESEDE) {
				DESEDE.init(Cipher.ENCRYPT_MODE, secretKey);
				return DESEDE.doFinal(new byte[] {0, 0, 0, 0, 0, 0, 0, 0});
			}
		} catch (Exception e) {
			throw new HSMCipherException(e);
		}
	}

	public byte[] generateDESedeKey(KeyScheme scheme) {
		byte[] key = null;
		SecureRandom rand = new SecureRandom();
		switch (scheme) {
			case ANSIX9_17_SINGLE:
				rand.nextBytes(key = new byte[8]);
				break;
			case ANSIX9_17_DOUBLE:
			case VARIANT_DOUBLE:
				rand.nextBytes(key = new byte[16]);
				break;
			case ANSIX9_17_TRIPLE:
			case VARIANT_TRIPLE:
				rand.nextBytes(key = new byte[24]);
				break;
		}
		// ajusta paridade impar
		for (int i = 0; i < key.length; i++) {
			// conta quantos 1 nos bits mais significativos
			int r = 0;
			for (int j = 7; j >= 0; j--) {
				int w = (key[i] & (1 << j));
				if (w != 0) r++;
			}
			if (r % 2 == 0) {
				key[i] ^= 1;
			}
		}
		return key;
	}

	public static byte[] doFinal(byte[] value, KeyScheme scheme, byte[] keyb, int mode) throws HSMCipherException, InvalidKeyException {
		keyb = adjustKey(keyb);
		byte y1 = keyb[8];
		byte[] result = null;
		SecretKey secretKey;
		try {
			if (scheme == KeyScheme.VARIANT_DOUBLE) {
				// double variant
				// U AAAA AAAA AAAA AAAA   BBBB BBBB BBBB BBBB
				// variant 1 - A6 - para AAAA AAAA AAAA AAAA
				keyb[8] = (byte) (y1 ^ 0xA6); // original xor A6
				secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(keyb));
				byte[] aaaa;
				synchronized (DESEDE) {
					DESEDE.init(mode, secretKey);
					aaaa = DESEDE.doFinal(Arrays.copyOfRange(value, 0, 8));
				}
				// variant 2 - 5A - para BBBB BBBB BBBB BBBB
				keyb[8] = (byte) (y1 ^ 0x5A); // original xor 5A
				secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(keyb));
				byte[] bbbb;
				synchronized (DESEDE) {
					DESEDE.init(mode, secretKey);
					bbbb = DESEDE.doFinal(Arrays.copyOfRange(value, 8, 16));
				}
				result = new byte[16];
				System.arraycopy(aaaa, 0, result, 0, 8);
				System.arraycopy(bbbb, 0, result, 8, 8);
			} else if (scheme == KeyScheme.VARIANT_TRIPLE) {
				// triple variant
				// T AAAA AAAA AAAA AAAA   BBBB BBBB BBBB BBBB   CCCC CCCC CCCC CCCC
				// variant 1 - 6A - para AAAA AAAA AAAA AAAA
				keyb[8] = (byte) (y1 ^ 0x6A); // original xor 6A
				secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(keyb));
				byte[] aaaa;
				synchronized (DESEDE) {
					DESEDE.init(mode, secretKey);
					aaaa = DESEDE.doFinal(Arrays.copyOfRange(value, 0, 8));
				}
				// variant 2 - DE - para BBBB BBBB BBBB BBBB
				keyb[8] = (byte) (y1 ^ 0xDE); // original xor DE
				secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(keyb));
				byte[] bbbb;
				synchronized (DESEDE) {
					DESEDE.init(mode, secretKey);
					bbbb = DESEDE.doFinal(Arrays.copyOfRange(value, 8, 16));
				}
				// variant 3 - 2B - para CCCC CCCC CCCC CCCC
				keyb[8] = (byte) (y1 ^ 0x2B); // original xor 2B
				secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(keyb));
				byte[] cccc;
				synchronized (DESEDE) {
					DESEDE.init(mode, secretKey);
					cccc = DESEDE.doFinal(Arrays.copyOfRange(value, 16, 24));
				}
				result = new byte[24];
				System.arraycopy(aaaa, 0, result, 0, 8);
				System.arraycopy(bbbb, 0, result, 8, 8);
				System.arraycopy(cccc, 0, result, 16, 8);
			} else {
				// ANSI X9.17 - DESede ECB normal
				secretKey = DESEDE_KEY_FACTORY.generateSecret(new DESedeKeySpec(keyb));
				synchronized (DESEDE) {
					DESEDE.init(mode, secretKey);
					result = DESEDE.doFinal(value);
				}
			}
		} catch (Exception e) {
			throw new HSMCipherException(e);
		}
		return result;
	}

	public static byte[] adjustKey(final byte[] key) throws InvalidKeyException {
		if (key.length == 24) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 24);
			return kn;
		}
		if (key.length == 8) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 8);
			System.arraycopy(key, 0, kn, 8, 8);
			System.arraycopy(key, 0, kn, 16, 8);
			return kn;
		}
		if (key.length == 16) {
			byte[] kn = new byte[24];
			System.arraycopy(key, 0, kn, 0, 16);
			System.arraycopy(key, 0, kn, 16, 8);
			return kn;
		}
		throw new InvalidKeyException("Invalid length " + key.length);
	}

}
