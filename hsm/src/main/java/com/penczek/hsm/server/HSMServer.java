package com.penczek.hsm.server;

import static com.penczek.hsm.Pinblock.Format.ISO_0;
import static com.penczek.hsm.Pinblock.Format.ISO_1;
import static com.penczek.hsm.Pinblock.Format.ISO_3;

import com.penczek.hsm.Pinblock;
import com.penczek.util.HexadecimalStringUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.security.InvalidKeyException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class HSMServer {

	public static void main(String[] args) throws Exception {
		System.out.println("Starting server....");
		int port;
		try {
			port = Integer.parseInt(args[0], 10);
		} catch (Exception e) {
			port = 1500;
		}
		new HSMServer(port).start();
	}

	LMK lmk;
	HSMCipher cipher;
	private ServerSocket server;
	private final AtomicInteger request = new AtomicInteger(0);
	private final boolean doubleLengthZMKs = true;
	private final String[][] globalExcludedPIN = new String[12][];
	private final int port;
	private final PrintStream out;

	public HSMServer() throws InvalidLMKException, IOException {
		this(1500, System.out);
	}

	/** Creates a new instance of HSM */
	public HSMServer(int port) throws InvalidLMKException, IOException {
		this(port, System.out);
  }

	/** Creates a new instance of HSM */
	public HSMServer(int port, PrintStream out) throws InvalidLMKException, IOException {
    this.lmk = new LMK(LMK.TEST_LMK);
		this.cipher = new HSMCipher(this.lmk);
		this.port = port;
		this.out = out;
	}

	public void start() {
		new Thread() {
      @Override
      public void run() {
				_start();
      }

		}.start();
  }

	void _start() {
		out.println("Server started");
		try {
			this.server = new ServerSocket(port);
			Socket client;
			while ((client = this.server.accept()) != null) {
				out.println("Client accepted " + client.getRemoteSocketAddress());
				new ClientHandler(this, client).start();
			}
		} catch (Exception e) {
      e.printStackTrace(out);
		} finally {
			if (this.server != null) try { this.server.close(); } catch (Exception e) {}
		}
	}

	public void stop() {
		try { this.server.close(); } catch (Exception ex) {}
	}

	class ClientHandler extends Thread {

		private HSMServer server;
		private Socket socket;

		ClientHandler(HSMServer server, Socket socket) {
			this.server = server;
			this.socket = socket;
		}

		@Override
		public void run() {
			try {
				this.execute();
			} catch (SocketException e) {
				if (!"Connection reset".equals(e.getMessage())) {
					e.printStackTrace(out);
				}
			} catch (Exception e) {
				e.printStackTrace(out);
			} finally {
				if (this.socket != null) try { this.socket.close(); } catch (Exception e) {}
				this.socket = null;
			}
		}

		private void execute() throws IOException {
			InputStream is = this.socket.getInputStream();
			OutputStream os = this.socket.getOutputStream();
			while (!this.socket.isClosed()) {
				int size = is.read(); // 8 most significant bits do tamanho
				if (size < 0) {
					throw new IOException("Fim prematuro da conexao - byte 1: " + size);
				}
				int i = is.read(); // 8 least significant bits do tamanho
				if (i < 0) {
					throw new IOException("Fim prematuro da conexao - byte 2: " + i);
				}
				// calcula size corretamente
				size = (size << 8) | i;
				byte[] resp = new byte[size];
				for (int off = 0; off < size; off += i) {
					i = is.read(resp, off, size - off);
					if (i < 0) break;
				}
				String command = new String(resp, 0);
				command = this.server.execute(command);
				size = command.length();
				os.write(size >>> 8);
				os.write(size);
				byte[] buf = new byte[command.length()];
				command.getBytes(0, command.length(), buf, 0);
				os.write(buf);
				os.flush();
				request.incrementAndGet();
			}
		}
	}

	private String execute(String command) {
		String cmd = command.substring(4, 6);
		if (cmd.equals("HC")) {
			int i = command.indexOf(';');
			try {
				String tmkLMK = command.substring(6, i);
				KeyScheme tmkScheme = KeyScheme.valueOf(command.charAt(i + 1));
				KeyScheme lmkScheme = KeyScheme.valueOf(command.charAt(i + 2));
				String[] tpk = this.generateKey(tmkLMK, KeyType.TMK, tmkScheme, lmkScheme, KeyType.TPK);
				return command.substring(0, 4) + "HD00" + tpk[0] + tpk[1];
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "HD15";
			}
		} else if (cmd.equals("IA")) {
			int i = command.indexOf(';');
			try {
				String zmkLMK = command.substring(6, i);
				// TODO: doubleKeyZMKs
				KeyScheme zmkScheme = KeyScheme.valueOf(command.charAt(i + 1));
				KeyScheme lmkScheme = KeyScheme.valueOf(command.charAt(i + 2));
				char kcv = command.charAt(i + 3);
				String[] zpk = this.generateKey(zmkLMK, KeyType.ZMK, zmkScheme, lmkScheme, KeyType.ZPK);
				return command.substring(0, 4) + "IB00" + zpk[0] + zpk[1] + (kcv == '0' ? zpk[2] : zpk[2].substring(0, 6));
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "IB15";
			}
		} else if (cmd.equals("CC")) {
			try {
				KeyScheme szpks = KeyScheme.valueOf(command.charAt(6));
				String szpk;
				if (szpks == null) {
					szpk = command.substring(6, 22);
				} else {
					szpk = command.substring(6, 6 + 1 + szpks.getSize() * 2);
				}
				int offset = 6 + szpk.length();
				KeyScheme dzpks = KeyScheme.valueOf(command.charAt(offset));
				String dzpk;
				if (dzpks == null) {
					dzpk = command.substring(offset, offset + 16);
				} else {
					dzpk = command.substring(offset, offset + 1 + dzpks.getSize() * 2);
				}
				offset += dzpk.length() + 2;
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format sourceFormat = this.getFormat(command, offset);
				offset += 2;
				Pinblock.Format destFormat = this.getFormat(command, offset);
				offset += 2;
				String sourcePan = command.substring(offset, offset + 12);
				offset += 12;
				String destPan = sourcePan;
				if (command.length() > offset && command.charAt(offset++) == ';') {
					destPan = command.substring(offset, offset + 12);
				}
				String pb = this.translatePinblock(sourcePan, pinblock, sourceFormat, szpk, KeyType.ZPK, destPan, destFormat, dzpk, KeyType.ZPK);
				return command.substring(0, 4) + "CD0004" + pb + this.getFormat(destFormat);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "CD15";
			}
		} else if (cmd.equals("CA")) {
			try {
				KeyScheme stpks = KeyScheme.valueOf(command.charAt(6));
				String stpk;
				if (stpks == null) {
					stpk = command.substring(6, 22);
				} else {
					stpk = command.substring(6, 6 + 1 + stpks.getSize() * 2);
				}
				int offset = 6 + stpk.length();
				KeyScheme dzpks = KeyScheme.valueOf(command.charAt(offset));
				String dzpk;
				if (dzpks == null) {
					dzpk = command.substring(offset, offset + 16);
				} else {
					dzpk = command.substring(offset, offset + 1 + dzpks.getSize() * 2);
				}
				offset += dzpk.length() + 2;
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format sourceFormat = this.getFormat(command, offset);
				offset += 2;
				Pinblock.Format destFormat = this.getFormat(command, offset);
				offset += 2;
				String sourcePan = command.substring(offset, offset + 12);
				offset += 12;
				String destPan = sourcePan;
				if (command.length() > offset && command.charAt(offset++) == ';') {
					destPan = command.substring(offset, offset + 12);
				}
				String pb = this.translatePinblock(sourcePan, pinblock, sourceFormat, stpk, KeyType.TPK, destPan, destFormat, dzpk, KeyType.ZPK);
				return command.substring(0, 4) + "CB0004" + pb + this.getFormat(destFormat);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "CB15";
			}
		} else if (cmd.equals("BC")) {
			try {
				KeyScheme stpks = KeyScheme.valueOf(command.charAt(6));
				String stpk;
				if (stpks == null) {
					stpk = command.substring(6, 22);
				} else {
					stpk = command.substring(6, 6 + 1 + stpks.getSize() * 2);
				}
				int offset = 6 + stpk.length();
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format format = this.getFormat(command, offset);
				offset += 2;
				String pan = command.substring(offset, offset + 12);
				offset += 12;
				String pinLMK = command.substring(offset);
				return command.substring(0, 4) + "BD" + (this.verifyPinblock(pan, pinblock, format, stpk, pinLMK) ? "00" : "01");
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "BD15";
			}
		} else if (cmd.equals("JE")) {
			try {
				KeyScheme szpks = KeyScheme.valueOf(command.charAt(6));
				String szpk;
				if (szpks == null) {
					szpk = command.substring(6, 22);
				} else {
					szpk = command.substring(6, 6 + 1 + szpks.getSize() * 2);
				}
				int offset = 6 + szpk.length();
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format format = this.getFormat(command, offset);
				offset += 2;
				String pan = command.substring(offset, offset + 12);
				return command.substring(0, 4) + "JF00" + this.translatePinblockZPKtoLMK(pan, pinblock, format, szpk);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "JF15";
			}
		} else if (cmd.equals("CW")) {
			try {
				KeyScheme scvks = KeyScheme.valueOf(command.charAt(6));
				String scvk;
				if (scvks == null) {
					scvk = command.substring(6, 38);
				} else {
					scvk = command.substring(6, 6 + 1 + scvks.getSize() * 2);
				}
				int offset = 6 + scvk.length();
				int sci = command.indexOf(';', offset);
				String pan = command.substring(offset, sci);
				offset = sci + 1;
				String expire = command.substring(offset, offset + 4);
				offset += 4;
				String serviceCode = command.substring(offset, offset + 3);
				return command.substring(0, 4) + "CX00" + this.generateCVV(scvk, pan, expire, serviceCode);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "CX15";
			}
		} else if (cmd.equals("CY")) {
			try {
				KeyScheme scvks = KeyScheme.valueOf(command.charAt(6));
				String scvk;
				if (scvks == null) {
					scvk = command.substring(6, 38);
				} else {
					scvk = command.substring(6, 6 + 1 + scvks.getSize() * 2);
				}
				int offset = 6 + scvk.length();
				String cvv = command.substring(offset, offset + 3);
				offset += 3;
				int sci = command.indexOf(';', offset);
				String pan = command.substring(offset, sci);
				offset = sci + 1;
				String expire = command.substring(offset, offset + 4);
				offset += 4;
				String serviceCode = command.substring(offset, offset + 3);
				return command.substring(0, 4) + "CZ" + (this.verifyCVV(scvk, cvv, pan, expire, serviceCode) ? "00" : "01");
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "CZ15";
			}
		} else if (cmd.equals("AS")) {
			int i = command.indexOf(';');
			KeyScheme lmkScheme = KeyScheme.ANSIX9_17_DOUBLE;
			boolean keyCheckOld = true;
			if (i >= 0) {
				if (command.length() > i + 2 && command.charAt(i + 2) == KeyScheme.VARIANT_DOUBLE.getTag()) {
					lmkScheme = KeyScheme.VARIANT_DOUBLE;
				}
				if (command.length() > i + 3) {
					keyCheckOld = command.charAt(i + 3) == '0';
				}
			}
			try {
				String[] cvk = this.generateCVK(lmkScheme, keyCheckOld);
				return command.substring(0, 4) + "AT00" + cvk[0] + cvk[1];
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "AT15";
			}
		} else if (cmd.equals("AU")) {
			try {
				KeyScheme szmks = KeyScheme.valueOf(command.charAt(6));
				String szmk;
				if (szmks == null) {
					szmk = command.substring(6, this.doubleLengthZMKs ? 38 : 22);
				} else {
					szmk = command.substring(6, 6 + 1 + szmks.getSize() * 2);
				}
				int offset = 6 + szmk.length();
				KeyScheme dcvks = KeyScheme.valueOf(command.charAt(offset));
				String dcvk;
				if (dcvks == null) {
					dcvk = command.substring(offset, offset + 32);
				} else {
					dcvk = command.substring(offset, offset + 1 + dcvks.getSize() * 2);
				}
				offset += dcvk.length();
				int i = command.indexOf(';', offset);
				KeyScheme zmkScheme = KeyScheme.ANSIX9_17_DOUBLE;
				boolean keyCheckOld = true;
				if (i >= 0) {
					if (command.length() > i + 1 && command.charAt(i + 1) == KeyScheme.VARIANT_DOUBLE.getTag()) {
						zmkScheme = KeyScheme.VARIANT_DOUBLE;
					}
					if (command.length() > i + 3) {
						keyCheckOld = command.charAt(i + 3) == '0';
					}
				}
				String[] cvks = this.translateCVKtoZMK(szmk, dcvk, zmkScheme, keyCheckOld);
				return command.substring(0, 4) + "AV00" + cvks[0] + cvks[1];
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "AV15";
			}
		} else if (cmd.equals("FW")) {
			try {
				// tpk/zpk
				KeyType keyType = KeyType.valueOfCode(command.substring(6, 9));
				KeyScheme spks = KeyScheme.valueOf(command.charAt(9));
				String spk;
				if (spks == null) {
					spk = command.substring(9, 25);
				} else {
					spk = command.substring(9, 9 + 1 + spks.getSize() * 2);
				}
				int offset = 9 + spk.length();
				// pvk
				KeyScheme spvks = KeyScheme.valueOf(command.charAt(offset));
				String spvk;
				if (spvks == null) {
					spvk = command.substring(offset, offset + 32);
				} else {
					spvk = command.substring(offset, offset + 1 + spvks.getSize() * 2);
				}
				offset += spvk.length();
				// pinblock
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format format = this.getFormat(command, offset);
				offset += 2;
				// pan
				String pan = command.substring(offset, offset + 12);
				offset += 12;
				// pvvi
				int pvvi = command.charAt(offset) - '0';
				offset++;
				String[] excluded = null;
				if (command.length() > offset && command.charAt(offset) == '*') {
					offset++;
					int pinCount = Integer.parseInt(command.substring(offset, offset + 2), 10);
					offset += 2;
					int pinLen = Integer.parseInt(command.substring(offset, offset + 2), 10);
					offset += 2;
					int total = offset + pinCount * pinLen;
					excluded = new String[pinCount];
					for (int i = 0; offset < total; i++) {
						excluded[i] = command.substring(offset, offset + pinLen);
						offset += pinLen;
					}
				}
				return command.substring(0, 4) + "FX00" + this.generatePVV(pan, pinblock, format, spk, keyType, pan, pvvi, spvk, excluded);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "FX15";
			}
		} else if (cmd.equals("DC") || cmd.equals("EC")) {
			KeyType pkType = cmd.equals("DC") ? KeyType.TPK : KeyType.ZPK;
			try {
				KeyScheme spks = KeyScheme.valueOf(command.charAt(6));
				String spk;
				if (spks == null) {
					spk = command.substring(6, 22);
				} else {
					spk = command.substring(6, 6 + 1 + spks.getSize() * 2);
				}
				int offset = 6 + spk.length();
				// pvk
				KeyScheme spvks = KeyScheme.valueOf(command.charAt(offset));
				String spvk;
				if (spvks == null) {
					spvk = command.substring(offset, offset + 32);
				} else {
					spvk = command.substring(offset, offset + 1 + spvks.getSize() * 2);
				}
				offset += spvk.length();
				// pinblock
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format format = this.getFormat(command, offset);
				offset += 2;
				// pan
				String sourcePan = command.substring(offset, offset + 12);
				offset += 12;
				String destPan = sourcePan;
				// pvvi or destpan ';'
				char c = command.charAt(offset++);
				if (c == ';') {
					destPan = command.substring(offset, offset + 12);
					offset += 12;
					// pvvi
					c = command.charAt(offset++);
				}
				int pvvi = c - '0';
				String pvv = command.substring(offset, offset + 4);
				return command.substring(0, 4) + (pkType == KeyType.TPK ? "DD" : "ED") + (this.verifyPVV(sourcePan, pinblock, format, spk, pkType, destPan, pvvi, pvv, spvk) ? "00" : "01");
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + (pkType == KeyType.TPK ? "DD" : "ED") + "15";
			}
		} else if (cmd.equals("BK")) {
			try {
				// tpk/zpk
				KeyType keyType = KeyType.valueOfCode(command.substring(6, 9));
				KeyScheme spks = KeyScheme.valueOf(command.charAt(9));
				String spk;
				if (spks == null) {
					spk = command.substring(9, 25);
				} else {
					spk = command.substring(9, 9 + 1 + spks.getSize() * 2);
				}
				int offset = 9 + spk.length();
				// pvk
				KeyScheme spvks = KeyScheme.valueOf(command.charAt(offset));
				String spvk;
				if (spvks == null) {
					spvk = command.substring(offset, offset + 32);
				} else {
					spvk = command.substring(offset, offset + 1 + spvks.getSize() * 2);
				}
				offset += spvk.length();
				// pinblock
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format format = this.getFormat(command, offset);
				offset += 2;
				int checkLength = Integer.parseInt(command.substring(offset, offset + 2), 16);
				offset += 2;
				// pan
				String pan = command.substring(offset, offset + 12);
				offset += 12;
				// dec table
				String decTable = command.substring(offset, offset + 16);
				offset += 16;
				// pin validation data
				String validationData;
				if (command.charAt(offset) == 'P') {
					// 'P' + 16H
					validationData = command.substring(offset + 1, offset + 17);
					offset += 17;
				} else {
					// 12 (N = 5 last digits account number)
					validationData = command.substring(offset, offset + 12);
					offset += 12;
					validationData = validationData.replace("N", pan.substring(pan.length() - 5));
				}
				String[] excluded = null;
				if (command.length() > offset && command.charAt(offset) == '*') {
					offset++;
					int pinCount = Integer.parseInt(command.substring(offset, offset + 2), 10);
					offset += 2;
					int pinLen = Integer.parseInt(command.substring(offset, offset + 2), 10);
					offset += 2;
					int total = offset + pinCount * pinLen;
					excluded = new String[pinCount];
					for (int i = 0; offset < total; i++) {
						excluded[i] = command.substring(offset, offset + pinLen);
						offset += pinLen;
					}
				}
				return command.substring(0, 4) + "BL00" + this.generateIBMOffset(pan, pinblock, format, spk, keyType, decTable, validationData, checkLength, spvk, excluded);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "BL15";
			}
		} else if (cmd.equals("DA") || cmd.equals("EA")) {
			KeyType pkType = cmd.equals("DA") ? KeyType.TPK : KeyType.ZPK;
			try {
				KeyScheme spks = KeyScheme.valueOf(command.charAt(6));
				String spk;
				if (spks == null) {
					spk = command.substring(6, 22);
				} else {
					spk = command.substring(6, 6 + 1 + spks.getSize() * 2);
				}
				int offset = 6 + spk.length();
				// pvk
				KeyScheme spvks = KeyScheme.valueOf(command.charAt(offset));
				String spvk;
				if (spvks == null) {
					spvk = command.substring(offset, offset + 32);
				} else {
					spvk = command.substring(offset, offset + 1 + spvks.getSize() * 2);
				}
				offset += spvk.length() + 2;
				// pinblock
				String pinblock = command.substring(offset, offset + 16);
				offset += 16;
				Pinblock.Format format = this.getFormat(command, offset);
				offset += 2;
				int checkLength = Integer.parseInt(command.substring(offset, offset + 2), 16);
				offset += 2;
				// pan
				String pan = command.substring(offset, offset + 12);
				offset += 12;
				// dec table
				String decTable = command.substring(offset, offset + 16);
				offset += 16;
				// pin validation data
				String validationData;
				if (command.charAt(offset) == 'P') {
					// 'P' + 16H
					validationData = command.substring(offset + 1, offset + 17);
					offset += 17;
				} else {
					// 12 (N = 5 last digits account number)
					validationData = command.substring(offset, offset + 12);
					offset += 12;
					validationData = validationData.replace("N", pan.substring(pan.length() - 5));
				}
				// offset
				String ibm = command.substring(offset, offset + 12);
				return command.substring(0, 4) + (pkType == KeyType.TPK ? "DB" : "EB") + (this.verifyIBMOffset(pan, pinblock, format, spk, pkType, decTable, validationData, checkLength, spvk, ibm) ? "00" : "01");
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + (pkType == KeyType.TPK ? "DB" : "EB") + "15";
			}
		} else if (cmd.equals("HA")) {
			int i = command.indexOf(';');
			try {
				String tmkLMK = command.substring(6, i);
				KeyScheme tmkScheme = KeyScheme.valueOf(command.charAt(i + 1));
				KeyScheme lmkScheme = KeyScheme.valueOf(command.charAt(i + 2));
				String[] tak = this.generateKey(tmkLMK, KeyType.TMK, tmkScheme, lmkScheme, KeyType.TAK);
				return command.substring(0, 4) + "HB00" + tak[0] + tak[1];
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "HB15";
			}
		} else if (cmd.equals("FI")) {
			int i = command.indexOf(';');
			try {
				boolean isZak = command.charAt(6) == '1';
				String zmkLMK = command.substring(7, i);
				// TODO: doubleKeyZMKs
				KeyScheme zmkScheme = KeyScheme.valueOf(command.charAt(i + 1));
				KeyScheme lmkScheme = KeyScheme.valueOf(command.charAt(i + 2));
				char kcv = command.charAt(i + 3);
				String[] zaek = this.generateKey(zmkLMK, KeyType.ZMK, zmkScheme, lmkScheme, isZak ? KeyType.ZAK : KeyType.ZEK);
				return command.substring(0, 4) + "FJ00" + zaek[0] + zaek[1] + (kcv == '0' ? zaek[2] : zaek[2].substring(0, 6));
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "FJ15";
			}
		} else if (cmd.equals("M6")) {
			try {
				int stage = command.charAt(6) - '0';
				int macAlg = command.charAt(9) - '0';
				int padAlg = command.charAt(10) - '0';
				KeyType keyType = KeyType.valueOfCode(command.substring(11, 14));
				KeyScheme saks = KeyScheme.valueOf(command.charAt(14));
				String sak;
				if (saks == null) {
					sak = command.substring(14, 30);
				} else {
					sak = command.substring(14, 14 + 1 + saks.getSize() * 2);
				}
				int offset = 14 + sak.length();
				String iv = null;
				if (stage == 2 || stage == 3) {
					iv = command.substring(offset, offset + 16);
					offset += 16;
				}
				int length = Integer.parseInt(command.substring(offset, offset + 4), 16);
				offset += 4;
				byte[] data = new byte[length];
				command.getBytes(offset, offset + length, data, 0);
				if (padAlg == 1) {
					data = this.pad1(data);
				} else if (padAlg == 2) {
					data = this.pad2(data);
				}
				String macOrIv = macAlg == 3
						? this.generateMACAlg3(data, stage, iv != null ? HexadecimalStringUtil.bytesFromString(iv) : null, sak, keyType)
						: this.generateMACAlg1(data, stage, iv != null ? HexadecimalStringUtil.bytesFromString(iv) : null, sak, keyType);
				return command.substring(0, 4) + "M700" + (stage == 0 || stage == 3 ? macOrIv.substring(0, 8) : macOrIv);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "M715";
			}
		} else if (cmd.equals("M8")) {
			try {
				int stage = command.charAt(6) - '0';
				int macAlg = command.charAt(9) - '0';
				int padAlg = command.charAt(10) - '0';
				KeyType keyType = KeyType.valueOfCode(command.substring(11, 14));
				KeyScheme saks = KeyScheme.valueOf(command.charAt(14));
				String sak;
				if (saks == null) {
					sak = command.substring(14, 30);
				} else {
					sak = command.substring(14, 14 + 1 + saks.getSize() * 2);
				}
				int offset = 14 + sak.length();
				String iv = null;
				if (stage == 2 || stage == 3) {
					iv = command.substring(offset, offset + 16);
					offset += 16;
				}
				int length = Integer.parseInt(command.substring(offset, offset + 4), 16);
				offset += 4;
				byte[] data = new byte[length];
				command.getBytes(offset, offset + length, data, 0);
				if (padAlg == 1) {
					data = this.pad1(data);
				} else if (padAlg == 2) {
					data = this.pad2(data);
				}
				String mac = null;
				if (stage == 0 || stage == 3) {
					offset += length;
					mac = command.substring(offset, offset + 8);
				}
				String macOrIv = macAlg == 3
						? this.generateMACAlg3(data, stage, iv != null ? HexadecimalStringUtil.bytesFromString(iv) : null, sak, keyType)
						: this.generateMACAlg1(data, stage, iv != null ? HexadecimalStringUtil.bytesFromString(iv) : null, sak, keyType);
				if (stage == 0 || stage == 3) {
					return command.substring(0, 4) + "M9" + (macOrIv.startsWith(mac) ? "00" : "01");
				} else {
					return command.substring(0, 4) + "M900" + macOrIv;
				}
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "M915";
			}
		} else if (cmd.equals("KQ")) {
			try {
				int mode = command.charAt(6) - '0';
				if (mode < 0 || mode > 3) {
					throw new Exception("Invalid mode " + mode);
				}
				int schemeId = command.charAt(7) - '0';
				KeyScheme saks = KeyScheme.valueOf(command.charAt(8));
				String imkAC;
				if (saks == null) {
					imkAC = command.substring(8, 24);
				} else {
					imkAC = command.substring(8, 8 + 1 + saks.getSize() * 2);
				}
				int offset = 8 + imkAC.length();
				byte[] pan = new byte[8];
				command.getBytes(offset, offset + 8, pan, 0);
				offset += 8;
				byte[] atc = new byte[2];
				command.getBytes(offset, offset + 2, atc, 0);
				offset += 2;
				byte[] un = new byte[4];
				command.getBytes(offset, offset + 4, un, 0);
				offset += 4;
				byte[] data = null;
				if (mode != 2) {
					int length = Integer.parseInt(command.substring(offset, offset + 2), 16);
					offset += 2;
					data = new byte[length];
					command.getBytes(offset, offset + length, data, 0);
					offset += length + 1;
				}
				byte[] arqc = new byte[8];
				command.getBytes(offset, offset + 8, arqc, 0);
				byte[] arc = null;
				if (mode == 1 || mode == 2) {
					offset += 8;
					arc = new byte[2];
					command.getBytes(offset, offset + 2, arc, 0);
				}
				// derive keys
				byte[] mkAC = generateMK(pan, new EncryptedKey(imkAC));
				byte[] skAC = generateSK(mkAC, atc, schemeId == 0 ? null : un);
				byte[] arqcExpected = null;
				if (mode != 2) {
					arqcExpected = generateMACAlg3(pad1(data), skAC);
				}
				// modes
				// '0' : Perform ARQC Verification only.
				// '1' : Perform ARQC Verification and ARPC Generation.
				// '2' : Perform ARPC Generation only.
				byte[] arcp = null;
				if (mode == 1 || mode == 2) {
					arcp = Arrays.copyOf(arc, 8);
					for (int i = 0; i < 8; i++) {
						arcp[i] ^= arqc[i];
					}
					arcp = generateMACAlg3(arcp, mkAC);
				}
				return command.substring(0, 4) + "KR"
						+ (mode != 2 ? (Arrays.equals(arqcExpected, arqc) ? "00" : "01") : "00")
						+ (arcp != null ? new String(arcp, 0) : "");
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "KR15";
			}
		} else if (cmd.equals("BU")) {
			int i = command.indexOf(';');
			try {
				int type = Integer.parseInt(command.substring(6, 8), 16);
				int keyFlag = command.charAt(8) - '0';
				KeyScheme ks = KeyScheme.valueOf(command.charAt(9));
				String key;
				if (ks == null) {
					key = command.substring(9, keyFlag == 3 ? (i >= 0 ? i : command.length()) : (9 + (keyFlag + 1) * 16));
				} else {
					key = command.substring(9, 9 + 1 + ks.getSize() * 2);
				}
				byte[] lmkPair = null;
				if (type == 0xFF) {
					KeyType keyType = KeyType.valueOfCode(command.substring(i + 1, i + 4));
					lmkPair = this.lmk.getPair(keyType);
				} else {
					switch (type) {
						case 0: lmkPair = this.lmk.getPair(4); break;
						case 1: lmkPair = this.lmk.getPair(6); break;
						case 2: lmkPair = this.lmk.getPair(14); break;
						case 3: lmkPair = this.lmk.getPair(16); break;
						case 4: lmkPair = this.lmk.getPair(18); break;
						case 5: lmkPair = this.lmk.getPair(20); break;
						case 6: lmkPair = this.lmk.getPair(22); break;
						case 7: lmkPair = this.lmk.getPair(24); break;
						case 8: lmkPair = this.lmk.getPair(26); break;
						case 9: lmkPair = this.lmk.getPair(28); break;
						case 0x0A: lmkPair = this.lmk.getPair(30); break;
						case 0x0B: lmkPair = this.lmk.getPair(32); break;
						case 0x10: lmkPair = this.lmk.getPair(4, 1); break;
						case 0x1C: lmkPair = this.lmk.getPair(34, 1); break;
						case 0x42: lmkPair = this.lmk.getPair(14, 4); break;
					}
				}
				EncryptedKey ekey = new EncryptedKey(key);
				byte[] bkey = ekey.decrypt(lmkPair);
				return command.substring(0, 4) + "BV00" + HexadecimalStringUtil.bytesToString(
						Arrays.copyOfRange(this.cipher.calculateKeyCheckValue(bkey), 0, 3)
				);
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "BV15";
			}
		} else if (cmd.equals("BM")) {
			try {
				int offset = 6;
				int pinCount = Integer.parseInt(command.substring(offset, offset + 2), 10);
				offset += 2;
				int pinLen = Integer.parseInt(command.substring(offset, offset + 2), 10);
				offset += 2;
				int total = offset + pinCount * pinLen;
				this.globalExcludedPIN[pinLen - 4] = new String[pinCount];
				for (int i = 0; offset < total; i++) {
					this.globalExcludedPIN[pinLen - 4][i] = command.substring(offset, offset + pinLen);
					offset += pinLen;
				}
				return command.substring(0, 4) + "BN00";
			} catch (Exception e) {
				e.printStackTrace(out);
				return command.substring(0, 4) + "BN15";
			}
		} else {
			out.println("Comando desconhecido: " + cmd);
		}
		return command.substring(0, 5) + ((char) (command.charAt(5) + 1)) + "15";
	}

	public String[] generateKey(String mkLMK, KeyType mkType, KeyScheme mkScheme, KeyScheme lmkScheme, KeyType keyType) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException {
		if (mkScheme == KeyScheme.ANSIX9_17_SINGLE) {
			mkScheme = KeyScheme.ANSIX9_17_DOUBLE;
		}
		if (lmkScheme == KeyScheme.ANSIX9_17_SINGLE) {
			lmkScheme = KeyScheme.ANSIX9_17_DOUBLE;
		}
		// decrypt tmk
		byte[] mk = new EncryptedKey(mkLMK).decrypt(this.lmk.getPair(mkType));
		byte[] key = this.cipher.generateDESedeKey(mkScheme);
		return new String[] {
			EncryptedKey.encrypt(key, mkScheme, mk).toString(),
			EncryptedKey.encrypt(key, lmkScheme, this.lmk.getPair(keyType)).toString(),
			HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(key))
		};
	}

	public String translatePinblock(String sourcePan, String sourcePinblockPK, Pinblock.Format sourceFormat, String sourcePKlmk, KeyType spks, String destinationPan, Pinblock.Format destFormat, String destinationPKlmk, KeyType dpks) throws IOException, InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException {
		out.println("translatePinblock");
		out.println("PAN " + sourcePan);
		out.println("PB " + sourcePinblockPK);
		out.println("PB " + sourceFormat);
		out.println("SourcePK " + sourcePKlmk);
		out.println("SrcPK Type " + spks);
		out.println("DestPAN " + destinationPan);
		out.println("Dest " + destFormat);
		out.println("DestPK " + destinationPKlmk);
		out.println("DstPK Type " + dpks);
		EncryptedKey spk = new EncryptedKey(sourcePKlmk);
		byte[] dpb = this.cipher.decryptBlock(HexadecimalStringUtil.bytesFromString(sourcePinblockPK), spk, spks);
		out.println("Decrypted PB " + HexadecimalStringUtil.bytesToString(dpb));
		Pinblock pb = Pinblock.createPinblock(sourceFormat, dpb);
		out.println("PIN " + pb.getPIN(sourcePan, false));
		if (sourceFormat != destFormat || !sourcePan.equals(destinationPan)) {
			pb = Pinblock.createPinblock(destFormat, pb.getPIN(sourcePan, false), destinationPan, false);
		}
		EncryptedKey dpk = new EncryptedKey(destinationPKlmk);
		dpb = this.cipher.encryptBlock(pb.getPinblock(), dpk, dpks);
		return HexadecimalStringUtil.bytesToString(dpb);
	}

	public boolean verifyPinblock(String pan, String pinblockTPK, Pinblock.Format format, String tpkLMK, String pinLMK) throws HSMCipherException, InvalidKeyException, InvalidEncryptedKeyException, InvalidPINSizeException, InvalidPINBlockException  {
		out.println("verifyPinblock");
		out.println("PAN " + pan);
		out.println("PB " + pinblockTPK);
		out.println("PB " + format);
		out.println("TPK " + tpkLMK);
		out.println("pinLMK " + pinLMK);
		EncryptedKey tpk = new EncryptedKey(tpkLMK);
		byte[] pb = this.cipher.decryptBlock(HexadecimalStringUtil.bytesFromString(pinblockTPK), tpk, KeyType.TPK);
		out.println("Decrypted PB " + HexadecimalStringUtil.bytesToString(pb));
		Pinblock pb0 = Pinblock.createPinblock(format, pb);
		String pin = pb0.getPIN(pan, false);
		out.println("PIN " + pin);
		byte[] plmk = this.cipher.decryptPINLMK(HexadecimalStringUtil.bytesFromString(pinLMK));
		String pplmk = HexadecimalStringUtil.bytesToString(plmk);
		out.println("PIN' " + pplmk);
		return pin.equals(pplmk);
	}

	public String translatePinblockZPKtoLMK(String pan, String pinblockZPK, Pinblock.Format format, String zpkLMK) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException, InvalidPINSizeException, InvalidPINBlockException {
		out.println("translatePinblockZPKtoLMK");
		out.println("PAN " + pan);
		out.println("PB " + pinblockZPK);
		out.println("PB " + format);
		out.println("ZPK " + zpkLMK);
		EncryptedKey zpk = new EncryptedKey(zpkLMK);
		byte[] pb = this.cipher.decryptBlock(HexadecimalStringUtil.bytesFromString(pinblockZPK), zpk, KeyType.ZPK);
		out.println("Decrypted PB " + HexadecimalStringUtil.bytesToString(pb));
		Pinblock pb0 = Pinblock.createPinblock(format, pb);
		String pin = pb0.getPIN(pan, false);
		out.println("PIN " + pin);
		byte[] plmk = this.cipher.encryptPINLMK(HexadecimalStringUtil.bytesFromString(pin));
		return HexadecimalStringUtil.bytesToString(plmk);
	}

	public boolean verifyCVV(String scvk, String cvv, String pan, String expire, String serviceCode) throws InvalidKeyException, InvalidEncryptedKeyException, HSMCipherException {
		return cvv.equals(this.generateCVV(scvk, pan, expire, serviceCode));
	}

	public String generateCVV(String scvk, String pan, String expire, String serviceCode) throws InvalidEncryptedKeyException, InvalidKeyException, HSMCipherException {
		out.println("generateCVV");
		out.println("PAN " + pan);
		out.println("expire " + expire);
		out.println("serviceCode " + serviceCode);
		StringBuilder block = new StringBuilder(32);
		block.append(pan);
		block.append(expire);
		block.append(serviceCode);
		for (int i = block.length(); i < 32; i++) {
			block.append('0');
		}
		String blockA = block.substring(0, 16);
		out.println("Block A " + blockA);
		String blockB = block.substring(16);
		out.println("Block B " + blockB);
		EncryptedKey cvk = new EncryptedKey(scvk);
		byte[] bA = this.cipher.encryptDESBlock(HexadecimalStringUtil.bytesFromString(blockA), cvk, KeyType.CVK);
		out.println("Encrypted block A " + HexadecimalStringUtil.bytesToString(bA));
		byte[] bB = HexadecimalStringUtil.bytesFromString(blockB);
		for (int i = 0; i < 8; i++) {
			bB[i] ^= bA[i];
		}
		out.println("XOR'ed block B " + HexadecimalStringUtil.bytesToString(bB));
		bB = this.cipher.encryptBlock(bB, cvk, KeyType.CVK);
		blockB = HexadecimalStringUtil.bytesToString(bB);
		out.println("Encrypted block B " + blockB);
		return this.decimalize(blockB, 3);
	}

	public String[] generateCVK(KeyScheme lmkScheme, boolean keyCheckOld) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException {
		byte[] key = this.cipher.generateDESedeKey(lmkScheme);
		return new String[] {
			EncryptedKey.encrypt(key, lmkScheme, this.lmk.getPair(KeyType.CVK)).toString(),
			keyCheckOld ? (
				HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(Arrays.copyOfRange(key, 0, 8)))
				+ HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(Arrays.copyOfRange(key, 8, 16)))
			) : HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(key))
		};
	}

	public String[] translateCVKtoZMK(String zmkLMK, String cvkLMK, KeyScheme zmkScheme, boolean keyCheckOld) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException {
		EncryptedKey zmk = new EncryptedKey(zmkLMK);
		EncryptedKey cvk = new EncryptedKey(cvkLMK);
		byte[] zmkClear = zmk.decrypt(this.lmk.getPair(KeyType.ZMK));
		byte[] cvkClear = cvk.decrypt(this.lmk.getPair(KeyType.CVK));
		return new String[] {
			EncryptedKey.encrypt(cvkClear, zmkScheme, zmkClear).toString(),
			keyCheckOld ? (
				HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(Arrays.copyOfRange(cvkClear, 0, 8))).substring(0, 6)
				+ HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(Arrays.copyOfRange(cvkClear, 8, 16))).substring(0, 6)
			) : HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(cvkClear)).substring(0, 6)
		};
	}

	public String generatePVV(String sourcePan, String pinblock, Pinblock.Format format, String pkLMK, KeyType keyType, String destPan, int pvki, String pvkLMK, String... excluded) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException, InvalidPINSizeException, InvalidPINBlockException {
		out.println("generatePVV");
		out.println("PAN " + sourcePan);
		out.println("PB " + pinblock);
		out.println("PB " + format);
		out.println("PK " + pkLMK);
		out.println("PK type " + keyType);
		out.println("DestPAN " + destPan);
		out.println("PVVi " + pvki);
		out.println("PVK " + pvkLMK);
		out.println("Excluded " + (excluded != null ? Arrays.toString(excluded) : "null"));

		EncryptedKey pk = new EncryptedKey(pkLMK);
		byte[] pb = this.cipher.decryptBlock(HexadecimalStringUtil.bytesFromString(pinblock), pk, keyType);
		out.println("Decrypted PB " + HexadecimalStringUtil.bytesToString(pb));
		Pinblock pb0 = Pinblock.createPinblock(format, pb);
		String pin = pb0.getPIN(sourcePan, false);
		out.println("PIN " + pin);
		if (excluded != null && excluded.length > 0) {
			for (int i = 0; i < excluded.length; i++) {
				if (excluded[i].equals(pin)) {
					throw new InvalidPINException();
				}
			}
		}
		excluded = this.globalExcludedPIN[pin.length() - 4];
		if (excluded != null && excluded.length > 0) {
			for (int i = 0; i < excluded.length; i++) {
				if (excluded[i].equals(pin)) {
					throw new InvalidPINException();
				}
			}
		}
		EncryptedKey pvk = new EncryptedKey(pvkLMK);
		String stage1 = destPan.substring(1, 12) + pvki + pin.substring(0, 4);
		out.println("Stage1 " + stage1);
		byte[] s2 = this.cipher.encryptBlock(HexadecimalStringUtil.bytesFromString(stage1), pvk, KeyType.PVK);
		String stage2 = HexadecimalStringUtil.bytesToString(s2);
		out.println("Stage2 " + stage2);
		return this.decimalize(stage2, 4);
	}

	public boolean verifyPVV(String pan, String pinblock, Pinblock.Format format, String pk, KeyType pkType, String destPan, int pvvi, String pvv, String spvk) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException, InvalidPINSizeException, InvalidPINBlockException {
		return pvv.equals(this.generatePVV(pan, pinblock, format, pk, pkType, destPan, pvvi, spvk));
	}

	public String decimalize(String s, int qty) {
		char[] dec = new char[qty];
		int z = 0;
		for (int i = 0; i < s.length() && z < qty; i++) {
			char c = s.charAt(i);
			if (c >= '0' && c <= '9') {
				dec[z++] = c;
			}
		}
		// ainda nao completou, pega dos hexadecimais
		if (z < qty) {
			for (int i = 0; i < s.length() && z < qty; i++) {
				char c = Character.toUpperCase(s.charAt(i));
				if (c >= 'A' && c <= 'F') {
					dec[z++] = (char) (c - 'A' + '0');
				}
			}
		}
		return new String(dec);
	}

	public String generateIBMOffset(String pan, String pinblock, Pinblock.Format format, String pkLMK, KeyType keyType, String decTable, String validationData, int checkLength, String pvkLMK, String... excluded) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException, InvalidPINSizeException, InvalidPINBlockException {
		out.println("generateIBMOffset");
		out.println("PAN " + pan);
		out.println("PB " + pinblock);
		out.println("PB " + format);
		out.println("PK " + pkLMK);
		out.println("PK type " + keyType);
		out.println("DecTable " + decTable);
		out.println("ValData " + validationData);
		out.println("checkLength " + checkLength);
		out.println("PVK " + pvkLMK);
		out.println("Excluded " + (excluded != null ? Arrays.toString(excluded) : "null"));
		EncryptedKey pk = new EncryptedKey(pkLMK);
		byte[] pb = this.cipher.decryptBlock(HexadecimalStringUtil.bytesFromString(pinblock), pk, keyType);
		out.println("Decrypted PB " + HexadecimalStringUtil.bytesToString(pb));
		Pinblock pb0 = Pinblock.createPinblock(format, pb);
		String pin = pb0.getPIN(pan, false);
		out.println("PIN " + pin);
		if (pin.length() != checkLength) {
			throw new InvalidPINException();
		}
		if (excluded != null && excluded.length > 0) {
			for (int i = 0; i < excluded.length; i++) {
				if (excluded[i].equals(pin)) {
					throw new InvalidPINException();
				}
			}
		}
		excluded = this.globalExcludedPIN[pin.length() - 4];
		if (excluded != null && excluded.length > 0) {
			for (int i = 0; i < excluded.length; i++) {
				if (excluded[i].equals(pin)) {
					throw new InvalidPINException();
				}
			}
		}
		EncryptedKey pvk = new EncryptedKey(pvkLMK);
		// decrypt decimalization table
//		byte[] _dtab = cipher.decryptBlock(HexadecimalStringUtil.bytesFromString(decTable), lmk.getPair(KeyType.DTAB));
		byte[] _dtab = HexadecimalStringUtil.bytesFromString(decTable);
		byte[] dtab = new byte[16];
		for (int i = 0; i < 16; i++) {
			dtab[i] = (byte) ((_dtab[i / 2] >> (i % 2 == 0 ? 4 : 0)) & 0xF);
		}
		out.println("DecTable " + Arrays.toString(dtab));
		// generate natural PIN
		byte[] data = HexadecimalStringUtil.bytesFromString(validationData);
		data = cipher.encryptBlock(data, pvk, KeyType.PVK);
		out.println("Data " + HexadecimalStringUtil.bytesToString(data));
		for (int i = 0; i < 8; i++) {
			data[i] = (byte) ((dtab[data[i] >> 4 & 0xF] << 4) | dtab[data[i] & 0xF]);
		}
		out.println("Data " + HexadecimalStringUtil.bytesToString(data));
		// generate offset
		char[] offset = new char[] { 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F' };
		for (int i = 0; i < checkLength; i++) {
			int pinD = pin.charAt(i) - '0';
			int natPinD = (data[i / 2] >> (i % 2 == 0 ? 4 : 0)) & 0xF;
			offset[i] = (char) ('0' + pinD - natPinD + (natPinD > pinD ? 10 : 0));
		}
		// Geracao
		//    2  3  4  5
		// -  4  3  0  5
		//--------------
		//  (-2) 0  4  0
		//  +10
		//--------------
		//    8  0  4  0

		// Verificacao
		//    4  3  0  5
		// +  8  0  4  0
		//--------------
		//   12  3  4  5
		//--------------
		//    2  3  4  5
		return new String(offset);
	}

	public boolean verifyIBMOffset(String pan, String pinblock, Pinblock.Format format, String pkLMK, KeyType keyType, String decTable, String validationData, int checkLength, String pvkLMK, String offset) throws InvalidEncryptedKeyException, HSMCipherException, InvalidKeyException, InvalidPINSizeException, InvalidPINBlockException {
		return offset.equals(this.generateIBMOffset(pan, pinblock, format, pkLMK, keyType, decTable, validationData, checkLength, pvkLMK));
	}

	public String generateMACAlg1(byte[] data, int stage, byte[] iv, String akLMK, KeyType akType) throws Exception {
		out.println("Alg1 " + HexadecimalStringUtil.bytesToString(data));
		if (iv != null) out.println("IV " + HexadecimalStringUtil.bytesToString(iv));
		out.println("Stage " + stage);
		if (iv == null) {
			iv = new byte[8];
		}
		EncryptedKey ak = new EncryptedKey(akLMK);
		out.println("AK " + ak);
		out.println("AK type " + akType);
		out.println("Alg1     " + System.currentTimeMillis());
		byte[] enc = this.cipher.encryptDESedeCBCBlock(data, iv, ak, akType);
		out.println("Alg1 enc " + System.currentTimeMillis());
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		// output transformation 1 - no change
		// G = Hq
		// -- nao faz nada
		out.println("ENC " + HexadecimalStringUtil.bytesToString(enc));
		return HexadecimalStringUtil.bytesToString(enc);
	}

	public String generateMACAlg3(byte[] data, int stage, byte[] iv, String akLMK, KeyType akType) throws Exception {
		out.println("Alg3 " + HexadecimalStringUtil.bytesToString(data));
		if (iv != null) out.println("IV " + HexadecimalStringUtil.bytesToString(iv));
		out.println("Stage " + stage);
		if (iv == null) {
			iv = new byte[8];
		}
		EncryptedKey ak = new EncryptedKey(akLMK);
		out.println("AK " + ak);
		out.println("AK type " + akType);
		out.println("Alg3     " + System.currentTimeMillis());
		byte[] enc = this.cipher.encryptDESCBCBlock(data, iv, ak, akType);
		out.println("Alg3 enc " + System.currentTimeMillis());
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		out.println("ENC " + HexadecimalStringUtil.bytesToString(enc));
		if (stage == 0 || stage == 3) {
			// output transformation 3
			// Hq (last block encrypted) is decrypted with the key K? and the result encrypted with the key K:
			// G = eK(dK?(Hq))
			// hq1
			enc = this.cipher.decryptDESBlock(enc, ak, akType, 8);
			// g
			enc = this.cipher.encryptDESBlock(enc, ak, akType, 16);
			out.println("Alg3 g   " + System.currentTimeMillis());
		}
		out.println("ENC " + HexadecimalStringUtil.bytesToString(enc));
		return HexadecimalStringUtil.bytesToString(enc);
	}

	public byte[] generateMACAlg3(byte[] data, byte[] sk) throws Exception {
		byte[] enc = this.cipher.encryptDESCBCBlock(data, new byte[8], sk);
		enc = Arrays.copyOfRange(enc, enc.length - 8, enc.length);
		// output transformation 3
		// Hq (last block encrypted) is decrypted with the key K? and the result encrypted with the key K:
		// G = eK(dK?(Hq))
		// hq1
		enc = this.cipher.decryptDESBlock(enc, sk, 8);
		// g
		return this.cipher.encryptDESBlock(enc, sk, 16);
	}

	public byte[] pad1(byte[] data) {
		// pad1 - 0x00
		if (data.length % 8 != 0) {
			byte[] newdata = new byte[(data.length / 8 + 1) * 8];
			System.arraycopy(data, 0, newdata, 0, data.length);
			// 00 padding - do nothing
			data = newdata;
		}
		return data;
	}

	public byte[] pad2(byte[] data) {
		// pad3 - 0x80 + 0x00 ...
		byte[] newdata = new byte[(data.length / 8 + 1) * 8];
		System.arraycopy(data, 0, newdata, 0, data.length);
		newdata[data.length] = (byte) 0x80;
		// remaining 00 padding - do nothing
		return newdata;
	}

	public byte[] generateMK(byte[] panBlock, EncryptedKey mkAC) throws Exception {
		byte[] block2 = new BigInteger(panBlock).xor(new BigInteger(HexadecimalStringUtil.bytesFromString("FFFFFFFFFFFFFFFF"))).toByteArray();
		byte[] mk1 = cipher.encryptBlock(panBlock, mkAC, KeyType.MK_AC);
		byte[] mk2 = cipher.encryptBlock(block2, mkAC, KeyType.MK_AC);
		byte[] mk = Arrays.copyOf(mk1, 16);
		System.arraycopy(mk2, 0, mk, 8, 8);
		mk = HSMCipher.adjustKey(adjustParity(mk));
		return mk;
	}

	public byte[] generateSK(byte[] mk, byte[] atc, byte[] un) throws Exception {
		byte[] r1 = new byte[8];
		System.arraycopy(atc, 0, r1, 0, 2);
		if (un != null) {
			System.arraycopy(un, 0, r1, 4, 4);
		}
		r1[2] = -16; // F0
		byte[] r2 = Arrays.copyOf(r1, 8);
		r2[2] = 15; // 0F

		byte[] sk1 = cipher.encryptBlock(r1, mk);
		byte[] sk2 = cipher.encryptBlock(r2, mk);
		byte[] sk = Arrays.copyOf(sk1, 16);
		System.arraycopy(sk2, 0, sk, 8, 8);
		return adjustParity(sk);
	}

	public static byte[] adjustParity(byte[] key) {
		// ajusta paridade impar
		for (int i = 0; i < key.length; i++) {
			// conta quantos 1 nos bits mais significativos
			int r = 0;
			for (int j = 7; j >= 0; j--) {
				int w = (key[i] & (1 << j));
				if (w != 0) r++;
			}
			if (r % 2 == 0) {
				key[i] ^= 1;
			}
		}
		return key;
	}

	public Pinblock.Format getFormat(String s, int offset) {
		if (s.regionMatches(offset, "01", 0, 2)) {
			return Pinblock.Format.ISO_0;
		}
		if (s.regionMatches(offset, "05", 0, 2)) {
			return Pinblock.Format.ISO_1;
		}
		if (s.regionMatches(offset, "47", 0, 2)) {
			return Pinblock.Format.ISO_3;
		}
		return null;
	}

	public String getFormat(Pinblock.Format format) {
		switch (format) {
			case ISO_0: return "01";
			case ISO_1: return "05";
			case ISO_3: return "47";
		}
		return null;
	}

}
