package com.penczek.hsm;

import java.util.List;

public interface HSMLoadBalancerMBean {

	List<HSM> getHsms();

	void setHsms(List<HSM> hsms);

	int getWaitTime();

	void setWaitTime(int waitTime);

	public int getTps();

}
