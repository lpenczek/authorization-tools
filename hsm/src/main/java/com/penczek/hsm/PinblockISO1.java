package com.penczek.hsm;

import java.math.BigInteger;

public class PinblockISO1 extends Pinblock {

	protected PinblockISO1() {}

	@Override
	protected void setPinAndPan(String pin, String pan, boolean dv) {
		int p = pin.length();
		if (p < 4 || p > 12) throw new InvalidPINException("Invalid size " + p);
		long pinpad = 0x10 + p; // '1' + PIN len
		int i = 0;
		while (i < 14) {
			// random padding
			byte b = (byte) RANDOM.nextInt(16); // between 0 and F
			if (i < p) {
				b = (byte) (pin.charAt(i) - '0');
				if (b > 9) throw new InvalidPINException("Invalid digit " + pin.charAt(i));
			}
			pinpad = (pinpad << 4) | b;
			i++;
		}
		this.pinblock = BigInteger.valueOf(pinpad).toByteArray();
	}

	@Override
	public String getPIN(String pan, boolean dv) {
		long pin = new BigInteger(this.pinblock).longValue();
		int pinlen = (int) (pin / 0x0100000000000000L);
		// verifies '1' + PIN len (4-12)
		if (pinlen < 0x14 || pinlen > 0x1C) {
			pinlen &= 0xEF;
			throw new InvalidPINBlockException(pinlen > 0 && pinlen <= 127 ? "Invalid pin length " + pinlen : "Not ISO-3");
		}
		pinlen %= 0x10;
		for (int i = 14; i > pinlen; i--) {
			pin >>= 4;
		}
		char[] c = new char[pinlen];
		for (int i = pinlen - 1; i >= 0; i--) {
			c[i] = (char) ('0' + (pin & 0xF));
			if (c[i] < '0' || c[i] > '9') {
				throw new InvalidPINBlockException("Invalid PIN digit " + c[i]);
			}
			pin >>= 4;
		}
		return new String(c);
	}

	@Override
	protected void verifyPinblock(byte[] pinblock) {
		if (pinblock == null || pinblock.length != 8) {
			throw new InvalidPINBlockException("Invalid pinblock");
		}
		if (pinblock[0] < 0x14 || pinblock[0] > 0x1C) {
			throw new InvalidPINBlockException("Not ISO-1");
		}
	}

	@Override
	public Format getFormat() {
		return Format.ISO_1;
	}

}
