package com.penczek.hsm;

import com.penczek.util.HexadecimalStringUtil;
import java.security.SecureRandom;
import java.util.Arrays;

public abstract class Pinblock {

	public enum Format {
		/** ISO format 0: '0000' + 12 right-most PAN digits XOR '0' + PIN len + PIN + F's padding. */
		ISO_0,
		/** ISO format 1: '1' + PIN len + PIN + random padding, no PAN. */
		ISO_1,
		/** ISO format 3: '0000' + 12 right-most PAN digits XOR '3' + PIN len + PIN + (A-F)'s random padding. */
		ISO_3;
	}

	protected static final SecureRandom RANDOM = new SecureRandom();

	protected byte[] pinblock;
	private int hash;

	private void setPinblock(byte[] pinblock) {
		this.verifyPinblock(pinblock);
		this.pinblock = pinblock.clone();
	}

	public byte[] getPinblock() {
		return this.pinblock.clone();
	}

	@Override
	public String toString() {
		return HexadecimalStringUtil.bytesToString(this.pinblock);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Pinblock)) return false;
		Pinblock pb = (Pinblock) obj;
		if (this.getFormat() != pb.getFormat()) return false;
		return Arrays.equals(this.pinblock, pb.pinblock);
	}

	@Override
	public int hashCode() {
		if (this.hash == 0 && this.pinblock.length > 0) {
			int h = this.getFormat().hashCode();
			for (int i = 0; i < this.pinblock.length; i++) {
				h = 31 * h + this.pinblock[i];
			}
			this.hash = h;
		}
		return this.hash;
	}

	public String getPIN(String pan) {
		return this.getPIN(pan, true);
	}

	public abstract String getPIN(String pan, boolean dv);

	public abstract Format getFormat();

	protected abstract void setPinAndPan(String pin, String pan, boolean dv);

	protected abstract void verifyPinblock(byte[] pinblock);

	public static final Pinblock createPinblock(Format format, String pin, String pan) {
		return createPinblock(format, pin, pan, true);
	}

	public static final Pinblock createPinblock(Format format, String pin, String pan, boolean dv) {
		Pinblock pb = createPinblock(format);
		pb.setPinAndPan(pin, pan, dv);
		return pb;
	}

	public static final Pinblock createPinblock(Format format, String pinblock) {
		return createPinblock(format, HexadecimalStringUtil.bytesFromString(pinblock));
	}

	public static final Pinblock createPinblock(Format format, byte[] pinblock) {
		Pinblock pb = createPinblock(format);
		pb.setPinblock(pinblock);
		return pb;
	}

	private static Pinblock createPinblock(Format format) {
		if (format != null) {
			switch (format) {
				case ISO_0: return new PinblockISO0();
				case ISO_1: return new PinblockISO1();
				case ISO_3: return new PinblockISO3();
			}
		}
		throw new InvalidPINBlockException("Format not supported");
	}

	public static String extractAccountNumber(String pan) {
		return extractAccountNumber(pan, true);
	}

	public static String extractAccountNumber(String pan, boolean dv) {
		int t = pan.length();
		return
				t <= 0 ? "000000000000" // se vazio, retorna zeros
				: dv ? (
						t >= 13 ? pan.substring(t - 13, t - 1) // ultimos 12 digitos do PAN excluindo o ultimo (verificador)
						: "000000000000".substring(t - 1) + pan.substring(0, t - 1) // quando menor que 12 (sem DV), preenche zeros a esquerda
				) : (
						t >= 12 ? pan.substring(t - 12) // ultimos 12 digitos do PAN
						: "000000000000".substring(t) + pan // quando menor que 12, preenche zeros a esquerda
				);
	}

}
