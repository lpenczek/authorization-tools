package com.penczek.hsm;

import java.math.BigInteger;

public class PinblockISO3 extends Pinblock {

	protected PinblockISO3() {}

	@Override
	protected void setPinAndPan(String pin, String pan, boolean dv) {
		int p = pin.length();
		if (p < 4 || p > 12) throw new InvalidPINException("Invalid size " + p);
		long pinpad = 0x30 + p; // '3' + PIN len
		int i = 0;
		while (i < 14) {
			// random padding
			byte b = (byte) (RANDOM.nextInt(6) + 10); // between A and F
			if (i < p) {
				b = (byte) (pin.charAt(i) - '0');
				if (b > 9) throw new InvalidPINException("Invalid digit " + pin.charAt(i));
			}
			pinpad = (pinpad << 4) | b;
			i++;
		}
		// account xor pinpadded
		this.pinblock = BigInteger.valueOf(Long.parseLong(extractAccountNumber(pan, dv), 16) ^ pinpad).toByteArray();
	}

	@Override
	public String getPIN(String pan, boolean dv) {
		pan = extractAccountNumber(pan, dv);
		long pin = Long.parseLong(pan, 16) ^ new BigInteger(this.pinblock).longValue();
		int pinlen = (int) (pin / 0x0100000000000000L);
		// verifies '3' + PIN len (4-12)
		if (pinlen < 0x34 || pinlen > 0x3C) {
			pinlen &= 0xCF;
			throw new InvalidPINBlockException(pinlen > 0 && pinlen <= 127 ? "Invalid pin length " + pinlen : "Not ISO-3");
		}
		pinlen %= 0x10;
		for (int i = 14; i > pinlen; i--) {
			if ((pin & 0xF) < 0xA) {
				throw new InvalidPINBlockException("Invalid padding");
			}
			pin >>= 4;
		}
		char[] c = new char[pinlen];
		for (int i = pinlen - 1; i >= 0; i--) {
			c[i] = (char) ('0' + (pin & 0xF));
			if (c[i] < '0' || c[i] > '9') {
				throw new InvalidPINBlockException("Invalid PIN digit " + c[i]);
			}
			pin >>= 4;
		}
		return new String(c);
	}

	@Override
	protected void verifyPinblock(byte[] pinblock) {
		if (pinblock == null || pinblock.length != 8) {
			throw new InvalidPINBlockException("Invalid pinblock");
		}
		if (pinblock[0] < 0x34 || pinblock[0] > 0x3C) {
			throw new InvalidPINBlockException("Not ISO-3");
		}
	}

	@Override
	public Format getFormat() {
		return Format.ISO_3;
	}

}
