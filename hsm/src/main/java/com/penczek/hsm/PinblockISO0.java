package com.penczek.hsm;

import java.math.BigInteger;

public class PinblockISO0 extends Pinblock {

	protected PinblockISO0() {}

	@Override
	protected void setPinAndPan(String pin, String pan, boolean dv) {
		int p = pin.length();
		if (p < 4 || p > 12) throw new InvalidPINException("Invalid size " + p);
		long pinpad = p; // '0' + PIN len
		int i = 0;
		while (i < 14) {
			byte b = 0x0F;
			if (i < p) {
				b = (byte) (pin.charAt(i) - '0');
				if (b > 9) throw new InvalidPINException("Invalid digit " + pin.charAt(i));
			}
			pinpad = (pinpad << 4) | b;
			i++;
		}
		// account xor pinpadded
		this.pinblock = BigInteger.valueOf(Long.parseLong(extractAccountNumber(pan, dv), 16) ^ pinpad).toByteArray();
	}


	@Override
	public String getPIN(String pan, boolean dv) {
		pan = extractAccountNumber(pan, dv);
		long pin = Long.parseLong(pan, 16) ^ new BigInteger(this.pinblock).longValue();
		int pinlen = (int) (pin / 0x0100000000000000L);
		// verifies '0' + PIN len (4-12)
		if (pinlen < 0x04 || pinlen > 0x0C) {
			throw new InvalidPINBlockException(pinlen > 0 && pinlen <= 127 ? "Invalid pin length " + pinlen : "Not ISO-0");
		}
		for (int i = 14; i > pinlen; i--) {
			if ((pin & 0xF) != 0xF) {
				throw new InvalidPINBlockException("Invalid padding");
			}
			pin >>= 4;
		}
		char[] c = new char[pinlen];
		for (int i = pinlen - 1; i >= 0; i--) {
			c[i] = (char) ('0' + (pin & 0xF));
			if (c[i] < '0' || c[i] > '9') {
				throw new InvalidPINBlockException("Invalid PIN digit " + c[i]);
			}
			pin >>= 4;
		}
		return new String(c);
	}

	@Override
	protected void verifyPinblock(byte[] pinblock) {
		if (pinblock == null || pinblock.length != 8) {
			throw new InvalidPINBlockException("Invalid pinblock");
		}
		if (pinblock[0] < 0x04 || pinblock[0] > 0x0C) {
			throw new InvalidPINBlockException("Not ISO-0");
		}
	}

	@Override
	public Format getFormat() {
		return Format.ISO_0;
	}

}
