package com.penczek.hsm;

public enum EMVScheme {
	VISA_VSDC,
	CCD,
	EUROPAY,
	MASTERCARD_MCHIP
}
