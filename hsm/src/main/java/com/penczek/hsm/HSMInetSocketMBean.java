package com.penczek.hsm;

public interface HSMInetSocketMBean {

	public String getIp();

	public void setIp(String ip);

	public int getPort();

	public void setPort(int port);

	public boolean isTcp();

	public void setTcp(boolean tcp);

	public boolean isPersistent();

	public void setPersistent(boolean persistent);

	public int getConnectionTimeout();

	public void setConnectionTimeout(int socketTimeout);

	public int getReadTimeout();

	public void setReadTimeout(int socketTimeout);

	public int getTps();

	public void setTps(int tps);

	public boolean isBurstMode();

	public void setBurstMode(boolean burstMode);

	public void reset();

}
