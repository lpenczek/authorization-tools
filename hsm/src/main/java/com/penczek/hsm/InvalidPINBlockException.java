package com.penczek.hsm;

public class InvalidPINBlockException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidPINBlockException() {}

	public InvalidPINBlockException(String message) {
		super(message);
	}

}
