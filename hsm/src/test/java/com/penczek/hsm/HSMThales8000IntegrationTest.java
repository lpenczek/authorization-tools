package com.penczek.hsm;

import static org.junit.Assert.*;

import com.penczek.pinpad.PinPadHSM;
import com.penczek.tlv.EMVDataElement;
import com.penczek.tlv.PrimitiveTLV;
import com.penczek.tlv.TLV;
import com.penczek.tlv.TLVData;
import com.penczek.tlv.TLVDataParser;
import com.penczek.util.HexadecimalStringUtil;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.logging.LogManager;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.junit.Test;

public class HSMThales8000IntegrationTest {

	private static final String HSM_IP = "localhost";
//	private static final String DEC_TABLE = "FD64FC3FAD504BB3";
	private static final String DEC_TABLE = "0123456789012345";

	private static final int HSM_PORT = 1500;

	protected HSM hsm;

	public HSMThales8000IntegrationTest() {
		try {
			LogManager.getLogManager().readConfiguration(new ByteArrayInputStream((
					"handlers = java.util.logging.ConsoleHandler\n" +
					"java.util.logging.ConsoleHandler.level = FINEST\n" +
					"com.level = FINEST\n"
					).getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.hsm = getHSM();
	}

	protected HSM getHSM() {
		HSMThales8000 hsm = new HSMThales8000(HSM_IP, HSM_PORT, true);
		hsm.setConnectionTimeout(10000);
		hsm.setReadTimeout(10000);
		hsm.setTps(100);
		return hsm;
	}

	@Test
	public void testPVV() throws Exception {
		String pan = "6056740146280020";
		String pbTPK = "CE5AA5222143475D";
		String tpk = "U5FE21198A7909ADD97B7B563D57C2BDD";
		String pvk = "UC7117B8B71B1674E9AC2BE583648C465";
		String pvv =  this.hsm.generatePVV(pan, pbTPK, tpk, DataKeyType.TPK, 1, pvk);
		System.out.println("PVV " + pvv);
		assertEquals("7854", pvv);
	}

	@Test
	public void testTranslate() throws Exception {
		String pan = "6056760000000124";
		String zpkU = "U54B73F1DF5E5EF4EC56DD1EDA6889369";
		String pbZpkU = "CCAD456F9EDA091D";
		String zpkZ = "1EC9CFF3B29AA3AC";
		String pbZpkZ = this.hsm.translatePinblock(pan, pbZpkU, zpkU, zpkZ);
		System.out.println("PB0 " + pbZpkZ);
		assertEquals("98C77B0629ACCE3C", pbZpkZ);
		String pbZ3 = this.hsm.translatePinblock(pan, pbZpkU, Pinblock.Format.ISO_0, zpkU, Pinblock.Format.ISO_3, zpkZ);
		System.out.println("PB3 " + pbZ3); // random filled, cannot be predetermined
		assertNotEquals(pbZpkU, pbZ3);
		String pbZ1 = this.hsm.translatePinblock(pan, pbZpkU, Pinblock.Format.ISO_0, zpkU, Pinblock.Format.ISO_1, zpkZ);
		System.out.println("PB1 " + pbZ1); // random filled, cannot be predetermined
		assertNotEquals(pbZpkU, pbZ1);
		String pbZ11 = this.hsm.translatePinblock(pan, pbZ3, Pinblock.Format.ISO_3, zpkZ, Pinblock.Format.ISO_1, zpkZ);
		System.out.println("PB1 " + pbZ11); // random filled, cannot be predetermined
		// pbZ1 possibly != pbZ11 (random fill)
		assertNotEquals(pbZ1, pbZ11);
		assertNotEquals(pbZpkU, pbZ11);
		String pb0ZpkU = this.hsm.translatePinblock(pan, pbZ3, Pinblock.Format.ISO_3, zpkZ, Pinblock.Format.ISO_0, zpkU);
		System.out.println("PB0 " + pb0ZpkU);
		assertEquals(pbZpkU, pb0ZpkU);
		pb0ZpkU = this.hsm.translatePinblock(pan, pbZ1, Pinblock.Format.ISO_1, zpkZ, Pinblock.Format.ISO_0, zpkU);
		System.out.println("PB0 " + pb0ZpkU);
		assertEquals(pbZpkU, pb0ZpkU);
		pb0ZpkU = this.hsm.translatePinblock(pan, pbZ11, Pinblock.Format.ISO_1, zpkZ, Pinblock.Format.ISO_0, zpkU);
		System.out.println("PB0 " + pb0ZpkU);
		assertEquals(pbZpkU, pb0ZpkU);
	}

	@Test
	public void testTranslateToken() throws Exception {
		String token = "099988887777";
		String tPbZpkU = "44C2E6A875C366AD";
		String tPb3ZpkU = "D4B8BFB8C1A92BDE";
		String pan = "6056760000000124";
		String panPbZpkU = "CCAD456F9EDA091D";
		String zpkU = "U54B73F1DF5E5EF4EC56DD1EDA6889369";
		String zpkZ = "1EC9CFF3B29AA3AC";
		String pbZZ = this.hsm.translatePinblock(token, tPbZpkU, zpkU, pan, zpkZ);
		System.out.println("PB0 " + pbZZ);
		assertEquals("98C77B0629ACCE3C", pbZZ);
		String pbZ3 = this.hsm.translatePinblock(token, tPbZpkU, Pinblock.Format.ISO_0, zpkU, Pinblock.Format.ISO_3, pan, zpkZ);
		System.out.println("PB3 " + pbZ3); // random filled, cannot be predetermined
		assertNotEquals(tPbZpkU, pbZ3);
		String pbZ1 = this.hsm.translatePinblock(token, tPbZpkU, Pinblock.Format.ISO_0, zpkU, Pinblock.Format.ISO_1, pan, zpkZ);
		System.out.println("PB1 " + pbZ1); // random filled, cannot be predetermined
		assertNotEquals(tPbZpkU, pbZ1);
		String pbZ11 = this.hsm.translatePinblock(token, tPb3ZpkU, Pinblock.Format.ISO_3, zpkU, Pinblock.Format.ISO_1, pan, zpkZ);
		System.out.println("PB1 " + pbZ11); // random filled, cannot be predetermined
		assertNotEquals(pbZ1, pbZ11);
		assertNotEquals(tPbZpkU, pbZ11);
		String panPb0ZpkU = this.hsm.translatePinblock(pan, pbZ3, Pinblock.Format.ISO_3, zpkZ, Pinblock.Format.ISO_0, zpkU);
		System.out.println("PB0 " + panPb0ZpkU);
		assertNotEquals(tPbZpkU, panPb0ZpkU);
		assertEquals(panPbZpkU, panPb0ZpkU);
		panPb0ZpkU = this.hsm.translatePinblock(pan, pbZ1, Pinblock.Format.ISO_1, zpkZ, Pinblock.Format.ISO_0, zpkU);
		System.out.println("PB0 " + panPb0ZpkU);
		assertNotEquals(tPbZpkU, panPb0ZpkU);
		assertEquals(panPbZpkU, panPb0ZpkU);
		panPb0ZpkU = this.hsm.translatePinblock(pan, pbZ11, Pinblock.Format.ISO_1, zpkZ, Pinblock.Format.ISO_0, zpkU);
		System.out.println("PB0 " + panPb0ZpkU);
		assertNotEquals(tPbZpkU, panPb0ZpkU);
		assertEquals(panPbZpkU, panPb0ZpkU);
		// invalid
		panPb0ZpkU = this.hsm.translatePinblock(pan, pbZ11, Pinblock.Format.ISO_3, zpkZ, Pinblock.Format.ISO_0, zpkU);
		assertNull(panPb0ZpkU);
	}

	@Test
	public void testTranslateTPKZPK() throws Exception {
		String pan = "6394010300000072";
		String tpk = "U5FE21198A7909ADD97B7B563D57C2BDD";
		String pbTPK = "880FEC4E5C43118E";
		String ZPK = "1EC9CFF3B29AA3AC";
		String pbZPK = "0DC5172643A11B3F";
		System.out.println("translateTPKtoZPK");
		String pinblockDestZPK = this.hsm.translatePinblockTPKtoZPK(pan, pbTPK, tpk, ZPK);
		System.out.println("Destination PB " + pinblockDestZPK);
		assertEquals(pbZPK, pinblockDestZPK);
		String token = "123456789";
		pbTPK = "E09A683A65FD5A14";
		pinblockDestZPK = this.hsm.translatePinblockTPKtoZPK(token, pbTPK, tpk, pan, ZPK);
		System.out.println("Destination PB " + pinblockDestZPK);
		assertEquals(pbZPK, pinblockDestZPK);
		token = "12345678"; // Token invalido
		pbTPK = "E09A683A65FD5A14";
		pinblockDestZPK = this.hsm.translatePinblockTPKtoZPK(token, pbTPK, tpk, pan, ZPK);
		assertNull(pinblockDestZPK);
	}

	@Test
	public void testTPKZPK() throws Exception {
		String TMK_LMK = "U1750CDFB0757D3B3021AE502DDEDD980";
		String ZMK_LMK = "BF1CBFE96DE6CF2844E2F2AB0EB44C49";
		String ZPK = "1EC9CFF3B29AA3AC";
		String[] zpks;

		System.out.println("generateZPK(scheme U)");
		zpks = this.hsm.generateKey(DataKeyType.ZPK, ZMK_LMK, KeyScheme.VARIANT_DOUBLE);
		assertNotNull(zpks);
		assertEquals(2, zpks.length);
		System.out.println("ZPKzmk " + zpks[0] + " ZPKlmk " + zpks[1]);

		System.out.println("generateZPK(scheme U)");
		zpks = this.hsm.generateKey(DataKeyType.ZPK, ZMK_LMK, KeyScheme.VARIANT_DOUBLE);
		assertNotNull(zpks);
		assertEquals(2, zpks.length);
		String zpk2 = zpks[1];
		System.out.println("ZPKzmk " + zpks[0] + " ZPKlmk " + zpks[1]);

		System.out.println("generateZPK(scheme T)");
		zpks = this.hsm.generateKey(DataKeyType.ZPK, ZMK_LMK, KeyScheme.VARIANT_TRIPLE);
		assertNotNull(zpks);
		assertEquals(2, zpks.length);
		String zpk3 = zpks[1];
		System.out.println("ZPKzmk " + zpks[0] + " ZPKlmk " + zpks[1]);

		String[] tpks;

		System.out.println("generateTPK(scheme U)");
		tpks = this.hsm.generateKey(DataKeyType.TPK, TMK_LMK, KeyScheme.VARIANT_DOUBLE);
		assertNotNull(tpks);
		assertEquals(2, tpks.length);
		System.out.println("TPKtmk " + tpks[0] + " TPKlmk " + tpks[1]);

		System.out.println("generateTPK(scheme T)");
		tpks = this.hsm.generateKey(DataKeyType.TPK, TMK_LMK, KeyScheme.VARIANT_TRIPLE);
		assertNotNull(tpks);
		assertEquals(2, tpks.length);
		System.out.println("TPKtmk " + tpks[0] + " TPKlmk " + tpks[1]);

		// senha 1234
		String tpk = "U5FE21198A7909ADD97B7B563D57C2BDD";
		String pan = "6394010300000072";
		String pbTPK = "880FEC4E5C43118E";
		String pbZPK = "0DC5172643A11B3F";
		System.out.println("verifyPinblock");
		assertTrue(this.hsm.verifyPinblock(pan, pbTPK, tpk, pbZPK, ZPK));
		// pinblock incorreto
		assertFalse(this.hsm.verifyPinblock(pan, pbTPK, tpk, "28FA2889D8160A98", ZPK));
		System.out.println("translatePinblock");

		String destZPK = "34B0C8029E8487F4";

		String pinblockDestZPK = this.hsm.translatePinblock(pan, pbZPK, ZPK, destZPK);
		System.out.println("pinblockDestZPK " + pinblockDestZPK);
		assertTrue(this.hsm.verifyPinblock(pan, pbTPK, tpk, pinblockDestZPK, destZPK));
		// pinblock incorreto
		assertFalse(this.hsm.verifyPinblock(pan, "C3B572CC565E42DE", tpk, pinblockDestZPK, destZPK));

		System.out.println("translateTPKtoZPK");
		pinblockDestZPK = this.hsm.translatePinblockTPKtoZPK(pan, pbTPK, tpk, ZPK);
		System.out.println("Destination PB " + pinblockDestZPK);
		assertEquals(pbZPK, pinblockDestZPK);

		System.out.println("verify generated ZPK2 " + zpk2);
		pinblockDestZPK = this.hsm.translatePinblockTPKtoZPK(pan, pbTPK, tpk, zpk2);
		pinblockDestZPK = this.hsm.translatePinblock(pan, pinblockDestZPK, zpk2, ZPK);
		assertEquals(pbZPK, pinblockDestZPK);

		System.out.println("verify generated ZPK3 " + zpk3);
		pinblockDestZPK = this.hsm.translatePinblockTPKtoZPK(pan, pbTPK, tpk, zpk3);
		pinblockDestZPK = this.hsm.translatePinblock(pan, pinblockDestZPK, zpk3, ZPK);
		assertEquals(pbZPK, pinblockDestZPK);
	}

	@Test
	public void testCVKCVV() throws Exception {
		String ZMK_LMK = "BF1CBFE96DE6CF2844E2F2AB0EB44C49";
		String pan = "6394010300000072";
		String cvk = "8165A82084135877BADDDD51860D0B46";

		String cvv =  this.hsm.generateCVV(pan, "0516", "015", cvk);
		assertEquals("613", cvv);
		assertTrue(this.hsm.verifyCVV("613", pan, "0516", "015", cvk));
		assertFalse(this.hsm.verifyCVV("122", pan, "0516", "015", cvk));
		assertFalse(this.hsm.verifyCVV("564", pan, "0516", "015", cvk));
		assertFalse(this.hsm.verifyCVV("613", pan, "0517", "015", cvk));
		assertFalse(this.hsm.verifyCVV("613", pan, "0717", "015", cvk));
		assertFalse(this.hsm.verifyCVV("613", pan, "0516", "016", cvk));
		assertFalse(this.hsm.verifyCVV("613", pan, "0516", "121", cvk));

		String cvv2 =  this.hsm.generateCVV2(pan, "0516", cvk);
		assertEquals("309", cvv2);
		assertTrue(this.hsm.verifyCVV2("309", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVV2("121", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVV2("741", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVV2("470", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVV2("309", pan, "0517", cvk));
		assertFalse(this.hsm.verifyCVV2("121", pan, "0717", cvk));
		assertFalse(this.hsm.verifyCVV2("309", pan, "0717", cvk));

		String cvvEmv = this.hsm.generateCVVEMV(pan, "0516", cvk);
		assertEquals("126", cvvEmv);
		assertTrue(this.hsm.verifyCVVEMV("126", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVVEMV("121", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVVEMV("741", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVVEMV("470", pan, "0516", cvk));
		assertFalse(this.hsm.verifyCVVEMV("126", pan, "0517", cvk));
		assertFalse(this.hsm.verifyCVVEMV("121", pan, "0717", cvk));
		assertFalse(this.hsm.verifyCVVEMV("126", pan, "0717", cvk));

		//CVK
		System.out.println("generateCVK");
		String[] cvks;
		try {
			cvks = this.hsm.generateKey(DataKeyType.CVK, ZMK_LMK, KeyScheme.VARIANT_TRIPLE);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid key scheme (VARIANT_TRIPLE) for key type CVK", e.getMessage());
		}
		cvks = this.hsm.generateKey(DataKeyType.CVK, ZMK_LMK, KeyScheme.VARIANT_DOUBLE);
		assertNotNull(cvks);
		System.out.println("CVK ZMK : " + cvks[0]);
		System.out.println("CVK LMK : " + cvks[1]);
		cvv = this.hsm.generateCVV(pan, "0516", "015", cvks[1]);
		System.out.println("CVV : " + cvv);
		assertFalse(this.hsm.verifyCVV(cvv, pan, "0516", "015", "8165A82084135877BADDDD51860D0B46"));
		assertTrue(this.hsm.verifyCVV(cvv, pan, "0516", "015", cvks[1]));
	}

	@Test
	public void testTAK() throws Exception {
		String TMK_LMK = "U1750CDFB0757D3B3021AE502DDEDD980";
		System.out.println("TAKTAK");
		String[] zaks = this.hsm.generateKey(DataKeyType.TAK, TMK_LMK, KeyScheme.ANSIX9_17_SINGLE);
		assertNotNull(zaks);
		assertEquals(2, zaks.length);
		System.out.println("ZAKzmk " + zaks[0] + " ZAKlmk " + zaks[1]);
	}

	@Test
	public void testMacZak() throws Exception {
		String ZMK_LMK = "BF1CBFE96DE6CF2844E2F2AB0EB44C49";
		System.out.println("generateZAK(scheme U)");
		String[] zaks = this.hsm.generateKey(DataKeyType.ZAK, ZMK_LMK, KeyScheme.VARIANT_DOUBLE);
		assertNotNull(zaks);
		assertEquals(2, zaks.length);
		String zak1 = zaks[1];
		System.out.println("ZAKzmk " + zaks[0] + " ZAKlmk " + zaks[1]);

		System.out.println("generateZAK(scheme T)");
		zaks = this.hsm.generateKey(DataKeyType.ZAK, ZMK_LMK, KeyScheme.VARIANT_TRIPLE);
		assertNotNull(zaks);
		assertEquals(2, zaks.length);
		String zak2 = zaks[1];
		System.out.println("ZAKzmk " + zaks[0] + " ZAKlmk " + zaks[1]);

		byte[] content = "0200323C640128E1921000200000000001234502181540126543211440120218110307420760511100000006142376394010300000072".getBytes(StandardCharsets.ISO_8859_1);

		try {
			this.hsm.generateMAC(content, zak1, DataKeyType.TPK, MACAlgorithm.ISO9797_ALG1_PAD1);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid key type TPK", e.getMessage());
		}
		byte[] mac1 = this.hsm.generateMAC(content, zak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG1_PAD1);
		byte[] mac2 = this.hsm.generateMAC(content, zak2, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC1 " + new String(mac1, 0));
		System.out.println("MAC2 " + new String(mac2, 0));
		assertTrue(this.hsm.verifyMAC(mac1, content, zak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG1_PAD1));
		assertTrue(this.hsm.verifyMAC(mac2, content, zak2, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertFalse(this.hsm.verifyMAC(mac1, content, zak2, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG1_PAD1));
		assertFalse(this.hsm.verifyMAC(mac1, content, zak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG1_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, zak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG3_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, zak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertFalse(this.hsm.verifyMAC(mac2, content, zak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG1_PAD1));
		assertFalse(this.hsm.verifyMAC(mac1, content, zak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD1));
	}

	@Test
	public void testMacTak() throws Exception {
		String TMK_LMK = "U1750CDFB0757D3B3021AE502DDEDD980";
		System.out.println("generateTAK(scheme U)");
		String[] taks = this.hsm.generateKey(DataKeyType.TAK, TMK_LMK, KeyScheme.VARIANT_DOUBLE);
		assertNotNull(taks);
		assertEquals(2, taks.length);
		String tak1 = taks[1];
		System.out.println("TAKzmk " + taks[0] + " TAKlmk " + taks[1]);

		System.out.println("generateTAK(scheme T)");
		taks = this.hsm.generateKey(DataKeyType.TAK, TMK_LMK, KeyScheme.VARIANT_TRIPLE);
		assertNotNull(taks);
		assertEquals(2, taks.length);
		String tak2 = taks[1];
		System.out.println("TAKzmk " + taks[0] + " TAKlmk " + taks[1]);

		byte[] content = "0200323C640128E1921000200000000001234502181540126543211440120218110307420760511100000006142376394010300000072".getBytes(StandardCharsets.ISO_8859_1);

		byte[] mac1 = this.hsm.generateMAC(content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD2);
		byte[] mac2 = this.hsm.generateMAC(content, tak2, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD2);
		System.out.println("MAC1 " + new String(mac1, 0));
		System.out.println("MAC2 " + new String(mac2, 0));
		assertTrue(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD2));
		assertTrue(this.hsm.verifyMAC(mac2, content, tak2, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak2, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD1));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertFalse(this.hsm.verifyMAC(mac2, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD1));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG1_PAD1));

		content = "01234567".getBytes(StandardCharsets.ISO_8859_1);
		mac1 = this.hsm.generateMAC(content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD2);
		mac2 = this.hsm.generateMAC(content, tak2, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD2);
		System.out.println("MAC1 " + new String(mac1, 0));
		System.out.println("MAC2 " + new String(mac2, 0));
		assertTrue(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD2));
		assertTrue(this.hsm.verifyMAC(mac2, content, tak2, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak2, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD1));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD2));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertFalse(this.hsm.verifyMAC(mac2, content, tak1, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG1_PAD1));
		assertFalse(this.hsm.verifyMAC(mac1, content, tak1, DataKeyType.ZAK, MACAlgorithm.ISO9797_ALG1_PAD1));
	}

	@Test
	public void testGreatMac() throws Exception {
		String taklmk = "U2E4B14522879924B9F88BE0FA84AC0C4";
		StringBuilder sb;
		char c;

		sb = new StringBuilder(32000);
		c = 32;
		for (int i = 0; i < 32000; i++) {
			sb.append(c++);
			if (c == 127) {
				c = 160;
			} else if (c == 256) {
				c = 32;
			}
		}
		byte[] content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		System.out.println("Length " + content.length);
		byte[] mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC  32000 " + new String(mac, 0));
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));
	}

	@Test
	public void testBigMac() throws Exception {
		String taklmk = "U2E4B14522879924B9F88BE0FA84AC0C4";
		StringBuilder sb;
		char c;

		sb = new StringBuilder(32003);
		c = 32;
		for (int i = 0; i < 32003; i++) {
			sb.append(c++);
			if (c == 127) {
				c = 160;
			} else if (c == 256) {
				c = 32;
			}
		}
		byte[] content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		byte[] mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC  32003 " + new String(mac, 0));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));

		sb = new StringBuilder(32023);
		c = 0;
		for (int i = 0; i < 32023; i++, c++) {
			sb.append(c);
		}
		content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC  32023 " + new String(mac, 0));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));

		sb = new StringBuilder(32024);
		c = 0;
		for (int i = 0; i < 32024; i++, c++) {
			sb.append(c);
		}
		content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC  32024 " + new String(mac, 0));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));

		sb = new StringBuilder(64000);
		c = 'A';
		for (int i = 0; i < 64000; i++, c++) {
			if (c > 'Z') c = 'A';
			sb.append(c);
		}
		content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC  64000 " + mac);
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));

		sb = new StringBuilder(64003);
		c = 'A';
		for (int i = 0; i < 64003; i++, c++) {
			if (c > 'Z') c = 'A';
			sb.append(c);
		}
		content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC  64003 " + mac);
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));

		sb = new StringBuilder(64023);
		c = 'A';
		for (int i = 0; i < 64023; i++, c++) {
			if (c > 'Z') c = 'A';
			sb.append(c);
		}
		content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC  64023 " + mac);
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));

		sb = new StringBuilder(160003);
		c = 'A';
		for (int i = 0; i < 160003; i++, c++) {
			if (c > 255) c = 0;
			sb.append(c);
		}
		content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC 160003 " + mac);
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));

		byte[] nn = new byte[content.length + 8];
		System.arraycopy(content, 0, nn, 0, content.length);
		System.arraycopy(mac, 0, nn, content.length, 8);

		System.out.println("MAC 160003 embutido no conteudo");
		assertTrue(this.hsm.verifyMAC(nn, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));

		sb = new StringBuilder(160033);
		c = 'A';
		for (int i = 0; i < 160033; i++, c++) {
			if (c > 255) c = 0;
			sb.append(c);
		}
		content = sb.toString().getBytes(StandardCharsets.ISO_8859_1);
		mac = this.hsm.generateMAC(content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1);
		System.out.println("MAC 160033 " + mac);
		assertTrue(this.hsm.verifyMAC(mac, content, taklmk, DataKeyType.TAK, MACAlgorithm.ISO9797_ALG3_PAD1));
		assertEquals(new String(mac, 0), this.generateMAC(new String(content, 0)));

	}

	private String generateMAC(String content) throws Exception {
		byte[] tak = HexadecimalStringUtil.bytesFromString("041F199483A86832490B57DFA7C19EC7");
		// alg3 pad1
		byte[] data = content.getBytes(StandardCharsets.ISO_8859_1);
		Cipher c = Cipher.getInstance("DES/CBC/NoPadding");
		SecretKeyFactory deskeyfact = SecretKeyFactory.getInstance("DES");
		SecretKey tak1 = deskeyfact.generateSecret(new DESKeySpec(tak));
		c.init(Cipher.ENCRYPT_MODE, tak1, new IvParameterSpec(new byte[8]));
		if (data.length % 8 != 0) {
			byte[] newdata = new byte[(data.length / 8 + 1) * 8];
			System.arraycopy(data, 0, newdata, 0, data.length);
			// 00 padding - do nothing
			data = newdata;
			System.out.println("padded");
		}
		byte[] enc = c.doFinal(data);
		// output transformation 3
		// Hq (last block encrypted) is decrypted with the key K? and the result encrypted with the key K:
		// G = eK(dK?(Hq))
		SecretKey tak2 = deskeyfact.generateSecret(new DESKeySpec(tak, 8));
		c = Cipher.getInstance("DES/ECB/NoPadding");
		c.init(Cipher.DECRYPT_MODE, tak2);
		byte[] hq1 = c.doFinal(enc, enc.length - 8, 8);
		c.init(Cipher.ENCRYPT_MODE, tak1);
		byte[] g = c.doFinal(hq1);
		String gg = HexadecimalStringUtil.bytesToString(g).substring(0, 8);
		System.out.println("Generated " + gg);
		return gg;
	}

	@Test
	public void testPVKPVV() throws Exception {
		String TMK_LMK = "U1750CDFB0757D3B3021AE502DDEDD980";
		String pvk = "U22E37C58EA6041878F21842A9AAFE514";
		// senha 1234
		String tpk = "U5FE21198A7909ADD97B7B563D57C2BDD";
		String pan = "6394010300000072";
		String pbTPK = "880FEC4E5C43118E";
		try {
			this.hsm.generatePVV(pan, pbTPK, tpk, DataKeyType.TAK, 0, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid key type TAK", e.getMessage());
		}
		try {
			this.hsm.generatePVV(pan, pbTPK, tpk, DataKeyType.TPK, -1, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid index -1", e.getMessage());
		}
		try {
			this.hsm.generatePVV(pan, pbTPK, tpk, DataKeyType.TPK, 10, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid index 10", e.getMessage());
		}
		String pvv = this.hsm.generatePVV(pan, pbTPK, tpk, DataKeyType.TPK, 0, pvk);
		assertEquals("5431", pvv);
		assertTrue(this.hsm.verifyPVV("5431", pan, pbTPK, tpk, DataKeyType.TPK, 0, pvk));
		assertFalse(this.hsm.verifyPVV("5431", pan, pbTPK, tpk, DataKeyType.TPK, 1, pvk));
		assertFalse(this.hsm.verifyPVV("5430", pan, pbTPK, tpk, DataKeyType.TPK, 0, pvk));
		//PVK
		System.out.println("generatePVK");
		String[] pvks = this.hsm.generateKey(DataKeyType.PVK, TMK_LMK, KeyScheme.VARIANT_DOUBLE);
		assertNotNull(pvks);
		System.out.println("PVK TMK : " + pvks[0]);
		System.out.println("PVK LMK : " + pvks[1]);
		pvv = this.hsm.generatePVV(pan, pbTPK, tpk, DataKeyType.TPK, 0, pvks[1]);
		System.out.println("PVV : " + pvv);
		assertFalse(this.hsm.verifyPVV(pvv, pan, pbTPK, tpk, DataKeyType.TPK, 0, "8165A82084135877BADDDD51860D0B46"));
		assertTrue(this.hsm.verifyPVV(pvv, pan, pbTPK, tpk, DataKeyType.TPK, 0, pvks[1]));
		try {
			this.hsm.verifyPVV(pvv, pan, pbTPK, tpk, DataKeyType.TAK, 0, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid key type TAK", e.getMessage());
		}
		try {
			this.hsm.verifyPVV(pvv, pan, pbTPK, tpk, DataKeyType.TPK, -1, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid index -1", e.getMessage());
		}
		try {
			this.hsm.verifyPVV(pvv, pan, pbTPK, tpk, DataKeyType.TPK, 10, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid index 10", e.getMessage());
		}
	}

	@Test
	public void testPVKPVV_Token() throws Exception {
		String pvk = "U22E37C58EA6041878F21842A9AAFE514";
		String pan = "6394010300000072";
		String pvv = "5431"; // senha 1234

		String zpk = "U6CFED088DF27F5B0EE665AEF8DEA07EC";
		String token = "123456789";
		String pb0ZPK = "E23F4D9CE15A6D9B";
		String pb3ZPK = "E718693376192D6C";

		assertTrue(this.hsm.verifyPVV(pvv, token, pan, pb0ZPK, zpk, 0, pvk));
		assertFalse(this.hsm.verifyPVV(pvv, token, pan, pb0ZPK, zpk, 1, pvk));
		assertFalse(this.hsm.verifyPVV("5430", token, pan, pb0ZPK, zpk, 0, pvk));

		assertTrue(this.hsm.verifyPVV(pvv, token, pan, pb3ZPK, Pinblock.Format.ISO_3, zpk, 0, pvk));
		assertFalse(this.hsm.verifyPVV(pvv, token, pan, pb3ZPK, Pinblock.Format.ISO_3, zpk, 1, pvk));
		assertFalse(this.hsm.verifyPVV("5430", token, pan, pb3ZPK, Pinblock.Format.ISO_3, zpk, 0, pvk));
		try {
			this.hsm.verifyPVV(pvv, token, pan, pb0ZPK, zpk, -1, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid index -1", e.getMessage());
		}
		try {
			this.hsm.verifyPVV(pvv, token, pan, pb0ZPK, zpk, 10, pvk);
			fail("Must throw Exception");
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid index 10", e.getMessage());
		}
	}

	@Test
	public void testPVVToken2() throws Exception {
		String pvk = "U22E37C58EA6041878F21842A9AAFE514";
		// senha 1234
		String pan = "6394010300000072";
		String token = "099988887777";
		String tkPbZPK = "5B65E04FBB28998F";
		String zpk = "U54B73F1DF5E5EF4EC56DD1EDA6889369";
		String pvv =  "5431";
		assertTrue(this.hsm.verifyPVV(pvv, token, pan, tkPbZPK, zpk, 0, pvk));
		assertFalse(this.hsm.verifyPVV(pvv, token, pan, tkPbZPK, zpk, 1, pvk));
		assertFalse(this.hsm.verifyPVV("5430", token, pan, tkPbZPK, zpk, 0, pvk));
	}

	@Test
	public void testPVVToken3() throws Exception {
		String pvk = "UC7117B8B71B1674E9AC2BE583648C465";
		// senha 1111
		String pan = "6035740358253257";
		String token = "99441099440";
		String tkPbZPK = "99e8fc4d8542afc9";
		String zpk = "U063A0E7C0F2124E54BFA531C4095771D";
		String pvv =  "0677";
		assertTrue(this.hsm.verifyPVV(pvv, token, pan, tkPbZPK, zpk, 1, pvk));
		assertFalse(this.hsm.verifyPVV(pvv, token, pan, tkPbZPK, zpk, 0, pvk));
		assertFalse(this.hsm.verifyPVV("1677", token, pan, tkPbZPK, zpk, 1, pvk));
	}

	@Test
	public void testVerifyARQC_Frete() throws Exception {//6056630000000301
//		this.verifyARQC("6056630000000293", HexadecimalStringUtil.bytesFromString("5A0860566300000001375F3401009A031104015F2A0209868407A0000005081214950560A00808009C01009F02060000000010009F03060000000000009F1008010103A03CF000009F2701809F26085890E9E83B7E10349F360200029F3704C16B8B02820258005F25031210015F2403150131"));

		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-121, (byte)-75, (byte)-101, (byte)-83, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)126, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)21, (byte)-24, (byte)-56, (byte)-1, (byte)-119, (byte)90, (byte)94, (byte)-41, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-88, (byte)-6, (byte)44, (byte)-19, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-121, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)116, (byte)7, (byte)93, (byte)-21, (byte)63, (byte)83, (byte)-60, (byte)103, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-54, (byte)68, (byte)-30, (byte)6, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-75, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)25, (byte)-24, (byte)-127, (byte)42, (byte)-76, (byte)-106, (byte)42, (byte)126, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-72, (byte)45, (byte)52, (byte)38, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-64, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)45, (byte)-77, (byte)33, (byte)-104, (byte)75, (byte)3, (byte)25, (byte)-109, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-85, (byte)-107, (byte)102, (byte)-83, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-72, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-53, (byte)61, (byte)122, (byte)-74, (byte)2, (byte)99, (byte)-17, (byte)-34, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)121, (byte)-23, (byte)-28, (byte)-99, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-73, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)14, (byte)14, (byte)7, (byte)-7, (byte)-45, (byte)3, (byte)-86, (byte)26, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)23, (byte)-81, (byte)-83, (byte)-44, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-68, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)49, (byte)30, (byte)-59, (byte)11, (byte)50, (byte)-53, (byte)65, (byte)105, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)41, (byte)-53, (byte)86, (byte)-47, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-76, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-93, (byte)-25, (byte)-105, (byte)122, (byte)16, (byte)14, (byte)114, (byte)117, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)44, (byte)29, (byte)3, (byte)-19, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-71, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-31, (byte)12, (byte)80, (byte)-23, (byte)-35, (byte)-5, (byte)51, (byte)-48, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)54, (byte)-75, (byte)-77, (byte)58, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-77, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)82, (byte)23, (byte)-22, (byte)19, (byte)-89, (byte)21, (byte)123, (byte)9, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)8, (byte)23, (byte)-66, (byte)-35, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-65, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-64, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)103, (byte)50, (byte)67, (byte)-108, (byte)60, (byte)-36, (byte)50, (byte)-49, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)23, (byte)-37, (byte)58, (byte)-59, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-70, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)111, (byte)-80, (byte)103, (byte)45, (byte)-127, (byte)57, (byte)14, (byte)83, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-24, (byte)57, (byte)-13, (byte)2, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-125, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-23, (byte)18, (byte)88, (byte)125, (byte)-31, (byte)37, (byte)41, (byte)-13, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-55, (byte)120, (byte)59, (byte)-16, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-126, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)113, (byte)20, (byte)-126, (byte)74, (byte)82, (byte)93, (byte)32, (byte)-30, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)3, (byte)1, (byte)100, (byte)-13, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)127, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)106, (byte)36, (byte)-3, (byte)40, (byte)-49, (byte)55, (byte)122, (byte)60, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)54, (byte)21, (byte)110, (byte)121, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)125, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-83, (byte)-8, (byte)67, (byte)-37, (byte)-43, (byte)56, (byte)26, (byte)102, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)89, (byte)20, (byte)1, (byte)76, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-127, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)125, (byte)-87, (byte)60, (byte)-28, (byte)90, (byte)1, (byte)-96, (byte)-65, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-118, (byte)-60, (byte)-7, (byte)124, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-122, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)104, (byte)31, (byte)-51, (byte)-126, (byte)91, (byte)-32, (byte)9, (byte)100, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-12, (byte)120, (byte)-20, (byte)13, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-119, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)12, (byte)-62, (byte)41, (byte)114, (byte)-53, (byte)-3, (byte)108, (byte)-48, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-22, (byte)-6, (byte)84, (byte)-112, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-123, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)45, (byte)80, (byte)48, (byte)-108, (byte)-55, (byte)70, (byte)49, (byte)53, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)85, (byte)86, (byte)-15, (byte)73, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-120, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-33, (byte)-70, (byte)-46, (byte)-100, (byte)-19, (byte)-51, (byte)68, (byte)76, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)32, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)56, (byte)-25, (byte)78, (byte)91, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-90, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)42, (byte)67, (byte)-14, (byte)-108, (byte)33, (byte)-15, (byte)57, (byte)79, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-35, (byte)115, (byte)117, (byte)91, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-69, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-90, (byte)53, (byte)-35, (byte)-9, (byte)31, (byte)44, (byte)-34, (byte)-76, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)35, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-113, (byte)91, (byte)-79, (byte)107, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-63, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)80, (byte)-91, (byte)-32, (byte)100, (byte)40, (byte)89, (byte)-50, (byte)112, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)35, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)13, (byte)27, (byte)-35, (byte)75, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-67, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)45, (byte)-99, (byte)47, (byte)120, (byte)123, (byte)-54, (byte)90, (byte)53, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)35, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)5, (byte)96, (byte)40, (byte)-114, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-66, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)83, (byte)-40, (byte)19, (byte)127, (byte)-127, (byte)96, (byte)124, (byte)-66, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)16, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-27, (byte)-106, (byte)7, (byte)99, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-128, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-117, (byte)82, (byte)-125, (byte)38, (byte)4, (byte)-74, (byte)-80, (byte)-123, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000301", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)16, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)123, (byte)37, (byte)107, (byte)-32, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-124, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-29, (byte)115, (byte)-43, (byte)-22, (byte)99, (byte)49, (byte)73, (byte)-11, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.failedARQC_Frete("6056630000000293", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)16, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)123, (byte)37, (byte)107, (byte)-32, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)15, (byte)-124, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-29, (byte)115, (byte)-43, (byte)-22, (byte)99, (byte)49, (byte)73, (byte)-11, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});

		this.verifyARQC_Frete("6056630000000392", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)1, (byte)80, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)64, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)42, (byte)12, (byte)-19, (byte)84, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)8, (byte)125, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)48, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-4, (byte)-78, (byte)-72, (byte)-10, (byte)-4, (byte)68, (byte)100, (byte)2, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.failedARQC_Frete("6056630000000392", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)1, (byte)0, (byte)0, (byte)1, (byte)80, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)64, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)42, (byte)12, (byte)-19, (byte)84, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)8, (byte)125, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)48, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-4, (byte)-78, (byte)-72, (byte)-10, (byte)-4, (byte)68, (byte)100, (byte)2, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000392", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)32, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-61, (byte)30, (byte)74, (byte)10, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)8, (byte)-114, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)48, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-46, (byte)17, (byte)12, (byte)12, (byte)88, (byte)-32, (byte)-5, (byte)46, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000392", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)1, (byte)80, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)64, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-56, (byte)-120, (byte)-35, (byte)1, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)8, (byte)123, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)102, (byte)52, (byte)118, (byte)-106, (byte)-5, (byte)-114, (byte)-2, (byte)4, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});

		this.verifyARQC_Frete("6056630000000061", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-106, (byte)-22, (byte)53, (byte)120, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)35, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)28, (byte)54, (byte)-116, (byte)8, (byte)116, (byte)-35, (byte)5, (byte)-26, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-111, (byte)-13, (byte)-110, (byte)-40, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)38, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)1, (byte)100, (byte)7, (byte)105, (byte)112, (byte)95, (byte)101, (byte)26, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-107, (byte)40, (byte)79, (byte)-50, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)32, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)81, (byte)1, (byte)-101, (byte)-104, (byte)-127, (byte)-89, (byte)-87, (byte)34, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-60, (byte)92, (byte)77, (byte)120, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)41, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-92, (byte)-5, (byte)77, (byte)-11, (byte)64, (byte)-91, (byte)126, (byte)-118, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)109, (byte)62, (byte)-8, (byte)-82, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)39, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)41, (byte)27, (byte)73, (byte)74, (byte)46, (byte)68, (byte)121, (byte)73, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)16, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)23, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-26, (byte)-91, (byte)48, (byte)57, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)30, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)114, (byte)-4, (byte)85, (byte)7, (byte)-3, (byte)-80, (byte)-119, (byte)-76, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)16, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-5, (byte)-90, (byte)-31, (byte)-19, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)45, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-102, (byte)-63, (byte)10, (byte)-13, (byte)-17, (byte)-111, (byte)-55, (byte)114, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)16, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)64, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-117, (byte)117, (byte)52, (byte)-43, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)49, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-51, (byte)109, (byte)-26, (byte)-118, (byte)67, (byte)36, (byte)-26, (byte)111, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000061", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)3, (byte)-103, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)64, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-9, (byte)71, (byte)61, (byte)79, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)43, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-26, (byte)2, (byte)-11, (byte)-101, (byte)-67, (byte)83, (byte)89, (byte)14, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.failedARQC_Frete("6056630000000293", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)3, (byte)-103, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)64, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-9, (byte)71, (byte)61, (byte)79, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)43, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-26, (byte)2, (byte)-11, (byte)-101, (byte)-67, (byte)83, (byte)89, (byte)14, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.failedARQC_Frete("6056630000000061", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)3, (byte)-103, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)64, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)24, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-9, (byte)71, (byte)61, (byte)79, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)1, (byte)43, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-93, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-26, (byte)2, (byte)-11, (byte)-101, (byte)-67, (byte)83, (byte)89, (byte)14, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});

		this.verifyARQC_Frete("6056630000000293", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)32, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-37, (byte)2, (byte)32, (byte)14, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)4, (byte)113, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-94, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)112, (byte)105, (byte)-39, (byte)117, (byte)-100, (byte)39, (byte)-70, (byte)-105, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000293", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)32, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)105, (byte)-23, (byte)-72, (byte)14, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)4, (byte)106, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-94, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-108, (byte)-29, (byte)35, (byte)-25, (byte)17, (byte)82, (byte)28, (byte)73, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000293", false, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)1, (byte)0, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)33, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)77, (byte)97, (byte)-55, (byte)-101, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)4, (byte)114, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-94, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)-18, (byte)51, (byte)-110, (byte)-123, (byte)-95, (byte)31, (byte)-3, (byte)11, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
		this.verifyARQC_Frete("6056630000000293", true, new byte[] {(byte)95, (byte)52, (byte)1, (byte)0, (byte)-97, (byte)2, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)18, (byte)52, (byte)-97, (byte)3, (byte)6, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)0, (byte)-97, (byte)26, (byte)2, (byte)0, (byte)118, (byte)-107, (byte)5, (byte)0, (byte)0, (byte)4, (byte)-128, (byte)0, (byte)95, (byte)42, (byte)2, (byte)9, (byte)-122, (byte)-102, (byte)3, (byte)20, (byte)17, (byte)32, (byte)-100, (byte)1, (byte)0, (byte)-97, (byte)55, (byte)4, (byte)-18, (byte)-128, (byte)124, (byte)14, (byte)-126, (byte)2, (byte)88, (byte)0, (byte)-97, (byte)54, (byte)2, (byte)4, (byte)108, (byte)-97, (byte)39, (byte)1, (byte)-128, (byte)-97, (byte)16, (byte)8, (byte)1, (byte)1, (byte)3, (byte)-94, (byte)49, (byte)-16, (byte)0, (byte)0, (byte)-97, (byte)38, (byte)8, (byte)125, (byte)68, (byte)-13, (byte)-115, (byte)-12, (byte)78, (byte)0, (byte)62, (byte)-97, (byte)51, (byte)3, (byte)-32, (byte)-8, (byte)-56});
	}

	private void verifyARQC_Frete(String pan, boolean cvr, byte[] dadosEMV) throws Exception {
		String imkAC = "U83C1C0670EF7FA5A70DF9C2BAE45470D";
		TLVData tlvData = TLVDataParser.parseData(dadosEMV);
		if (cvr) {
			// trocamos o IAD por CVR, para testar as duas formas implementadas no verifyARQC
			TLV[] tlvs = tlvData.getData();
			for (int i = 0; i < tlvs.length; i++) {
				if (tlvs[i].getDataElement() == EMVDataElement.ISSUER_APPLICATION_DATA) {
					PrimitiveTLV tlv = (PrimitiveTLV) tlvs[i];
					tlvs[i] = new PrimitiveTLV(EMVDataElement.CARD_VERIFICATION_RESULTS_MCHIP, Arrays.copyOfRange(tlv.getValue(), 2, 6));
				}
			}
		}
		assertTrue("Failed " + pan + ' ' +  HexadecimalStringUtil.bytesToString(dadosEMV), this.hsm.verifyARQC(EMVScheme.MASTERCARD_MCHIP, imkAC, pan, 0, tlvData));
	}

	private void failedARQC_Frete(String pan, boolean cvr, byte[] dadosEMV) throws Exception {
		String imkAC = "U83C1C0670EF7FA5A70DF9C2BAE45470D";
		TLVData tlvData = TLVDataParser.parseData(dadosEMV);
		if (cvr) {
			// trocamos o IAD por CVR, para testar as duas formas implementadas no verifyARQC
			TLV[] tlvs = tlvData.getData();
			for (int i = 0; i < tlvs.length; i++) {
				if (tlvs[i].getDataElement() == EMVDataElement.ISSUER_APPLICATION_DATA) {
					PrimitiveTLV tlv = (PrimitiveTLV) tlvs[i];
					tlvs[i] = new PrimitiveTLV(EMVDataElement.CARD_VERIFICATION_RESULTS_MCHIP, Arrays.copyOfRange(tlv.getValue(), 2, 6));
				}
			}
		}
		assertFalse("Do not failed " + pan + ' ' +  HexadecimalStringUtil.bytesToString(dadosEMV), this.hsm.verifyARQC(EMVScheme.MASTERCARD_MCHIP, imkAC, pan, 0, tlvData));
	}

	@Test
	public void testVerifyARQC_PAT() throws Exception {//6056630000000301
		System.out.println("VerifyARQC_PAT");
		this.verifyARQC_PAT("6056650302915154", HexadecimalStringUtil.bytesFromString("9F02060017698300349F03060000000000009F1A020076950560800000005F2A0209869A031503239C01009F3704507F9EC682025800"
				+ "9F2701809F360200189F10200FA501A00300040000000000000000000F058305089091BA01930E00000000009F260830B8BDD656AF02AD"));
		this.verifyARQCGenerateARPC_PAT("6056650302915154", HexadecimalStringUtil.bytesFromString("9F02060017698300349F03060000000000009F1A020076950560800000005F2A0209869A031503239C01009F3704507F9EC682025800"
				+ "9F2701809F360200189F10200FA501A00300040000000000000000000F058305089091BA01930E00000000009F260830B8BDD656AF02AD"),
				HexadecimalStringUtil.bytesFromString("F03AC175038685D7"));
		this.failedARQC_PAT("6056650302915154", HexadecimalStringUtil.bytesFromString("9F02060013652029959F03060000000000009F1A020076950560800000005F2A0209869A031503239C01009F3704" + /* OK "7B8BB16F" */ "7B8BC16F" + "82025800"
				+ "9F2701809F360200199F10200FA501A00300040000000000000000000F058305089091BA01930E00000000009F2608A7E9A3832CAC878C"));

		this.verifyARQC_PAT("6056640350222538", HexadecimalStringUtil.bytesFromString("9F02060003974208269F03060000000000009F1A020076950560800000005F2A0209869A031503239C01009F37046F19CD6882025800"
				+ "9F2701809F360200039F10200FA501A00200040000000000000000000F058305089091BA01935C00000000009F26088420F9C0110E68BE"));
		this.verifyARQCGenerateARPC_PAT("6056640350222538", HexadecimalStringUtil.bytesFromString("9F02060003974208269F03060000000000009F1A020076950560800000005F2A0209869A031503239C01009F37046F19CD6882025800"
				+ "9F2701809F360200039F10200FA501A00200040000000000000000000F058305089091BA01935C00000000009F26088420F9C0110E68BE"),
				HexadecimalStringUtil.bytesFromString("309670ED48F612BB"));

		this.verifyARQC_PAT("6056640350222520", HexadecimalStringUtil.bytesFromString("9F02060004722300699F03060000000000009F1A020076950560800000005F2A0209869A031503239C01009F370417FB97A482025800"
				+ "9F2701809F360200029F10200FA501A00200000000000000000000000F058305089091BA0193B200000000009F26081441FEB48B5548E0"));
		this.verifyARQCGenerateARPC_PAT("6056640350222520", HexadecimalStringUtil.bytesFromString("9F02060004722300699F03060000000000009F1A020076950560800000005F2A0209869A031503239C01009F370417FB97A482025800"
				+ "9F2701809F360200029F10200FA501A00200000000000000000000000F058305089091BA0193B200000000009F26081441FEB48B5548E0"),
				HexadecimalStringUtil.bytesFromString("FB9092C8A866EDAA"));
	}

	private void verifyARQC_PAT(String pan, byte[] dadosEMV) throws Exception {
		String imkAC = "U87E7B360B5E6DF7101F481B145E1A809";
		TLVData tlvData = TLVDataParser.parseData(dadosEMV);
		assertTrue("Failed " + pan + ' ' +  HexadecimalStringUtil.bytesToString(dadosEMV), this.hsm.verifyARQC(EMVScheme.CCD, imkAC, pan, 0, tlvData));
	}

	private void verifyARQCGenerateARPC_PAT(String pan, byte[] dadosEMV, byte[] arpc) throws Exception {
		String imkAC = "U87E7B360B5E6DF7101F481B145E1A809";
		TLVData tlvData = TLVDataParser.parseData(dadosEMV);
		System.out.println("PAN " + pan + " " + tlvData);
		byte[] _arpc = this.hsm.verifyARQCGenerateARPC(EMVScheme.CCD, imkAC, pan, 0, tlvData, "00");
		System.out.println("ARPC " + HexadecimalStringUtil.bytesToString(_arpc));
		assertArrayEquals("Failed " + pan + ' ' +  HexadecimalStringUtil.bytesToString(dadosEMV)
				+ " Expected " + HexadecimalStringUtil.bytesToString(arpc)
				+ " Found " + HexadecimalStringUtil.bytesToString(_arpc),
				arpc, _arpc);
	}

	private void failedARQC_PAT(String pan, byte[] dadosEMV) throws Exception {
		String imkAC = "U87E7B360B5E6DF7101F481B145E1A809";
		TLVData tlvData = TLVDataParser.parseData(dadosEMV);
		assertFalse("Do not failed " + pan + ' ' +  HexadecimalStringUtil.bytesToString(dadosEMV), this.hsm.verifyARQC(EMVScheme.CCD, imkAC, pan, 0, tlvData));
	}

	@Test
	public void testWeakPIN() throws Exception {
		String TMK = "0123456789ABCDEF0123456789ABCDEF";
		String TMK_LMK = "U1750CDFB0757D3B3021AE502DDEDD980";
		String pvk = "UC7117B8B71B1674E9AC2BE583648C465";
		String[] tpks = this.hsm.generateKey(DataKeyType.TPK, TMK_LMK, KeyScheme.VARIANT_DOUBLE);
		String tpkTMK = tpks[0];
		String tpkLMK = tpks[1];
		PinPadHSM pinpad = new PinPadHSM(TMK, tpkTMK);
		String pan = "6035740107811157";

		this.hsm.loadExcludedPINTable(4);

		String pin = "1234";
		String pb = pinpad.encrypt(pin, pan);
		String pvv = this.hsm.generatePVV(pan, pb, tpkLMK, DataKeyType.TPK, 0, pvk,
				"0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999");
		System.out.println(pin + ' ' + pb + ' ' + (pvv != null ? pvv : "ERROR"));
		assertEquals("7633", pvv);
		String offset = this.hsm.generateIBMOffset(pan, pb, 4, tpkLMK, DataKeyType.TPK, pvk, DEC_TABLE, pan,
				"0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999");
		assertEquals("7815", offset);

		pb = pinpad.encrypt(pin, pan);
		pvv = this.hsm.generatePVV(pan, pb, tpkLMK, DataKeyType.TPK, 0, pvk,
				"1234", "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999" );
		System.out.println(pin + ' ' + pb + ' ' + (pvv != null ? pvv : "ERROR"));
		assertNull(pvv);
		offset = this.hsm.generateIBMOffset(pan, pb, 4, tpkLMK, DataKeyType.TPK, pvk, DEC_TABLE, pan,
				"1234", "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999" );
		assertNull(offset);

		pin = "5555";
		pb = pinpad.encrypt(pin, pan);
		pvv = this.hsm.generatePVV(pan, pb, tpkLMK, DataKeyType.TPK, 0, pvk,
				"1234", "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999" );
		System.out.println(pin + ' ' + pb + ' ' + (pvv != null ? pvv : "ERROR"));
		assertNull(pvv);
		offset = this.hsm.generateIBMOffset(pan, pb, 4, tpkLMK, DataKeyType.TPK, pvk, DEC_TABLE, pan,
				"1234", "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999" );
		assertNull(offset);

		this.hsm.loadExcludedPINTable(4,
				"1234", "0000", "1111", "2222", "3333", "4444", "5555", "6666", "7777", "8888", "9999"
		);

		pin = "1234";
		pb = pinpad.encrypt(pin, pan);
		pvv = this.hsm.generatePVV(pan, pb, tpkLMK, DataKeyType.TPK, 0, pvk);
		System.out.println(pin + ' ' + pb + ' ' + (pvv != null ? pvv : "ERROR"));
		assertNull(pvv);
		offset = this.hsm.generateIBMOffset(pan, pb, 4, tpkLMK, DataKeyType.TPK, pvk, DEC_TABLE, pan);
		assertNull(offset);

		pin = "5555";
		pb = pinpad.encrypt(pin, pan);
		pvv = this.hsm.generatePVV(pan, pb, tpkLMK, DataKeyType.TPK, 0, pvk);
		System.out.println(pin + ' ' + pb + ' ' + (pvv != null ? pvv : "ERROR"));
		assertNull(pvv);
		offset = this.hsm.generateIBMOffset(pan, pb, 4, tpkLMK, DataKeyType.TPK, pvk, DEC_TABLE, pan);
		assertNull(offset);

		this.hsm.loadExcludedPINTable(4);

		pin = "1234";
		pb = pinpad.encrypt(pin, pan);
		pvv = this.hsm.generatePVV(pan, pb, tpkLMK, DataKeyType.TPK, 0, pvk);
		System.out.println(pin + ' ' + pb + ' ' + (pvv != null ? pvv : "ERROR"));
		assertEquals("7633", pvv);
		offset = this.hsm.generateIBMOffset(pan, pb, 4, tpkLMK, DataKeyType.TPK, pvk, DEC_TABLE, pan);
		assertEquals("7815", offset);

		pin = "5555";
		pb = pinpad.encrypt(pin, pan);
		pvv = this.hsm.generatePVV(pan, pb, tpkLMK, DataKeyType.TPK, 0, pvk);
		System.out.println(pin + ' ' + pb + ' ' + (pvv != null ? pvv : "ERROR"));
		assertEquals("9306", pvv);
		offset = this.hsm.generateIBMOffset(pan, pb, 4, tpkLMK, DataKeyType.TPK, pvk, DEC_TABLE, pan);
		assertEquals("1136", offset);

	}

	@Test
	public void testPINs() throws Exception {
		String cartao = "6394010302535828";
		String pin4 = "D4D0A41FBEB923EC";
		String pin5 = "3F83C7DE05662046";
		String zpk = "U3F58B01483A4D42013214E43762B02DE";
		String pvk = "UC7117B8B71B1674E9AC2BE583648C465";
		String pvv;
		boolean ok;

		pvv = this.hsm.generatePVV(cartao, pin4, zpk, DataKeyType.ZPK, 1, pvk);
		System.out.println("PVV " + pvv);
		assertEquals("6908", pvv);

		ok = hsm.verifyPVV("6908", cartao, pin4, zpk, DataKeyType.ZPK, 1, pvk);
		System.out.println("PVV " + ok);
		assertTrue(ok);

		pvv = this.hsm.generatePVV(cartao, pin5, zpk, DataKeyType.ZPK, 1, pvk);
		System.out.println("PVV " + pvv);
		assertEquals("6908", pvv);

		ok = hsm.verifyPVV("6908", cartao, pin5, zpk, DataKeyType.ZPK, 1, pvk);
		System.out.println("PVV " + ok);
		assertTrue(ok);
	}

	@Test
	public void testKCV() throws Exception {
		String kcv;
		kcv = this.hsm.generateKCV("U5FE21198A7909ADD97B7B563D57C2BDD", DataKeyType.TPK);
		assertEquals("AD64F5", kcv);
		kcv = this.hsm.generateKCV("UE23E50551C1516DC2C54766BFCBFAF07", DataKeyType.ZPK);
		assertEquals("C663B5", kcv);
		kcv = this.hsm.generateKCV("U2F6EECB76D69448418D4502C1033E474", DataKeyType.CVK);
		assertEquals("645F0B", kcv);
		kcv = this.hsm.generateKCV("UC7117B8B71B1674E9AC2BE583648C465", DataKeyType.PVK);
		assertEquals("9ABE79", kcv);
		kcv = this.hsm.generateKCV("U97FD6BF8C125552B723554293B8B33E6", DataKeyType.ZPK);
		assertEquals("C651D4", kcv);
		kcv = this.hsm.generateKCV("U3F58B01483A4D42013214E43762B02DE", DataKeyType.ZPK);
		assertEquals("C78FDB", kcv);
		kcv = this.hsm.generateKCV("UA82F5C8910CA46BC92D772571984D271", DataKeyType.ZPK);
		assertEquals("CB7548", kcv);
		kcv = this.hsm.generateKCV("D287940E6651BDBD", DataKeyType.ZPK);
		assertEquals("7ABDC7", kcv);
		kcv = this.hsm.generateKCV("TAE6A014E4F1C5BA2864C8BEB70A3CEFC754CDACAF8F9B76D", DataKeyType.ZPK);
		assertEquals("440EBC", kcv);
		kcv = this.hsm.generateKCV("U7A0DCCA076573C155476E2E67A8BA2BB", DataKeyType.MK_AC);
		assertEquals("D970CC", kcv);
		kcv = this.hsm.generateKCV("U7FF70096BFA4E8222953FD895DB17F0D", DataKeyType.MK_SMI);
		assertEquals("D970CC", kcv);
		kcv = this.hsm.generateKCV("UCF5DB75995C27F8FFD38F9655CE01AA1", DataKeyType.MK_SMC);
		assertEquals("D970CC", kcv);
		kcv = this.hsm.generateKCV("C6841CD66AF681A1", DataKeyType.TAK);
		assertEquals("A94254", kcv);
		kcv = this.hsm.generateKCV("UD6C23C18E84DFE167D4995910777D5F9", DataKeyType.ZAK);
		assertEquals("3FA962", kcv);
	}

	@Test
	public void testIBMPIN() throws Exception {
		String validationData = "7777555533331111";
		String pan = "6056760000000124";
		String pb1 = "CCAD456F9EDA091D";
		String zpk = "U54B73F1DF5E5EF4EC56DD1EDA6889369";
		String pvk = "UC7117B8B71B1674E9AC2BE583648C465";
		String offset =  this.hsm.generateIBMOffset(pan, pb1, 4, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData);
		System.out.println("offset " + offset);
		assertEquals("8040", offset);

		String pan2 = "6394010350234936";
		String pb2 = "91386063EE28180F";

		assertTrue(this.hsm.verifyIBMOffset(offset, pan2, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData));
		assertFalse(this.hsm.verifyIBMOffset(offset, pan, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData));
		assertFalse(this.hsm.verifyIBMOffset("9040FFFFFFFF", pan2, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData));
		assertFalse(this.hsm.verifyIBMOffset(offset, pan2, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, "7777555533331112"));

		pan = "6056740146280020";
		// ISO-0, PIN 8682
		String pbTPK = "CE5AA5222143475D";
		String tpk = "U5FE21198A7909ADD97B7B563D57C2BDD";
		offset = this.hsm.generateIBMOffset(pan, pbTPK, 4, Pinblock.Format.ISO_0, tpk, DataKeyType.TPK, pvk, DEC_TABLE, validationData);
		System.out.println("offset " + offset);
		assertEquals("4387", offset);

		// ISO-0, PIN 1357, 73AFE88007D69947
		assertFalse(this.hsm.verifyIBMOffset(offset, pan, "73AFE88007D69947", Pinblock.Format.ISO_0, tpk, DataKeyType.TPK, pvk, DEC_TABLE, validationData));

		// ISO-1, PIN 8682, 3AE3C756320655F4
		assertTrue(this.hsm.verifyIBMOffset(offset, pan, "3AE3C756320655F4", Pinblock.Format.ISO_1, tpk, DataKeyType.TPK, pvk, DEC_TABLE, validationData));

		// ISO-3, PIN 8682, 8E25829517B0DDDC
		assertTrue(this.hsm.verifyIBMOffset(offset + "FFFFFFFF", pan, "8E25829517B0DDDC", Pinblock.Format.ISO_3, tpk, DataKeyType.TPK, pvk, DEC_TABLE, validationData));
	}

	@Test
	public void testIBMPIN2() throws Exception {
		String validationData = "7777555533331111";
		String pan = "6056760000000124";
		String pb3 = "BFF6394AEB5C676A"; // 234567
		String zpk = "U54B73F1DF5E5EF4EC56DD1EDA6889369";
		String pvk = "UC7117B8B71B1674E9AC2BE583648C465";
		String offset =  this.hsm.generateIBMOffset(pan, pb3, 6, Pinblock.Format.ISO_3, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData);
		System.out.println("offset " + offset);
		assertEquals("804010", offset);

		String pan2 = "6394010350234936";
		String pb2 = "91386063EE28180F";

		assertTrue(this.hsm.verifyIBMOffset(offset.substring(0, 4), pan2, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData));
		assertFalse(this.hsm.verifyIBMOffset(offset, pan, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData));
		assertFalse(this.hsm.verifyIBMOffset("9040", pan2, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, validationData));
		assertFalse(this.hsm.verifyIBMOffset(offset, pan2, pb2, zpk, DataKeyType.ZPK, pvk, DEC_TABLE, "7777555533331112"));
	}

}
