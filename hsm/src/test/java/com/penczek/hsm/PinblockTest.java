package com.penczek.hsm;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import com.penczek.util.HexadecimalStringUtil;
import org.junit.Test;

/**
 *
 * @author Leonardo Penczek
 */
public class PinblockTest {
	
	@Test
	public void testExtractAccountNumber() {
		System.out.println("extractAccountNumber");
		assertEquals("574012345678", Pinblock.extractAccountNumber("6035740123456789"));
		assertEquals("574012345678", Pinblock.extractAccountNumber("5740123456789"));
		assertEquals("074012345678", Pinblock.extractAccountNumber("740123456789"));
		assertEquals("000012345678", Pinblock.extractAccountNumber("123456789"));
		assertEquals("000000000000", Pinblock.extractAccountNumber(""));
		assertEquals("740123456789", Pinblock.extractAccountNumber("6035740123456789", false));
		assertEquals("740123456789", Pinblock.extractAccountNumber("5740123456789", false));
		assertEquals("740123456789", Pinblock.extractAccountNumber("740123456789", false));
		assertEquals("000123456789", Pinblock.extractAccountNumber("123456789", false));
		assertEquals("000000000000", Pinblock.extractAccountNumber("", false));
	}

	@Test
	public void testError() {
		try {
			Pinblock.createPinblock(null, "");
			fail("invalid format, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Format not supported", e.getMessage());
		}
	}
	
	@Test
	public void testISO0() {
		System.out.println("ISO-0");
		System.out.println("Pinblock0 1234, 6035740123456789");
		Pinblock pb1 = Pinblock.createPinblock(Pinblock.Format.ISO_0, "1234", "6035740123456789");
		System.out.println(pb1);
		assertEquals(Pinblock.Format.ISO_0, pb1.getFormat());
		assertEquals("041263BFEDCBA987", pb1.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("041263BFEDCBA987"), pb1.getPinblock());
		assertEquals("1234", pb1.getPIN("6035740123456789"));
		try {
			pb1.getPIN("1234567890123456");
			fail("Invalid PAN, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid padding", e.getMessage());
		}
		System.out.println("Pinblock0 654321, 6056640377889900");
		Pinblock pb2 = Pinblock.createPinblock(Pinblock.Format.ISO_0, "06652561C887766F");
		System.out.println(pb2);
		assertEquals(Pinblock.Format.ISO_0, pb2.getFormat());
		assertEquals("06652561C887766F", pb2.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("06652561C887766F"), pb2.getPinblock());
		assertEquals("654321", pb2.getPIN("6056640377889900"));
		// se eu mudo um digito composto na senha, nao da erro, e esta "certo"
		assertEquals("651321", pb2.getPIN("6053640377889900"));
		try {
			// ira dar erro quando mexer em digitos do padding
			pb2.getPIN("6056640477889900");
			fail("Invalid PAN, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid padding", e.getMessage());
		}
		System.out.println("Pinblock0 654321, 6056640377889900");
		Pinblock pb3 = Pinblock.createPinblock(Pinblock.Format.ISO_0, HexadecimalStringUtil.bytesFromString("06652561C887766F"));
		System.out.println(pb3);
		assertEquals(Pinblock.Format.ISO_0, pb3.getFormat());
		assertEquals("06652561C887766F", pb3.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("06652561C887766F"), pb3.getPinblock());
		assertEquals(pb2, pb3);
		assertEquals(pb3, pb2);
		assertEquals(pb2.hashCode(), pb3.hashCode());
		assertNotEquals(pb1, pb2);
		assertNotEquals(pb2, pb1);
		assertNotEquals(pb2, null);
		assertNotEquals(pb2, "1");

		System.out.println("Pinblock0 333444555666, 6035740123456789");
		Pinblock pb4 = Pinblock.createPinblock(Pinblock.Format.ISO_0, "333444555666", "6035740123456789");
		System.out.println(pb4);
		assertEquals(Pinblock.Format.ISO_0, pb4.getFormat());
		assertEquals("0C33630447623087", pb4.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("0C33630447623087"), pb4.getPinblock());
		assertEquals("333444555666", pb4.getPIN("6035740123456789"));
		try {
			pb4.getPIN("1234567890123456");
			fail("Invalid PAN, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid padding", e.getMessage());
		}
		
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, "123", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid size 3", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, "1234567890123", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid size 13", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, "123A", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid digit A", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, "16652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-0", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, "02652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-0", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, "0D652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-0", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, "0D652561C88776");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid pinblock", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_0, (byte[]) null);
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid pinblock", e.getMessage());
		}

		Pinblock pb5 = Pinblock.createPinblock(Pinblock.Format.ISO_0, "066A2561C887766F");
		System.out.println(pb5);
		try {
			pb5.getPIN("6056640377889900");
			fail("Invalid PIN digit, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid PIN digit :", e.getMessage());
		}
	}

	@Test
	public void testISO1() {
		System.out.println("ISO-1");
		System.out.println("Pinblock1 1234, 6035740123456789");
		Pinblock pb1 = Pinblock.createPinblock(Pinblock.Format.ISO_1, "1234", "6035740123456789");
		System.out.println(pb1);
		assertEquals(Pinblock.Format.ISO_1, pb1.getFormat());
		assertEquals("141234", pb1.toString().subSequence(0, 6));
		assertEquals("1234", pb1.getPIN("6035740123456789"));
		// format ISO-1 � independente do pan
		assertEquals("1234", pb1.getPIN("1234567890123456"));
		assertEquals("1234", pb1.getPIN(""));
		assertEquals("1234", pb1.getPIN(null));

		System.out.println("Pinblock1 654321, 6056640377889900");
		Pinblock pb2 = Pinblock.createPinblock(Pinblock.Format.ISO_1, "166543218DB6742A");
		System.out.println(pb2);
		assertEquals(Pinblock.Format.ISO_1, pb2.getFormat());
		assertEquals("166543218DB6742A", pb2.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("166543218DB6742A"), pb2.getPinblock());
		assertEquals("654321", pb2.getPIN("6056640377889900"));
		assertEquals("654321", pb2.getPIN("6053640377889900"));
		assertEquals("654321", pb2.getPIN("60566403F7889900"));
		assertEquals("654321", pb2.getPIN(""));
		assertEquals("654321", pb2.getPIN(null));
		
		System.out.println("Pinblock1 654321, 6056640377889900");
		Pinblock pb3 = Pinblock.createPinblock(Pinblock.Format.ISO_1, HexadecimalStringUtil.bytesFromString("166543218DB6742A"));
		System.out.println(pb3);
		assertEquals(Pinblock.Format.ISO_1, pb3.getFormat());
		assertEquals("166543218DB6742A", pb3.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("166543218DB6742A"), pb3.getPinblock());
		assertEquals(pb2, pb3);
		assertEquals(pb3, pb2);
		assertEquals(pb2.hashCode(), pb3.hashCode());
		assertNotEquals(pb1, pb2);
		assertNotEquals(pb2, pb1);

		Pinblock pb35 = Pinblock.createPinblock(Pinblock.Format.ISO_1, HexadecimalStringUtil.bytesFromString("16654321C887766F"));
		assertEquals(pb2.getPIN(null), pb35.getPIN(null));
		assertNotEquals(pb2, pb35);

		System.out.println("Pinblock1 333444555666, 6035740123456789");
		Pinblock pb4 = Pinblock.createPinblock(Pinblock.Format.ISO_1, "333444555666", "6035740123456789");
		System.out.println(pb4);
		assertEquals(Pinblock.Format.ISO_1, pb4.getFormat());
		assertEquals("1C333444555666", pb4.toString().subSequence(0, 14));
		assertEquals("333444555666", pb4.getPIN("6035740123456789"));
		assertEquals("333444555666", pb4.getPIN("1234567890123456"));
		assertEquals("333444555666", pb4.getPIN(""));
		assertEquals("333444555666", pb4.getPIN(null));
		
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, "123", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid size 3", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, "1234567890123", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid size 13", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, "123A", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid digit A", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, "06652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-1", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, "12652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-1", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, "1D652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-1", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, "1D652561C887766F11");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid pinblock", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_1, (byte[]) null);
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid pinblock", e.getMessage());
		}

		Pinblock pb5 = Pinblock.createPinblock(Pinblock.Format.ISO_1, "166A2561C887766F");
		System.out.println(pb5);
		try {
			pb5.getPIN("6056640377889900");
			fail("Invalid PIN digit, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid PIN digit :", e.getMessage());
		}
	}

	@Test
	public void testISO3() {
		System.out.println("ISO-3");
		System.out.println("Pinblock3 1234, 6035740123456789");
		Pinblock pb1 = Pinblock.createPinblock(Pinblock.Format.ISO_3, "1234", "6035740123456789");
		System.out.println(pb1);
		assertEquals(Pinblock.Format.ISO_3, pb1.getFormat());
		assertEquals("341263", pb1.toString().subSequence(0, 6));
		byte[] panpad = HexadecimalStringUtil.bytesFromString("4012345678");
		byte[] pb = pb1.getPinblock();
		assertEquals(0x34, pb[0]);
		assertEquals(0x12, pb[1]);
		assertEquals(0x63, pb[2]);
		for (int i = 3; i < 8; i++) {
			byte b = (byte) (pb[i] ^ panpad[i - 3]);
			if ((b & 0xF) < 10 || (b & 0xF) > 15) {
				fail("Invalid padding " + b);
			}
			if ((b >> 4 & 0xF) < 10 || (b >> 4 & 0xF) > 15) {
				fail("Invalid padding " + b);
			}
		}
		assertEquals("1234", pb1.getPIN("6035740123456789"));
		try {
			pb1.getPIN("1234567890123456");
			fail("Invalid PAN, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid padding", e.getMessage());
		}
		System.out.println("Pinblock3 654321, 6056640377889900");
		Pinblock pb2 = Pinblock.createPinblock(Pinblock.Format.ISO_3, "366525618DB6742A");
		System.out.println(pb2);
		assertEquals(Pinblock.Format.ISO_3, pb2.getFormat());
		assertEquals("366525618DB6742A", pb2.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("366525618DB6742A"), pb2.getPinblock());
		assertEquals("654321", pb2.getPIN("6056640377889900"));
		// se eu mudo um digito composto na senha, nao da erro, e esta "certo"
		assertEquals("651321", pb2.getPIN("6053640377889900"));
		try {
			// ira dar erro quando mexer em digitos do padding
			pb2.getPIN("60566403F7889900");
			fail("Invalid PAN, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid padding", e.getMessage());
		}
		
		
		System.out.println("Pinblock3 654321, 6056640377889900");
		Pinblock pb3 = Pinblock.createPinblock(Pinblock.Format.ISO_3, HexadecimalStringUtil.bytesFromString("366525618DB6742A"));
		System.out.println(pb3);
		assertEquals(Pinblock.Format.ISO_3, pb3.getFormat());
		assertEquals("366525618DB6742A", pb3.toString());
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("366525618DB6742A"), pb3.getPinblock());
		assertEquals(pb2, pb3);
		assertEquals(pb3, pb2);
		assertEquals(pb2.hashCode(), pb3.hashCode());
		assertNotEquals(pb1, pb2);
		assertNotEquals(pb2, pb1);

		Pinblock pb35 = Pinblock.createPinblock(Pinblock.Format.ISO_3, HexadecimalStringUtil.bytesFromString("36652561C887766F"));
		assertEquals(pb2.getPIN("6056640377889900"), pb35.getPIN("6056640377889900"));
		assertNotEquals(pb2, pb35);

		System.out.println("Pinblock3 333444555666, 6035740123456789");
		Pinblock pb4 = Pinblock.createPinblock(Pinblock.Format.ISO_3, "333444555666", "6035740123456789");
		System.out.println(pb4);
		assertEquals(Pinblock.Format.ISO_3, pb4.getFormat());
		assertEquals("3C336304476230", pb4.toString().subSequence(0, 14));
		pb = pb4.getPinblock();
		assertEquals(0x3C, pb[0]);
		assertEquals(0x33, pb[1]);
		assertEquals(0x63, pb[2]);
		assertEquals(0x04, pb[3]);
		assertEquals(0x47, pb[4]);
		assertEquals(0x62, pb[5]);
		assertEquals(0x30, pb[6]);
		byte b = (byte) (pb[7] ^ 0x78);
		if ((b & 0xF) < 10 || (b & 0xF) > 15) {
			fail("Invalid padding " + b);
		}
		if ((b >> 4 & 0xF) < 10 || (b >> 4 & 0xF) > 15) {
			fail("Invalid padding " + b);
		}
		assertEquals("333444555666", pb4.getPIN("6035740123456789"));
		try {
			pb4.getPIN("1234567890123456");
			fail("Invalid PAN, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid padding", e.getMessage());
		}
		
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, "123", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid size 3", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, "1234567890123", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid size 13", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, "123A", "6035740123456789");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINException e) {
			assertEquals("Invalid digit A", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, "16652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-3", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, "32652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-3", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, "3D652561C887766F");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Not ISO-3", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, "3D652561C887");
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid pinblock", e.getMessage());
		}
		try {
			Pinblock.createPinblock(Pinblock.Format.ISO_3, (byte[]) null);
			fail("Invalid pinblock, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid pinblock", e.getMessage());
		}

		Pinblock pb5 = Pinblock.createPinblock(Pinblock.Format.ISO_3, "366A2561C887766F");
		System.out.println(pb5);
		try {
			pb5.getPIN("6056640377889900");
			fail("Invalid PIN digit, exception expected");
		} catch (InvalidPINBlockException e) {
			assertEquals("Invalid PIN digit :", e.getMessage());
		}

		Pinblock pbIso0 = Pinblock.createPinblock(Pinblock.Format.ISO_0, "1234", "6035740123456789");
		assertNotEquals(pb1, pbIso0);
		assertNotEquals(pbIso0, pb1);
		int hash = pbIso0.hashCode();
		assertEquals(hash, pbIso0.hashCode());
	}

}
