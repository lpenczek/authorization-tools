package com.penczek.hsm;

import java.io.IOException;

public class HSMFailoverBackupIntegrationTest extends HSMThales8000IntegrationTest {

	private static final String HSM_IP = "localhost";
	private static final int HSM_PORT = 1500;

	@Override
	protected HSM getHSM() {
		HSMThales8000 hsm = new HSMThales8000(HSM_IP, HSM_PORT, true);
		hsm.setTps(100);
		HSMFailOver fo = new HSMFailOver();
		// Tests the backup HSM
		fo.setBackup(hsm);
		HSMThales8000 errorhsm = new HSMThales8000(HSM_IP, HSM_PORT, true) {
			@Override
			protected synchronized String sendMessage(String msg) throws IOException {
				throw new IOException("Malfunction");
			}
		};
		fo.setMain(errorhsm);
		return fo;
	}

}
