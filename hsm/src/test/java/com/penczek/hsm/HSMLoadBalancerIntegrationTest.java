package com.penczek.hsm;

import java.util.Arrays;

/**
 *
 * @author Leonardo Penczek
 */
public class HSMLoadBalancerIntegrationTest extends HSMThales8000IntegrationTest {

	private static final String HSM_IP = "localhost";
	private static final int HSM_PORT = 1500;

	@Override
	protected HSM getHSM() {
		HSMThales8000 hsm = new HSMThales8000(HSM_IP, HSM_PORT, true);
		hsm.setTps(100);
		HSMLoadBalancer lb = new HSMLoadBalancer();
		lb.setHsms(Arrays.<HSM>asList(hsm, hsm));
		return lb;
	}

}
