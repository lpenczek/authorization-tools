package com.penczek.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class HexadecimalStringUtilTest {

  @Test
	public void test() {
		byte[] test = {
			(byte) 0x01, (byte) 0x23, (byte) 0x45, (byte) 0x67, (byte) 0x89, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF,
			(byte) 0x01, (byte) 0x23, (byte) 0x45, (byte) 0x67, (byte) 0x89, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF,
			(byte) 0x01, (byte) 0x23, (byte) 0x45, (byte) 0x67, (byte) 0x89, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF
		};
		String s1, s2;
		byte[] bs;

		s1 = "0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
		assertEquals(s1, HexadecimalStringUtil.bytesToString(test));
		bs = HexadecimalStringUtil.bytesFromString(s1);
		assertEquals(test.length, bs.length);
		for (int i = 0; i < bs.length; i++) {
			System.out.println("#" + i + ' ' + test[i] + " -> " + bs[i]);
			assertEquals("Erro #" + i, test[i], bs[i]);
		}
		s2 = "123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
		bs = HexadecimalStringUtil.bytesFromString(s2);
		assertEquals(test.length, bs.length);
		for (int i = 0; i < bs.length; i++) {
			assertEquals("Erro #" + i, test[i], bs[i]);
		}
		assertEquals(s1, HexadecimalStringUtil.bytesToString(bs));

		test = new byte[] {
			(byte) 0xA3, (byte) 0x14, (byte) 0x4B, (byte) 0x10, (byte) 0x2B, (byte) 0xA0, (byte) 0x30, (byte) 0xCD,
			(byte) 0xFD, (byte) 0x5C, (byte) 0xAC, (byte) 0x41, (byte) 0x81, (byte) 0xC0, (byte) 0xDC, (byte) 0x63
		};
		s1 = "A3144B102BA030CDFD5CAC4181C0DC63";
		assertEquals(s1, HexadecimalStringUtil.bytesToString(test));
		bs = HexadecimalStringUtil.bytesFromString(s1);
		assertEquals(test.length, bs.length);
		for (int i = 0; i < bs.length; i++) {
			System.out.println("#" + i + ' ' + test[i] + " -> " + bs[i]);
			assertEquals("Erro #" + i, test[i], bs[i]);
		}
	}

}
