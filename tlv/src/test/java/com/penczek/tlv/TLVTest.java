package com.penczek.tlv;

import static org.junit.Assert.*;

import com.penczek.util.HexadecimalStringUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.junit.Test;

public class TLVTest {

	@Test
	public void testGetDataElementValue() throws ParseException {
		System.out.println("testGetDataElementValue");
		String pak = "21818D5A0857042301020304055F3401007016820259008407AAA000000101039F02060000000001007715" +
				"9F03060000000000009F1A02007695056080000000A50d5F2A0209869A031005149C0100BF0C109F3704AFFB4A7A" +
				"9F2701809F360200059F10200FA501A20800580000000000000000000F000000000000000000000000000000" +
				"9F26084218EE98008AC74A";
		TLVData tlvs = TLVDataParser.parseData(pak);
		String pan = (String) tlvs.getDataElementValue(EMVDataElement.APPLICATION_PRIMARY_ACCOUNT_NUMBER);
		System.out.println("Pan " + pan);
		assertEquals("5704230102030405", pan);
		Long un = (Long) tlvs.getDataElementValue(EMVDataElement.UNPREDICTABLE_NUMBER);
		System.out.println("UN " + un);
		assertEquals(0xAFFB4A7AL, un.longValue());
		assertNull(tlvs.getDataElementValue(EMVDataElement.ACQUIRER_IDENTIFIER));
		assertNotNull(tlvs.getDataElementValue(EMVDataElement.CRYPTOGRAM_INFORMATION_DATA));
	}

	@Test
	public void testBit55() throws Exception {
		System.out.println("bit55");
		String b55 = ""
				+ "9F0206000000050000"
				+ "9F0306000000000000"
				+ "9F1A020076"
				+ "95050000048000"
				+ "5F2A020986"
				+ "9A03130304"
				+ "9C0101"
				+ "9F370425494740"
				+ "82025800"
				+ "9F36020044"
				+ "5A086056630000000087"
				+ "5F340100"
				+ "9F1008010103A000000000"
				+ "9F270180"
				+ "9F2608D751F593A6A72682";
		TLVData tlvs = TLVDataParser.parseData(b55);
		System.out.println("TLV " + tlvs.toString());
		assertEquals(new BigDecimal("500.00"), tlvs.getDataElementValue(EMVDataElement.AMOUNT_AUTHORISED_NUMERIC));
		assertEquals(new BigDecimal("0.00"), tlvs.getDataElementValue(EMVDataElement.AMOUNT_OTHER_NUMERIC));
		assertEquals(76L, tlvs.getDataElementValue(EMVDataElement.TERMINAL_COUNTRY_CODE));
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("0000048000"), tlvs.getDataElementBinaryValue(EMVDataElement.TERMINAL_VERIFICATION_RESULTS));
		assertEquals(986L, tlvs.getDataElementValue(EMVDataElement.TRANSACTION_CURRENCY_CODE));
		assertEquals(new SimpleDateFormat("ddMMyy").parse("040313"), tlvs.getDataElementValue(EMVDataElement.TRANSACTION_DATE));
		assertEquals(1L, tlvs.getDataElementValue(EMVDataElement.TRANSACTION_TYPE));
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("25494740"), tlvs.getDataElementBinaryValue(EMVDataElement.UNPREDICTABLE_NUMBER));
		assertEquals(0x25494740L, tlvs.getDataElementValue(EMVDataElement.UNPREDICTABLE_NUMBER));
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("5800"), tlvs.getDataElementBinaryValue(EMVDataElement.APPLICATION_INTERCHANGE_PROFILE));
		assertEquals(0x5800L, tlvs.getDataElementValue(EMVDataElement.APPLICATION_INTERCHANGE_PROFILE));
		assertEquals(0x44L, tlvs.getDataElementValue(EMVDataElement.APPLICATION_TRANSACTION_COUNTER));
		assertEquals("6056630000000087", tlvs.getDataElementValue(EMVDataElement.APPLICATION_PRIMARY_ACCOUNT_NUMBER));
		assertEquals(0L, tlvs.getDataElementValue(EMVDataElement.APPLICATION_PRIMARY_ACCOUNT_NUMBER_SEQUENCE_NUMBER));
		assertArrayEquals(HexadecimalStringUtil.bytesFromString("010103A000000000"), tlvs.getDataElementBinaryValue(EMVDataElement.ISSUER_APPLICATION_DATA));
		assertEquals(0x80L, tlvs.getDataElementValue(EMVDataElement.CRYPTOGRAM_INFORMATION_DATA));
		assertEquals(new BigInteger(HexadecimalStringUtil.bytesFromString("D751F593A6A72682")).longValue(),
				((Number) tlvs.getDataElementValue(EMVDataElement.APPLICATION_CRYPTOGRAM)).longValue());
	}

}
