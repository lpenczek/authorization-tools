package com.penczek.tlv;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import com.penczek.util.HexadecimalStringUtil;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Base64;
import org.junit.Test;

/**
 *
 * @author lpenczek
 */
public class TLVDataParserTest {

	String tlv1 = "5A085704230102030405";
	String tlv2 = "5F340100";
	String tlv3 = "82025900";
	String tlv4 = "8407AAA00000010103";
	String tlv5 = "9F0206000000000100";
	String tlv6 = "9F0306000000000000";
	String tlv7 = "9F1A020076";
	String tlv8 = "95056080000000";
	String tlv9 = "5F2A020986";
	String tlv10 = "9A03100514";
	String tlv11 = "9C0100";
	String tlv12 = "9F3704AFFB4A7A";
	String tlv13 = "9F270180";
	String tlv14 = "9F36020005";
	String tlv15 = "9F10200FA501A20800580000000000000000000F000000000000000000000000000000";
	String tlv16 = "9F26084218EE98008AC74A";

	String template1 = "70";
	String template2 = "77";
	String template3 = "A5";
	String template4 = "BF0C";

	@Test
	public void testParse() throws Exception {
		TLVData tlvs;
		String pak;
		pak = tlv1 + tlv2 + tlv3 + tlv4 + tlv5 + tlv6 + tlv7 + tlv8 + tlv9 + tlv10 +
				tlv11 + tlv12 + tlv13 + tlv14 + tlv15 + tlv16;
		System.out.println("Pak " + pak);
		tlvs = TLVDataParser.parseData(pak);
		for (TLV tlv : tlvs.getData()) {
			System.out.println("Returned\n" + tlv);
		}
		System.out.println("OK\n\n");

		pak = tlv1 + tlv2 +
				template1 + String.format("%02X", (tlv3.length() + tlv4.length() + tlv5.length()) >> 1) + tlv3 + tlv4 + tlv5 +
				template2 + String.format("%02X", (tlv6.length() + tlv7.length() + tlv8.length()) >> 1) + tlv6 + tlv7 + tlv8 +
				template3 + String.format("%02X", (tlv9.length() + tlv10.length() + tlv11.length()) >> 1) + tlv9 + tlv10 + tlv11 +
				template4 + String.format("%02X", (tlv12.length() + tlv13.length() + tlv14.length()) >> 1) + tlv12 + tlv13 + tlv14 +
				tlv15 + tlv16;
		System.out.println("Pak " + pak);
		tlvs = TLVDataParser.parseData(pak);
		for (TLV tlv : tlvs.getData()) {
			System.out.println("Returned\n" + tlv);
		}
		String hx = tlvs.toHexadecimal();
		System.out.println("Org " + pak);
		System.out.println("Hex " + hx);
		assertEquals(pak, hx);
		System.out.println("OK\n\n");

		pak = "21818D5A0857042301020304055F3401007016820259008407AAA000000101039F020600000000010077159F03060000000000009F1A02007695056080000000A50D5F2A0209869A031005149C0100BF0C109F3704AFFB4A7A9F2701809F360200059F10200FA501A20800580000000000000000000F0000000000000000000000000000009F26084218EE98008AC74A";
		System.out.println("Pak " + pak);
		tlvs = TLVDataParser.parseData(pak);
		for (TLV tlv : tlvs.getData()) {
			System.out.println("Returned\n" + tlv);
		}
		hx = tlvs.toHexadecimal();
		System.out.println("Org " + pak);
		System.out.println("Hex " + hx);
		assertEquals(pak, hx);
		System.out.println("OK\n\n");
	}

	@Test
	public void testParseMultiple() throws Exception {
		String pak = ""
				+ "91080ECE147C00820000"
				+ "72279F180400000001861E8C240002198711019B2682B4AA213C0F4726428C3A4916AF8E04935237CC"
				+ "72219F180400000002860B8C240000068E0452C06171860B8C180000068E0494050CBB";
		TLVData tlvdata = TLVDataParser.parseData(pak);
		byte[] iad = (byte[]) tlvdata.getDataElementValue(EMVDataElement.ISSUER_AUTHENTICATION_DATA);
		org.junit.Assert.assertArrayEquals(HexadecimalStringUtil.bytesFromString("0ECE147C00820000"), iad);
		TLV s72 = tlvdata.getDataElement(EMVDataElement.ISSUER_SCRIPT_TEMPLATE_2);
		System.out.println("S72 " + s72);
		ConstructedTLV cs72 = (ConstructedTLV) s72;
		TLV sid = cs72.getChild(EMVDataElement.ISSUER_SCRIPT_IDENTIFIER);
		assertEquals(1L, sid.getInterpretedValue());
		// verifica multiplos scripts
		TLV[] scripts = tlvdata.getDataElements(EMVDataElement.ISSUER_SCRIPT_TEMPLATE_2);
		assertEquals(2, scripts.length);
		cs72 = (ConstructedTLV) scripts[0];
		sid = cs72.getChild(EMVDataElement.ISSUER_SCRIPT_IDENTIFIER);
		assertEquals(1L, sid.getInterpretedValue());
		TLV[] cmds = cs72.getChildren(EMVDataElement.ISSUER_SCRIPT_COMMAND);
		assertEquals(1, cmds.length);
		byte[] cmd = (byte[]) cmds[0].getInterpretedValue();
		org.junit.Assert.assertArrayEquals(HexadecimalStringUtil.bytesFromString("8C240002198711019B2682B4AA213C0F4726428C3A4916AF8E04935237CC"), cmd);
		cs72 = (ConstructedTLV) scripts[1];
		sid = cs72.getChild(EMVDataElement.ISSUER_SCRIPT_IDENTIFIER);
		assertEquals(2L, sid.getInterpretedValue());
		cmds = cs72.getChildren(EMVDataElement.ISSUER_SCRIPT_COMMAND);
		assertEquals(2, cmds.length);
		cmd = (byte[]) cmds[0].getInterpretedValue();
		org.junit.Assert.assertArrayEquals(HexadecimalStringUtil.bytesFromString("8C240000068E0452C06171"), cmd);
		cmd = (byte[]) cmds[1].getInterpretedValue();
		org.junit.Assert.assertArrayEquals(HexadecimalStringUtil.bytesFromString("8C180000068E0494050CBB"), cmd);
	}

}
