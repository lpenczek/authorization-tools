package com.penczek.tlv;

import org.junit.Test;

public class EMVDataElementTest {

	@Test
	public void test() {
		EMVDataElement[] result = EMVDataElement.values();
		for (EMVDataElement r : result) {
			System.out.println(r + "\n" + String.format(r.getTag() > 0x0FF ? "%04X" : "%02X", r.getTag()) + "\t" + r.getTlvClass() + "\t" + r.getTagFormat() + " " + r.getMaximumSize());
		}
	}

}
