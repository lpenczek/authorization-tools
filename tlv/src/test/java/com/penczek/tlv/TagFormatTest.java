package com.penczek.tlv;

import static org.junit.Assert.*;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.Date;
import org.junit.Test;

public class TagFormatTest {

	@Test
	public void testInterpretANS() throws UnsupportedEncodingException {
		String s = "Hello World";
		assertEquals(s, (String) TagFormat.ALPHA_NUMERIC_SPECIAL.interpret(s.getBytes("ISO-8859-1"), 1, 1));
		s = "Hello 123\u0000\u0000\u0000\u0000\u0000";
		assertEquals("Hello 123", (String) TagFormat.ALPHA_NUMERIC_SPECIAL.interpret(s.getBytes("ISO-8859-1"), 1, 1));
		s = "Hello123\u0000\u0000\u0000\u0000\u0000";
		assertEquals("Hello123", (String) TagFormat.ALPHA_NUMERIC.interpret(s.getBytes("ISO-8859-1"), 1, 1));
		s = "Hello\u0000\u0000\u0000\u0000\u0000";
		assertEquals("Hello", (String) TagFormat.ALPHABETIC.interpret(s.getBytes("ISO-8859-1"), 1, 1));
	}

	@Test
	public void testInterpretB() {
		byte[] bin = {
			(byte)0x60, (byte)0x35, (byte)0x74, (byte)0x01,
			(byte)0x07, (byte)0x81, (byte)0x11, (byte)0x57,
		};
		byte[] r = (byte[]) TagFormat.BINARY.interpret(bin, 1, 10);
		for (int i = 0; i < bin.length; i++) {
			assertEquals(bin[i], r[i]);
		}
		r = (byte[]) TagFormat.VARIABLE.interpret(bin, 1, 10);
		for (int i = 0; i < bin.length; i++) {
			assertEquals(bin[i], r[i]);
		}
	}

	@Test
	public void testInterpretCN() {
		byte[] pan = {
			(byte)0x60, (byte)0x35, (byte)0x74, (byte)0x01,
			(byte)0x07, (byte)0x81, (byte)0x11, (byte)0x57,
		};
		String card = (String) TagFormat.COMPRESSED_NUMERIC.interpret(pan, 1, 1);
		System.out.println("PAN " + card);
		assertEquals("6035740107811157", card);
		byte[] pan2 = {
			(byte)0x60, (byte)0x35, (byte)0x74, (byte)0x01,
			(byte)0x07, (byte)0x81, (byte)0x11, (byte)0x57,
			(byte)0x9F, (byte)0xFF
		};
		String card2 = (String) TagFormat.COMPRESSED_NUMERIC.interpret(pan2, 1, 1);
		System.out.println("PAN2 " + card2);
		assertEquals("60357401078111579", card2);
	}

	@Test
	public void testInterpretNAmount() {
		byte[] n = {
			(byte)0x12, (byte)0x34, (byte)0x56, (byte)0x78, (byte)0x90, (byte)0x12
		};
		BigDecimal num = (BigDecimal) TagFormat.NUMERIC_AMOUNT.interpret(n, 1, 1);
		System.out.println("Num " + num);
		assertEquals(new BigDecimal("1234567890.12"), num);
	}

	@Test
	public void testInterpretNNumeric() {
		byte[] n = { (byte)0x00, (byte)0x00, (byte)0x35, (byte)0x74, (byte)0x01 };
		Long num = (Long) TagFormat.NUMERIC.interpret(n, 1, 1);
		System.out.println("Num " + num);
		assertEquals(Long.valueOf(357401), num);
		byte[] n2 = { (byte)0x09, (byte)0x86 };
		Long num2 = (Long) TagFormat.NUMERIC.interpret(n2, 1, 1);
		System.out.println("Num2 " + num2);
		assertEquals(Long.valueOf(986), num2);
	}

	@Test
	public void testInterpretNDate() {
		byte[] date1 = { (byte)0x78, (byte)0x04, (byte)0x06 };
		Date d = (Date) TagFormat.NUMERIC_DATE.interpret(date1, 1, 1);
		assertEquals(6, d.getDate());
		assertEquals(3, d.getMonth());
		assertEquals(78, d.getYear());

		byte[] date2 = { (byte)0x11, (byte)0x04, (byte)0x18 };
		d = (Date) TagFormat.NUMERIC_DATE.interpret(date2, 1, 1);
		assertEquals(18, d.getDate());
		assertEquals(3, d.getMonth());
		assertEquals(111, d.getYear());
	}

	@Test
	public void testInterpretNTime() {
		byte[] hour = { (byte)0x15, (byte)0x34, (byte)0x59 };
		Date d = (Date) TagFormat.NUMERIC_TIME.interpret(hour, 1, 1);
		assertEquals(15, d.getHours());
		assertEquals(34, d.getMinutes());
		assertEquals(59, d.getSeconds());
	}

	@Test
	public void testInterpretDecimal() {
		byte[] n = {
			(byte)0x81, (byte)0x23, (byte)0xF4, (byte)0x56, (byte)0x78, (byte)0x90, (byte)0x10
		};
		BigDecimal num = (BigDecimal) TagFormat.DECIMAL.interpret(n, 1, 1);
		System.out.println("Num " + num);
		assertEquals(new BigDecimal("-123.456789010"), num);
		byte[] p = {
			(byte)0x00, (byte)0x23, (byte)0x4F, (byte)0x56, (byte)0x78, (byte)0x90, (byte)0x12
		};
		num = (BigDecimal) TagFormat.DECIMAL.interpret(p, 1, 1);
		System.out.println("Num " + num);
		assertEquals(new BigDecimal("234.56789012"), num);
	}

}
