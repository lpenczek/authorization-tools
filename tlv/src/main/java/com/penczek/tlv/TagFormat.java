package com.penczek.tlv;

import com.penczek.util.HexadecimalStringUtil;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Represents a data element format.
 * @author Leonardo Penczek
 * @version $Revision$
 */
public enum TagFormat {
	/** Alphabetic (only letters). */
	ALPHABETIC,
	/** Alphabetic and numeric (letters and digits). */
	ALPHA_NUMERIC,
	/** Alphabetic, numeric and specials. */
	ALPHA_NUMERIC_SPECIAL,
	/** Binary. */
	BINARY,
	/** Compressed numeric (BCD representation (2 digits per byte) of a number, right padded with F's (1111)). */
	COMPRESSED_NUMERIC,
	/** Numeric (BCD representation (2 digits per byte) of a number, left padded with 0's (0000)). */
	NUMERIC,
	/** Numeric (BCD representation (2 digits per byte) of a number, left padded with 0's (0000)), with scale 2. */
	NUMERIC_AMOUNT,
	/** Numeric (BCD representation (2 digits per byte) of a number, left padded with 0's (0000)), with scale 3. */
	NUMERIC_SCALE3,
	/** Numeric (BCD) date (YYMMDD). */
	NUMERIC_DATE,
	/** Numeric (BCD) time (HHMMSS). */
	NUMERIC_TIME,
	/** Variable (anything). */
	VARIABLE,
	/** Decimal value as BCD signal/magnitude:<ul>
	 * <li>1st digit: 0 positive, 8 negative</li>
	 * <li>next digits until 'F': integer part</li>
	 * <li>decimal separator 'F'</li>
	 * <li>next digits: decimal part</li>
	 * </ul>s
	 */
	DECIMAL;

	/**
	 * Interprets and returns data according to this format.
	 * @param data a data byte array.
	 * @param minSize the minimum size of the field.
	 * @param maxSize the maximum size of the field.
	 * @return the value interpreted according to this format: <ul>
	 * <li>{@link String} for {@link #ALPHABETIC}, {@link #ALPHA_NUMERIC}, {@link #ALPHA_NUMERIC_SPECIAL} and {@link #COMPRESSED_NUMERIC};</li>
	 * <li>{@link Long} for {@link #NUMERIC};</li>
	 * <li>{@link BigDecimal} for {@link #NUMERIC_AMOUNT} and {@link #NUMERIC_SCALE3};</li>
	 * <li>{@link Date} for {@link #NUMERIC_DATE} and {@link #NUMERIC_TIME};</li>
	 * <li>byte[] for {@link #BINARY} and {@link #VARIABLE};</li>
	 * </ul>
	 */
	public Object interpret(byte[] data, int minSize, int maxSize) {
		String s;
		int i;
		switch (this) {
			case ALPHABETIC:
			case ALPHA_NUMERIC:
			case ALPHA_NUMERIC_SPECIAL:
				s = new String(data, StandardCharsets.ISO_8859_1);
				i = s.indexOf('\0');
				return i >= 0 ? s.substring(0, i) : s;
			case COMPRESSED_NUMERIC:
				// BCD - 4 bits 1 digitos decimal
				s = HexadecimalStringUtil.bytesToString(data);
				i = s.indexOf('F');
				return i >= 0 ? s.substring(0, i) : s;
			case NUMERIC:
			case NUMERIC_AMOUNT:
			case NUMERIC_SCALE3:
				long n = 0;
				for (i = 0; i < data.length; i++) {
					n = 100 * n + (data[i] & 0x000F) + ((data[i] & 0x00F0) >> 4) * 10;
				}
				return this == NUMERIC_AMOUNT ? BigDecimal.valueOf(n, 2)
						: this == NUMERIC_SCALE3 ? BigDecimal.valueOf(n, 3)
						: Long.valueOf(n);
			case NUMERIC_DATE:
			case NUMERIC_TIME:
				try {
					return new SimpleDateFormat(this == NUMERIC_DATE ? "yyMMdd" : "HHmmss").parse(HexadecimalStringUtil.bytesToString(data));
				} catch (Exception e) {
					Logger.getLogger(this.getClass().getName()).warning(e.toString());
					return null;
				}
			case DECIMAL:
				char[] c = HexadecimalStringUtil.bytesToString(data).toCharArray();
				c[0] = c[0] == '8' ? '-' : '+';
				for (int j = 1; j < c.length; j++) {
					if (c[j] == 'F') c[j] = '.';
				}
				return new BigDecimal(c);
			case BINARY:
			case VARIABLE:
			default:
				if (minSize == maxSize && maxSize <= 8) {
					// converte para Long, ja que tamanho fixo e cabe dentro
					BigInteger bi = new BigInteger(1, data);
					return Long.valueOf(bi.longValue());
				}
				return data;
		}
	}

}
