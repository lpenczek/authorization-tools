package com.penczek.tlv;

import java.io.Serializable;

/**
 * Represents a tag-length-value (TLV) element. This element can be a primitive or constructed element.
 * @author Leonardo Penczek
 * @version $Revision$
 */
public abstract class TLV implements Serializable {

	private static final long serialVersionUID = 1L;

	/** The EMV type of this element. */
	protected final EMVDataElement dataElement;
	/** The TLV class of this element. */
	protected final TLVClass tlvClass;
	/** The tag of this element. */
	protected final int tag;

	/**
	 * Creates a new instance of TLV.
	 * @param tag the tag of this element.
	 */
	protected TLV(int tag) {
		this.tag = tag;
		this.dataElement = EMVDataElement.getByTag(tag);
		this.tlvClass = TLVClass.getTLVClass(tag >>> (tag > 0x0FF ? 14 : 6));
	}

	/**
	 * Creates a new instance of TLV.
	 * @param dataElement the EMV type of this element.
	 */
	protected TLV(EMVDataElement dataElement) {
		this.tag = dataElement.getTag();
		this.dataElement = dataElement;
		this.tlvClass = dataElement.getTlvClass();
	}

	/**
	 * Returns the TLV class of this element.
	 * @return the TLV class of this element.
	 */
	public TLVClass getTlvClass() {
		return this.tlvClass;
	}

	/**
	 * Returns the tag of this element.
	 * @return the tag of this element.
	 */
	public int getTag() {
		return this.tag;
	}

	/**
	 * Returns the EMV type of this element.
	 * @return the EMV type of this element.
	 */
	public EMVDataElement getDataElement() {
		return this.dataElement;
	}

	/**
	 * Returns the interpreted value of this element according to its defined EMV type.
	 * @return the interpreted value (BigDecimal, Long, String, etc.)
	 */
	public abstract Object getInterpretedValue();

	/**
	 * Returns the hexadecimal representation of this element.
	 * @return the hexadecimal representation of this element.
	 */
	public String toHexadecimal() {
		return TLVDataParser.toHexadecimal(this);
	}

}
