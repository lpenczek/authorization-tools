package com.penczek.tlv;

import com.penczek.util.HexadecimalStringUtil;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * TLV data parser.
 * @author Leonardo Penczek
 * @version $Revision$
 */
public class TLVDataParser {

	private TLVDataParser() {}

	/**
	 * Parses the hexadecimal representation of TLV data elements into a TLVData object.
	 * @param data the hexadecimal representation of TLV data.
	 * @return a TLVData object.
	 * @throws ParseException if an error in parsing has occurred .
	 */
	public static TLVData parseData(final String data) throws ParseException {
		return parseData(HexadecimalStringUtil.bytesFromString(data));
	}

	/**
	 * Parses the binary TLV data elements into a TLVData object.
	 * @param data the binary TLV data.
	 * @return a TLVData object.
	 * @throws ParseException if an error in parsing has occurred .
	 */
	public static TLVData parseData(final byte[] data) throws ParseException {
		return new TLVData(parse(data, 0, data.length));
	}

	private static TLV[] parse(final byte[] data, final int offset, int length) throws ParseException {
		if (data == null || data.length == 0) return null;
		length += offset; // setando length de acordo com o offset, ja que comparamos o indice absoluto e nao relativo
		if (length > data.length) throw new ParseException("Invalid TLV data: " + length + " > " + data.length, offset);
		List<TLV> tlvs = new ArrayList<TLV>(32);
		boolean constructed;
		int tag, lg, j, i = offset;
		TLV[] constructedData = null;
		byte[] primitiveData = null;
		// cada laco fazemos um objeto completo, por isso o indice eh incrementado manualmente
		while (i < length) {
			// TAG
			tag = data[i];
			if (tag == 0 || tag == -1) {
				// 0x00 e 0xFF podem ocorrer como padding antes, entre e depois de objetos TLV
				i++;
				continue;
			}
			tag &= 0x00FF;
			// T - 1o byte
			constructed = (tag & 0x0020) != 0;
			// verifica se tag possui mais bytes, para EMV o maximo sao 2 bytes por tag
			if ((tag & 0x001F) == 0x001F) {
				if (++i >= length) throw new ParseException("Invalid TLV tag", i);
				if (data[i] < 0) throw new ParseException("EMV TLV's tags does not support more than 2 bytes: " + data[i], i);
				tag = tag << 8 | data[i];
			}
			// LENGTH
			if (++i >= length) throw new ParseException("Invalid TLV data: " + i + " > " + length, i);
			lg = data[i];
			if (lg < 0) {
				// b7-b0 numero de bytes com o tamanho
				int lsize = lg & 0x007F;
				if (lsize > 4) throw new ParseException("EMV TLV's length does not support more than 4 bytes: " + lsize, i);
				if (i + lsize >= length) throw new ParseException("Invalid TLV length: " + (i + lsize), i);
				lg = 0;
				for (j = 0; j < lsize; j++) {
					lg |= (data[++i] & 0x00FF) << 8 * (lsize - 1 - j);
				}
			}
			// VALUE
			if (lg > 0) {
				if (i + lg >= length) throw new ParseException("Invalid TLV value's size: " + lg, i);
				i++;
				// i esta no inicio dos dados, lg tamanho do value
				if (constructed) {
					constructedData = parse(data, i, lg);
				} else {
					primitiveData = new byte[lg];
					System.arraycopy(data, i, primitiveData, 0, lg);
				}
				i += lg;
			} else {
				constructedData = null;
				primitiveData = null;
			}
			if (constructed) {
				tlvs.add(new ConstructedTLV(tag, constructedData));
			} else {
				tlvs.add(new PrimitiveTLV(tag, primitiveData));
			}
		}
		return tlvs.toArray(new TLV[tlvs.size()]);
	}

	public static String toHumanString(TLV[] tlvs) {
		StringBuilder sb = new StringBuilder(4096);
		int level = 0;
		int i = 0;
		for (TLV tlv : tlvs) {
			if (i++ > 0) sb.append('\n');
			toHumanString(tlv, level, sb);
		}
		return sb.toString();
	}

	public static void toHumanString(TLV tlv, int level, StringBuilder sb) {
		for (int i = 0; i < level; i++) {
			sb.append('\t');
		}
		if (tlv instanceof ConstructedTLV) {
			ConstructedTLV c = (ConstructedTLV) tlv;
			sb.append("Constructed ").append(c.getDataElement()).append(" (").append(String.format("%H", c.getTag())).append(")\n");
			int i = 0;
			for (TLV child : c.getValue()) {
				if (i++ > 0) sb.append('\n');
				toHumanString(child, level + 1, sb);
			}
		} else if (tlv instanceof PrimitiveTLV) {
			PrimitiveTLV p = (PrimitiveTLV) tlv;
			if (p.getDataElement() != null) {
				TagFormat tf = p.getDataElement().getTagFormat();
				if (tf == TagFormat.BINARY || tf == TagFormat.VARIABLE) {
					sb.append("Primitive ").append(p.getDataElement()).append(" (").append(String.format("%X", p.getTag())).append(')').append(" - binary ").append(p.getValue() != null ? HexadecimalStringUtil.bytesToString(p.getValue()) : null);
				} else {
					sb.append("Primitive ").append(p.getDataElement()).append(" (").append(String.format("%X", p.getTag())).append(')').append(" - value ").append(p.getInterpretedValue());
				}
			} else {
				sb.append("Primitive ").append(String.format("%X", p.getTag())).append(" - value ").append(HexadecimalStringUtil.bytesToString(p.getValue()));
			}
		}
	}

	public static String toHexadecimal(TLV[] tlvs) {
		StringBuilder sb = new StringBuilder(4096);
		for (TLV tlv : tlvs) {
			sb.append(toHexadecimal(tlv));
		}
		return sb.toString();
	}

	public static String toHexadecimal(TLV tlv) {
		if (tlv instanceof ConstructedTLV) {
			ConstructedTLV c = (ConstructedTLV) tlv;
			StringBuilder sb = new StringBuilder(1024);
			int total = 0;
			for (TLV child : c.getValue()) {
				String s = toHexadecimal(child);
				sb.append(s);
				total += s.length() / 2;
			}
			int tag = c.getTag();
			String stag = String.format(tag > 0x0FF ? "%04X" : "%02X", tag);
			sb.insert(0, stag);
			sb.insert(stag.length(), formatLength(total));
			return sb.toString();
		}
		if (tlv instanceof PrimitiveTLV) {
			PrimitiveTLV p = (PrimitiveTLV) tlv;
			int tag = p.getTag();
			byte[] value = p.getValue();
			return String.format(tag > 0x0FF ? "%04X" : "%02X", tag)
					+ formatLength(value != null ? value.length : 0)
					+ HexadecimalStringUtil.bytesToString(value);
		}
		return "";
	}

	public static String formatLength(int length) {
		if (length < 128) {
			return String.format("%02X", length);
		}
		if (length <= 0x0FF) {
			return String.format("81%02X", length);
		}
		if (length <= 0x0FFFF) {
			return String.format("82%04X", length);
		}
		if (length <= 0x0FFFFFF) {
			return String.format("83%06X", length);
		}
		return String.format("84%08X", length);
	}

}
