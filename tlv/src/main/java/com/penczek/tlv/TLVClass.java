package com.penczek.tlv;

/**
 * Represents a TLV tag class.
 * @author Leonardo Penczek
 * @version $Revision$
 */
public enum TLVClass {
	UNIVERSAL,
	APPLICATION,
	CONTEXT_SPECIFIC,
	PRIVATE;

	/**
	 * Returns the TLV class according to the given numeric code.
	 * @param code the numeric code.
	 * @return the TLV class, or {@code null} if {@code code} does not correspond to a valid class.
	 */
	public static TLVClass getTLVClass(int code) {
		return getTLVClass((byte) code);
	}

	/**
	 * Returns the TLV class according to the given numeric code.
	 * @param code the numeric code.
	 * @return the TLV class, or {@code null} if {@code code} does not correspond to a valid class.
	 */
	public static TLVClass getTLVClass(byte code) {
		switch (code) {
			case 0: return UNIVERSAL;
			case 1: return APPLICATION;
			case 2: return CONTEXT_SPECIFIC;
			case 3: return PRIVATE;
		}
		return null;
	}

}
