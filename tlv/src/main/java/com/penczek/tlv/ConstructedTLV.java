package com.penczek.tlv;

import java.util.EnumMap;
import java.util.Map;

/**
 * Represents a constructed element (an element composed by other elements in TLV format).
 * @author Leonardo Penczek
 * @version $Revision$
 */
public final class ConstructedTLV extends TLV {

	private static final long serialVersionUID = 1L;

	private final TLV[] value;
	private Map<EMVDataElement, TLV[]> map;

	/**
	 * Creates a new instance of ConstructedTLV.
	 * @param tag the tag of this element.
	 * @param value the child elements.
	 */
	public ConstructedTLV(int tag, TLV[] value) {
		super(tag);
		this.value = value;
		setValue(value);
	}

	/**
	 * Creates a new instance of ConstructedTLV.
	 * @param dataElement the EMV type of this element.
	 * @param value the child elements.
	 */
	public ConstructedTLV(EMVDataElement dataElement, TLV[] value) {
		super(dataElement);
		this.value = value;
		setValue(value);
	}

	/**
	 * Returns the child elements.
	 * @return the child elements.
	 */
	public TLV[] getValue() {
		return this.value;
	}

	private void setValue(TLV[] value) {
		if (value != null) {
			this.map = new EnumMap<EMVDataElement, TLV[]>(EMVDataElement.class);
			for (TLV tlv : value) {
				if (tlv.getDataElement() != null) {
					TLV[] ts = this.map.get(tlv.getDataElement());
					if (ts == null) {
						this.map.put(tlv.getDataElement(), new TLV[] { tlv });
					} else {
						// multiple EMV data elements
						TLV[] newts = new TLV[ts.length + 1];
						System.arraycopy(ts, 0, newts, 0, ts.length);
						newts[ts.length] = tlv;
						this.map.put(tlv.getDataElement(), newts);
					}
				}
			}
		}
	}

	/**
	 * Returns the first child of the given EMV type.
	 * @param element the EMV type.
	 * @return the first child of the given type, or {@code null} if not found.
	 */
	public TLV getChild(EMVDataElement element) {
		if (this.map != null) {
			TLV[] tlv = this.map.get(element);
			if (tlv != null) {
				return tlv[0];
			}
		}
		return null;
	}

	/**
	 * Returns all children of the given EMV type.
	 * @param element the EMV type.
	 * @return all children of the given EMV type, or {@code null} if not found.
	 */
	public TLV[] getChildren(EMVDataElement element) {
		return this.map != null ? this.map.get(element) : null;
	}

	/**
	 * Same as {@link #getValue()}.
	 * @return the child elements.
	 */
	@Override
	public Object getInterpretedValue() {
		return this.value;
	}

	/**
	 * Returns a human-readable representation of this element.
	 * @return a human-readable representation of this element.
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder(1024);
		TLVDataParser.toHumanString(this, 0, s);
		return s.toString();
	}

}
