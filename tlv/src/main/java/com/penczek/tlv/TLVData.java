package com.penczek.tlv;

import com.penczek.util.HexadecimalStringUtil;
import java.util.EnumMap;
import java.util.Map;

/**
 * Represents parsed TLV-data. This creates an internal map to optimze the search of TLV elements.
 * @author Leonardo Penczek
 */
public class TLVData {

	private TLV[] tlvs;
	private final Map<EMVDataElement, TLV[]> map = new EnumMap<EMVDataElement, TLV[]>(EMVDataElement.class);

	/**
	 * Creates a new TLVData object.
	 * @param tlvs the TLV data elements.
	 */
	public TLVData(TLV[] tlvs) {
		this.tlvs = tlvs;
		mapTLVTags(tlvs);
	}

	private void mapTLVTags(TLV[] tlvData) {
		for (TLV tlv : tlvData) {
			if (tlv.getDataElement() != null) {
				TLV[] ts = this.map.get(tlv.getDataElement());
				if (ts == null) {
					this.map.put(tlv.getDataElement(), new TLV[] { tlv });
				} else {
					// multiple EMV data elements
					TLV[] newts = new TLV[ts.length + 1];
					System.arraycopy(ts, 0, newts, 0, ts.length);
					newts[ts.length] = tlv;
					this.map.put(tlv.getDataElement(), newts);
				}
			}
			if (tlv instanceof ConstructedTLV) {
				ConstructedTLV c = (ConstructedTLV) tlv;
				mapTLVTags(c.getValue());
			}
		}
	}

	/**
	 * Returns the data elements.
	 * @return the data elements.
	 */
	public TLV[] getData() {
		return this.tlvs;
	}

	/**
	 * Searches for the first occurrence of data element of type {@code dataElement} and
	 * returns its interpreted value if found, or {@code null} otherwise.
	 * @param dataElement data element's type
	 * @return the element of the given type found, or null if not found.
	 */
	public Object getDataElementValue(EMVDataElement dataElement) {
		TLV[] tlv = map.get(dataElement);
		return tlv != null ? tlv[0].getInterpretedValue() : null;
	}

	/**
	 * Searches for the first occurrence of data element of type {@code dataElement} and
	 * returns its binary value (not interpreted), or {@code null} if the data element was not found.
	 * @param dataElement data element's type
	 * @return the element of the given type found, or null if not found.
	 */
	public byte[] getDataElementBinaryValue(EMVDataElement dataElement) {
		TLV[] tlv = map.get(dataElement);
		if (tlv == null) {
			return null;
		}
		if (tlv[0] instanceof PrimitiveTLV) {
			return ((PrimitiveTLV) tlv[0]).getValue();
		}
		if (tlv[0] instanceof ConstructedTLV) {
			ConstructedTLV ctlv = (ConstructedTLV) tlv[0];
			// remove tag and value
			return HexadecimalStringUtil.bytesFromString(ctlv.toHexadecimal());
		}
		return null;
	}

	/**
	 * Searches for the first occurrence of data element of type {@code dataElement} and
	 * returns the TLV element, or {@code null} if the data element was not found.
	 * @param dataElement data element's type
	 * @return the element of the given type found, or null if not found.
	 */
	public TLV getDataElement(EMVDataElement dataElement) {
		TLV[] tlv = map.get(dataElement);
		return tlv != null ? tlv[0] : null;
	}

	/**
	 * Returns data elements of type {@code dataElement}, or {@code null} if
	 * the data element was not found. This only differs of {@link #getDataElement(com.penczek.tlv.EMVDataElement) }
	 * if the type allows multiple elements (for example, issuer scripts).
	 * @param dataElement data element's type
	 * @return the elements of the given type found, or null if not found.
	 */
	public TLV[] getDataElements(EMVDataElement dataElement) {
		return map.get(dataElement);
	}

	/**
	 * Returns a human-readable representation of this object's data.
	 * @return a human-readable representation of this object's data.
	 */
	@Override
	public String toString() {
		return TLVDataParser.toHumanString(this.tlvs);
	}

	/**
	 * Returns the hexadecimal representation of this data.
	 * @return the hexadecimal representation of this data.
	 */
	public String toHexadecimal() {
		return TLVDataParser.toHexadecimal(this.tlvs);
	}

	/**
	 * Adds the given data elements to this object.
	 * @param vals the data elements to be added.
	 */
	public void add(TLV... vals) {
		if (this.tlvs == null) {
			this.tlvs = vals;
		} else {
			TLV[] newData = new TLV[this.tlvs.length + vals.length];
			System.arraycopy(this.tlvs, 0, newData, 0, this.tlvs.length);
			System.arraycopy(vals, 0, newData, this.tlvs.length, vals.length);
			this.tlvs = newData;
		}
		mapTLVTags(vals);
	}

	/**
	 * Returns {@code true} if this object is empty.
	 * @return {@code true} if this object is empty.
	 */
	public boolean isEmpty() {
		return this.tlvs == null || this.tlvs.length == 0;
	}

}
