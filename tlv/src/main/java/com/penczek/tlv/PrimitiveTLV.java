package com.penczek.tlv;

/**
 * Represents a primitive element (an element that holds a value).
 * @author Leonardo Penczek
 * @version $Revision$
 */
public final class PrimitiveTLV extends TLV {

	private static final long serialVersionUID = 1L;

	private final byte[] value;

	/**
	 * Creates a new instance of PrimitiveTLV.
	 * @param tag the tag of this element.
	 * @param value the binary value.
	 */
	public PrimitiveTLV(int tag, byte[] value) {
		super(tag);
		this.value = value;
	}

	/**
	 * Creates a new instance of PrimitiveTLV.
	 * @param dataElement the EMV type of this element.
	 * @param value the binary value.
	 */
	public PrimitiveTLV(EMVDataElement dataElement, byte[] value) {
		super(dataElement);
		this.value = value;
	}

	/**
	 * Returns the binary value of this element.
	 * @return the binary value of this element.
	 */
	public byte[] getValue() {
		return this.value;
	}

	/**
	 * Returns a human-readable representation of this element.
	 * @return a human-readable representation of this element.
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder(256);
		TLVDataParser.toHumanString(this, 0, s);
		return s.toString();
	}

	/**
	 * Returns the interpreted value of this element according to its defined EMV type.
	 * @see TagFormat#interpret(byte[], int, int)
	 * @return the interpreted value (BigDecimal, Long, String, etc.)
	 */
	@Override
	public Object getInterpretedValue() {
		return this.dataElement!= null
				? this.dataElement.interpret(this.value)
				: this.value;
	}

}
