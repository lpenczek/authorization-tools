package com.penczek.util;

/**
 * M�todos utilit�rios para convers�o de strings representando bytes em hexadecimal em vetores de bytes e vice-versa.
 * @author Leonardo Penczek
 * @version $Revision$
 */
public class HexadecimalStringUtil {

	private HexadecimalStringUtil() {}

	/**
	 * Converte uma string com a representa��o hexadecimal de bytes em valores propriamente ditos.
	 * @param s uma string com valores em hexadecimal.
	 * @return os bytes com os valores informados.
	 */
	public static byte[] bytesFromString(String s) {
		s = s.toUpperCase();
		// parse otimizado
		byte[] bs = new byte[s.length() / 2 + s.length() % 2];
		// tras pra frente pois primeiro byte pode ser algo como '0E' e representado apenas com 'E'
		byte b;
		for (int i = s.length() - 1; i >= 0; i -= 2) {
			b = (byte) (s.charAt(i) - '0');
			bs[i / 2] = b > 9 ? (byte) (b - 7) : b; // offset do 'A' em relacao ao '9'
			if (i > 0) {
				b = (byte) (s.charAt(i - 1) - '0');
				bs[i / 2] |= (b > 9 ? (byte) (b - 7) : b) << 4;
			}
		}
		return bs;
	}

	/**
	 * Converte um vetor de bytes em sua representa��o hexadecimal.
	 * @param bs um vetor de bytes.
	 * @return a representa��o hexadecimal dos valores informados.
	 */
	public static String bytesToString(byte[] bs) {
		// toString otimizado
		StringBuilder sb = new StringBuilder(bs.length << 1);
		int t;
		for (byte b : bs) {
			t = (b >>> 4) & 0xF;
			sb.append(t < 10 ? (char) (t + 48) : (char) (t + 55)); // 48 = '0'; 65 (55 - 10) = 'A'
			t = b & 0xF;
			sb.append(t < 10 ? (char) (t + 48) : (char) (t + 55)); // 48 = '0'; 65 (55 - 10) = 'A'
		}
		return sb.toString();
	}

}
