package com.penczek.tlvviewer;

import com.penczek.tlv.TLVDataParser;
import javax.swing.JOptionPane;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
		dtd = "-//com.penczek.tlvviewer//TLVViewer//EN",
		autostore = false)
@TopComponent.Description(
		preferredID = "TLVViewerTopComponent",
		iconBase="com/penczek/tlvviewer/iso.png",
		persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "editor", openAtStartup = true)
@ActionID(category = "Window", id = "com.penczek.tlvviewer.TLVViewerTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
		displayName = "#CTL_TLVViewerAction",
		preferredID = "TLVViewerTopComponent")
@Messages({
	"CTL_TLVViewerAction=TLV Viewer",
	"CTL_TLVViewerTopComponent=TLV Viewer",
	"HINT_TLVViewerTopComponent=TLV Viewer"
})
public final class TLVViewerTopComponent extends TopComponent {

	public TLVViewerTopComponent() {
		initComponents();
		setName(Bundle.CTL_TLVViewerTopComponent());
		setToolTipText(Bundle.HINT_TLVViewerTopComponent());

	}

	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this
	 * method is always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        rawLabel = new javax.swing.JLabel();
        rawScrollPane = new javax.swing.JScrollPane();
        rawTextArea = new javax.swing.JTextArea();
        parsedLabel = new javax.swing.JLabel();
        parsedScrollPane = new javax.swing.JScrollPane();
        parsedTextArea = new javax.swing.JTextArea();
        parseButton = new javax.swing.JButton();

        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("com/penczek/tlvviewer/Bundle"); // NOI18N
        org.openide.awt.Mnemonics.setLocalizedText(rawLabel, bundle.getString("TLVViewerTopComponent.rawLabel.text")); // NOI18N

        rawTextArea.setColumns(20);
        rawTextArea.setRows(5);
        rawScrollPane.setViewportView(rawTextArea);

        org.openide.awt.Mnemonics.setLocalizedText(parsedLabel, org.openide.util.NbBundle.getMessage(TLVViewerTopComponent.class, "TLVViewerTopComponent.parsedLabel.text")); // NOI18N

        parsedTextArea.setColumns(20);
        parsedTextArea.setRows(5);
        parsedScrollPane.setViewportView(parsedTextArea);

        org.openide.awt.Mnemonics.setLocalizedText(parseButton, org.openide.util.NbBundle.getMessage(TLVViewerTopComponent.class, "TLVViewerTopComponent.parseButton.text")); // NOI18N
        parseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                parseButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(parsedLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(rawLabel)
                                .addGap(25, 25, 25)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rawScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
                            .addComponent(parsedScrollPane)))
                    .addComponent(parseButton))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rawScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rawLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(parsedLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(parsedScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(parseButton)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void parseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_parseButtonActionPerformed
		String data = this.rawTextArea.getText();
		try {
			if (data.indexOf(',') >= 0) {
				data = data.replaceAll("[^0-9,-]", "");
				String[] val = data.split(",");
				byte[] b = new byte[val.length];
				for (int i = 0; i < b.length; i++) {
					b[i] = (byte) Integer.parseInt(val[i]);
				}
				this.parsedTextArea.setText(TLVDataParser.parseData(b).toString());
			} else {
				data = data.replaceAll("[^0-9A-Fa-f]", "");
				this.parsedTextArea.setText(TLVDataParser.parseData(data).toString());
			}
		} catch (Exception e) {
			StringBuilder stack = new StringBuilder(512);
			stack.append(e).append('\n');
			for (StackTraceElement el : e.getStackTrace()) {
				stack.append(el).append('\n');
			}
			JOptionPane.showMessageDialog(this, "Erro: " + e.getMessage() + '\n' + stack, "Erro", JOptionPane.ERROR_MESSAGE);
		}
    }//GEN-LAST:event_parseButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton parseButton;
    private javax.swing.JLabel parsedLabel;
    private javax.swing.JScrollPane parsedScrollPane;
    private javax.swing.JTextArea parsedTextArea;
    private javax.swing.JLabel rawLabel;
    private javax.swing.JScrollPane rawScrollPane;
    private javax.swing.JTextArea rawTextArea;
    // End of variables declaration//GEN-END:variables
	@Override
	public void componentOpened() {
		// TODO add custom code on component opening
	}

	@Override
	public void componentClosed() {
		// TODO add custom code on component closing
	}

	void writeProperties(java.util.Properties p) {
		// better to version settings since initial version as advocated at
		// http://wiki.apidesign.org/wiki/PropertyFiles
		p.setProperty("version", "1.0");
		// TODO store your settings
	}

	void readProperties(java.util.Properties p) {
		String version = p.getProperty("version");
		// TODO read your settings according to their version
	}
}
