/*
j8583 A Java implementation of the ISO8583 protocol
Copyright (C) 2007 Enrique Zamudio Lopez

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
package com.solab.iso8583;

import com.solab.iso8583.parse.FieldParseInfo;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/** This class is used to create messages, either from scratch or from an existing String or byte
 * buffer. It can be configured to put default values on newly created messages, and also to know
 * what to expect when reading messages from an InputStream.
 * <P>
 * The factory can be configured to know what values to set for newly created messages, both from
 * a template (useful for fields that must be set with the same value for EVERY message created)
 * and individually (for trace [field 11] and message date [field 7]).
 * <P>
 * It can also be configured to know what fields to expect in incoming messages (all possible values
 * must be stated, indicating the date type for each). This way the messages can be parsed from
 * a byte buffer.
 * 
 * @author Enrique Zamudio
 */
public class MessageFactory {

	private static final Logger log = Logger.getLogger(MessageFactory.class.getSimpleName());

	/** This map stores the message template for each message type. */
	private Map<Integer, IsoMessage> typeTemplates = new HashMap<Integer, IsoMessage>();
	/** Stores the information needed to parse messages sorted by type. */
	private Map<Integer, Map<Integer, FieldParseInfo>> parseMap = new HashMap<Integer, Map<Integer, FieldParseInfo>>();
	/** Stores the field numbers to be parsed, in order of appearance. */
	private Map<Integer, List<Integer>> parseOrder = new HashMap<Integer, List<Integer>>();

	private TraceNumberGenerator traceGen;
	/** The ISO header to be included in each message type. */
	private Map<Integer, String> isoHeaders = new HashMap<Integer, String>();
	/** A map for the custom field encoder/decoders, keyed by field number. */
	private Map<Integer, CustomField<?>> customFields = new HashMap<Integer, CustomField<?>>();
	/** Indicates if the factory should create binary messages and also parse binary messages. */
	private boolean useBinaryType;
	private boolean useBinaryBitmap;
	private boolean useBinaryData;
	private int etx = -1;
	/** Flag to specify if missing fields should be ignored as long as they're at the end of the message. */
	private boolean ignoreLast;
	private boolean forceb2;

	/** Sets or clears the flag to pass to new messages, to include a secondary bitmap even if it's not needed. */
	public void setForceSecondaryBitmap(boolean flag) {
		this.forceb2 = flag;
	}
	public boolean getForceSecondaryBitmap() {
		return this.forceb2;
	}

	/** Setting this property to true avoids getting a ParseException when parsing messages that don't have
	 * the last field specified in the bitmap. This is common with certain providers where field 128 is
	 * specified in the bitmap but not actually included in the messages. Default is false, which has
	 * been the behavior in previous versions when this option didn't exist. */
	public void setIgnoreLastMissingField(boolean flag) {
		this.ignoreLast = flag;
	}
	/** This flag indicates if the MessageFactory throws an exception if the last field of a message
	 * is not really present even though it's specified in the bitmap. Default is false which means
	 * an exception is thrown. */
	public boolean getIgnoreLastMissingField() {
		return this.ignoreLast;
	}

	/** Specifies a map for custom field encoder/decoders. The keys are the field numbers. */
	public void setCustomFields(Map<Integer, CustomField<?>> value) {
		this.customFields = value;
	}

	/** Sets the CustomField encoder for the specified field number. */
	public void setCustomField(int index, CustomField<?> value) {
		this.customFields.put(Integer.valueOf(index), value);
	}
	/** Returns a custom field encoder/decoder for the specified field number, if one is available. */
	public CustomField<?> getCustomField(int index) {
		return this.customFields.get(Integer.valueOf(index));
	}
	/** Returns a custom field encoder/decoder for the specified field number, if one is available. */
	public CustomField<?> getCustomField(Integer index) {
		return this.customFields.get(index);
	}

	/** Tells the receiver to create and parse binary messages if the flag is true.
	 * Default is false, that is, create and parse ASCII messages. */
	public void setUseBinaryMessages(boolean flag) {
		this.useBinaryType = flag;
		this.useBinaryBitmap = flag;
		this.useBinaryData = flag;
	}

	/**
	 * @return the useBinaryType
	 */
	public boolean isUseBinaryType() {
		return this.useBinaryType;
	}

	/**
	 * @param useBinaryType the useBinaryType to set
	 */
	public void setUseBinaryType(boolean useBinaryType) {
		this.useBinaryType = useBinaryType;
	}

	/**
	 * @return the useBinaryBitmap
	 */
	public boolean isUseBinaryBitmap() {
		return this.useBinaryBitmap;
	}

	/**
	 * @param useBinaryBitmap the useBinaryBitmap to set
	 */
	public void setUseBinaryBitmap(boolean useBinaryBitmap) {
		this.useBinaryBitmap = useBinaryBitmap;
	}

	/**
	 * @return the useBinaryData
	 */
	public boolean isUseBinaryData() {
		return this.useBinaryData;
	}

	/**
	 * @param useBinaryData the useBinaryData to set
	 */
	public void setUseBinaryData(boolean useBinaryData) {
		this.useBinaryData = useBinaryData;
	}

	/** Sets the ETX character to be sent at the end of the message. This is optional and the
	 * default is -1, which means nothing should be sent as terminator.
	 * @param value The ASCII value of the ETX character or -1 to indicate no terminator should be used. */
	public void setEtx(int value) {
		this.etx = value;
	}
	public int getEtx() {
		return this.etx;
	}

	/** Creates a new message of the specified type, with optional trace and date values as well
	 * as any other values specified in a message template. If the factory is set to use binary
	 * messages, then the returned message will be written using binary coding.
	 * @param type The message type, for example 0x200, 0x400, etc. */
	public IsoMessage newMessage(int type) {
		IsoMessage m = new IsoMessage(this.isoHeaders.get(Integer.valueOf(type)));
		m.setType(type);
		m.setEtx(this.etx);
		m.setBinaryType(this.useBinaryType);
		m.setBinaryBitmap(this.useBinaryBitmap);
		m.setBinaryData(this.useBinaryData);
		m.setForceSecondaryBitmap(this.forceb2);

		//Copy the values from the template
		IsoMessage templ = this.typeTemplates.get(Integer.valueOf(type));
		if (templ != null) {
			for (int i = 2; i <= 128; i++) {
				if (templ.hasField(i)) {
					m.setField(i, templ.getField(i).clone());
				}
			}
		}
		if (this.traceGen != null) {
			m.setValue(11, Integer.valueOf(this.traceGen.nextTrace()), IsoType.NUMERIC, 6);
		}
		return m;
	}

	/** Creates a message to respond to a request. Increments the message type by 16,
	 * sets all fields from the template if there is one, and copies all values from the request,
	 * overwriting fields from the template if they overlap.
	 * @param request An ISO8583 message with a request type (ending in 00). */
	public IsoMessage createResponse(IsoMessage request) {
		IsoMessage resp = new IsoMessage(this.isoHeaders.get(Integer.valueOf(request.getType() + 16)));
		resp.setBinaryType(request.isBinaryType());
		resp.setBinaryBitmap(request.isBinaryBitmap());
		resp.setBinaryData(request.isBinaryData());
		resp.setType(request.getType() + 16);
		resp.setEtx(this.etx);
		resp.setForceSecondaryBitmap(this.forceb2);
		//Copy the values from the template or the request (request has preference)
		IsoMessage templ = this.typeTemplates.get(Integer.valueOf(resp.getType()));
		if (templ == null) {
			for (int i = 2; i < 128; i++) {
				if (request.hasField(i)) {
					resp.setField(i, request.getField(i).clone());
				}
			}
		} else {
			for (int i = 2; i < 128; i++) {
				if (request.hasField(i)) {
					resp.setField(i, request.getField(i).clone());
				} else if (templ.hasField(i)) {
					resp.setField(i, templ.getField(i).clone());
				}
			}
		}
		return resp;
	}

	/** Creates a new message instance from the buffer, which must contain a valid ISO8583
	 * message. If the factory is set to use binary messages then it will try to parse
	 * a binary message.
	 * @param buf The byte buffer containing the message. Must not include the length header.
	 * @param isoHeaderLength The expected length of the ISO header, after which the message type
	 * and the rest of the message must come. */
	public IsoMessage parseMessage(byte[] buf, int isoHeaderLength) throws ParseException {
		IsoMessage m = new IsoMessage(isoHeaderLength > 0 ? Arrays.copyOfRange(buf, 0, isoHeaderLength) : null);
		int currentPos = isoHeaderLength;
		int type = 0;
		if (this.useBinaryType) {
			type = ((buf[currentPos] & 0xff) << 8) | (buf[currentPos + 1] & 0xff);
			currentPos += 2;
		} else {
			type = ((buf[currentPos] - '0') << 12)
					| ((buf[currentPos + 1] - '0') << 8)
					| ((buf[currentPos + 2] - '0') << 4)
					| (buf[currentPos + 3] - '0');
			currentPos += 4;
		}
		m.setType(type);
		//Parse the bitmap (primary first)
		BitSet bs = new BitSet(128);
		if (this.useBinaryBitmap) {
			int pos = 0;
			for (int i = 0; i < 8; i++) {
				int bit = 128;
				for (int b = 0; b < 8; b++) {
					bs.set(pos++, (buf[currentPos + i] & bit) != 0);
					bit >>= 1;
				}
			}
			currentPos += 8;
			//Check for secondary bitmap and parse if necessary
			if (bs.get(0)) {
				for (int i = 0; i < 8; i++) {
					int bit = 128;
					for (int b = 0; b < 8; b++) {
						bs.set(pos++, (buf[currentPos + i] & bit) != 0);
						bit >>= 1;
					}
				}
				currentPos += 8;
			}
		} else {
			int pos = 0;
			try {
				for (int i = 0; i < 16; i++) {
					byte b = buf[i + currentPos];
					if (b >= '0' && b <= '9') {
						b = (byte) (b - '0');
					} else if (b >= 'A' && b <= 'F') {
						b = (byte) (b - 'A' + 10);
					} else if (b >= 'a' && b <= 'f') {
						b = (byte) (b - 'a' + 10);
					}
					bs.set(pos++, (b & 8) > 0);
					bs.set(pos++, (b & 4) > 0);
					bs.set(pos++, (b & 2) > 0);
					bs.set(pos++, (b & 1) > 0);
				}
				currentPos += 16;
				//Check for secondary bitmap and parse it if necessary
				if (bs.get(0)) {
					for (int i = 0; i < 16; i++) {
						byte b = buf[i + currentPos];
						if (b >= '0' && b <= '9') {
							b = (byte) (b - '0');
						} else if (b >= 'A' && b <= 'F') {
							b = (byte) (b - 'A' + 10);
						} else if (b >= 'a' && b <= 'f') {
							b = (byte) (b - 'a' + 10);
						}
						bs.set(pos++, (b & 8) > 0);
						bs.set(pos++, (b & 4) > 0);
						bs.set(pos++, (b & 2) > 0);
						bs.set(pos++, (b & 1) > 0);
					}
					currentPos += 16;
				}
			} catch (NumberFormatException ex) {
				ParseException _e = new ParseException("Invalid ISO8583 bitmap", pos);
				_e.initCause(ex);
				throw _e;
			}
		}
		//Parse each field
		Map<Integer, FieldParseInfo> parseGuide = this.parseMap.get(Integer.valueOf(type));
		List<Integer> index = this.parseOrder.get(Integer.valueOf(type));
		if (index == null) {
			throw new ParseException(String.format("ISO8583 MessageFactory has no parsing guide for message type %04x", Integer.valueOf(type)), 0);
		}
		//First we check if the message contains fields not specified in the parsing template
		for (int i = 1; i < bs.length(); i++) {
			if (bs.get(i) && !index.contains(Integer.valueOf(i+1))) {
				throw new ParseException("ISO8583 MessageFactory cannot parse field " + (i + 1) + ": unspecified in parsing guide", i + 1);
			}
		}
		//Now we parse each field
		if (this.useBinaryData) {
			for (Integer i : index) {
				FieldParseInfo fpi = parseGuide.get(i);
				if (bs.get(i.intValue() - 1)) {
					if (this.ignoreLast && currentPos >= buf.length && i == index.get(index.size() -1)) {
						log.warning("Field " + i + " is not really in the message even though it's in the bitmap");
						bs.clear(i.intValue() - 1);
					} else {
						IsoValue<?> val = fpi.parseBinary(buf, currentPos, this.getCustomField(i));
						m.setField(i.intValue(), val);
						if (!(val.getType() == IsoType.ALPHA || val.getType() == IsoType.LLVAR
								|| val.getType() == IsoType.LLLVAR || val.getType() == IsoType.LLLLVAR)) {
							currentPos += (val.getLength() / 2) + (val.getLength() % 2);
						} else {
							currentPos += val.getLength();
						}
						if (val.getType() == IsoType.LLVAR) {
							currentPos++;
						} else if (val.getType() == IsoType.LLLVAR || val.getType() == IsoType.LLLLVAR) {
							currentPos += 2;
						}
					}
				}
			}
		} else {
			for (Integer i : index) {
				FieldParseInfo fpi = parseGuide.get(i);
				if (bs.get(i.intValue() - 1)) {
					if (this.ignoreLast && currentPos >= buf.length && i == index.get(index.size() -1)) {
						log.warning("Field " + i + " is not really in the message even though it's in the bitmap");
						bs.clear(i.intValue() - 1);
					} else {
						IsoValue<?> val = fpi.parse(buf, currentPos, this.getCustomField(i));
						m.setField(i.intValue(), val);
						currentPos += val.getLength();
						if (val.getType() == IsoType.LLVAR) {
							currentPos += 2;
						} else if (val.getType() == IsoType.LLLVAR) {
							currentPos += 3;
						} else if (val.getType() == IsoType.LLLLVAR) {
							currentPos += 4;
						}
					}
				}
			}
		}
		m.setBinaryType(this.useBinaryType);
		m.setBinaryBitmap(this.useBinaryBitmap);
		m.setBinaryData(this.useBinaryData);
		return m;
	}

	/** Sets the generator that this factory will get new trace numbers from. There is no
	 * default generator. */
	public void setTraceNumberGenerator(TraceNumberGenerator value) {
		this.traceGen = value;
	}
	/** Returns the generator used to assign trace numbers to new messages. */
	public TraceNumberGenerator getTraceNumberGenerator() {
		return this.traceGen;
	}

	/** Sets the ISO header to be used in each message type.
	 * @param value A map where the keys are the message types and the values are the ISO headers.
	 */
	public void setIsoHeaders(Map<Integer, String> value) {
		this.isoHeaders.clear();
		this.isoHeaders.putAll(value);
	}

	/** Sets the ISO header for a specific message type.
	 * @param type The message type, for example 0x200.
	 * @param value The ISO header, or NULL to remove any headers for this message type. */
	public void setIsoHeader(int type, String value) {
		if (value == null) {
			this.isoHeaders.remove(Integer.valueOf(type));
		} else {
			this.isoHeaders.put(Integer.valueOf(type), value);
		}
	}

	/** Returns the ISO header used for the specified type. */
	public String getIsoHeader(int type) {
		return this.isoHeaders.get(Integer.valueOf(type));
	}

	/** Adds a message template to the factory. If there was a template for the same
	 * message type as the new one, it is overwritten. */
	public void addMessageTemplate(IsoMessage templ) {
		if (templ != null) {
			this.typeTemplates.put(Integer.valueOf(templ.getType()), templ);
		}
	}

	/** Removes the message template for the specified type. */
	public void removeMessageTemplate(int type) {
		this.typeTemplates.remove(Integer.valueOf(type));
	}

	/** Returns the template for the specified message type. This allows templates to be modified
	 * programatically. */
	public IsoMessage getMessageTemplate(int type) {
		return this.typeTemplates.get(Integer.valueOf(type));
	}

	/** Invoke this method in case you want to freeze the configuration, making message and parsing
	 * templates, as well as iso headers and custom fields, immutable. */
	public void freeze() {
		this.typeTemplates = Collections.unmodifiableMap(this.typeTemplates);
		this.parseMap = Collections.unmodifiableMap(this.parseMap);
		this.parseOrder = Collections.unmodifiableMap(this.parseOrder);
		this.isoHeaders = Collections.unmodifiableMap(this.isoHeaders);
		this.customFields = Collections.unmodifiableMap(this.customFields);
	}

	/** Sets a map with the fields that are to be expected when parsing a certain type of
	 * message.
	 * @param type The message type.
	 * @param map A map of FieldParseInfo instances, each of which define what type and length
	 * of field to expect. The keys will be the field numbers. */
	public void setParseMap(int type, Map<Integer, FieldParseInfo> map) {
		this.parseMap.put(Integer.valueOf(type), map);
		ArrayList<Integer> index = new ArrayList<Integer>();
		index.addAll(map.keySet());
		Collections.sort(index);
		this.parseOrder.put(Integer.valueOf(type), index);
	}

	public Map<Integer, FieldParseInfo> getParseMap(Integer i) {
		return this.parseMap.get(i);
	}

}
