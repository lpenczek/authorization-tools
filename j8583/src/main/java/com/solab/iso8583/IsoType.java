/*
j8583 A Java implementation of the ISO8583 protocol
Copyright (C) 2007 Enrique Zamudio Lopez

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
package com.solab.iso8583;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/** Defines the possible values types that can be used in the fields.
 * Some types required the length of the value to be specified (NUMERIC
 * and ALPHA). Other types have a fixed length, like dates and times.
 * Other types do not require a length to be specified, like LLVAR,
 * LLLVAR and LLLLVAR.
 * @author Enrique Zamudio
 */
public enum IsoType {

	/** A fixed-length numeric value. It is zero-filled to the left. */
	NUMERIC(true, 0),
	/** A fixed-length alphanumeric value. It is filled with spaces to the right. */
	ALPHA(true, 0),
	/** A variable length alphanumeric value with a 2-digit header length. */
	LLVAR(false, 0),
	/** A variable length alphanumeric value with a 3-digit header length. */
	LLLVAR(false, 0),
	/** A variable length alphanumeric value with a 4-digit header length. */
	LLLLVAR(false, 0),
	/** A date in format MMddHHmmss */
	DATE10(false, 10),
	/** A date in format yyMMddHHmm */
	// DATE10YY
	DATE10YY(false, 10),
	/** A date in format yyMMddHHmmss */
	// DATE12
	DATE12(false, 12),
	/** A date in format MMdd */
	DATE4(false, 4),
	// DATE6
	/** A date in format yyMMdd */
	DATE6(false, 6),
	/** A date in format yyMM */
	DATE_EXP(false, 4),
	/** A date in format MMyy */
	// DATE_EXP_MMYY
	DATE_EXP_MMYY(false, 4),
	/** Time of day in format HHmmss */
	TIME(false, 6),
	/** An amount, expressed in cents with a fixed length of 12. */
	AMOUNT(false, 12),
	// RATE
	/** A conversion rate, 1st digit is scale, rest of is magnitude. */
	RATE(false, 8);

	private boolean needsLen;
	private int length;

	IsoType(boolean flag, int l) {
		this.needsLen = flag;
		this.length = l;
	}

	/** Returns true if the type needs a specified length. */
	public boolean needsLength() {
		return this.needsLen;
	}

	/** Returns the length of the type if it's always fixed, or 0 if it's variable. */
	public int getLength() {
		return this.length;
	}

	/** Formats a Date if the receiver is DATE10, DATE4, DATE_EXP or TIME; throws an exception
	 * otherwise. */
	public String format(Date value) {
		if (this == DATE10) {
			return new SimpleDateFormat("MMddHHmmss").format(value);
		} else if (this == DATE4) {
			return new SimpleDateFormat("MMdd").format(value);
		} else if (this == DATE10YY) {
			return new SimpleDateFormat("yyMMddHHmm").format(value);
		} else if (this == DATE12) {
			return new SimpleDateFormat("yyMMddHHmmss").format(value);
		} else if (this == DATE6) {
			return new SimpleDateFormat("yyMMdd").format(value);
		} else if (this == DATE_EXP) {
			return new SimpleDateFormat("yyMM").format(value);
		} else if (this == DATE_EXP_MMYY) {
			return new SimpleDateFormat("MMyy").format(value);
		} else if (this == TIME) {
			return new SimpleDateFormat("HHmmss").format(value);
		}
		throw new IllegalArgumentException("Cannot format date as " + this);
	}

	/** Formats the string to the given length (length is only useful if type is ALPHA). */
	public String format(String value, int len) {
		if (this == ALPHA) {
	    	if (value == null) {
	    		value = "";
	    	}
	        if (value.length() > len) {
	            return value.substring(0, len);
	        }
	        char[] c = new char[len];
	        System.arraycopy(value.toCharArray(), 0, c, 0, value.length());
	        for (int i = value.length(); i < c.length; i++) {
	            c[i] = ' ';
	        }
	        return new String(c);
		} else if (this == LLVAR || this == LLLVAR || this == LLLLVAR) {
			return value;
		} else if (this == NUMERIC) {
	        char[] c = new char[len];
	        char[] x = value.toCharArray();
	        if (x.length > len) {
	        	throw new IllegalArgumentException("Numeric value is larger than intended length: " + value + " LEN " + len);
	        }
	        int lim = c.length - x.length;
	        for (int i = 0; i < lim; i++) {
	            c[i] = '0';
	        }
	        System.arraycopy(x, 0, c, lim, x.length);
	        return new String(c);
		} else if (this == AMOUNT) {
			return IsoType.NUMERIC.format(new BigDecimal(value).movePointRight(2).intValue(), this.length);
		} else if (this == RATE) {
	        int dif = this.length - value.length();
			if (dif < 0) {
	        	throw new IllegalArgumentException("Numeric value is larger than intended length: " + value + " LEN " + this.length);
			}
			// se tamanho correto, assume que esta no formato certo, senao assume escala 0
			if (dif == 0) {
				return value;
			}
	        char[] c = new char[this.length];
	        for (int i = 0; i < dif; i++) {
	            c[i] = '0';
	        }
			for (int i = 0; i < value.length(); i++) {
				c[i + dif] = value.charAt(i);
			}
			return new String(c);
		}
		throw new IllegalArgumentException("Cannot format String as " + this);
	}

	/** Formats the integer value as a NUMERIC, an AMOUNT, a RATE or a String. */
	public String format(long value, int len) {
		if (this == NUMERIC) {
	        char[] c = new char[len];
	        char[] x = Long.toString(value).toCharArray();
	        if (x.length > len) {
	        	throw new IllegalArgumentException("Numeric value is larger than intended length: " + value + " LEN " + len);
	        }
	        int lim = c.length - x.length;
	        for (int i = 0; i < lim; i++) {
	            c[i] = '0';
	        }
	        System.arraycopy(x, 0, c, lim, x.length);
	        return new String(c);
		} else if (this == ALPHA || this == LLVAR || this == LLLVAR || this == LLLLVAR) {
			return this.format(Long.toString(value), len);
		} else if (this == AMOUNT) {
			String v = Long.toString(value);
			char[] digits = new char[12];
			for (int i = 0; i < 12; i++) {
				digits[i] = '0';
			}
			//No hay decimales asi que dejamos los dos ultimos digitos como 0
			System.arraycopy(v.toCharArray(), 0, digits, 10 - v.length(), v.length());
			return new String(digits);
		} else if (this == RATE) {
			return this.format(new BigDecimal(value), len);
		}
		throw new IllegalArgumentException("Cannot format number as " + this);
	}

	/** Formats the BigDecimal as an AMOUNT, NUMERIC, RATE or a String. */
	public String format(BigDecimal value, int len) {
		if (this == AMOUNT) {
			String v = new DecimalFormat(value.signum() >= 0 ? "0000000000.00" : "000000000.00").format(value);
			return String.format("%s%s", v.substring(0, 10), v.substring(11));
		} else if (this == NUMERIC) {
			return this.format(value.longValue(), len);
		} else if (this == ALPHA || this == LLVAR || this == LLLVAR || this == LLLLVAR) {
			return this.format(value.toString(), len);
		} else if (this == RATE) {
			if (value.scale() < 0) {
				value = value.setScale(0);
			} else if (value.scale() > 9) {
				value = value.setScale(9, RoundingMode.HALF_UP);
			}
			String s = value.unscaledValue().toString();
			if (s.length() > 7) {
				s = s.substring(0, 7);
			} else if (s.length() < 7) {
				char[] mantissa = new char[] {'0', '0', '0', '0', '0', '0', '0' };
				for (int i = s.length() - 1, j = 6; i >= 0; i--, j--) {
					mantissa[j] = s.charAt(i);
				}
				s = new String(mantissa);
			}
			return value.scale() + s;
		}
		throw new IllegalArgumentException("Cannot format BigDecimal as " + this);
	}

	/** Parses the character data from the buffer and returns the
	 * IsoValue with the correct data type in it. */
	public <T extends Object> IsoValue<?> parse(byte[] buf, int pos, CustomField<T> custom, int len) throws ParseException {
		if (pos < 0) {
			throw new ParseException(String.format("Invalid position %d", Integer.valueOf(pos)), pos);
		}
		if (this == IsoType.NUMERIC || this == IsoType.ALPHA) {
			if (pos + len > buf.length) {
				throw new ParseException(String.format("Insufficient data for ALPHA or NUMERIC field of length %d, pos %d",
						Integer.valueOf(len), Integer.valueOf(pos)), pos);
			}
			String val = null;
			try {
				val = new String(buf, pos, len, "ISO-8859-1");
			} catch (Exception e) {}
			if (custom == null) {
				return new IsoValue<String>(this, val, len, null);
			}
			IsoValue<T> v = new IsoValue<T>(this, custom.decodeField(val), len, custom);
			if (v.getValue() == null) {
				return new IsoValue<String>(this, val, len, null);
			}
			return v;
		} else if (this == IsoType.LLVAR || this == IsoType.LLLVAR || this == IsoType.LLLLVAR) {
			int digits = this == IsoType.LLLLVAR ? 4 : this == IsoType.LLLVAR ? 3 : 2;
			int ll = 0;
			for (int i = pos + digits - 1, pot = 1; i >= pos; i--, pot *= 10) {
				if (!Character.isDigit(buf[i])) {
					throw new ParseException(String.format("Invalid %s length '%s' pos %d", this.toString(), new String(buf, pos, digits), Integer.valueOf(pos)), pos);
				}
				ll += (buf[i] - '0') * pot;
			}
			if (ll < 0) {
				throw new ParseException(String.format("Invalid %s length %d pos %d", this.toString(), Integer.valueOf(ll), Integer.valueOf(pos)), pos);
			}
			if (ll + pos + digits > buf.length) {
				throw new ParseException(String.format("Insufficient data for %s field, pos %d", this.toString(), Integer.valueOf(pos)), pos);
			}
			String val = null;
			try {
				val = ll > 0 ? new String(buf, pos + digits, ll, "ISO-8859-1") : "";
			} catch (Exception e) {}
			if (custom == null) {
				return new IsoValue<String>(this, val, ll, null);
			}
			IsoValue<T> v = new IsoValue<T>(this, custom.decodeField(val), ll, custom);
			if (v.getValue() == null) {
				return new IsoValue<String>(this, val, ll, null);
			}
			return v;
		} else if (this == IsoType.AMOUNT) {
			if (pos+12 > buf.length) {
				throw new ParseException(String.format("Insufficient data for AMOUNT field of length %d, pos %d",
						Integer.valueOf(12), Integer.valueOf(pos)), pos);
			}
			byte[] c = new byte[13];
			System.arraycopy(buf, pos, c, 0, 10);
			System.arraycopy(buf, pos + 10, c, 11, 2);
			c[10] = '.';
			try {
				return new IsoValue<BigDecimal>(this, new BigDecimal(new String(c, "ISO-8859-1")), null);
			} catch (Exception ex) {
				throw new ParseException(String.format("Cannot read amount '%s' pos %d", new String(c), Integer.valueOf(pos)), pos);
			}
		} else if (this == IsoType.RATE) {
			int ll = IsoType.RATE.getLength();
			if (pos + ll > buf.length) {
				throw new ParseException(String.format("Insufficient data for RATE field of length %d, pos %d",
						Integer.valueOf(ll), Integer.valueOf(pos)), pos);
			}
			int scale = buf[pos] - '0';
			if (scale < 0 || scale > 9) {
				throw new ParseException(String.format("Invalid scale for RATE field %d, pos %d",
						Integer.valueOf(scale), Integer.valueOf(pos)), pos);
			}
			byte[] c = new byte[ll - 1];
			System.arraycopy(buf, pos + 1, c, 0, ll - 1);
			try {
				return new IsoValue<BigDecimal>(this, BigDecimal.valueOf(Long.parseLong(new String(c, "ISO-8859-1"), 10), scale), null);
			} catch (Exception ex) {
				throw new ParseException(String.format("Cannot read conversion rate '%s' pos %d", new String(c), Integer.valueOf(pos)), pos);
			}
		} else if (this == IsoType.DATE10) {
			if (pos+10 > buf.length) {
				throw new ParseException(String.format("Insufficient data for DATE10 field of length %d, pos %d",
						Integer.valueOf(10), Integer.valueOf(pos)), pos);
			}
			//A SimpleDateFormat in the case of dates won't help because of the missing data
			//we have to use the current date for reference and change what comes in the buffer
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			//Set the month in the date
			cal.set(Calendar.MONTH, ((buf[pos] - '0') * 10) + buf[pos + 1] - '0' - 1);
			cal.set(Calendar.DATE, ((buf[pos + 2] - '0') * 10) + buf[pos + 3] - '0');
			cal.set(Calendar.HOUR_OF_DAY, ((buf[pos + 4] - '0') * 10) + buf[pos + 5] - '0');
			cal.set(Calendar.MINUTE, ((buf[pos + 6] - '0') * 10) + buf[pos + 7] - '0');
			cal.set(Calendar.SECOND, ((buf[pos + 8] - '0') * 10) + buf[pos + 9] - '0');
			cal.set(Calendar.MILLISECOND, 0);
			this.adjustYear(cal);
			return new IsoValue<Date>(this, cal.getTime(), null);
		} else if (this == IsoType.DATE4) {
			if (pos+4 > buf.length) {
				throw new ParseException(String.format("Insufficient data for DATE4 field of length %d, pos %d",
						Integer.valueOf(4), Integer.valueOf(pos)), pos);
			}
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			//Set the month in the date
			cal.set(Calendar.MONTH, ((buf[pos] - '0') * 10) + buf[pos + 1] - '0' - 1);
			cal.set(Calendar.DATE, ((buf[pos + 2] - '0') * 10) + buf[pos + 3] - '0');
			this.adjustYear(cal);
			return new IsoValue<Date>(this, cal.getTime(), null);
		} else if (this == IsoType.DATE10YY) {
			if (pos + 10 > buf.length) {
				throw new ParseException(String.format("Insufficient data for DATE10YY field of length %d, pos %d",
						Integer.valueOf(10), Integer.valueOf(pos)), pos);
			}
			//A SimpleDateFormat in the case of dates won't help because of the missing data
			//we have to use the current date for reference and change what comes in the buffer
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			//Set the month in the date
			cal.set(Calendar.YEAR, this.adjustYear((buf[pos] - '0') * 10 + buf[pos + 1] - '0'));
			cal.set(Calendar.MONTH, (buf[pos + 2] - '0') * 10 + buf[pos + 3] - '0' - 1);
			cal.set(Calendar.DATE, (buf[pos + 4] - '0') * 10 + buf[pos + 5] - '0');
			cal.set(Calendar.HOUR_OF_DAY, (buf[pos + 6] - '0') * 10 + buf[pos + 7] - '0');
			cal.set(Calendar.MINUTE, (buf[pos + 8] - '0') * 10 + buf[pos + 9] - '0');
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return new IsoValue<Date>(this, cal.getTime(), null);
		} else if (this == IsoType.DATE12) {
			if (pos + 12 > buf.length) {
				throw new ParseException(String.format("Insufficient data for DATE12 field of length %d, pos %d",
						Integer.valueOf(12), Integer.valueOf(pos)), pos);
			}
			//A SimpleDateFormat in the case of dates won't help because of the missing data
			//we have to use the current date for reference and change what comes in the buffer
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			//Set the month in the date
			cal.set(Calendar.YEAR, this.adjustYear((buf[pos] - '0') * 10 + buf[pos + 1] - '0'));
			cal.set(Calendar.MONTH, (buf[pos + 2] - '0') * 10 + buf[pos + 3] - '0' - 1);
			cal.set(Calendar.DATE, (buf[pos + 4] - '0') * 10 + buf[pos + 5] - '0');
			cal.set(Calendar.HOUR_OF_DAY, (buf[pos + 6] - '0') * 10 + buf[pos + 7] - '0');
			cal.set(Calendar.MINUTE, (buf[pos + 8] - '0') * 10 + buf[pos + 9] - '0');
			cal.set(Calendar.SECOND, (buf[pos + 10] - '0') * 10 + buf[pos + 11] - '0');
			cal.set(Calendar.MILLISECOND, 0);
			return new IsoValue<Date>(this, cal.getTime(), null);
		} else if (this == IsoType.DATE6) {
			if (pos + 6 > buf.length) {
				throw new ParseException(String.format("Insufficient data for DATE6 field of length %d, pos %d",
						Integer.valueOf(6), Integer.valueOf(pos)), pos);
			}
			//A SimpleDateFormat in the case of dates won't help because of the missing data
			//we have to use the current date for reference and change what comes in the buffer
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			//Set the month in the date
			cal.set(Calendar.YEAR, this.adjustYear((buf[pos] - '0') * 10 + buf[pos + 1] - '0'));
			cal.set(Calendar.MONTH, (buf[pos + 2] - '0') * 10 + buf[pos + 3] - '0' - 1);
			cal.set(Calendar.DATE, (buf[pos + 4] - '0') * 10 + buf[pos + 5] - '0');
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			return new IsoValue<Date>(this, cal.getTime(), null);
		} else if (this == IsoType.DATE_EXP) {
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.YEAR, this.adjustYear((buf[pos] - '0') * 10 + buf[pos + 1] - '0'));
			cal.set(Calendar.MONTH, ((buf[pos + 2] - '0') * 10) + buf[pos + 3] - '0' - 1);
			cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
			return new IsoValue<Date>(this, cal.getTime(), null);
		} else if (this == IsoType.DATE_EXP_MMYY) {
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			cal.set(Calendar.HOUR, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.YEAR, this.adjustYear((buf[pos + 2] - '0') * 10 + buf[pos + 3] - '0'));
			cal.set(Calendar.MONTH, ((buf[pos] - '0') * 10) + buf[pos + 1] - '0' - 1);
			cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
			return new IsoValue<Date>(this, cal.getTime(), null);
		} else if (this == IsoType.TIME) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, ((buf[pos] - '0') * 10) + buf[pos + 1] - '0');
			cal.set(Calendar.MINUTE, ((buf[pos + 2] - '0') * 10) + buf[pos + 3] - '0');
			cal.set(Calendar.SECOND, ((buf[pos + 4] - '0') * 10) + buf[pos + 5] - '0');
			cal.set(Calendar.MILLISECOND, 0);
			return new IsoValue<Date>(this, cal.getTime(), null);
		}
		return null;
	}

	/** Parses binary data from the buffer, creating and returning an IsoValue of the configured
	 * type and length. */
	public <T extends Object> IsoValue<?> parseBinary(byte[] buf, int pos, CustomField<T> custom, int len) throws ParseException {
		if (this == IsoType.ALPHA) {
			String val = null;
			try {
				val = new String(buf, pos, len, "ISO-8859-1");
			} catch (Exception e) {}
			if (custom == null) {
				return new IsoValue<String>(this, val, len, null);
			}
			IsoValue<T> v = new IsoValue<T>(this, custom.decodeField(val), len, custom);
			if (v.getValue() == null) {
				return new IsoValue<String>(this, val, len, null);
			}
			return v;
		} else if (this == IsoType.NUMERIC) {
			//A long covers up to 18 digits
			if (len < 19) {
				long l = 0;
				long power = 1L;
				for (int i = pos + (len / 2) + (len % 2) - 1; i >= pos; i--) {
					l += (buf[i] & 0x0f) * power;
					power *= 10L;
					l += ((buf[i] & 0xf0) >> 4) * power;
					power *= 10L;
				}
				return new IsoValue<Number>(IsoType.NUMERIC, Long.valueOf(l), len, null);
			}
			//Use a BigInteger
			char[] digits = new char[len];
			int start = 0;
			for (int i = pos; i < pos + (len / 2) + (len % 2); i++) {
				digits[start++] = (char)(((buf[i] & 0xf0) >> 4) + '0');
				digits[start++] = (char)((buf[i] & 0x0f) + '0');
			}
			return new IsoValue<Number>(IsoType.NUMERIC, new BigInteger(new String(digits)), len, null);
		} else if (this == IsoType.LLVAR || this == IsoType.LLLVAR || this == IsoType.LLLLVAR) {
			int digits = this == IsoType.LLLLVAR || this == IsoType.LLLVAR ? 2 : 1;
			int ll = 0;
			for (int i = pos + digits - 1, pot = 1; i >= pos; i--, pot *= 100) {
				ll += (((buf[i] & 0xf0) >> 4) *  pot * 10) + ((buf[i] & 0x0f) * pot);
			}
			if (ll < 0) {
				throw new ParseException(String.format("Invalid %s length %d pos %d", this.toString(), Integer.valueOf(ll), Integer.valueOf(pos)), pos);
			}
			if (ll + pos + digits > buf.length) {
				throw new ParseException(String.format("Insufficient data for %s field, pos %d", this.toString(), Integer.valueOf(pos)), pos);
			}
			String val = null;
			try {
				val = ll > 0 ? new String(buf, pos + digits, ll, "ISO-8859-1") : "";
			} catch (Exception e) {}
			if (custom == null) {
				return new IsoValue<String>(this, val, ll, null);
			}
			IsoValue<T> v = new IsoValue<T>(this, custom.decodeField(val), ll, custom);
			if (v.getValue() == null) {
				return new IsoValue<String>(this, val, ll, null);
			}
			return v;
		} else if (this == IsoType.AMOUNT) {
			char[] digits = new char[13];
			digits[10] = '.';
			int start = 0;
			for (int i = pos; i < pos + 6; i++) {
				digits[start++] = (char)(((buf[i] & 0xf0) >> 4) + '0');
				digits[start++] = (char)((buf[i] & 0x0f) + '0');
				if (start == 10) {
					start++;
				}
			}
			return new IsoValue<BigDecimal>(IsoType.AMOUNT, new BigDecimal(new String(digits)), null);
		} else if (this == IsoType.RATE) {
			int ll = IsoType.RATE.getLength();
			char[] digits = new char[ll];
			int start = 0;
			for (int i = pos; i < pos + (ll / 2); i++) {
				digits[start++] = (char)(((buf[i] & 0xf0) >> 4) + '0');
				digits[start++] = (char)((buf[i] & 0x0f) + '0');
			}
			int scale = (buf[pos] & 0xf0) >> 4;
			return new IsoValue<BigDecimal>(IsoType.RATE, BigDecimal.valueOf(Long.parseLong(new String(digits, 1, digits.length - 1), 10), scale), null);
		} else if (this == IsoType.DATE10 || this == IsoType.DATE4 || this == IsoType.DATE_EXP
				|| this == IsoType.DATE10YY || this == IsoType.DATE12 || this == IsoType.DATE6
				|| this == IsoType.DATE_EXP_MMYY || this == IsoType.TIME) {
			int[] tens = new int[(this.getLength() / 2) + (this.getLength() % 2)];
			int start = 0;
			for (int i = pos; i < pos + tens.length; i++) {
				tens[start++] = (((buf[i] & 0xf0) >> 4) * 10) + (buf[i] & 0x0f);
			}
			Calendar cal = Calendar.getInstance();
			// To avoid lenient problems, must reset the calendar
			cal.set(Calendar.DATE, 1);
			if (this == IsoType.DATE10) {
				//A SimpleDateFormat in the case of dates won't help because of the missing data
				//we have to use the current date for reference and change what comes in the buffer
				//Set the month in the date
				cal.set(Calendar.MONTH, tens[0] - 1);
				cal.set(Calendar.DATE, tens[1]);
				cal.set(Calendar.HOUR_OF_DAY, tens[2]);
				cal.set(Calendar.MINUTE, tens[3]);
				cal.set(Calendar.SECOND, tens[4]);
				cal.set(Calendar.MILLISECOND, 0);
				this.adjustYear(cal);
				return new IsoValue<Date>(this, cal.getTime(), null);
			} else if (this == IsoType.DATE4) {
				cal.set(Calendar.HOUR, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				//Set the month in the date
				cal.set(Calendar.MONTH, tens[0] - 1);
				cal.set(Calendar.DATE, tens[1]);
				this.adjustYear(cal);
				return new IsoValue<Date>(this, cal.getTime(), null);
			} else if (this == IsoType.DATE10YY) {
				//A SimpleDateFormat in the case of dates won't help because of the missing data
				//we have to use the current date for reference and change what comes in the buffer
				//Set the month in the date
				cal.set(Calendar.YEAR, this.adjustYear(tens[0]));
				cal.set(Calendar.MONTH, tens[1] - 1);
				cal.set(Calendar.DATE, tens[2]);
				cal.set(Calendar.HOUR_OF_DAY, tens[3]);
				cal.set(Calendar.MINUTE, tens[4]);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				return new IsoValue<Date>(this, cal.getTime(), null);
			} else if (this == IsoType.DATE12) {
				//A SimpleDateFormat in the case of dates won't help because of the missing data
				//we have to use the current date for reference and change what comes in the buffer
				//Set the month in the date
				cal.set(Calendar.YEAR, this.adjustYear(tens[0]));
				cal.set(Calendar.MONTH, tens[1] - 1);
				cal.set(Calendar.DATE, tens[2]);
				cal.set(Calendar.HOUR_OF_DAY, tens[3]);
				cal.set(Calendar.MINUTE, tens[4]);
				cal.set(Calendar.SECOND, tens[5]);
				cal.set(Calendar.MILLISECOND, 0);
				return new IsoValue<Date>(this, cal.getTime(), null);
			} else if (this == IsoType.DATE6) {
				//A SimpleDateFormat in the case of dates won't help because of the missing data
				//we have to use the current date for reference and change what comes in the buffer
				//Set the month in the date
				cal.set(Calendar.YEAR, this.adjustYear(tens[0]));
				cal.set(Calendar.MONTH, tens[1] - 1);
				cal.set(Calendar.DATE, tens[2]);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				return new IsoValue<Date>(this, cal.getTime(), null);
			} else if (this == IsoType.DATE_EXP) {
				cal.set(Calendar.HOUR, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.YEAR, this.adjustYear(tens[0]));
				cal.set(Calendar.MONTH, tens[1] - 1);
				cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
				return new IsoValue<Date>(this, cal.getTime(), null);
			} else if (this == IsoType.DATE_EXP_MMYY) {
				cal.set(Calendar.HOUR, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.YEAR, this.adjustYear(tens[1]));
				cal.set(Calendar.MONTH, tens[0] - 1);
				cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
				return new IsoValue<Date>(this, cal.getTime(), null);
			} else if (this == IsoType.TIME) {
				cal.set(Calendar.HOUR_OF_DAY, tens[0]);
				cal.set(Calendar.MINUTE, tens[1]);
				cal.set(Calendar.SECOND, tens[2]);
				return new IsoValue<Date>(this, cal.getTime(), null);
			}
			return new IsoValue<Date>(this, cal.getTime(), null);
		}
		return null;
	}

	private void adjustYear(Calendar c) {
		// ajusta o ano, pois esta informacao nao vem no pacote
		// algoritmo original considera datas no futuro como do ano passado, o que esta correto:
		// se hoje 08/janeiro e recebe um desfazimento de 19/novembro, com certeza eh do ano passado
		// mas foi solicitada uma tolerancia, pois os relogios podem nao estar totalmente sincronizados
		// e por isso nem sempre datas no passado sao mesmo ano (ver virada do ano)

		// data na futuro sera considerada no ano seguinte quando:
		// data sistema = 31/12
		// data recebida = 01/01 ou 02/01
		Calendar now = Calendar.getInstance();
		if (now.get(Calendar.DAY_OF_MONTH) == 31 && now.get(Calendar.MONTH) == Calendar.DECEMBER) {
			if (c.get(Calendar.MONTH) == Calendar.JANUARY && (c.get(Calendar.DAY_OF_MONTH) == 1 || c.get(Calendar.DAY_OF_MONTH) == 2)) {
				c.add(Calendar.YEAR, 1);
			}
			// como estamos no ultimo dia do ano, nao devemos considerar nenhuma data como do ano passado
		} else {
			// damos 2 dias de tolerancia - ate 2 dias no futuro eh mesmo ano, senao eh ano passado
			if (c.getTimeInMillis() > now.getTimeInMillis() + 2 * 86400000) {
				c.add(Calendar.YEAR, -1);
			}
		}
	}

	private int adjustYear(int yy) {
		if (yy >= 100) return yy;
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -70);
		int year = c.get(Calendar.YEAR);
		// de 70 anos atras ate 30 anos pra frente
		return yy + (year / 100) * 100 + (yy < year % 100 ? 100 : 0);
	}

}
