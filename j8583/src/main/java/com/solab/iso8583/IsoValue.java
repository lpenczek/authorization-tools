/*
j8583 A Java implementation of the ISO8583 protocol
Copyright (C) 2007 Enrique Zamudio Lopez

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
*/
package com.solab.iso8583;

import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.Date;

/** Represents a value that is stored in a field inside an ISO8583 message.
 * It can format the value when the message is generated.
 * Some values have a fixed length, other values require a length to be specified
 * so that the value can be padded to the specified length. LLVAR, LLLVAR and LLLLVAR
 * values do not need a length specification because the length is calculated
 * from the stored value.
 *
 * @author Enrique Zamudio
 */
public class IsoValue<T> implements Cloneable {

	private final IsoType type;
	private final T value;
	private final CustomField<T> encoder;
	private final int length;

	public IsoValue(IsoType t, T value) {
		this(t, value, null);
	}

	/** Creates a new instance that stores the specified value as the specified type.
	 * Useful for storing LLVAR, LLLVAR or LLLLVAR types, as well as fixed-length value types
	 * like DATE10, DATE4, AMOUNT, etc.
	 * @param t the ISO type.
	 * @param value The value to be stored.
	 * @param custom An optional CustomField to encode/decode a custom value.
	 */
	public IsoValue(IsoType t, T value, CustomField<T> custom) {
		if (t.needsLength()) {
			throw new IllegalArgumentException("Fixed-value types must use constructor that specifies length");
		}
		this.encoder = custom;
		this.type = t;
		this.value = value;
		if (this.type == IsoType.LLVAR || this.type == IsoType.LLLVAR || this.type == IsoType.LLLLVAR) {
			if (custom == null) {
				this.length = value.toString().length();
			} else {
				String enc = custom.encodeField(value);
				if (enc == null) {
					enc = value == null ? "" : value.toString();
				}
				this.length = enc.length();
			}
			if (t == IsoType.LLVAR && this.length > 99) {
				throw new IllegalArgumentException("LLVAR can only hold values up to 99 chars");
			} else if (t == IsoType.LLLVAR && this.length > 999) {
				throw new IllegalArgumentException("LLLVAR can only hold values up to 999 chars");
			} else if (t == IsoType.LLLLVAR && this.length > 9999) {
				throw new IllegalArgumentException("LLLLVAR can only hold values up to 9999 chars");
			}
		} else {
			this.length = this.type.getLength();
		}
	}

	public IsoValue(IsoType t, T val, int len) {
		this(t, val, len, null);
	}

	/** Creates a new instance that stores the specified value as the specified type.
	 * Useful for storing fixed-length value types.
	 * @param t The ISO8583 type for this field.
	 * @param val The value to store in the field.
	 * @param len The length for the value.
	 * @param custom An optional CustomField to encode/decode a custom value.
	 */
	public IsoValue(IsoType t, T val, int len, CustomField<T> custom) {
		this.type = t;
		this.value = val;
		this.encoder = custom;
		if (len == 0 && t.needsLength()) {
			throw new IllegalArgumentException(String.format("Length must be greater than zero for type %s (value '%s')", t, val));
		} else if (t == IsoType.LLVAR || t == IsoType.LLLVAR || t == IsoType.LLLLVAR) {
			this.length = len > 0 ? len : custom == null ? val.toString().length() : custom.encodeField(this.value).length();
			if (t == IsoType.LLVAR && this.length > 99) {
				throw new IllegalArgumentException("LLVAR can only hold values up to 99 chars");
			} else if (t == IsoType.LLLVAR && this.length > 999) {
				throw new IllegalArgumentException("LLLVAR can only hold values up to 999 chars");
			} else if (t == IsoType.LLLLVAR && this.length > 9999) {
				throw new IllegalArgumentException("LLLLVAR can only hold values up to 9999 chars");
			}
		} else {
			this.length = len;
		}
	}

	/** Returns the ISO type to which the value must be formatted. */
	public IsoType getType() {
		return this.type;
	}

	/** Returns the length of the stored value, of the length of the formatted value
	 * in case of NUMERIC or ALPHA. It doesn't include the field length header in case
	 * of LLVAR, LLLVAR or LLLLVAR. */
	public int getLength() {
		return this.length;
	}

	/** Returns the stored value without any conversion or formatting. */
	public T getValue() {
		return this.value;
	}

	/** Returns the formatted value as a String. The formatting depends on the type of the
	 * receiver. */
	@Override
	public String toString() {
		if (this.value == null) {
			return "ISOValue<null>";
		}
		if (this.type == IsoType.NUMERIC || this.type == IsoType.AMOUNT || this.type == IsoType.RATE) {
			if (this.type == IsoType.AMOUNT || this.type == IsoType.RATE) {
				if (this.value instanceof BigDecimal) {
					return this.type.format((BigDecimal)this.value, this.type.getLength());
				}
				if (this.value instanceof Number) {
					return this.type.format(new BigDecimal(((Number) this.value).doubleValue()), this.type.getLength());
				}
				return this.type.format(this.value.toString(), this.type.getLength());
			} else if (this.value instanceof Number) {
				return this.type.format(((Number)this.value).longValue(), this.length);
			} else {
				return this.type.format(this.encoder == null ? this.value.toString() : this.encoder.encodeField(this.value), this.length);
			}
		} else if (this.type == IsoType.ALPHA) {
			return this.type.format(this.encoder == null ? this.value.toString() : this.encoder.encodeField(this.value), this.length);
		} else if (this.value instanceof Date) {
			return this.type.format((Date)this.value);
		}
		return this.encoder == null ? this.value.toString() : this.encoder.encodeField(this.value);
	}

	/** Returns a copy of the receiver that references the same value object. */
	@Override
	@SuppressWarnings("unchecked")
	public IsoValue<T> clone() {
		try {
			return (IsoValue<T>)super.clone();
		} catch (CloneNotSupportedException ex) {
			return null;
		}
	}

	/** Returns true of the other object is also an IsoValue and has the same type and length,
	 * and if other.getValue().equals(getValue()) returns true. */
	@Override
	public boolean equals(Object other) {
		if (other == null || !(other instanceof IsoValue<?>)) {
			return false;
		}
		IsoValue<?> comp = (IsoValue<?>)other;
		return (comp.getType() == this.getType() && comp.getValue().equals(this.getValue()) && comp.getLength() == this.getLength());
	}

	@Override
	public int hashCode() {
		return this.value == null ? 0 : this.toString().hashCode();
	}

	/** Returns the CustomField encoder for this value. */
	public CustomField<T> getEncoder() {
		return this.encoder;
	}

	/** Writes the formatted value to a stream, with the length header
	 * if it's a variable length type. */
	public void write(OutputStream outs, boolean binary) throws IOException {
		if (this.type == IsoType.LLVAR || this.type == IsoType.LLLVAR || this.type == IsoType.LLLLVAR) {
			if (binary) {
				int rem;
				if (this.type == IsoType.LLLLVAR) {
					rem = this.length % 1000;
					outs.write(((this.length / 1000) << 4) | (rem / 100));
					rem = rem % 100;
				} else if (this.type == IsoType.LLLVAR) {
					outs.write(this.length / 100); //00 to 09 automatically in BCD
					rem = this.length % 100;
				} else {
					rem = this.length;
				}
				//BCD encode the rest of the length
				outs.write(((rem / 10) << 4) | (rem % 10));
			} else {
				//write the length in ASCII
				int rem = this.length;
				if (this.type == IsoType.LLLLVAR) {
					outs.write((rem / 1000) + '0');
					rem = rem % 1000;
				}
				if (this.type == IsoType.LLLLVAR || this.type == IsoType.LLLVAR) {
					outs.write((rem / 100) + '0');
					rem = rem % 100;
				}
				outs.write((rem / 10) + '0');
				outs.write((rem % 10) + '0');
			}
		} else if (binary) {
			//numeric types in binary are coded like this
			byte[] buf = null;
			if (this.type == IsoType.NUMERIC) {
				buf = new byte[(this.length / 2) + (this.length % 2)];
			} else if (this.type == IsoType.AMOUNT || this.type == IsoType.RATE) {
				buf = new byte[this.type.getLength() / 2];
			} else if (
					this.type == IsoType.DATE10 || this.type == IsoType.DATE4
					|| this.type == IsoType.DATE10YY || this.type == IsoType.DATE12 || this.type == IsoType.DATE6
					|| this.type == IsoType.DATE_EXP || this.type == IsoType.DATE_EXP_MMYY || this.type == IsoType.TIME
			) {
				buf = new byte[this.length / 2];
			}
			//Encode in BCD if it's one of these types
			if (buf != null) {
				this.toBcd(this.toString(), buf);
				outs.write(buf);
				return;
			}
		}
		//Just write the value as text
		outs.write(this.toString().getBytes("ISO-8859-1"));
	}

	/** Encode the value as BCD and put it in the buffer. The buffer must be big enough
	 * to store the digits in the original value (half the length of the string). */
	private void toBcd(String val, byte[] buf) {
		int charpos = 0; //char where we start
		int bufpos = 0;
		if (val.length() % 2 == 1) {
			//for odd lengths we encode just the first digit in the first byte
			buf[0] = (byte)(val.charAt(0) - '0');
			charpos = 1;
			bufpos = 1;
		}
		//encode the rest of the string
		while (charpos < val.length()) {
			buf[bufpos] = (byte)(((val.charAt(charpos) - '0') << 4)
					| (val.charAt(charpos + 1) - '0'));
			charpos += 2;
			bufpos++;
		}
	}

}
