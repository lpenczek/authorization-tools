package com.penczek.base64;

import java.io.UnsupportedEncodingException;
import java.util.Base64;
import javax.swing.JOptionPane;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
		dtd = "-//com.penczek.base64//Base64Viewer//EN",
		autostore = false)
@TopComponent.Description(
		preferredID = "Base64ViewerTopComponent",
		iconBase="com/penczek/base64/base64_16x16.gif",
		persistenceType = TopComponent.PERSISTENCE_ALWAYS)
@TopComponent.Registration(mode = "editor", openAtStartup = true)
@ActionID(category = "Window", id = "com.penczek.base64.Base64ViewerTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
		displayName = "#CTL_Base64ViewerAction",
		preferredID = "Base64ViewerTopComponent")
@Messages({
	"CTL_Base64ViewerAction=Base64 Viewer",
	"CTL_Base64ViewerTopComponent=Base64 Viewer",
	"HINT_Base64ViewerTopComponent=Base64 Viewer"
})
public final class Base64TopComponent extends TopComponent {
	
	public Base64TopComponent() {
		initComponents();
		setName(Bundle.CTL_Base64ViewerTopComponent());
		setToolTipText(Bundle.HINT_Base64ViewerTopComponent());
		this.encTextArea.grabFocus();
	}

	private byte[] decoded;
	
	/**
	 * This method is called from within the constructor to initialize the form. WARNING: Do NOT modify this code. The content of this
	 * method is always regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        type = new javax.swing.ButtonGroup();
        labelEncoded = new javax.swing.JLabel();
        encButton = new javax.swing.JButton();
        decScrollPane = new javax.swing.JScrollPane();
        decTextArea = new javax.swing.JTextArea();
        encScrollPane = new javax.swing.JScrollPane();
        encTextArea = new javax.swing.JTextArea();
        labelDecoded = new javax.swing.JLabel();
        decButton = new javax.swing.JButton();
        hexaCheckBox = new javax.swing.JCheckBox();
        typeNormal = new javax.swing.JRadioButton();
        typeMime = new javax.swing.JRadioButton();
        typeURL = new javax.swing.JRadioButton();

        org.openide.awt.Mnemonics.setLocalizedText(labelEncoded, "Encoded");

        org.openide.awt.Mnemonics.setLocalizedText(encButton, "Encode");
        encButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                encButtonActionPerformed(evt);
            }
        });

        decTextArea.setColumns(20);
        decTextArea.setRows(5);
        decScrollPane.setViewportView(decTextArea);

        encTextArea.setColumns(20);
        encTextArea.setRows(5);
        encScrollPane.setViewportView(encTextArea);

        org.openide.awt.Mnemonics.setLocalizedText(labelDecoded, "Decoded");

        org.openide.awt.Mnemonics.setLocalizedText(decButton, "Decode");
        decButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                decButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(hexaCheckBox, "16");
        hexaCheckBox.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        hexaCheckBox.setMargin(new java.awt.Insets(0, 0, 0, 0));

        type.add(typeNormal);
        org.openide.awt.Mnemonics.setLocalizedText(typeNormal, org.openide.util.NbBundle.getMessage(Base64TopComponent.class, "Base64TopComponent.typeNormal.text")); // NOI18N

        type.add(typeMime);
        org.openide.awt.Mnemonics.setLocalizedText(typeMime, org.openide.util.NbBundle.getMessage(Base64TopComponent.class, "Base64TopComponent.typeMime.text")); // NOI18N

        type.add(typeURL);
        org.openide.awt.Mnemonics.setLocalizedText(typeURL, org.openide.util.NbBundle.getMessage(Base64TopComponent.class, "Base64TopComponent.typeURL.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(encScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE)
                            .addComponent(decScrollPane, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 702, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(encButton))
                            .addComponent(typeNormal)
                            .addComponent(typeMime)
                            .addComponent(decButton)
                            .addComponent(typeURL)
                            .addComponent(hexaCheckBox))
                        .addGap(6, 6, 6))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelDecoded)
                            .addComponent(labelEncoded))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelEncoded)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(typeNormal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(typeMime)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(typeURL)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(decButton))
                    .addComponent(encScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelDecoded)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(hexaCheckBox)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(encButton))
                    .addComponent(decScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

	private char[] HEXA = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	private void decButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decButtonActionPerformed
		Base64.Decoder decoder;
		if (this.typeMime.isSelected()) {
			decoder = Base64.getMimeDecoder();
		} else if (this.typeURL.isSelected()) {
			decoder = Base64.getUrlDecoder();
		} else {
			decoder = Base64.getDecoder();
		}
		this.decoded = decoder.decode(this.encTextArea.getText());
		if (this.hexaCheckBox.isSelected()) {
			StringBuilder s = new StringBuilder(this.decoded.length << 1);
			for (int i = 0; i < this.decoded.length; i++) {
				int q1 = (0x000000FF & this.decoded[i]) / 16;
				int r1 = (0x000000FF & this.decoded[i]) % 16;
				int r2 = q1 % 16;
				if (i > 0) s.append(' ');
				s.append(HEXA[r2]).append(HEXA[r1]);
			}
			this.decTextArea.setText(s.toString());
		} else {
			this.decTextArea.setText(new String(this.decoded));
		}
	}//GEN-LAST:event_decButtonActionPerformed
		
	private void encButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_encButtonActionPerformed
		if (this.hexaCheckBox.isSelected()) {
			String s = this.decTextArea.getText().replaceAll("(\\s+|\\r+|\\n+)", "");
			this.decoded = new byte[s.length() >> 1];
			for (int i = 0; i < s.length(); i += 2) {
				if (i + 1 < s.length()) {
					this.decoded[i >> 1] = (byte) Integer.parseInt(s.substring(i, i + 2), 16);
				}
			}
		} else {
			try {
				this.decoded = this.decTextArea.getText().getBytes("ISO-8859-1");
			} catch (UnsupportedEncodingException ex) {
				JOptionPane.showMessageDialog(this, ex.getMessage(), "Error encoding", JOptionPane.ERROR_MESSAGE);
			}
		}
		Base64.Encoder encoder;
		if (this.typeMime.isSelected()) {
			encoder = Base64.getMimeEncoder();
		} else if (this.typeURL.isSelected()) {
			encoder = Base64.getUrlEncoder();
		} else {
			encoder = Base64.getEncoder();
		}
		this.encTextArea.setText(encoder.encodeToString(this.decoded));
	}//GEN-LAST:event_encButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton decButton;
    private javax.swing.JScrollPane decScrollPane;
    private javax.swing.JTextArea decTextArea;
    private javax.swing.JButton encButton;
    private javax.swing.JScrollPane encScrollPane;
    private javax.swing.JTextArea encTextArea;
    private javax.swing.JCheckBox hexaCheckBox;
    private javax.swing.JLabel labelDecoded;
    private javax.swing.JLabel labelEncoded;
    private javax.swing.ButtonGroup type;
    private javax.swing.JRadioButton typeMime;
    private javax.swing.JRadioButton typeNormal;
    private javax.swing.JRadioButton typeURL;
    // End of variables declaration//GEN-END:variables
	@Override
	public void componentOpened() {
		// TODO add custom code on component opening
		this.encTextArea.grabFocus();
	}

	@Override
	public void componentClosed() {
		// TODO add custom code on component closing
		this.encTextArea.setText(null);
		this.decTextArea.setText(null);
	}

	void writeProperties(java.util.Properties p) {
		// better to version settings since initial version as advocated at
		// http://wiki.apidesign.org/wiki/PropertyFiles
		p.setProperty("version", "1.0");
		// TODO store your settings
	}

	void readProperties(java.util.Properties p) {
		String version = p.getProperty("version");
		// TODO read your settings according to their version
	}
}
