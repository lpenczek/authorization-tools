package com.penczek.dilbert;

public interface ImageLoaderListener {
	void setImage(Image img);
	void setTextLabel(String s);
}
