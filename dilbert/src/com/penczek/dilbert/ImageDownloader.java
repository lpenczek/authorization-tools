package com.penczek.dilbert;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;

public class ImageDownloader {
        // <meta property="og:image" content="

	//<img src="https://assets.amuniversal.com/1c32b820b39401365f17005056a9545d">
	private static final Pattern p2 = Pattern.compile("<img.+?src\\=\"(?:https:)?(//assets.amuniversal.com/.*?)\"");
	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private final Date date;

	public ImageDownloader(Date date, Proxy proxy) {
		this.date = date;
	}

	public Image getImage() {
		String imageURL = getImageURL();
		Image img = null;
		if (imageURL != null) {
			InputStream is = null;
			try {
				URL url = new URL(imageURL);
				URLConnection conn = url.openConnection();
				conn.connect();
				is = conn.getInputStream();
				img = new Image();
				img.mimeType = conn.getContentType();
				img.image = ImageIO.read(is);
			} catch (Exception ex) {
				Logger.getLogger("global").log(Level.WARNING, null, ex);
			} finally {
				try { if (is != null) is.close(); } catch (Exception e) {}
			}
		}
		return img;
	}

	public String getImageURL() {
		InputStream is = null;
		BufferedReader in = null;
		String baseURL = "https://dilbert.com";
		try {
			// https://dilbert.com/strip/2018-11-16
			URL url = new URL(baseURL + "/strip/" + formatter.format(this.date));
			URLConnection conn = url.openConnection();
			conn.connect();
			is = conn.getInputStream();
			in = new BufferedReader(new InputStreamReader(is));
			String inputLine = null;
			while ((inputLine = in.readLine()) != null) {
				Matcher m;
				m = p2.matcher(inputLine);
				if (m.find()) {
					return "https:" + m.group(1);
				}
			}
		} catch (Exception ex) {
			Logger.getLogger("global").log(Level.WARNING, null, ex);
		} finally {
			try { if (is != null) is.close(); } catch (Exception e) {}
			try { if (in != null) in.close(); } catch (Exception e) {}
		}
		return null;
	}

}
