package com.penczek.dilbert;

import java.awt.image.BufferedImage;

/**
 *
 * @author lpenczek
 */
public class Image {
	public String mimeType;
	public BufferedImage image;
}
