package com.penczek.dilbert;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.PreferenceChangeEvent;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import javax.imageio.ImageIO;

public class ImageLoaderThread implements Runnable {

	private static final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat format = new SimpleDateFormat(java.util.ResourceBundle.getBundle("com/penczek/dilbert/Bundle").getString("dateMask"));
	private final ImageLoaderListener listener;
	private final Calendar c;
	private Thread t;
	private boolean saveLocally;
	private String imagePath;

	ImageLoaderThread(ImageLoaderListener listener) {
		this.listener = listener;
		this.c = Calendar.getInstance();
		this.c.set(Calendar.HOUR_OF_DAY, 0);
		this.c.set(Calendar.MINUTE, 0);
		this.c.set(Calendar.SECOND, 0);
		this.c.set(Calendar.MILLISECOND, 0);
		Preferences prefs = Preferences.userNodeForPackage(ViewerPanel.class);
		this.saveLocally = prefs.getBoolean("saveLocally", false);
		this.imagePath = prefs.get("localPath", "");
		prefs.addPreferenceChangeListener(new PreferenceChangeListener() {
			public void preferenceChange(PreferenceChangeEvent evt) {
				String key = evt.getKey();
				if (key.equals("saveLocally")) {
					saveLocally = Boolean.parseBoolean(evt.getNewValue());
				} else if (key.equals("localPath")) {
					imagePath = evt.getNewValue();
				}
			}
		});

	}

	public void run() {
		t = Thread.currentThread();
		Image img = null;
		if (this.imagePath != null && !this.imagePath.isEmpty()) {
			// tenta pegar do diretorio local
			String now = formatter.format(c.getTime());
			File f = new File(this.imagePath, now + ".gif");
			InputStream is = null;
			try {
				if (f.exists() && f.canRead()) {
					is = new FileInputStream(f);
					img = new Image();
					img.image = ImageIO.read(is);
					img.mimeType = "image/gif";
				} else {
					f = new File(this.imagePath, now + ".jpg");
					if (f.exists() && f.canRead()) {
						is = new FileInputStream(f);
						img = new Image();
						img.image = ImageIO.read(is);
						img.mimeType = "image/jpeg";
					}
				}
			} catch (Exception ex) {
				Logger.getLogger("global").log(Level.WARNING, null, ex);
				img = null;
			} finally {
				try { if (is != null) is.close(); } catch (Exception e) {}
			}
			if (img == null) {
				// senao, download e salva no dir local
				img = this.download();
				if (img != null && this.saveLocally) {
					try {
						ImageIO.write(img.image, img.mimeType.contains("gif") ? "gif" : "jpg", f);
					} catch (Exception e) {
						Logger.getLogger("global").log(Level.WARNING, null, e);
					}
				}
			}
		} else {
			// download apenas
			img = this.download();
		}
		if (t != Thread.currentThread()) return;
		if (img != null) {
			listener.setImage(img);
			listener.setTextLabel(new StringBuilder().append(java.util.ResourceBundle.getBundle("com/penczek/dilbert/Bundle").getString("_Dilbert_strip_from_")).append(format.format(c.getTime())).toString());
		} else {
			listener.setTextLabel(java.util.ResourceBundle.getBundle("com/penczek/dilbert/Bundle").getString("_Couldn't_load_Dilbert_strip,_sorry."));
		}
	}

	private Image download() {
		// load proxy settings
		return new ImageDownloader(c.getTime(), null).getImage();
	}

	public void nextDay() {
		this.stop();
		c.add(Calendar.DAY_OF_MONTH, 1);
	}

	public void previousDay() {
		this.stop();
		c.add(Calendar.DAY_OF_MONTH, -1);
	}

	public boolean haveNextDay() {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		return this.c.before(today);
	}

	void setDate(Date date) {
		this.stop();
		c.setTime(date);
	}

	private void stop() {
		t = null;
	}

}
