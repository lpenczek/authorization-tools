package com.penczek.hsmutils;

import com.penczek.hsm.server.HSMServer;
import com.penczek.hsm.server.InvalidLMKException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
		dtd = "-//com.penczek.hsmutils//HSM//EN",
		autostore = false
)
@TopComponent.Description(
		preferredID = "HSMTopComponent",
		iconBase = "com/penczek/hsmutils/hsm.png",
		persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "editor", openAtStartup = true)
@ActionID(category = "Window", id = "com.penczek.hsmutils.HSMTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
		displayName = "#CTL_HSMAction",
		preferredID = "HSMTopComponent"
)
@Messages({
	"CTL_HSMAction=HSM server",
	"CTL_HSMTopComponent=HSM server",
	"HINT_HSMTopComponent=HSM server, does exactly what Thales do"
})
public final class HSMTopComponent extends TopComponent {

	private boolean started;
	private HSMServer server;
	private Document doc;

	public HSMTopComponent() {
		initComponents();
		setName(Bundle.CTL_HSMTopComponent());
		setToolTipText(Bundle.HINT_HSMTopComponent());
		this.doc = this.output.getDocument();
		try {
			this.server = new HSMServer(1500, new PrintStream(new OutputStream() {
				@Override
				public void write(byte b[], int off, int len) throws IOException {
					if (b == null) throw new NullPointerException();
					if (off < 0 || off > b.length || len < 0 || (off + len) > b.length || (off + len) < 0) {
						throw new IndexOutOfBoundsException();
					}
					if (len == 0) return;
					synchronized (doc) {
						try {
							doc.insertString(doc.getLength(), new String(b, off, len, StandardCharsets.ISO_8859_1), null);
						} catch (BadLocationException e) {}
					}
					scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
				}

				@Override
				public void write(int b) throws IOException {
					synchronized (doc) {
						try {
							doc.insertString(doc.getLength(), new String(new char[] {(char) b}), null);
						} catch (BadLocationException e) {}
					}
					scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
				}
			}));
		} catch (InvalidLMKException ex) {
			Exceptions.printStackTrace(ex);
		} catch (IOException ex) {
			Exceptions.printStackTrace(ex);
		}
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        onoff = new javax.swing.JButton();
        scrollPane = new javax.swing.JScrollPane();
        output = new javax.swing.JTextPane();
        clear = new javax.swing.JButton();

        org.openide.awt.Mnemonics.setLocalizedText(onoff, org.openide.util.NbBundle.getMessage(HSMTopComponent.class, "HSMTopComponent.onoff.text")); // NOI18N
        onoff.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onoffActionPerformed(evt);
            }
        });

        scrollPane.setViewportView(output);

        org.openide.awt.Mnemonics.setLocalizedText(clear, org.openide.util.NbBundle.getMessage(HSMTopComponent.class, "HSMTopComponent.clear.text")); // NOI18N
        clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(onoff)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(clear)
                        .addGap(0, 697, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(scrollPane)
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(onoff)
                    .addComponent(clear))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 222, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void onoffActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onoffActionPerformed
        if (this.started) {
			this.server.stop();
			this.started = false;
			this.onoff.setText("Start");
		} else {
			this.output.setText(null);
			this.server.start();
			this.started = true;
			this.onoff.setText("Stop");
		}
    }//GEN-LAST:event_onoffActionPerformed

    private void clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearActionPerformed
		synchronized (doc) {
			try {
				this.doc.remove(0, this.doc.getLength());
			} catch (BadLocationException ex) {}
		}
    }//GEN-LAST:event_clearActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clear;
    private javax.swing.JButton onoff;
    private javax.swing.JTextPane output;
    private javax.swing.JScrollPane scrollPane;
    // End of variables declaration//GEN-END:variables
	@Override
	public void componentOpened() {
		// TODO add custom code on component opening
	}

	@Override
	public void componentClosed() {
		// TODO add custom code on component closing
	}

	void writeProperties(java.util.Properties p) {
		// better to version settings since initial version as advocated at
		// http://wiki.apidesign.org/wiki/PropertyFiles
		p.setProperty("version", "1.0");
		// TODO store your settings
	}

	void readProperties(java.util.Properties p) {
		String version = p.getProperty("version");
		// TODO read your settings according to their version
	}
}
