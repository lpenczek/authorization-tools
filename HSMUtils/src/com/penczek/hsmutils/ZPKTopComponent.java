package com.penczek.hsmutils;

import com.penczek.hsm.Pinblock;
import com.penczek.hsm.server.AtallaVariant;
import com.penczek.hsm.server.EncryptedKey;
import com.penczek.hsm.server.HSMCipher;
import com.penczek.hsm.server.HSMServer;
import com.penczek.hsm.server.KeyScheme;
import com.penczek.hsm.server.KeyType;
import com.penczek.hsm.server.LMK;
import com.penczek.util.HexadecimalStringUtil;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import javax.crypto.Cipher;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
		dtd = "-//com.penczek.hsmutils//ZPK//EN",
		autostore = false
)
@TopComponent.Description(
		preferredID = "ZPKTopComponent",
		iconBase = "com/penczek/hsmutils/hsm.png",
		persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "editor", openAtStartup = true)
@ActionID(category = "Window", id = "com.penczek.hsmutils.ZPKTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
		displayName = "#CTL_ZPKAction",
		preferredID = "ZPKTopComponent"
)
@Messages({
	"CTL_ZPKAction=ZPK Utils",
	"CTL_ZPKTopComponent=ZPK Utils",
	"HINT_ZPKTopComponent=ZPK Utilities"
})
public final class ZPKTopComponent extends TopComponent implements ClipboardOwner {

	private byte[] c1;
	private byte[] c2;
	private byte[] c3;
	private byte[] zpkey;
	private byte[] zmk;
	private HSMCipher cipher;
	private LMK lmk;
	private volatile boolean changingZPK = false;
	private KeyScheme zpkScheme;
	private int atallaVariant;

	public ZPKTopComponent() {
		initComponents();
		setName(Bundle.CTL_ZPKTopComponent());
		setToolTipText(Bundle.HINT_ZPKTopComponent());
		try {
			this.cipher = new HSMCipher(this.lmk = new LMK(LMK.TEST_LMK));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro iniciando aplicação: " + e, "Erro", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		DocumentListener keyChanged = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				zpkChanged((String) e.getDocument().getProperty("owner"));
			}
			public void removeUpdate(DocumentEvent e) {
				zpkChanged((String) e.getDocument().getProperty("owner"));
			}
			public void insertUpdate(DocumentEvent e) {
				zpkChanged((String) e.getDocument().getProperty("owner"));
			}
		};
		this.componente1.getDocument().addDocumentListener(keyChanged);
		this.componente1.getDocument().putProperty("owner", "componente");
		this.componente2.getDocument().addDocumentListener(keyChanged);
		this.componente2.getDocument().putProperty("owner", "componente");
		this.componente3.getDocument().addDocumentListener(keyChanged);
		this.componente3.getDocument().putProperty("owner", "componente");
		this.zpkZMK.getDocument().addDocumentListener(keyChanged);
		this.zpkZMK.getDocument().putProperty("owner", "zpkZMK");
		this.zpkLMK.getDocument().addDocumentListener(keyChanged);
		this.zpkLMK.getDocument().putProperty("owner", "zpkLMK");
		this.zpkClear.getDocument().addDocumentListener(keyChanged);
		this.zpkClear.getDocument().putProperty("owner", "zpkClear");
		this.atallaField.getDocument().addDocumentListener(keyChanged);
		this.atallaField.getDocument().putProperty("owner", "atalla");

		DocumentListener panPinChanged = new DocumentListener() {
			public void changedUpdate(DocumentEvent evt) {
				changedPANPIN();
			}
			public void removeUpdate(DocumentEvent evt) {
				changedPANPIN();
			}
			public void insertUpdate(DocumentEvent evt) {
				changedPANPIN();
			}
		};
		this.pan.getDocument().addDocumentListener(panPinChanged);
		this.pin.getDocument().addDocumentListener(panPinChanged);
		DocumentListener panPbChanged = new DocumentListener() {
			public void changedUpdate(DocumentEvent evt) {
				changedPBZPK();
			}
			public void removeUpdate(DocumentEvent evt) {
				changedPBZPK();
			}
			public void insertUpdate(DocumentEvent evt) {
				changedPBZPK();
			}
		};
		this.pinblockZPK2.getDocument().addDocumentListener(panPbChanged);
		this.pan2.getDocument().addDocumentListener(panPbChanged);
	}

	void zpkChanged(String owner) {
		if (changingZPK) return;
		changingZPK = true;
    this.zpkError.setText(null);
		if ("atalla".equals(owner)) {
			if (this.atallaCheck.isSelected()) {
				try {
					this.atallaVariant = Integer.parseInt(this.atallaField.getText(), 10);
				} catch (Exception e) {
					this.atallaVariant = 0;
				}
			} else {
				this.atallaVariant = 0;
			}
		}
		if ("zpkClear".equals(owner) || "atalla".equals(owner)) {
			String t = this.zpkClear.getText();
			if (!t.isEmpty()) {
				this.zpkey = HexadecimalStringUtil.bytesFromString(t.replaceAll("[^A-Fa-f0-9]", ""));
				int zpkl = this.zpkey.length;
				this.zpkScheme = zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.ANSIX9_17_DOUBLE : KeyScheme.ANSIX9_17_TRIPLE;
				try {
					this.checkValueZPK.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(this.zpkey)));
				} catch (Exception e) {
					this.checkValueZPK.setText(null);
					e.printStackTrace();
				}
				try {
					this.zpkLMK.setText(EncryptedKey.encrypt(
							zpkey, zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.VARIANT_DOUBLE : KeyScheme.VARIANT_TRIPLE,
							lmk.getPair(KeyType.ZPK)
					).toFormattedString());
				} catch (Exception e) {
					this.zpkLMK.setText(null);
					e.printStackTrace();
				}
				if (zmk != null) {
					try {
						this.zpkZMK.setText(EncryptedKey.encrypt(
								zpkey, zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.ANSIX9_17_DOUBLE : KeyScheme.ANSIX9_17_TRIPLE,
								AtallaVariant.variant(zmk, this.atallaVariant)
						).toFormattedString());
					} catch (Exception e) {
						this.zpkZMK.setText(null);
						e.printStackTrace();
					}
				}
			} else {
				this.zpkLMK.setText(null);
				this.zpkZMK.setText(null);
				this.checkValueZPK.setText(null);
				this.zpkey = null;
			}
		} else if ("zpkLMK".equals(owner)) {
			String t = this.zpkLMK.getText();
			this.zpkey = null;
			try {
				if (!t.isEmpty()) {
					EncryptedKey zpklmk = new EncryptedKey(t);
					this.zpkey = zpklmk.decrypt(this.lmk.getPair(KeyType.ZPK));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (this.zpkey != null) {
				int zpkl = this.zpkey.length;
				this.zpkScheme = zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.ANSIX9_17_DOUBLE : KeyScheme.ANSIX9_17_TRIPLE;
				try {
					this.checkValueZPK.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(this.zpkey)));
				} catch (Exception e) {
					this.checkValueZPK.setText(null);
					e.printStackTrace();
				}
				try {
					this.zpkClear.setText(new EncryptedKey(HexadecimalStringUtil.bytesToString(this.zpkey)).toFormattedString());
				} catch (Exception e) {
					this.zpkClear.setText(null);
					e.printStackTrace();
				}
				if (zmk != null) {
					try {
						this.zpkZMK.setText(EncryptedKey.encrypt(
								zpkey, zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.ANSIX9_17_DOUBLE : KeyScheme.ANSIX9_17_TRIPLE,
								AtallaVariant.variant(zmk, this.atallaVariant)
						).toFormattedString());
					} catch (Exception e) {
						this.zpkZMK.setText(null);
						e.printStackTrace();
					}
				}
			} else {
				this.zpkClear.setText(null);
				this.zpkZMK.setText(null);
				this.checkValueZPK.setText(null);
				this.zpkey = null;
			}
		} else if ("zpkZMK".equals(owner)) {
			changedZMK();
		} else if ("componente".equals(owner)) {
			String t = this.componente1.getText();
			t = t.replaceAll("[^A-Fa-f0-9]", "");
			this.c1 = HexadecimalStringUtil.bytesFromString(t);
			int i = c1.length;
			if (i > 0 && (i % 8 == 0 || i % 16 == 0 || i % 24 == 0)) {
				try {
					this.checkValueC1.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(this.c1)));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				this.checkValueC1.setText(null);
				this.c1 = null;
			}
			t = this.componente2.getText();
			t = t.replaceAll("[^A-Fa-f0-9]", "");
			this.c2 = HexadecimalStringUtil.bytesFromString(t);
			i = c2.length;
			if (i > 0 && (i % 8 == 0 || i % 16 == 0 || i % 24 == 0)) {
				try {
					this.checkValueC2.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(this.c2)));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				this.checkValueC2.setText(null);
				this.c2 = null;
			}
			t = this.componente3.getText();
			t = t.replaceAll("[^A-Fa-f0-9]", "");
			this.c3 = HexadecimalStringUtil.bytesFromString(t);
			i = c3.length;
			if (i > 0 && (i % 8 == 0 || i % 16 == 0 || i % 24 == 0)) {
				try {
					this.checkValueC3.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(this.c3)));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} else {
				this.checkValueC3.setText(null);
				this.c3 = null;
			}
			this.zmk = this.getZMK();
			if (zmk != null) {
				try {
					this.checkValueZMK.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(zmk)));
					this.zmkClear.setText(HexadecimalStringUtil.bytesToString(zmk));
				} catch (Exception e) {
					this.checkValueZMK.setText(null);
					this.zmkClear.setText(null);
					e.printStackTrace();
				}
			} else {
				this.checkValueZMK.setText(null);
				this.zmkClear.setText(null);
			}
			this.changedZMK();
		}
		changingZPK = false;
		changedPANPIN();
		changedPBZPK();
	}

	private void changedZMK() {
		String t = this.zpkZMK.getText();
		if (!t.isEmpty() && zmk != null) {
			EncryptedKey ek = null;
			try {
				ek = new EncryptedKey(t);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (ek != null) {
				try {
					this.zpkey = ek.decrypt(AtallaVariant.variant(zmk, this.atallaVariant));
					int zpkl = this.zpkey.length;
					this.zpkScheme = zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.ANSIX9_17_DOUBLE : KeyScheme.ANSIX9_17_TRIPLE;
					this.checkValueZPK.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(this.zpkey)));
					this.zpkClear.setText(HexadecimalStringUtil.bytesToString(zpkey));

          this.zpkError.setText(null);
					this.zpkLMK.setText(EncryptedKey.encrypt(
							zpkey, zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.VARIANT_DOUBLE : KeyScheme.VARIANT_TRIPLE, lmk.getPair(KeyType.ZPK)
					).toFormattedString());
				} catch (Exception e) {
					e.printStackTrace();
          this.zpkError.setText(e.getMessage());
					this.zpkClear.setText(null);
					this.checkValueZPK.setText(null);
					this.zpkLMK.setText(null);
					this.zpkey = null;
          try {
            System.out.println("Trying to decrypt anyway");
            this.zpkey = HSMCipher.doFinal(ek.getValue(), ek.getScheme(), AtallaVariant.variant(zmk, this.atallaVariant), Cipher.DECRYPT_MODE);
            System.out.println("DECRYPTED " + HexadecimalStringUtil.bytesToString(this.zpkey));
            this.zpkey = HSMServer.adjustParity(zpkey);
            System.out.println("PARITY ADJ " + HexadecimalStringUtil.bytesToString(this.zpkey));
  					int zpkl = this.zpkey.length;
    				this.zpkScheme = zpkl == 8 ? KeyScheme.ANSIX9_17_SINGLE : zpkl == 16 ? KeyScheme.ANSIX9_17_DOUBLE : KeyScheme.ANSIX9_17_TRIPLE;
            this.zpkClear.setText(HexadecimalStringUtil.bytesToString(this.zpkey));
            this.checkValueZPK.setText(HexadecimalStringUtil.bytesToString(this.cipher.calculateKeyCheckValue(this.zpkey)));
          } catch (Exception ex) {
            ex.printStackTrace();
          }
				}
			} else {
        this.zpkError.setText(null);
				this.zpkClear.setText(null);
				this.checkValueZPK.setText(null);
				this.zpkLMK.setText(null);
				this.zpkey = null;
			}
		} else {
			this.zpkClear.setText(null);
			this.zpkLMK.setText(null);
			this.checkValueZPK.setText(null);
			this.zpkey = null;
		}
	}

	private void changedPANPIN() {
		String pan = this.pan.getText();
		pan = pan.replaceAll("\\D", "");
		String pin = this.pin.getText();
		pin = pin.replaceAll("\\D", "");
		if (pan.isEmpty() || pin.length() < 4 || pin.length() > 12) {
			this.pinblockClear.setText(null);
			this.pinblockZPK.setText(null);
		} else {
			Pinblock pb = Pinblock.createPinblock(this.getFormat((String) this.formatoPB.getSelectedItem()), pin, pan);
			this.pinblockClear.setText(pb.toString());
			if (this.zpkey != null) {
				try {
					this.pinblockZPK.setText(HexadecimalStringUtil.bytesToString(this.cipher.doFinal(pb.getPinblock(), this.zpkScheme, this.zpkey, Cipher.ENCRYPT_MODE)));
				} catch (Exception e) {
					JOptionPane.showMessageDialog(this, "Erro gerando PB: " + e, "Erro", JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			} else {
				this.pinblockZPK.setText(null);
			}
		}
	}

	private void changedPBZPK() {
		String pan = this.pan2.getText();
		pan = pan.replaceAll("\\D", "");
		String pinblock = this.pinblockZPK2.getText();
		pinblock = pinblock.replaceAll("[^A-Fa-f0-9]", "");
		if (pinblock.isEmpty()) {
			this.pinblockClear2.setText(null);
			this.pin2.setText(null);
		} else if (this.zpkey != null) {
			try {
				byte[] clear = this.cipher.doFinal(HexadecimalStringUtil.bytesFromString(pinblock), this.zpkScheme, this.zpkey, Cipher.DECRYPT_MODE);
				this.pinblockClear2.setText(HexadecimalStringUtil.bytesToString(clear));
				if (!pan.isEmpty()) {
					Pinblock pb = Pinblock.createPinblock(this.getFormat((String) this.formatoPB2.getSelectedItem()), clear);
					this.pin2.setText(pb.getPIN(pan));
				}
			} catch (Exception e) {
				JOptionPane.showMessageDialog(this, "Erro gerando PB: " + e, "Erro", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		} else {
			this.pinblockClear2.setText(null);
			this.pin2.setText(null);
		}
	}

	private byte[] getZMK() {
		if (this.c1 == null && this.c2 == null && this.c3 == null) {
			return null;
		}
		int length = 0;
		if (this.c1 != null) length = this.c1.length;
		if (this.c2 != null) {
			if (length == 0) length = this.c2.length;
			else if (this.c2.length != length) {
//				JOptionPane.showMessageDialog(this, "Componentes com tamanhos diferentes.", "Erro", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		}
		if (this.c3 != null) {
			if (length == 0) length = this.c3.length;
			else if (this.c3.length != length) {
//				JOptionPane.showMessageDialog(this, "Componentes com tamanhos diferentes.", "Erro", JOptionPane.ERROR_MESSAGE);
				return null;
			}
		}
		byte[] zmk = new byte[length];
		if (this.c1 != null) {
			for (int i = 0; i < length; i++) {
				zmk[i] ^= this.c1[i];
			}
		}
		if (this.c2 != null) {
			for (int i = 0; i < length; i++) {
				zmk[i] ^= this.c2[i];
			}
		}
		if (this.c3 != null) {
			for (int i = 0; i < length; i++) {
				zmk[i] ^= this.c3[i];
			}
		}
		return zmk;
	}

	private Pinblock.Format getFormat(String s) {
		if ("ISO-0".equals(s)) {
			return Pinblock.Format.ISO_0;
		}
		if ("ISO-1".equals(s)) {
			return Pinblock.Format.ISO_1;
		}
		if ("ISO-3".equals(s)) {
			return Pinblock.Format.ISO_3;
		}
		return null;
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel1 = new javax.swing.JPanel();
    jLabel15 = new javax.swing.JLabel();
    jLabel12 = new javax.swing.JLabel();
    jLabel11 = new javax.swing.JLabel();
    pin = new javax.swing.JTextField();
    pan = new javax.swing.JTextField();
    jLabel14 = new javax.swing.JLabel();
    pinblockClear = new javax.swing.JTextField();
    jLabel13 = new javax.swing.JLabel();
    pinblockZPK = new javax.swing.JTextField();
    formatoPB = new javax.swing.JComboBox();
    jLabel8 = new javax.swing.JLabel();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    componente1 = new javax.swing.JTextField();
    componente2 = new javax.swing.JTextField();
    componente3 = new javax.swing.JTextField();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    jLabel21 = new javax.swing.JLabel();
    zmkClear = new javax.swing.JTextField();
    jLabel22 = new javax.swing.JLabel();
    zpkLMK = new javax.swing.JTextField();
    jLabel23 = new javax.swing.JLabel();
    zpkClear = new javax.swing.JTextField();
    jLabel6 = new javax.swing.JLabel();
    checkValueC1 = new javax.swing.JLabel();
    checkValueC2 = new javax.swing.JLabel();
    checkValueC3 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    zpkZMK = new javax.swing.JTextField();
    jLabel9 = new javax.swing.JLabel();
    checkValueZPK = new javax.swing.JLabel();
    cleanButton = new javax.swing.JButton();
    jLabel10 = new javax.swing.JLabel();
    generateCompButton = new javax.swing.JButton();
    checkValueZMK = new javax.swing.JLabel();
    generateZPKButton = new javax.swing.JButton();
    jPanel2 = new javax.swing.JPanel();
    jLabel16 = new javax.swing.JLabel();
    jLabel17 = new javax.swing.JLabel();
    jLabel18 = new javax.swing.JLabel();
    pinblockZPK2 = new javax.swing.JTextField();
    pan2 = new javax.swing.JTextField();
    jLabel19 = new javax.swing.JLabel();
    pinblockClear2 = new javax.swing.JTextField();
    jLabel20 = new javax.swing.JLabel();
    pin2 = new javax.swing.JTextField();
    jLabel24 = new javax.swing.JLabel();
    formatoPB2 = new javax.swing.JComboBox();
    copyButton = new javax.swing.JButton();
    atallaCheck = new javax.swing.JCheckBox();
    atallaField = new javax.swing.JTextField();
    zpkError = new javax.swing.JLabel();
    parityButton = new javax.swing.JButton();

    jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    org.openide.awt.Mnemonics.setLocalizedText(jLabel15, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel15.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel12, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel12.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel11, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel11.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel14, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel14.text")); // NOI18N

    pinblockClear.setEditable(false);

    org.openide.awt.Mnemonics.setLocalizedText(jLabel13, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel13.text")); // NOI18N

    pinblockZPK.setEditable(false);

    formatoPB.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ISO-0", "ISO-1", "ISO-3" }));
    formatoPB.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        formatoPBActionPerformed(evt);
      }
    });

    org.openide.awt.Mnemonics.setLocalizedText(jLabel8, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel8.text")); // NOI18N

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel12)
              .addComponent(jLabel11))
            .addGap(88, 88, 88)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
              .addComponent(pin, javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(pan, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel13)
              .addComponent(jLabel14))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(pinblockClear)
              .addComponent(pinblockZPK, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGap(0, 0, Short.MAX_VALUE))
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addComponent(jLabel15)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel8)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(formatoPB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel15)
          .addComponent(formatoPB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel8))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel12)
          .addComponent(pan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel14)
          .addComponent(pinblockClear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel11)
          .addComponent(pin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel13)
          .addComponent(pinblockZPK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel1.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel2.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel3.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel4.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel5.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel21, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel21.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel22, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel22.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel23, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel23.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel6, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel6.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(checkValueC1, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.checkValueC1.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(checkValueC2, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.checkValueC2.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(checkValueC3, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.checkValueC3.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel7, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel7.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel9, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel9.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(checkValueZPK, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.checkValueZPK.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(cleanButton, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.cleanButton.text")); // NOI18N
    cleanButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        cleanButtonActionPerformed(evt);
      }
    });

    org.openide.awt.Mnemonics.setLocalizedText(jLabel10, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel10.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(generateCompButton, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.generateCompButton.text")); // NOI18N
    generateCompButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        generateCompButtonActionPerformed(evt);
      }
    });

    org.openide.awt.Mnemonics.setLocalizedText(checkValueZMK, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.checkValueZMK.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(generateZPKButton, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.generateZPKButton.text")); // NOI18N
    generateZPKButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        generateZPKButtonActionPerformed(evt);
      }
    });

    jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

    org.openide.awt.Mnemonics.setLocalizedText(jLabel16, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel16.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel17, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel17.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel18, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel18.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel19, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel19.text")); // NOI18N

    pinblockClear2.setEditable(false);

    org.openide.awt.Mnemonics.setLocalizedText(jLabel20, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel20.text")); // NOI18N

    pin2.setEditable(false);

    org.openide.awt.Mnemonics.setLocalizedText(jLabel24, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.jLabel24.text")); // NOI18N

    formatoPB2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ISO-0", "ISO-1", "ISO-3" }));
    formatoPB2.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        formatoPB2ActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel18)
              .addComponent(jLabel17))
            .addGap(26, 26, 26)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
              .addComponent(pinblockZPK2)
              .addComponent(pan2, javax.swing.GroupLayout.DEFAULT_SIZE, 169, Short.MAX_VALUE))
            .addGap(4, 4, 4)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel20)
              .addComponent(jLabel19))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(pinblockClear2)
              .addComponent(pin2)))
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addComponent(jLabel16)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jLabel24)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(formatoPB2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        .addContainerGap())
    );
    jPanel2Layout.setVerticalGroup(
      jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel2Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel19)
              .addComponent(pinblockClear2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel20)
              .addComponent(pin2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
          .addGroup(jPanel2Layout.createSequentialGroup()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(formatoPB2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jLabel24))
              .addComponent(jLabel16))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel17)
              .addComponent(pan2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
              .addComponent(jLabel18)
              .addComponent(pinblockZPK2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    org.openide.awt.Mnemonics.setLocalizedText(copyButton, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.copyButton.text")); // NOI18N
    copyButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        copyButtonActionPerformed(evt);
      }
    });

    org.openide.awt.Mnemonics.setLocalizedText(atallaCheck, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.atallaCheck.text")); // NOI18N
    atallaCheck.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        atallaCheckActionPerformed(evt);
      }
    });

    atallaField.setEditable(false);

    zpkError.setForeground(new java.awt.Color(204, 0, 0));
    org.openide.awt.Mnemonics.setLocalizedText(zpkError, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.zpkError.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(parityButton, org.openide.util.NbBundle.getMessage(ZPKTopComponent.class, "ZPKTopComponent.parityButton.text")); // NOI18N
    parityButton.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        parityButtonActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel3)
              .addComponent(jLabel21)
              .addComponent(jLabel1)
              .addComponent(jLabel2)
              .addComponent(jLabel7)
              .addComponent(jLabel22)
              .addComponent(jLabel23))
            .addGap(25, 25, 25)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addGroup(layout.createSequentialGroup()
                .addComponent(zpkClear, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkValueZPK, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
              .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                  .addComponent(componente2, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                  .addComponent(componente1)
                  .addComponent(componente3)
                  .addComponent(zmkClear)
                  .addComponent(zpkZMK)
                  .addComponent(zpkLMK))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                  .addGroup(layout.createSequentialGroup()
                    .addComponent(jLabel4)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(checkValueC1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                  .addGroup(layout.createSequentialGroup()
                    .addComponent(jLabel5)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(checkValueC2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                  .addGroup(layout.createSequentialGroup()
                    .addComponent(jLabel6)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(checkValueC3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                  .addGroup(layout.createSequentialGroup()
                    .addComponent(jLabel10)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(checkValueZMK, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                  .addGroup(layout.createSequentialGroup()
                    .addComponent(atallaCheck)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(atallaField, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(zpkError, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
          .addGroup(layout.createSequentialGroup()
            .addComponent(cleanButton)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(generateCompButton)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(generateZPKButton)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(copyButton)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(parityButton)))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel1)
          .addComponent(componente1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel4)
          .addComponent(checkValueC1))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(componente2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel5)
          .addComponent(checkValueC2))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel3)
          .addComponent(componente3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel6)
          .addComponent(checkValueC3))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel21)
          .addComponent(zmkClear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel10)
          .addComponent(checkValueZMK))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel7)
          .addComponent(zpkZMK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(atallaCheck)
          .addComponent(atallaField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(zpkError))
        .addGap(28, 28, 28)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(zpkLMK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel22))
        .addGap(4, 4, 4)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel23)
          .addComponent(zpkClear, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(jLabel9)
          .addComponent(checkValueZPK))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(cleanButton)
          .addComponent(generateCompButton)
          .addComponent(generateZPKButton)
          .addComponent(copyButton)
          .addComponent(parityButton))
        .addGap(11, 11, 11)
        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
  }// </editor-fold>//GEN-END:initComponents

    private void formatoPBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_formatoPBActionPerformed
        this.changedPANPIN();
    }//GEN-LAST:event_formatoPBActionPerformed

    private void cleanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cleanButtonActionPerformed
        this.pin.setText(null);
        this.pin2.setText(null);
        this.pan.setText(null);
        this.pan2.setText(null);
        this.pinblockClear.setText(null);
        this.pinblockClear2.setText(null);
        this.pinblockZPK.setText(null);
        this.pinblockZPK2.setText(null);
        this.componente1.setText(null);
        this.checkValueC1.setText(null);
        this.componente2.setText(null);
        this.checkValueC2.setText(null);
        this.componente3.setText(null);
        this.checkValueC3.setText(null);
        this.zmkClear.setText(null);
        this.checkValueZMK.setText(null);
        this.zpkZMK.setText(null);
        this.checkValueZPK.setText(null);
        this.zpkLMK.setText(null);
        this.zpkClear.setText(null);
    }//GEN-LAST:event_cleanButtonActionPerformed

    private void generateCompButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateCompButtonActionPerformed
        this.componente1.setText(HexadecimalStringUtil.bytesToString(this.cipher.generateDESedeKey(KeyScheme.ANSIX9_17_DOUBLE)));
        this.componente2.setText(HexadecimalStringUtil.bytesToString(this.cipher.generateDESedeKey(KeyScheme.ANSIX9_17_DOUBLE)));
        this.componente3.setText(HexadecimalStringUtil.bytesToString(this.cipher.generateDESedeKey(KeyScheme.ANSIX9_17_DOUBLE)));
        this.zpkChanged("componente");
    }//GEN-LAST:event_generateCompButtonActionPerformed

    private void generateZPKButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateZPKButtonActionPerformed
        this.zpkClear.setText(HexadecimalStringUtil.bytesToString(this.cipher.generateDESedeKey(KeyScheme.ANSIX9_17_DOUBLE)));
        this.zpkChanged("zpkClear");
    }//GEN-LAST:event_generateZPKButtonActionPerformed

    private void formatoPB2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_formatoPB2ActionPerformed
        this.changedPBZPK();
    }//GEN-LAST:event_formatoPB2ActionPerformed

    private void copyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyButtonActionPerformed
		StringBuilder sb = new StringBuilder(256);
		sb
				.append("Componente 1: ").append(this.componente1.getText())
				.append(" KCV1: ").append(this.checkValueC1.getText()).append('\n')
				.append("Componente 2: ").append(this.componente2.getText())
				.append(" KCV2: ").append(this.checkValueC2.getText()).append('\n')
				.append("Componente 3: ").append(this.componente3.getText())
				.append(" KCV3: ").append(this.checkValueC3.getText()).append('\n')
				.append("ZMK clear: ").append(this.zmkClear.getText())
				.append(" KCV ZMK: ").append(this.checkValueZMK.getText()).append('\n')
				.append("ZPK sob ZMK: ").append(this.zpkZMK.getText())
				.append(" KCV ZPK: ").append(this.checkValueZPK.getText()).append('\n')
				.append("ZPK sob LMK desenv: ").append(this.zpkLMK.getText()).append('\n')
				.append("ZPK clear: ").append(this.zpkClear.getText()).append('\n');
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(sb.toString()), this);
    }//GEN-LAST:event_copyButtonActionPerformed

    private void atallaCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_atallaCheckActionPerformed
        if (this.atallaCheck.isSelected()) {
			this.atallaField.setEditable(true);
		} else {
			this.atallaField.setEditable(false);
			this.atallaField.setText(null);
		}
        this.zpkChanged("atalla");
    }//GEN-LAST:event_atallaCheckActionPerformed

  private void parityButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_parityButtonActionPerformed
    String t = this.zpkClear.getText();
    if (!t.isEmpty()) {
      this.zpkey = HexadecimalStringUtil.bytesFromString(t.replaceAll("[^A-Fa-f0-9]", ""));
      this.zpkey = HSMServer.adjustParity(this.zpkey);
      this.zpkClear.setText(HexadecimalStringUtil.bytesToString(this.zpkey));
    }
    zpkChanged("zpkClear");
  }//GEN-LAST:event_parityButtonActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JCheckBox atallaCheck;
  private javax.swing.JTextField atallaField;
  private javax.swing.JLabel checkValueC1;
  private javax.swing.JLabel checkValueC2;
  private javax.swing.JLabel checkValueC3;
  private javax.swing.JLabel checkValueZMK;
  private javax.swing.JLabel checkValueZPK;
  private javax.swing.JButton cleanButton;
  private javax.swing.JTextField componente1;
  private javax.swing.JTextField componente2;
  private javax.swing.JTextField componente3;
  private javax.swing.JButton copyButton;
  private javax.swing.JComboBox formatoPB;
  private javax.swing.JComboBox formatoPB2;
  private javax.swing.JButton generateCompButton;
  private javax.swing.JButton generateZPKButton;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel10;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel13;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel17;
  private javax.swing.JLabel jLabel18;
  private javax.swing.JLabel jLabel19;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel20;
  private javax.swing.JLabel jLabel21;
  private javax.swing.JLabel jLabel22;
  private javax.swing.JLabel jLabel23;
  private javax.swing.JLabel jLabel24;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JLabel jLabel8;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel2;
  private javax.swing.JTextField pan;
  private javax.swing.JTextField pan2;
  private javax.swing.JButton parityButton;
  private javax.swing.JTextField pin;
  private javax.swing.JTextField pin2;
  private javax.swing.JTextField pinblockClear;
  private javax.swing.JTextField pinblockClear2;
  private javax.swing.JTextField pinblockZPK;
  private javax.swing.JTextField pinblockZPK2;
  private javax.swing.JTextField zmkClear;
  private javax.swing.JTextField zpkClear;
  private javax.swing.JLabel zpkError;
  private javax.swing.JTextField zpkLMK;
  private javax.swing.JTextField zpkZMK;
  // End of variables declaration//GEN-END:variables
	@Override
	public void componentOpened() {
		// TODO add custom code on component opening
	}

	@Override
	public void componentClosed() {
		// TODO add custom code on component closing
	}

	void writeProperties(java.util.Properties p) {
		// better to version settings since initial version as advocated at
		// http://wiki.apidesign.org/wiki/PropertyFiles
		p.setProperty("version", "1.0");
		// TODO store your settings
	}

	void readProperties(java.util.Properties p) {
		String version = p.getProperty("version");
		// TODO read your settings according to their version
	}
}
