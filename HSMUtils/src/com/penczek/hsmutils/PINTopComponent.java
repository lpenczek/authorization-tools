package com.penczek.hsmutils;

import com.penczek.hsm.Pinblock;
import com.penczek.hsm.server.EncryptedKey;
import com.penczek.hsm.server.HSMCipher;
import com.penczek.hsm.server.KeyType;
import com.penczek.hsm.server.LMK;
import com.penczek.pinpad.PinPadHSM;
import com.penczek.util.HexadecimalStringUtil;
import java.util.Arrays;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
		dtd = "-//com.penczek.hsmutils//PIN//EN",
		autostore = false
)
@TopComponent.Description(
		preferredID = "PINTopComponent",
		iconBase = "com/penczek/hsmutils/pinpad.png",
		persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "editor", openAtStartup = true)
@ActionID(category = "Window", id = "com.penczek.hsmutils.PINTopComponent")
@ActionReference(path = "Menu/Window" /*, position = 333 */)
@TopComponent.OpenActionRegistration(
		displayName = "#CTL_PINAction",
		preferredID = "PINTopComponent"
)
@Messages({
	"CTL_PINAction=PIN Utils",
	"CTL_PINTopComponent=PIN Utils",
	"HINT_PINTopComponent=PIN Utilities"
})
public final class PINTopComponent extends TopComponent {

	private HSMCipher cipher;
	private LMK lmk;

	public PINTopComponent() {
		initComponents();
		setName(Bundle.CTL_PINTopComponent());
		setToolTipText(Bundle.HINT_PINTopComponent());

		try {
			this.cipher = new HSMCipher(this.lmk = new LMK(LMK.TEST_LMK));
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Erro iniciando aplicação: " + e, "Erro", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

		DocumentListener hsmChanged = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				hsm();
			}
			public void removeUpdate(DocumentEvent e) {
				hsm();
			}
			public void insertUpdate(DocumentEvent e) {
				hsm();
			}

		};
		this.tmk.getDocument().addDocumentListener(hsmChanged);
		this.tpkTMK.getDocument().addDocumentListener(hsmChanged);
		this.pan.getDocument().addDocumentListener(hsmChanged);
		this.pin.getDocument().addDocumentListener(hsmChanged);

		DocumentListener ibmChanged = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				ibm();
			}
			public void removeUpdate(DocumentEvent e) {
				ibm();
			}
			public void insertUpdate(DocumentEvent e) {
				ibm();
			}

		};
		this.pvkIBM.getDocument().addDocumentListener(ibmChanged);
		this.decTableIBM.getDocument().addDocumentListener(ibmChanged);
		this.validationDataIBM.getDocument().addDocumentListener(ibmChanged);
		this.pinIBM.getDocument().addDocumentListener(ibmChanged);

		DocumentListener pvvChanged = new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				pvv();
			}
			public void removeUpdate(DocumentEvent e) {
				pvv();
			}
			public void insertUpdate(DocumentEvent e) {
				pvv();
			}

		};
		this.pvkPVV.getDocument().addDocumentListener(pvvChanged);
		this.indicePVV.getDocument().addDocumentListener(pvvChanged);
		this.panPVV.getDocument().addDocumentListener(pvvChanged);
		this.pinPVV.getDocument().addDocumentListener(pvvChanged);

	}

	void hsm() {
		String tmk = this.tmk.getText();
		String tpk = this.tpkTMK.getText();
		String pan = this.pan.getText();
		String pin = this.pin.getText();
		if (!tmk.isEmpty() && !tpk.isEmpty() && !pan.isEmpty() && !pin.isEmpty()) {
			try {
				PinPadHSM pp = new PinPadHSM(tmk, tpk);
				this.pinblockTPK.setText(pp.encrypt(pin, pan, this.getFormat((String) this.formato.getSelectedItem())));
			} catch (Exception e) {
				e.printStackTrace();
				this.pinblockTPK.setText(null);
			}
		} else {
			this.pinblockTPK.setText(null);
		}
	}

	private Pinblock.Format getFormat(String s) {
		if ("ISO-0".equals(s)) {
			return Pinblock.Format.ISO_0;
		}
		if ("ISO-1".equals(s)) {
			return Pinblock.Format.ISO_1;
		}
		if ("ISO-3".equals(s)) {
			return Pinblock.Format.ISO_3;
		}
		return null;
	}

	void ibm() {
		try {
			EncryptedKey pvk = new EncryptedKey(this.pvkIBM.getText());
			byte[] decTable = HexadecimalStringUtil.bytesFromString(this.decTableIBM.getText());
			if (decTable.length != 8) {
				if (decTable.length > 0) System.out.println("Tabela decimalização deve conter 16 dígitos hexadecimais");
				this.offsetIBM.setText(null);
				return;
			}
			byte[] validationData = HexadecimalStringUtil.bytesFromString(this.validationDataIBM.getText());
			if (validationData.length != 8) {
				if (validationData.length > 0) System.out.println("Validation data deve conter 16 dígitos hexadecimais");
				this.offsetIBM.setText(null);
				return;
			}
			String pin = this.pinIBM.getText();
			if (pin.length() < 4 || pin.length() > 12) {
				this.offsetIBM.setText(null);
				return;
			}
			if (this.decTableEncryptedIBM.isSelected()) {
				// decrypt decimalization table
				decTable = cipher.decryptBlock(decTable, lmk.getPair(KeyType.DTAB));
			}
			byte[] dtab = new byte[16];
			for (int i = 0; i < 16; i++) {
				dtab[i] = (byte) ((decTable[i / 2] >> (i % 2 == 0 ? 4 : 0)) & 0xF);
			}
			System.out.println("DecTable " + Arrays.toString(dtab));
			// generate natural PIN
			validationData = cipher.encryptBlock(validationData, pvk, KeyType.PVK);
			System.out.println("Data " + HexadecimalStringUtil.bytesToString(validationData));
			for (int i = 0; i < 8; i++) {
				validationData[i] = (byte) ((dtab[validationData[i] >> 4 & 0xF] << 4) | dtab[validationData[i] & 0xF]);
			}
			System.out.println("Data " + HexadecimalStringUtil.bytesToString(validationData));
			// generate offset
			char[] offset = new char[] { 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F' };
			for (int i = 0; i < pin.length(); i++) {
				int pinD = pin.charAt(i) - '0';
				int natPinD = (validationData[i / 2] >> (i % 2 == 0 ? 4 : 0)) & 0xF;
				offset[i] = (char) ('0' + pinD - natPinD + (natPinD > pinD ? 10 : 0));
			}
			this.offsetIBM.setText(new String(offset));
		} catch (Exception e) {
			e.printStackTrace();
			this.offsetIBM.setText(null);
		}
	}

	void pvv() {
		try {
			EncryptedKey pvk = new EncryptedKey(this.pvkPVV.getText());
			int indice = Integer.parseInt(this.indicePVV.getText(), 10);
			if (indice < 0 || indice > 9) {
				System.out.println("Indice invalido " + indice);
				this.pvv.setText(null);
				return;
			}
			String pan = Pinblock.extractAccountNumber(this.panPVV.getText());
			if (pan.length() != 12) {
				if (pan.length() > 0) System.out.println("Invalid pan " + pan);
				this.pvv.setText(null);
				return;
			}
			String pin = this.pinPVV.getText();
			if (pin.length() < 4 || pin.length() > 12) {
				this.pvv.setText(null);
				return;
			}
			String stage1 = pan.substring(1, 12) + indice + pin.substring(0, 4);
			System.out.println("Stage1 " + stage1);
			byte[] s2 = this.cipher.encryptBlock(HexadecimalStringUtil.bytesFromString(stage1), pvk, KeyType.PVK);
			String stage2 = HexadecimalStringUtil.bytesToString(s2);
			System.out.println("Stage2 " + stage2);
			this.pvv.setText(this.decimalize(stage2, 4));
		} catch (Exception e) {
			e.printStackTrace();
			this.pvv.setText(null);
		}
	}

	public String decimalize(String s, int qty) {
		char[] dec = new char[qty];
		int z = 0;
		for (int i = 0; i < s.length() && z < qty; i++) {
			char c = s.charAt(i);
			if (c >= '0' && c <= '9') {
				dec[z++] = c;
			}
		}
		// ainda nao completou, pega dos hexadecimais
		if (z < qty) {
			for (int i = 0; i < s.length() && z < qty; i++) {
				char c = Character.toUpperCase(s.charAt(i));
				if (c >= 'A' && c <= 'F') {
					dec[z++] = (char) (c - 'A' + '0');
				}
			}
		}
		return new String(dec);
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    jPanel3 = new javax.swing.JPanel();
    jLabel10 = new javax.swing.JLabel();
    jLabel11 = new javax.swing.JLabel();
    jLabel12 = new javax.swing.JLabel();
    offsetIBM = new javax.swing.JTextField();
    pinIBM = new javax.swing.JTextField();
    pvkIBM = new javax.swing.JTextField();
    jLabel13 = new javax.swing.JLabel();
    decTableIBM = new javax.swing.JTextField();
    jLabel14 = new javax.swing.JLabel();
    validationDataIBM = new javax.swing.JTextField();
    decTableEncryptedIBM = new javax.swing.JCheckBox();
    jPanel4 = new javax.swing.JPanel();
    jLabel15 = new javax.swing.JLabel();
    jLabel16 = new javax.swing.JLabel();
    jLabel17 = new javax.swing.JLabel();
    pvv = new javax.swing.JTextField();
    pinPVV = new javax.swing.JTextField();
    pvkPVV = new javax.swing.JTextField();
    jLabel18 = new javax.swing.JLabel();
    panPVV = new javax.swing.JTextField();
    jLabel19 = new javax.swing.JLabel();
    indicePVV = new javax.swing.JTextField();
    jPanel1 = new javax.swing.JPanel();
    jLabel1 = new javax.swing.JLabel();
    jLabel2 = new javax.swing.JLabel();
    jLabel3 = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jLabel5 = new javax.swing.JLabel();
    pinblockTPK = new javax.swing.JTextField();
    pin = new javax.swing.JTextField();
    pan = new javax.swing.JTextField();
    tpkTMK = new javax.swing.JTextField();
    tmk = new javax.swing.JTextField();
    jLabel9 = new javax.swing.JLabel();
    formato = new javax.swing.JComboBox();

    jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jPanel3.border.title"))); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel10, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel10.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel11, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel11.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel12, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel12.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel13, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel13.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel14, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel14.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(decTableEncryptedIBM, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.decTableEncryptedIBM.text")); // NOI18N
    decTableEncryptedIBM.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        decTableEncryptedIBMActionPerformed(evt);
      }
    });

    javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
    jPanel3.setLayout(jPanel3Layout);
    jPanel3Layout.setHorizontalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel3Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addComponent(jLabel10)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(pvkIBM))
          .addGroup(jPanel3Layout.createSequentialGroup()
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel13)
              .addComponent(jLabel14)
              .addComponent(jLabel11)
              .addComponent(jLabel12))
            .addGap(27, 27, 27)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(offsetIBM, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(pinIBM, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(validationDataIBM, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(decTableIBM, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(decTableEncryptedIBM)))
            .addGap(0, 92, Short.MAX_VALUE)))
        .addContainerGap())
    );
    jPanel3Layout.setVerticalGroup(
      jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel3Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel10)
          .addComponent(pvkIBM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel14)
          .addComponent(decTableIBM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
          .addComponent(decTableEncryptedIBM))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel13)
          .addComponent(validationDataIBM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel11)
          .addComponent(pinIBM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel12)
          .addComponent(offsetIBM, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jPanel4.border.title"))); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel15, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel15.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel16, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel16.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel17, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel17.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel18, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel18.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel19, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel19.text")); // NOI18N

    javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
    jPanel4.setLayout(jPanel4Layout);
    jPanel4Layout.setHorizontalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel4Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addGroup(jPanel4Layout.createSequentialGroup()
            .addComponent(jLabel15)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
            .addComponent(pvkPVV, javax.swing.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE))
          .addGroup(jPanel4Layout.createSequentialGroup()
            .addComponent(jLabel19)
            .addGap(69, 69, 69)
            .addComponent(indicePVV, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
          .addGroup(jPanel4Layout.createSequentialGroup()
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(jLabel18)
              .addComponent(jLabel16)
              .addComponent(jLabel17))
            .addGap(78, 78, 78)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(pinPVV, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(panPVV, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addComponent(pvv, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))))
        .addContainerGap())
    );
    jPanel4Layout.setVerticalGroup(
      jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel4Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel15)
          .addComponent(pvkPVV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel19)
          .addComponent(indicePVV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addGap(4, 4, 4)
        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel18)
          .addComponent(panPVV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel16)
          .addComponent(pinPVV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel17)
          .addComponent(pvv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jPanel1.border.title"))); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel1, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel1.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel2, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel2.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel3, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel3.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel4, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel4.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel5, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel5.text")); // NOI18N

    org.openide.awt.Mnemonics.setLocalizedText(jLabel9, org.openide.util.NbBundle.getMessage(PINTopComponent.class, "PINTopComponent.jLabel9.text")); // NOI18N

    formato.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "ISO-0", "ISO-1", "ISO-3" }));

    javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
    jPanel1.setLayout(jPanel1Layout);
    jPanel1Layout.setHorizontalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jLabel5)
          .addComponent(jLabel4)
          .addComponent(jLabel3)
          .addComponent(jLabel2)
          .addComponent(jLabel1)
          .addComponent(jLabel9))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(tmk)
          .addComponent(tpkTMK)
          .addGroup(jPanel1Layout.createSequentialGroup()
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
              .addComponent(formato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
              .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                .addComponent(pan)
                .addComponent(pin, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(pinblockTPK, javax.swing.GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)))
            .addGap(0, 0, Short.MAX_VALUE)))
        .addContainerGap())
    );
    jPanel1Layout.setVerticalGroup(
      jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(jPanel1Layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel1)
          .addComponent(tmk, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel2)
          .addComponent(tpkTMK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel3)
          .addComponent(pan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel4)
          .addComponent(pin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel9)
          .addComponent(formato, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
          .addComponent(jLabel5)
          .addComponent(pinblockTPK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
    this.setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
          .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
          .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        .addContainerGap())
    );
    layout.setVerticalGroup(
      layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGroup(layout.createSequentialGroup()
        .addContainerGap()
        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addGap(0, 0, 0)
        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );
  }// </editor-fold>//GEN-END:initComponents

    private void decTableEncryptedIBMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_decTableEncryptedIBMActionPerformed
        this.ibm();
    }//GEN-LAST:event_decTableEncryptedIBMActionPerformed

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JCheckBox decTableEncryptedIBM;
  private javax.swing.JTextField decTableIBM;
  private javax.swing.JComboBox formato;
  private javax.swing.JTextField indicePVV;
  private javax.swing.JLabel jLabel1;
  private javax.swing.JLabel jLabel10;
  private javax.swing.JLabel jLabel11;
  private javax.swing.JLabel jLabel12;
  private javax.swing.JLabel jLabel13;
  private javax.swing.JLabel jLabel14;
  private javax.swing.JLabel jLabel15;
  private javax.swing.JLabel jLabel16;
  private javax.swing.JLabel jLabel17;
  private javax.swing.JLabel jLabel18;
  private javax.swing.JLabel jLabel19;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JLabel jLabel3;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel5;
  private javax.swing.JLabel jLabel9;
  private javax.swing.JPanel jPanel1;
  private javax.swing.JPanel jPanel3;
  private javax.swing.JPanel jPanel4;
  private javax.swing.JTextField offsetIBM;
  private javax.swing.JTextField pan;
  private javax.swing.JTextField panPVV;
  private javax.swing.JTextField pin;
  private javax.swing.JTextField pinIBM;
  private javax.swing.JTextField pinPVV;
  private javax.swing.JTextField pinblockTPK;
  private javax.swing.JTextField pvkIBM;
  private javax.swing.JTextField pvkPVV;
  private javax.swing.JTextField pvv;
  private javax.swing.JTextField tmk;
  private javax.swing.JTextField tpkTMK;
  private javax.swing.JTextField validationDataIBM;
  // End of variables declaration//GEN-END:variables
	@Override
	public void componentOpened() {
		// TODO add custom code on component opening
	}

	@Override
	public void componentClosed() {
		// TODO add custom code on component closing
	}

	void writeProperties(java.util.Properties p) {
		// better to version settings since initial version as advocated at
		// http://wiki.apidesign.org/wiki/PropertyFiles
		p.setProperty("version", "1.0");
		// TODO store your settings
	}

	void readProperties(java.util.Properties p) {
		String version = p.getProperty("version");
		// TODO read your settings according to their version
	}
}
